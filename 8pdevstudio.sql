-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 08-Out-2019 às 19:57
-- Versão do servidor: 10.1.36-MariaDB
-- versão do PHP: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `8pdevstudio`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `analytics`
--

CREATE TABLE `analytics` (
  `id` int(11) NOT NULL,
  `codigo` varchar(140) NOT NULL,
  `ip` varchar(140) NOT NULL,
  `data` date NOT NULL,
  `hora` time NOT NULL,
  `dia` int(2) NOT NULL,
  `mes` int(2) NOT NULL,
  `ano` int(4) NOT NULL,
  `url` text NOT NULL,
  `pagina` varchar(140) NOT NULL,
  `navegador` varchar(140) NOT NULL,
  `cidade` varchar(140) NOT NULL,
  `estado` varchar(60) NOT NULL,
  `pais` varchar(60) NOT NULL,
  `sistema` varchar(140) NOT NULL,
  `dispositivo` varchar(140) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `analytics`
--

INSERT INTO `analytics` (`id`, `codigo`, `ip`, `data`, `hora`, `dia`, `mes`, `ano`, `url`, `pagina`, `navegador`, `cidade`, `estado`, `pais`, `sistema`, `dispositivo`) VALUES
(1, '5kjq27170', '189.7.33.177', '2019-09-27', '21:56:03', 27, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 8.0.0; ASUS_Z01KD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv8l', 'true'),
(2, 'whmv3pkkh', '189.40.90.86', '2019-09-27', '22:45:17', 27, 9, 2019, 'http://8pdev.studio/lulapalozo/', '8p estúdio de desenvolvimento', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1', 'São Paulo', 'SP', 'BR', 'iPhone', 'true'),
(3, 'whmv3pkkh', '189.40.90.86', '2019-09-27', '22:45:20', 27, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1', 'São Paulo', 'SP', 'BR', 'iPhone', 'true'),
(4, '4e6z54d41', '179.228.5.90', '2019-09-27', '00:01:58', 27, 9, 2019, 'http://8pdev.studio/novo/index.php', '8p estúdio de desenvolvimento', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(5, 'lec6tn7bd', '66.249.73.66', '2019-09-26', '18:59:18', 26, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(6, '7lg1qsxxx', '179.228.5.90', '2019-09-28', '02:03:09', 28, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(7, '7lg1qsxxx', '179.228.5.90', '2019-09-28', '02:18:34', 28, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(8, '7lg1qsxxx', '179.228.5.90', '2019-09-28', '02:18:41', 28, 9, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(9, '7lg1qsxxx', '179.228.5.90', '2019-09-28', '02:18:48', 28, 9, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(10, '7lg1qsxxx', '179.228.5.90', '2019-09-28', '02:18:52', 28, 9, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(11, '7lg1qsxxx', '179.228.5.90', '2019-09-28', '02:25:07', 28, 9, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(12, '7lg1qsxxx', '179.228.5.90', '2019-09-28', '02:25:11', 28, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(13, '7lg1qsxxx', '179.228.5.90', '2019-09-28', '02:25:32', 28, 9, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(14, 'yhd4ldlif', '66.249.73.69', '2019-09-26', '22:00:00', 26, 9, 2019, 'http://8pdev.studio/projeto/js_menu_web_site', '8p estúdio de desenvolvimento - JS Menu web site', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(15, '7lg1qsxxx', '179.228.5.90', '2019-09-28', '02:52:51', 28, 9, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(16, '7lg1qsxxx', '179.228.5.90', '2019-09-28', '02:53:42', 28, 9, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(17, '7lg1qsxxx', '179.228.5.90', '2019-09-28', '02:53:44', 28, 9, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(18, '7lg1qsxxx', '179.228.5.90', '2019-09-28', '02:53:48', 28, 9, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(19, '7lg1qsxxx', '179.228.5.90', '2019-09-28', '02:53:57', 28, 9, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(20, '7lg1qsxxx', '179.228.5.90', '2019-09-28', '02:54:01', 28, 9, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(21, 'yhd4ldlif', '66.249.73.69', '2019-09-27', '20:00:00', 27, 9, 2019, 'http://8pdev.studio/projeto/epics-landing-page', '8p estúdio de desenvolvimento - Epics Landing Page', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(22, 'fz295nhv4', '193.22.96.202', '2019-09-28', '04:01:34', 28, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 'Kyiv', 'Kyiv City', 'UA', 'Linux x86_64', 'false'),
(23, 'yhd4ldlif', '66.249.73.70', '2019-09-26', '22:00:00', 26, 9, 2019, 'http://8pdev.studio/servicos/e_commerce', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(24, 'z0xbw2kjr', '66.249.73.69', '2019-09-27', '22:00:00', 27, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(25, 'z0xbw2kjr', '66.249.73.66', '2019-09-27', '22:00:00', 27, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(26, 'yhd4ldlif', '66.249.73.66', '2019-09-26', '22:00:00', 26, 9, 2019, 'http://8pdev.studio/projeto/epics-landing-page', '8p estúdio de desenvolvimento - Epics Landing Page', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(27, 'yhd4ldlif', '66.249.73.70', '2019-09-26', '22:00:00', 26, 9, 2019, 'http://8pdev.studio/projeto/finnance_web_site', '8p estúdio de desenvolvimento - Finnance web site', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(28, 'whmv3pkkh', '179.228.5.90', '2019-09-28', '12:01:50', 28, 9, 2019, 'http://8pdev.studio/propostas/rpfitness', '8p estúdio de desenvolvimento', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1', 'São José do Rio Preto', 'SP', 'BR', 'iPhone', 'true'),
(29, '7lg1qsxxx', '179.228.5.90', '2019-09-28', '12:06:05', 28, 9, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(30, '7lg1qsxxx', '179.228.5.90', '2019-09-28', '12:10:03', 28, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(31, '7lg1qsxxx', '179.228.5.90', '2019-09-28', '12:16:02', 28, 9, 2019, 'https://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(32, 'ccdd8xq0y', '186.233.37.173', '2019-09-28', '13:40:51', 28, 9, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Linux; Android 4.2.1; en-us; Nexus 5 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko; googleweblight) Chrome/38.0.1025.166 ', 'Brazil', '', '', 'Linux x86_64', 'true'),
(33, 'x3gteze3f', '66.249.73.69', '2019-09-27', '20:00:00', 27, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(34, '848sh6enb', '173.252.95.32', '2019-09-28', '14:32:02', 28, 9, 2019, 'http://8pdev.studio/projeto/mar_aberto_loja_online?fbclid=IwAR3GKN3aNX-nJmQlmfgwho5yKgPai_K7PdFS9D15oQf77EbEnsHoUYGUp7s', '8p estúdio de desenvolvimento - Mar Aberto Loja Online', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Mobile/15E148 Safari/604.1', 'United States', '', '', 'iPhone', 'true'),
(35, 'jvv88skgw', '31.13.115.3', '2019-09-28', '14:32:02', 28, 9, 2019, 'http://8pdev.studio/projeto/mar-aberto-loja-online?fbclid=IwAR1MySyEwAsYjkLCkccBwhsx3hyffi7XD64K1sKMZ3eL87DWmVNuxmGNds4', '8p estúdio de desenvolvimento - Mar Aberto Loja Online', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Mobile/15E148 Safari/604.1', 'County Dublin', 'IE', '', 'iPhone', 'true'),
(36, '4e6z54d41', '179.228.5.90', '2019-09-28', '15:17:35', 28, 9, 2019, 'http://8pdev.studio/novo/index.php', '8p estúdio de desenvolvimento', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(37, 'yhd4ldlif', '66.249.73.69', '2019-09-26', '22:00:00', 26, 9, 2019, 'http://8pdev.studio/servicos/landing-page', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(38, 'yhd4ldlif', '66.249.73.70', '2019-09-26', '22:00:00', 26, 9, 2019, 'http://8pdev.studio/servicos/front-end-back-end', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(39, 'yhd4ldlif', '66.249.73.66', '2019-09-26', '22:00:00', 26, 9, 2019, 'http://8pdev.studio/servicos/web-site-cracao-de-aplicativos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(40, 'yhd4ldlif', '66.249.73.66', '2019-09-26', '22:00:00', 26, 9, 2019, 'http://8pdev.studio/projeto/boxsupreme_web_site', '8p estúdio de desenvolvimento - BoxSupreme web site', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(41, 'b8jpad2zf', '189.32.162.48', '2019-09-28', '16:00:42', 28, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 7.1.1; LG-M700) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv7l', 'true'),
(42, 'b8jpad2zf', '189.32.162.48', '2019-09-28', '16:01:07', 28, 9, 2019, 'http://8pdev.studio/servicos/e_commerce', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Linux; Android 7.1.1; LG-M700) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv7l', 'true'),
(43, 'yhd4ldlif', '66.249.73.66', '2019-09-26', '22:00:00', 26, 9, 2019, 'http://8pdev.studio/servicos/ux-ux-design', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(44, 'b8jpad2zf', '189.32.162.48', '2019-09-28', '16:02:03', 28, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 7.1.1; LG-M700) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv7l', 'true'),
(45, 'b8jpad2zf', '189.32.162.48', '2019-09-28', '16:02:10', 28, 9, 2019, 'http://8pdev.studio/servicos/site-institucional', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Linux; Android 7.1.1; LG-M700) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv7l', 'true'),
(46, 'b8jpad2zf', '189.32.162.48', '2019-09-28', '16:03:01', 28, 9, 2019, 'http://8pdev.studio/servicos/site-institucional', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Linux; Android 7.1.1; LG-M700) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv7l', 'true'),
(47, 'b8jpad2zf', '189.32.162.48', '2019-09-28', '16:03:06', 28, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 7.1.1; LG-M700) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv7l', 'true'),
(48, 'yhd4ldlif', '66.249.73.70', '2019-09-26', '22:00:00', 26, 9, 2019, 'http://8pdev.studio/servicos/site-catalogo', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(49, 'yhd4ldlif', '66.249.73.69', '2019-09-27', '20:00:00', 27, 9, 2019, 'http://8pdev.studio/projeto/epics-landing-page', '8p estúdio de desenvolvimento - Epics Landing Page', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(50, 'mzpgvw2pz', '66.249.73.70', '2019-09-26', '22:00:00', 26, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(51, 'yhd4ldlif', '66.249.73.66', '2019-09-27', '20:00:00', 27, 9, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(52, '6kogwvzuu', '193.22.96.202', '2019-09-28', '17:52:18', 28, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 'Kyiv', 'Kyiv City', 'UA', 'Linux x86_64', 'false'),
(53, '4e6z54d41', '189.46.111.136', '2019-09-28', '17:58:02', 28, 9, 2019, 'http://8pdev.studio/novo/index.php', '8p estúdio de desenvolvimento', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(54, 'b8jpad2zf', '189.32.162.48', '2019-09-28', '18:03:26', 28, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 7.1.1; LG-M700) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv7l', 'true'),
(55, 'b8jpad2zf', '189.32.162.48', '2019-09-28', '18:05:35', 28, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 7.1.1; LG-M700) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv7l', 'true'),
(56, 'edas9aj8a', '66.249.73.66', '2019-09-27', '20:00:00', 27, 9, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(57, 'yhd4ldlif', '66.249.73.66', '2019-09-27', '20:00:00', 27, 9, 2019, 'http://8pdev.studio/projeto/okamed-web-site', '8p estúdio de desenvolvimento - OKAMED web site', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(58, 'yhd4ldlif', '66.249.73.66', '2019-09-27', '20:00:00', 27, 9, 2019, 'http://8pdev.studio/projeto/okamed-web-site', '8p estúdio de desenvolvimento - OKAMED web site', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(59, '4e6z54d41', '189.46.111.136', '2019-09-28', '18:35:03', 28, 9, 2019, 'http://8pdev.studio/novo/index.php', '8p estúdio de desenvolvimento', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(60, 'yhd4ldlif', '66.249.73.70', '2019-09-26', '22:00:00', 26, 9, 2019, 'http://8pdev.studio/projeto/epics_landing_page', '8p estúdio de desenvolvimento - Epics Landing Page', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(61, 'yhd4ldlif', '66.249.73.69', '2019-09-26', '22:00:00', 26, 9, 2019, 'http://8pdev.studio/servicos/site-institucional', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(62, 'yhd4ldlif', '66.249.73.70', '2019-09-27', '20:00:00', 27, 9, 2019, 'http://8pdev.studio/contato', '8p estúdio de desenvolvimento - Contato', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(63, 'yhd4ldlif', '66.249.73.69', '2019-09-26', '22:00:00', 26, 9, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(64, 'yhd4ldlif', '66.249.73.69', '2019-09-27', '20:00:00', 27, 9, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(65, 'edas9aj8a', '66.249.73.69', '2019-09-26', '22:00:00', 26, 9, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(66, 'edas9aj8a', '66.249.73.66', '2019-09-27', '22:00:00', 27, 9, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(67, 'z0xbw2kjr', '66.249.73.70', '2019-09-27', '22:00:00', 27, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(68, 'edas9aj8a', '66.249.73.66', '2019-09-27', '22:00:00', 27, 9, 2019, 'http://8pdev.studio/contato', '8p estúdio de desenvolvimento - Contato', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(69, 'z0xbw2kjr', '66.249.73.66', '2019-09-27', '22:00:00', 27, 9, 2019, 'http://8pdev.studio/?l=ur', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(70, 'edas9aj8a', '66.249.73.66', '2019-09-27', '22:00:00', 27, 9, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(71, 'z0xbw2kjr', '66.249.73.66', '2019-09-27', '22:00:00', 27, 9, 2019, 'http://8pdev.studio/?l=hu', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(72, '44f1tkots', '66.249.73.66', '2019-09-27', '22:00:00', 27, 9, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(73, 'z0xbw2kjr', '66.249.73.69', '2019-09-27', '22:00:00', 27, 9, 2019, 'http://8pdev.studio/?l=tr', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(74, 'z0xbw2kjr', '66.249.73.69', '2019-09-27', '22:00:00', 27, 9, 2019, 'http://8pdev.studio/?l=ga', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(75, 'z0xbw2kjr', '66.249.73.66', '2019-09-27', '22:00:00', 27, 9, 2019, 'http://8pdev.studio/?l=sv', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(76, 'yhd4ldlif', '66.249.73.69', '2019-09-26', '22:00:00', 26, 9, 2019, 'http://8pdev.studio/projeto/okamed-web-site', '8p estúdio de desenvolvimento - OKAMED web site', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(77, 'z0xbw2kjr', '66.249.73.70', '2019-09-27', '22:00:00', 27, 9, 2019, 'http://8pdev.studio/?l=sq', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(78, 'yhd4ldlif', '66.249.73.66', '2019-09-26', '22:00:00', 26, 9, 2019, 'http://8pdev.studio/servicos/aplicativos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(79, 'h46cga3ld', '189.46.111.136', '2019-09-28', '21:14:14', 28, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(198, '7lg1qsxxx', '189.46.111.136', '2019-09-29', '15:06:28', 29, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(199, '7lg1qsxxx', '189.46.111.136', '2019-09-29', '15:06:59', 29, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(200, '4a1w9gyay', '189.46.111.136', '2019-09-29', '15:21:52', 29, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(201, '4a1w9gyay', '189.46.111.136', '2019-09-29', '15:22:07', 29, 9, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(202, '4a1w9gyay', '189.46.111.136', '2019-09-29', '15:22:21', 29, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(203, '4a1w9gyay', '189.46.111.136', '2019-09-29', '15:52:13', 29, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(204, 'zagzdnug8', '191.189.82.38', '2019-09-29', '17:23:35', 29, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0 Mobile/15E148 Safari/604.1', 'São José do Rio Preto', 'SP', 'BR', 'iPhone', 'true'),
(205, 'akgu8i89h', '149.154.154.92', '2019-09-29', '18:22:17', 29, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 'Wien', 'Wien', 'AT', 'Linux x86_64', 'false'),
(206, 'whmv3pkkh', '189.46.111.136', '2019-09-29', '19:33:32', 29, 9, 2019, 'http://8pdev.studio/lulapalozo/', '8p estúdio de desenvolvimento', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1', 'São Paulo', 'BR', '14024', 'iPhone', 'true'),
(207, 'whmv3pkkh', '189.46.111.136', '2019-09-29', '19:33:35', 29, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1', 'São Paulo', 'BR', '14024', 'iPhone', 'true'),
(208, 'whmv3pkkh', '189.46.111.136', '2019-09-29', '19:33:47', 29, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1', 'São Paulo', 'BR', '14024', 'iPhone', 'true'),
(209, 'whmv3pkkh', '189.46.111.136', '2019-09-29', '19:34:37', 29, 9, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1', 'São Paulo', 'BR', '14024', 'iPhone', 'true'),
(210, 'whmv3pkkh', '189.46.111.136', '2019-09-29', '19:34:41', 29, 9, 2019, 'http://8pdev.studio/projeto/epics-landing-page', '8p estúdio de desenvolvimento - Epics Landing Page', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1', 'São Paulo', 'BR', '14024', 'iPhone', 'true'),
(211, 'whmv3pkkh', '189.46.111.136', '2019-09-29', '19:34:51', 29, 9, 2019, 'http://8pdev.studio/contato', '8p estúdio de desenvolvimento - Contato', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1', 'São Paulo', 'BR', '14024', 'iPhone', 'true'),
(212, 'whmv3pkkh', '189.46.111.136', '2019-09-29', '19:35:00', 29, 9, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1', 'São Paulo', 'BR', '14024', 'iPhone', 'true'),
(213, 'gfmt2f8et', '66.249.73.66', '2019-09-28', '22:00:00', 28, 9, 2019, 'http://8pdev.studio/?l=lv', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(214, 'gfmt2f8et', '66.249.73.66', '2019-09-28', '22:00:00', 28, 9, 2019, 'http://8pdev.studio/?l=te', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(215, 'gfmt2f8et', '66.249.73.66', '2019-09-28', '22:00:00', 28, 9, 2019, 'http://8pdev.studio/?l=lv', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(216, 'gfmt2f8et', '66.249.73.69', '2019-09-29', '22:00:00', 29, 9, 2019, 'http://8pdev.studio/?l=fi', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(217, 'gfmt2f8et', '66.249.73.66', '2019-09-28', '22:00:00', 28, 9, 2019, 'http://8pdev.studio/?l=te', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(218, 'gfmt2f8et', '66.249.73.69', '2019-09-29', '22:00:00', 29, 9, 2019, 'http://8pdev.studio/?l=ms', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(219, 'xd8bt48a5', '37.235.48.176', '2019-09-30', '07:43:02', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 'Warszawa', 'Warszawa', 'mazowieckie', 'Linux x86_64', 'false'),
(220, 'weg7wjpwq', '191.189.82.38', '2019-09-30', '09:13:36', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:69.0) Gecko/20100101 Firefox/69.0', 'São José do Rio Preto', 'SP', 'BR', 'MacIntel', 'false'),
(221, 'weg7wjpwq', '191.189.82.38', '2019-09-30', '09:16:56', 30, 9, 2019, 'http://8pdev.studio/contato', '8p estúdio de desenvolvimento - Contato', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:69.0) Gecko/20100101 Firefox/69.0', 'São José do Rio Preto', 'SP', 'BR', 'MacIntel', 'false'),
(222, 'weg7wjpwq', '191.189.82.38', '2019-09-30', '09:17:03', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:69.0) Gecko/20100101 Firefox/69.0', 'São José do Rio Preto', 'SP', 'BR', 'MacIntel', 'false'),
(223, 'weg7wjpwq', '191.189.82.38', '2019-09-30', '09:18:03', 30, 9, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:69.0) Gecko/20100101 Firefox/69.0', 'São José do Rio Preto', 'SP', 'BR', 'MacIntel', 'false'),
(224, 'weg7wjpwq', '191.189.82.38', '2019-09-30', '09:18:05', 30, 9, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:69.0) Gecko/20100101 Firefox/69.0', 'São José do Rio Preto', 'SP', 'BR', 'MacIntel', 'false'),
(225, 'weg7wjpwq', '191.189.82.38', '2019-09-30', '09:18:07', 30, 9, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:69.0) Gecko/20100101 Firefox/69.0', 'São José do Rio Preto', 'SP', 'BR', 'MacIntel', 'false'),
(226, '2bgzq3esq', '187.53.131.2', '2019-09-30', '09:45:46', 30, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'Goiás', 'BR', '74660', 'Win32', 'false'),
(227, '2bgzq3esq', '187.53.131.2', '2019-09-30', '10:32:54', 30, 9, 2019, 'http://8pdev.studio/projeto/js-menu-web-site', '8p estúdio de desenvolvimento - JS Menu web site', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'Goiás', 'BR', '74660', 'Win32', 'false'),
(228, '2bgzq3esq', '187.53.131.2', '2019-09-30', '10:33:03', 30, 9, 2019, 'http://8pdev.studio/servicos/site-catalogo', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'Goiás', 'BR', '74660', 'Win32', 'false'),
(229, '2bgzq3esq', '187.53.131.2', '2019-09-30', '10:54:34', 30, 9, 2019, 'http://8pdev.studio/projeto/okamed_web_site', '8p estúdio de desenvolvimento - OKAMED web site', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'Goiás', 'BR', '74660', 'Win32', 'false'),
(230, 'oat6zxmcp', '191.19.55.197', '2019-09-30', '11:19:51', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(231, 'oat6zxmcp', '191.19.55.197', '2019-09-30', '11:21:53', 30, 9, 2019, 'http://8pdev.studio/projeto/epics-landing-page', '8p estúdio de desenvolvimento - Epics Landing Page', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(232, 'oat6zxmcp', '191.19.55.197', '2019-09-30', '11:22:36', 30, 9, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(233, 'tjicj9gpy', '95.179.154.158', '2019-09-30', '11:31:25', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 'Amsterdam', 'Amsterdam', 'NH', 'Linux x86_64', 'false'),
(234, '7lg1qsxxx', '189.46.111.136', '2019-09-30', '12:12:27', 30, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(235, '7lg1qsxxx', '189.46.111.136', '2019-09-30', '12:14:00', 30, 9, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(236, '7lg1qsxxx', '189.46.111.136', '2019-09-30', '12:14:03', 30, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(237, '7lg1qsxxx', '189.46.111.136', '2019-09-30', '12:14:10', 30, 9, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(238, '7lg1qsxxx', '189.46.111.136', '2019-09-30', '12:14:14', 30, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(239, '7lg1qsxxx', '189.46.111.136', '2019-09-30', '12:16:11', 30, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(240, '7lg1qsxxx', '189.46.111.136', '2019-09-30', '12:16:18', 30, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(241, '7lg1qsxxx', '189.46.111.136', '2019-09-30', '12:24:03', 30, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(242, '7lg1qsxxx', '189.46.111.136', '2019-09-30', '12:24:10', 30, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(243, '7lg1qsxxx', '189.46.111.136', '2019-09-30', '12:24:17', 30, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(244, '7lg1qsxxx', '189.46.111.136', '2019-09-30', '12:25:00', 30, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(245, '7lg1qsxxx', '189.46.111.136', '2019-09-30', '12:25:05', 30, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(246, '7lg1qsxxx', '189.46.111.136', '2019-09-30', '12:26:05', 30, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(247, '7lg1qsxxx', '189.46.111.136', '2019-09-30', '12:26:09', 30, 9, 2019, 'http://8pdev.studio/projeto/boxsupreme-web-site', '8p estúdio de desenvolvimento - BoxSupreme web site', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(248, '7lg1qsxxx', '189.46.111.136', '2019-09-30', '12:26:13', 30, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(249, '7lg1qsxxx', '189.46.111.136', '2019-09-30', '12:26:23', 30, 9, 2019, 'http://8pdev.studio/projeto/finnance-web-site', '8p estúdio de desenvolvimento - Finnance web site', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(250, '7lg1qsxxx', '189.46.111.136', '2019-09-30', '12:26:25', 30, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(251, '7lg1qsxxx', '189.46.111.136', '2019-09-30', '12:27:39', 30, 9, 2019, 'http://8pdev.studio/contato', '8p estúdio de desenvolvimento - Contato', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(252, '7lg1qsxxx', '189.46.111.136', '2019-09-30', '12:30:26', 30, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(253, '2bgzq3esq', '187.53.131.2', '2019-09-30', '13:33:12', 30, 9, 2019, 'http://8pdev.studio/projeto/epics_landing_page', '8p estúdio de desenvolvimento - Epics Landing Page', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'Goiás', 'BR', '74660', 'Win32', 'false'),
(254, '4a1w9gyay', '189.46.111.136', '2019-09-30', '13:41:26', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(255, '2bgzq3esq', '187.53.131.2', '2019-09-30', '13:42:17', 30, 9, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'Goiás', 'BR', '74660', 'Win32', 'false'),
(256, '2bgzq3esq', '187.53.131.2', '2019-09-30', '13:44:23', 30, 9, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'Goiás', 'BR', '74660', 'Win32', 'false'),
(257, '0dggwwi46', '187.181.223.132', '2019-09-30', '13:44:28', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 9; Redmi Note 6 Pro) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv8l', 'true'),
(258, '2bgzq3esq', '187.53.131.2', '2019-09-30', '13:46:26', 30, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'Goiás', 'BR', '74660', 'Win32', 'false'),
(259, '2bgzq3esq', '187.53.131.2', '2019-09-30', '13:47:21', 30, 9, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'Goiás', 'BR', '74660', 'Win32', 'false'),
(260, 'j33mlz373', '189.32.160.58', '2019-09-30', '13:48:55', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0 Mobile/15E148 Safari/604.1', 'São José do Rio Preto', 'SP', 'BR', 'iPhone', 'true'),
(261, '2bgzq3esq', '187.53.131.2', '2019-09-30', '14:12:54', 30, 9, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'Goiás', 'BR', '74660', 'Win32', 'false'),
(262, 'bedp8dp7a', '187.26.139.23', '2019-09-30', '17:01:54', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 5.1.1; SM-J120H Build/LMY47V; pt-br) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Mobile Safari/5', 'São Paulo', 'SP', 'BR', 'Linux x86_64', 'true'),
(263, '5kjq27170', '189.46.111.136', '2019-09-30', '17:16:02', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 8.0.0; ASUS_Z01KD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São Paulo', 'BR', '14024', 'Linux armv8l', 'true');
INSERT INTO `analytics` (`id`, `codigo`, `ip`, `data`, `hora`, `dia`, `mes`, `ano`, `url`, `pagina`, `navegador`, `cidade`, `estado`, `pais`, `sistema`, `dispositivo`) VALUES
(264, '5kjq27170', '189.46.111.136', '2019-09-30', '17:16:15', 30, 9, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Linux; Android 8.0.0; ASUS_Z01KD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São Paulo', 'BR', '14024', 'Linux armv8l', 'true'),
(265, '5kjq27170', '189.46.111.136', '2019-09-30', '17:16:39', 30, 9, 2019, 'http://8pdev.studio/projeto/boxsupreme-web-site', '8p estúdio de desenvolvimento - BoxSupreme web site', 'Mozilla/5.0 (Linux; Android 8.0.0; ASUS_Z01KD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São Paulo', 'BR', '14024', 'Linux armv8l', 'true'),
(266, '6rwuiyzu4', '187.23.52.43', '2019-09-30', '17:22:56', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 7.0; ASUS_Z017DC Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.92 Mobile', 'São José do Rio Preto', 'SP', 'BR', 'Linux aarch64', 'true'),
(267, '6rwuiyzu4', '187.23.52.43', '2019-09-30', '17:23:17', 30, 9, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Linux; Android 7.0; ASUS_Z017DC Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.92 Mobile', 'São José do Rio Preto', 'SP', 'BR', 'Linux aarch64', 'true'),
(268, '6rwuiyzu4', '187.23.52.43', '2019-09-30', '17:24:00', 30, 9, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Linux; Android 7.0; ASUS_Z017DC Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.92 Mobile', 'São José do Rio Preto', 'SP', 'BR', 'Linux aarch64', 'true'),
(269, '6rwuiyzu4', '187.23.52.43', '2019-09-30', '17:24:41', 30, 9, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Linux; Android 7.0; ASUS_Z017DC Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.92 Mobile', 'São José do Rio Preto', 'SP', 'BR', 'Linux aarch64', 'true'),
(270, 'h46cga3ld', '189.46.111.136', '2019-09-30', '17:25:05', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(271, 'h46cga3ld', '189.46.111.136', '2019-09-30', '17:25:09', 30, 9, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(272, 'hpet32t0v', '179.98.3.162', '2019-09-30', '17:45:24', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; SM-J500M Build/MMB29M) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.125 Mobile Safari/537.36', 'São Paulo', 'BR', '14024', 'Linux armv7l', 'true'),
(273, 'h46cga3ld', '189.46.111.136', '2019-09-30', '17:52:07', 30, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(274, '4a1w9gyay', '189.46.111.136', '2019-09-30', '18:01:10', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(275, '7lg1qsxxx', '189.46.111.136', '2019-09-30', '18:07:48', 30, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(276, 'gfmt2f8et', '66.249.73.66', '2019-09-29', '22:00:00', 29, 9, 2019, 'http://8pdev.studio/?l=lt', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(277, 'gwiauop6k', '189.46.111.136', '2019-09-30', '18:42:13', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0; Redmi Note 4X Build/MRA58K; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.92 Mobi', 'São Paulo', 'BR', '14024', 'Linux aarch64', 'true'),
(278, '4e6z54d41', '189.46.111.136', '2019-09-30', '18:42:49', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(279, '4e6z54d41', '189.46.111.136', '2019-09-30', '18:42:57', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Mobile Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'true'),
(280, '4e6z54d41', '189.46.111.136', '2019-09-30', '18:44:04', 30, 9, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Mobile Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'true'),
(281, '4e6z54d41', '189.46.111.136', '2019-09-30', '18:44:10', 30, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Mobile Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'true'),
(282, '4e6z54d41', '189.46.111.136', '2019-09-30', '18:44:15', 30, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Mobile Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'true'),
(283, '7lg1qsxxx', '189.46.111.136', '2019-09-30', '18:50:43', 30, 9, 2019, 'http://8pdev.studio/?fbclid=IwAR18MfoxNKxqSE3w9nrCWt9xWkG_20uuzjgvXb-5CBMD6uIPpAe6rGsLrjM', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(284, 'h46cga3ld', '189.46.111.136', '2019-09-30', '18:56:07', 30, 9, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(285, 'h46cga3ld', '189.46.111.136', '2019-09-30', '18:56:10', 30, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(286, 'gfmt2f8et', '66.249.73.69', '2019-09-29', '22:00:00', 29, 9, 2019, 'http://8pdev.studio/?l=th', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(287, '7lg1qsxxx', '189.46.111.136', '2019-09-30', '19:30:31', 30, 9, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(288, '7lg1qsxxx', '189.46.111.136', '2019-09-30', '19:41:28', 30, 9, 2019, 'http://8pdev.studio/p%C3%A1inel', '8p estúdio de desenvolvimento', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(289, 'ur5eki8fh', '37.235.48.176', '2019-10-01', '20:01:51', 1, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 'Warszawa', 'Warszawa', 'mazowieckie', 'Linux x86_64', 'false'),
(290, 'gfmt2f8et', '66.249.73.69', '2019-09-29', '22:00:00', 29, 9, 2019, 'http://8pdev.studio/?l=et', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(291, 'h46cga3ld', '189.46.111.136', '2019-09-30', '20:12:31', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(292, 'gfmt2f8et', '66.249.73.66', '2019-09-29', '22:00:00', 29, 9, 2019, 'http://8pdev.studio/?l=pl', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(293, 'r0zrm1b1i', '177.180.210.137', '2019-09-30', '20:42:29', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(294, 'r0zrm1b1i', '177.180.210.137', '2019-09-30', '20:42:51', 30, 9, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(295, 'r0zrm1b1i', '177.180.210.137', '2019-09-30', '20:42:54', 30, 9, 2019, 'http://8pdev.studio/projeto/epics-landing-page', '8p estúdio de desenvolvimento - Epics Landing Page', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(296, 'r0zrm1b1i', '177.180.210.137', '2019-09-30', '20:43:21', 30, 9, 2019, 'http://8pdev.studio/projeto/js_menu_web_site', '8p estúdio de desenvolvimento - JS Menu web site', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(297, 'r0zrm1b1i', '177.180.210.137', '2019-09-30', '20:43:34', 30, 9, 2019, 'http://8pdev.studio/projeto/okamed_web_site', '8p estúdio de desenvolvimento - OKAMED web site', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(298, 'r0zrm1b1i', '177.180.210.137', '2019-09-30', '20:43:51', 30, 9, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(299, 'r0zrm1b1i', '177.180.210.137', '2019-09-30', '20:43:53', 30, 9, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(300, '4a1w9gyay', '189.46.111.136', '2019-09-30', '20:45:26', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(301, '4a1w9gyay', '189.46.111.136', '2019-09-30', '20:45:31', 30, 9, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(302, 'r0zrm1b1i', '177.180.210.137', '2019-09-30', '20:44:31', 30, 9, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(303, 'r0zrm1b1i', '177.180.210.137', '2019-09-30', '20:45:51', 30, 9, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(304, 'r0zrm1b1i', '177.180.210.137', '2019-09-30', '20:45:53', 30, 9, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(305, 'whmv3pkkh', '189.46.111.136', '2019-09-30', '20:54:12', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1', 'São Paulo', 'BR', '14024', 'iPhone', 'true'),
(306, 'whmv3pkkh', '189.46.111.136', '2019-09-30', '20:54:55', 30, 9, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1', 'São Paulo', 'BR', '14024', 'iPhone', 'true'),
(307, 'whmv3pkkh', '189.46.111.136', '2019-09-30', '20:55:45', 30, 9, 2019, 'http://8pdev.studio/contato', '8p estúdio de desenvolvimento - Contato', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1', 'São Paulo', 'BR', '14024', 'iPhone', 'true'),
(308, 'whmv3pkkh', '189.46.111.136', '2019-09-30', '20:55:56', 30, 9, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1', 'São Paulo', 'BR', '14024', 'iPhone', 'true'),
(309, '6h6aff4c4', '66.249.73.69', '2019-09-29', '22:00:00', 29, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(310, '2dhfzzkw2', '152.254.193.248', '2019-09-30', '21:45:52', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 9; Redmi Note 7 Build/PKQ1.180904.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.9', 'Brazil', '15400-000', '', 'Linux armv8l', 'true'),
(311, '2dhfzzkw2', '152.254.193.248', '2019-09-30', '21:46:41', 30, 9, 2019, 'http://8pdev.studio/servicos/aplicativos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Linux; Android 9; Redmi Note 7 Build/PKQ1.180904.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.9', 'Brazil', '15400-000', '', 'Linux armv8l', 'true'),
(312, 'gfmt2f8et', '66.249.73.69', '2019-09-29', '22:00:00', 29, 9, 2019, 'http://8pdev.studio/?l=fa', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(313, 'kkgkdw3t0', '5.9.176.210', '2019-10-01', '21:47:37', 1, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 'Kassel', 'HE', 'DE', 'Linux x86_64', 'false'),
(314, 'zagzdnug8', '152.250.241.170', '2019-09-30', '21:47:39', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0 Mobile/15E148 Safari/604.1', 'São José do Rio Preto', 'SP', 'BR', 'iPhone', 'true'),
(315, 'gfmt2f8et', '66.249.73.66', '2019-09-29', '22:00:00', 29, 9, 2019, 'http://8pdev.studio/?l=fi', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(316, 'j33mlz373', '189.40.88.186', '2019-09-30', '21:56:15', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0 Mobile/15E148 Safari/604.1', 'São Paulo', 'SP', 'BR', 'iPhone', 'true'),
(317, 'j33mlz373', '189.40.88.186', '2019-09-30', '21:56:40', 30, 9, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_0 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0 Mobile/15E148 Safari/604.1', 'São Paulo', 'SP', 'BR', 'iPhone', 'true'),
(318, 'gfmt2f8et', '66.249.73.69', '2019-09-29', '22:00:00', 29, 9, 2019, 'http://8pdev.studio/?l=sk', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(319, 'mejrot420', '189.35.21.143', '2019-09-30', '22:22:46', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 9; Redmi Note 7 Build/PKQ1.180904.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.9', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv8l', 'true'),
(320, 'mejrot420', '189.35.21.143', '2019-09-30', '22:23:27', 30, 9, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Linux; Android 9; Redmi Note 7 Build/PKQ1.180904.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.9', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv8l', 'true'),
(321, '6zv5mwbmx', '189.35.21.143', '2019-09-30', '22:24:26', 30, 9, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Linux; Android 9; Redmi Note 7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv8l', 'true'),
(322, '5kjq27170', '189.40.75.242', '2019-09-30', '22:25:58', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 8.0.0; ASUS_Z01KD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São Paulo', 'SP', 'BR', 'Linux armv8l', 'true'),
(323, '4e6z54d41', '189.46.111.136', '2019-09-30', '22:30:46', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(324, '4e6z54d41', '189.46.111.136', '2019-09-30', '22:30:48', 30, 9, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(325, 'gfmt2f8et', '66.249.73.70', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=az', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(326, 'gfmt2f8et', '66.249.73.66', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=kk', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(327, 'k8s34wp6k', '177.180.217.172', '2019-09-30', '23:20:21', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 7.0; LG-M250 Build/NRD90U; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.92 Mobile Saf', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv7l', 'true'),
(328, '6h6aff4c4', '66.249.73.66', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=kn', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(329, 'q81ptkt3o', '191.189.73.166', '2019-09-30', '23:49:29', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 8.0.0; ASUS_Z017DC Build/OPR1.170623.026; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.386', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv8l', 'true'),
(330, 'gfmt2f8et', '66.249.73.66', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=ar', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(331, 'q81ptkt3o', '191.189.73.166', '2019-09-30', '23:50:38', 30, 9, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Linux; Android 8.0.0; ASUS_Z017DC Build/OPR1.170623.026; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.386', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv8l', 'true'),
(332, '86599hemj', '187.120.216.114', '2019-09-30', '23:50:44', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 9; Mi A1 Build/PKQ1.180917.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.92 Mobil', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv8l', 'true'),
(333, '86599hemj', '187.120.216.114', '2019-09-30', '23:51:01', 30, 9, 2019, 'http://8pdev.studio/servicos/landing-page', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Linux; Android 9; Mi A1 Build/PKQ1.180917.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.92 Mobil', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv8l', 'true'),
(334, '86599hemj', '187.120.216.114', '2019-09-30', '23:51:06', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 9; Mi A1 Build/PKQ1.180917.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.92 Mobil', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv8l', 'true'),
(335, 'q81ptkt3o', '191.189.73.166', '2019-09-30', '23:51:38', 30, 9, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Linux; Android 8.0.0; ASUS_Z017DC Build/OPR1.170623.026; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.386', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv8l', 'true'),
(336, '86599hemj', '187.120.216.114', '2019-09-30', '23:52:07', 30, 9, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Linux; Android 9; Mi A1 Build/PKQ1.180917.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.92 Mobil', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv8l', 'true'),
(337, '86599hemj', '187.120.216.114', '2019-09-30', '23:53:13', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 9; Mi A1 Build/PKQ1.180917.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.92 Mobil', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv8l', 'true'),
(338, 'bygw78tvy', '179.222.195.127', '2019-09-30', '23:54:39', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(339, 'gfmt2f8et', '66.249.73.69', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=ru', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(340, 'gfmt2f8et', '66.249.73.69', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=da', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(341, 'gfmt2f8et', '66.249.73.70', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=uk', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(342, 'edas9aj8a', '66.249.73.69', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/projeto/epics_landing_page', '8p estúdio de desenvolvimento - Epics Landing Page', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(343, 'gfmt2f8et', '66.249.73.69', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=hi', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(344, 'gfmt2f8et', '66.249.73.69', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=bg', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(345, 'a6br16t0z', '95.179.154.158', '2019-10-01', '05:38:18', 1, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 'Amsterdam', 'Amsterdam', 'NH', 'Linux x86_64', 'false'),
(346, 'gfmt2f8et', '66.249.73.69', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=id', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(347, '6h6aff4c4', '66.249.73.69', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(348, 'v2ndntiyj', '5.9.176.210', '2019-10-01', '07:17:31', 1, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 'Kassel', 'HE', 'DE', 'Linux x86_64', 'false'),
(349, 'gfmt2f8et', '66.249.73.69', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=sw', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(350, 'gfmt2f8et', '66.249.73.69', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=el', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(351, 'ixn2hvoev', '189.38.203.107', '2019-10-01', '08:14:48', 1, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 9; G8141) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São Paulo', 'SP', 'BR', 'Linux armv8l', 'true'),
(352, 'ixn2hvoev', '189.38.203.107', '2019-10-01', '08:15:02', 1, 10, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Linux; Android 9; G8141) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São Paulo', 'SP', 'BR', 'Linux armv8l', 'true'),
(353, 'gfmt2f8et', '66.249.73.69', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=it', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(354, 'gfmt2f8et', '66.249.73.66', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=hi', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(355, 'gfmt2f8et', '66.249.73.69', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=bg', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(356, 'gfmt2f8et', '66.249.73.70', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=id', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(357, 'ixn2hvoev', '189.92.125.17', '2019-10-01', '09:26:30', 1, 10, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Linux; Android 9; G8141) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São Paulo', 'SP', 'BR', 'Linux armv8l', 'true'),
(358, 'gpjrq60l0', '42.236.10.125', '2019-10-01', '10:01:01', 1, 10, 2019, 'https://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; U; Android 8.1.0; zh-CN; EML-AL00 Build/HUAWEIEML-AL00) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/57.0.2', 'Henan Sheng', 'CN', '', 'Win32', 'true'),
(359, 'ae9ikw3ob', '173.252.87.19', '2019-10-01', '14:41:37', 1, 10, 2019, 'http://8pdev.studio/?fbclid=IwAR0psCIKu0FbnXSoWx9Ahejt_WuW01IFuE4BF1VPBygzap2LUuhLal7dk94', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(360, 'n7j3oqm05', '200.233.231.219', '2019-10-01', '14:50:41', 1, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Instagram 113.0.0.28.122 (iP', 'São Paulo', 'BR', '14024', 'iPhone', 'true'),
(361, 'n7j3oqm05', '200.233.231.219', '2019-10-01', '14:50:52', 1, 10, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Instagram 113.0.0.28.122 (iP', 'São Paulo', 'BR', '14024', 'iPhone', 'true'),
(362, 'autarskz2', '45.35.57.217', '2019-10-01', '15:20:34', 1, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 'Dallas County', 'TX', 'US', 'Linux x86_64', 'false'),
(363, 'gfmt2f8et', '66.249.73.66', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=mk', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(364, 'gfmt2f8et', '66.249.73.66', '2019-10-01', '15:47:19', 1, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(365, 'h46cga3ld', '189.46.111.136', '2019-10-01', '15:48:55', 1, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(366, 'h46cga3ld', '189.46.111.136', '2019-10-01', '16:20:35', 1, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(367, 'gfmt2f8et', '66.249.73.70', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=es', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(368, 'gte2wd7is', '179.222.210.96', '2019-10-01', '17:54:40', 1, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Android 8.0.0; Mobile; rv:68.0) Gecko/68.0 Firefox/68.0', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv7l', 'true'),
(369, 'gfmt2f8et', '66.249.73.69', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=ta', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(370, 'gfmt2f8et', '66.249.73.69', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=ml', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(371, 'gfmt2f8et', '66.249.73.66', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=cs', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(372, 'gfmt2f8et', '66.249.73.66', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=mr', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(373, 'h46cga3ld', '189.46.111.136', '2019-10-01', '22:32:07', 1, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(374, 'gfmt2f8et', '66.249.73.70', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/?l=nb', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(375, '4a1w9gyay', '189.46.111.136', '2019-10-01', '23:45:53', 1, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(376, '4a1w9gyay', '189.46.111.136', '2019-10-01', '23:46:58', 1, 10, 2019, 'http://8pdev.studio/servicos/e_commerce', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(377, '4a1w9gyay', '189.46.111.136', '2019-10-01', '23:47:03', 1, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(378, 'gfmt2f8et', '66.249.73.69', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/?l=gu', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(379, 'gfmt2f8et', '66.249.73.69', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/?l=ro', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(380, 'whmv3pkkh', '187.66.60.101', '2019-10-02', '02:02:54', 2, 10, 2019, 'http://8pdev.studio/lulapalozo/', '8p estúdio de desenvolvimento', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1', 'São José do Rio Preto', 'SP', 'BR', 'iPhone', 'true'),
(381, 'whmv3pkkh', '187.66.60.101', '2019-10-02', '02:02:57', 2, 10, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1', 'São José do Rio Preto', 'SP', 'BR', 'iPhone', 'true'),
(382, 'gfmt2f8et', '66.249.73.69', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/?l=sr', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(383, 'gfmt2f8et', '66.249.73.66', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/?l=af', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(384, 'h46cga3ld', '189.46.111.136', '2019-10-02', '04:10:20', 2, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(385, 'gfmt2f8et', '66.249.73.66', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/?l=vi', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(386, '6h6aff4c4', '66.249.73.69', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(387, 'gfmt2f8et', '66.249.73.70', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/?l=bn', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(388, 'gfmt2f8et', '66.249.73.69', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/?l=uz', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(389, 'gfmt2f8et', '66.249.73.69', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/?l=sl', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(390, 'gfmt2f8et', '66.249.73.69', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/?l=fil', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(391, 'tefxr6v4h', '177.96.88.113', '2019-10-02', '07:01:29', 2, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0; XT1097 Build/MPES24.49-18-7; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.92 Mob', 'Brazil', '84145-000', '', 'Linux armv7l', 'true'),
(392, 'edas9aj8a', '66.249.73.69', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/servicos/e_commerce', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(393, 'gfmt2f8et', '66.249.73.69', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/?l=kn', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(394, '6h6aff4c4', '66.249.73.66', '2019-10-02', '09:00:29', 2, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(395, 'ki351wsmh', '179.242.172.92', '2019-10-02', '09:32:18', 2, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 8.1.0; SAMSUNG SM-J730G Build/M1AJQ) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/9.4 Chrome/67.0.3396.', '57690-000', 'SP', 'BR', 'Linux armv8l', 'true'),
(396, 'ki351wsmh', '179.242.172.92', '2019-10-02', '09:35:19', 2, 10, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Linux; Android 8.1.0; SAMSUNG SM-J730G Build/M1AJQ) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/9.4 Chrome/67.0.3396.', '57690-000', 'SP', '', 'Linux armv8l', 'true'),
(397, 'gfmt2f8et', '66.249.73.69', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/?l=en', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(398, 'gfmt2f8et', '66.249.73.69', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/?l=fr', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(399, 'edas9aj8a', '66.249.73.66', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/servicos/aplicativos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(400, 'gfmt2f8et', '66.249.73.66', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/?l=he', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(401, 'gfmt2f8et', '66.249.73.70', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=hi', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(402, 'gfmt2f8et', '66.249.73.66', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/?l=es', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(403, 'gfmt2f8et', '66.249.73.66', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/?l=af', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(404, 'gfmt2f8et', '66.249.73.69', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/?l=en', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false');
INSERT INTO `analytics` (`id`, `codigo`, `ip`, `data`, `hora`, `dia`, `mes`, `ano`, `url`, `pagina`, `navegador`, `cidade`, `estado`, `pais`, `sistema`, `dispositivo`) VALUES
(405, 'gfmt2f8et', '66.249.73.69', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/?l=gu', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(406, 'gfmt2f8et', '66.249.73.70', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/?l=zh_tw', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(407, 'ux3xlbbgw', '177.66.233.198', '2019-10-02', '16:38:06', 2, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(408, 'ux3xlbbgw', '177.66.233.198', '2019-10-02', '16:38:51', 2, 10, 2019, 'http://8pdev.studio/projeto/okamed-web-site', '8p estúdio de desenvolvimento - OKAMED web site', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(409, 'ux3xlbbgw', '177.66.233.198', '2019-10-02', '18:22:36', 2, 10, 2019, 'http://8pdev.studio/projeto/okamed-web-site', '8p estúdio de desenvolvimento - OKAMED web site', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(410, 'gfmt2f8et', '66.249.73.66', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/?l=pt_br', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(411, '6h6aff4c4', '66.249.73.70', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(412, 'gxliyy0c3', '69.171.251.18', '2019-10-02', '20:00:25', 2, 10, 2019, 'http://8pdev.studio/?fbclid=IwAR3seGJBNfxqR5qtehMfHEBHYfBbk6WbNi38qIGMSyXxa_tXKYa7oqRylR4', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Mobile/15E148 Safari/604.1', 'Nagpur', 'Nagpur', 'MH', 'iPhone', 'true'),
(413, 'edas9aj8a', '66.249.73.66', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/servicos/ux-ux-design', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(414, '5oglziu16', '31.13.115.17', '2019-10-02', '22:43:31', 2, 10, 2019, 'http://8pdev.studio/?fbclid=IwAR1LDSniVMrr_FitAjudDhBjcCUmdaId4Y4CZQG69aBx55X9DjreKLbGLis', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1 Mobile/15E148 Safari/604.1', 'County Dublin', 'IE', '', 'iPhone', 'true'),
(415, 'gfmt2f8et', '66.249.73.66', '2019-10-02', '22:00:00', 2, 10, 2019, 'http://8pdev.studio/?l=zh_cn', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(416, 'edas9aj8a', '66.249.73.69', '2019-10-02', '22:00:00', 2, 10, 2019, 'http://8pdev.studio/servicos/sistemas-web', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(417, 'gfmt2f8et', '66.249.73.70', '2019-10-02', '22:00:00', 2, 10, 2019, 'http://8pdev.studio/?l=pt_pt', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(418, '0y0nf9gah', '213.183.56.170', '2019-10-03', '00:56:48', 3, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 'Moskva', 'Khamovniki', 'Moskva', 'Linux x86_64', 'false'),
(419, 'whmv3pkkh', '189.40.90.81', '2019-10-03', '02:03:23', 3, 10, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1', 'São Paulo', 'SP', 'BR', 'iPhone', 'true'),
(420, '4a1w9gyay', '189.46.111.136', '2019-10-03', '02:15:09', 3, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(421, 'edas9aj8a', '66.249.73.70', '2019-09-30', '22:00:00', 30, 9, 2019, 'http://8pdev.studio/projeto/epics_landing_page', '8p estúdio de desenvolvimento - Epics Landing Page', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(422, 'edas9aj8a', '66.249.73.66', '2019-10-02', '22:00:00', 2, 10, 2019, 'http://8pdev.studio/projeto/finnance-web-site', '8p estúdio de desenvolvimento - Finnance web site', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(423, 'edas9aj8a', '66.249.73.69', '2019-10-02', '22:00:00', 2, 10, 2019, 'http://8pdev.studio/servicos/landing-page', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(424, 'edas9aj8a', '66.249.73.69', '2019-10-02', '22:00:00', 2, 10, 2019, 'http://8pdev.studio/projeto/epics_landing_page', '8p estúdio de desenvolvimento - Epics Landing Page', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(425, 'edas9aj8a', '66.249.73.69', '2019-10-02', '22:00:00', 2, 10, 2019, 'http://8pdev.studio/projeto/epics-landing-page', '8p estúdio de desenvolvimento - Epics Landing Page', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(426, 'mzpgvw2pz', '66.249.73.71', '2019-09-26', '22:00:00', 26, 9, 2019, 'https://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(427, 'edas9aj8a', '66.249.73.66', '2019-10-02', '22:00:00', 2, 10, 2019, 'http://8pdev.studio/servicos/site-institucional', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(428, '0g7per3l4', '95.179.154.158', '2019-10-03', '04:46:23', 3, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 'Amsterdam', 'Amsterdam', 'NH', 'Linux x86_64', 'false'),
(429, 'edas9aj8a', '66.249.73.69', '2019-10-02', '22:00:00', 2, 10, 2019, 'http://8pdev.studio/servicos/site-catalogo', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(430, 'edas9aj8a', '66.249.73.66', '2019-10-02', '22:00:00', 2, 10, 2019, 'http://8pdev.studio/projeto/boxsupreme-web-site', '8p estúdio de desenvolvimento - BoxSupreme web site', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(431, 'gfmt2f8et', '66.249.73.66', '2019-10-02', '22:00:00', 2, 10, 2019, 'http://8pdev.studio/?l=hr', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(432, 'qf440cryy', '186.222.214.21', '2019-10-03', '14:42:20', 3, 10, 2019, 'http://8pdev.studio/?fbclid=IwAR3iaqTtiuMpgJkecFqdN5P1W-eVVXc4dV32g6lLtK5fcuB9KaF_3FNMMJU', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(433, 'qf440cryy', '186.222.214.21', '2019-10-03', '14:42:57', 3, 10, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(434, 'qf440cryy', '186.222.214.21', '2019-10-03', '14:43:56', 3, 10, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(435, 'qf440cryy', '186.222.214.21', '2019-10-03', '14:44:29', 3, 10, 2019, 'http://8pdev.studio/projeto/intera-skateshop-loja-online', '8p estúdio de desenvolvimento - Intera skateshop Loja Online', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(436, 'qf440cryy', '186.222.214.21', '2019-10-03', '14:45:54', 3, 10, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(437, 'qf440cryy', '186.222.214.21', '2019-10-03', '14:45:59', 3, 10, 2019, 'http://8pdev.studio/projeto/mar-aberto-loja-online', '8p estúdio de desenvolvimento - Mar Aberto Loja Online', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(438, 'gfmt2f8et', '66.249.73.70', '2019-10-02', '22:00:00', 2, 10, 2019, 'http://8pdev.studio/?l=nl', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(439, '4a1w9gyay', '189.46.111.136', '2019-10-03', '16:37:38', 3, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(440, '4a1w9gyay', '189.46.111.136', '2019-10-03', '16:37:42', 3, 10, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(441, '4a1w9gyay', '189.46.111.136', '2019-10-03', '16:37:51', 3, 10, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(442, '4a1w9gyay', '189.46.111.136', '2019-10-03', '16:37:58', 3, 10, 2019, 'http://8pdev.studio/servicos/site-institucional', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(443, '4a1w9gyay', '189.46.111.136', '2019-10-03', '16:38:06', 3, 10, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'MacIntel', 'false'),
(444, 'gfmt2f8et', '66.249.73.69', '2019-10-02', '22:00:00', 2, 10, 2019, 'http://8pdev.studio/?l=ja', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(445, 'gfmt2f8et', '66.249.73.69', '2019-10-02', '22:00:00', 2, 10, 2019, 'http://8pdev.studio/?l=de', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(446, 'gfmt2f8et', '66.249.73.64', '2019-10-02', '22:00:00', 2, 10, 2019, 'https://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(447, 'edas9aj8a', '66.249.73.69', '2019-10-02', '22:00:00', 2, 10, 2019, 'http://8pdev.studio/projeto/finnance_web_site', '8p estúdio de desenvolvimento - Finnance web site', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(448, 'os3zqa4dq', '189.92.115.126', '2019-10-03', '21:03:08', 3, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 8.1.0; Moto G (5)) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São Paulo', 'SP', 'BR', 'Linux armv7l', 'true'),
(449, 'b06pbd2lt', '187.57.148.251', '2019-10-03', '21:06:45', 3, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'SP', 'BR', 'MacIntel', 'false'),
(450, 'b06pbd2lt', '187.57.148.251', '2019-10-03', '21:06:50', 3, 10, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'SP', 'BR', 'MacIntel', 'false'),
(451, 'b06pbd2lt', '187.57.148.251', '2019-10-03', '21:10:13', 3, 10, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'SP', 'BR', 'MacIntel', 'false'),
(452, 'b06pbd2lt', '187.57.148.251', '2019-10-03', '21:10:22', 3, 10, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'SP', 'BR', 'MacIntel', 'false'),
(453, 'edas9aj8a', '66.249.73.66', '2019-10-03', '22:00:00', 3, 10, 2019, 'http://8pdev.studio/projeto/boxsupreme_web_site', '8p estúdio de desenvolvimento - BoxSupreme web site', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(454, 'edas9aj8a', '66.249.73.70', '2019-10-03', '22:00:00', 3, 10, 2019, 'http://8pdev.studio/servicos/front-end-back-end', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(455, 'whmv3pkkh', '189.46.111.136', '2019-10-04', '04:31:21', 4, 10, 2019, 'http://8pdev.studio/lulapalozo/', '8p estúdio de desenvolvimento', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1', 'São Paulo', 'BR', '14024', 'iPhone', 'true'),
(456, 'whmv3pkkh', '189.46.111.136', '2019-10-04', '04:31:25', 4, 10, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1', 'São Paulo', 'BR', '14024', 'iPhone', 'true'),
(457, 'edas9aj8a', '66.249.73.66', '2019-10-03', '22:00:00', 3, 10, 2019, 'http://8pdev.studio/projeto/okamed-web-site', '8p estúdio de desenvolvimento - OKAMED web site', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(458, 'edas9aj8a', '66.249.73.69', '2019-10-03', '22:00:00', 3, 10, 2019, 'http://8pdev.studio/projeto/okamed_web_site', '8p estúdio de desenvolvimento - OKAMED web site', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(459, 'edas9aj8a', '66.249.73.69', '2019-10-03', '22:00:00', 3, 10, 2019, 'http://8pdev.studio/projeto/epics_landing_page', '8p estúdio de desenvolvimento - Epics Landing Page', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(460, 'd6lc50g5c', '69.171.251.21', '2019-10-04', '08:34:20', 4, 10, 2019, 'http://8pdev.studio/?fbclid=IwAR0jmkAQJfG93ZQHCWZMYRz8OCEHwDHHPXb0SnCJXwFbsgmy11q6WrKdWP8', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'Nagpur', 'Nagpur', 'MH', 'Linux x86_64', 'false'),
(461, 'whmv3pkkh', '189.46.111.136', '2019-10-04', '10:22:44', 4, 10, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1', 'São Paulo', 'BR', '14024', 'iPhone', 'true'),
(462, 'edas9aj8a', '66.249.73.66', '2019-10-03', '22:00:00', 3, 10, 2019, 'http://8pdev.studio/servicos/site-catalogo', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(463, '5kjq27170', '189.46.111.136', '2019-10-04', '16:07:11', 4, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 8.0.0; ASUS_Z01KD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São Paulo', 'BR', '14024', 'Linux armv8l', 'true'),
(464, '5kjq27170', '189.46.111.136', '2019-10-04', '16:07:15', 4, 10, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Linux; Android 8.0.0; ASUS_Z01KD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São Paulo', 'BR', '14024', 'Linux armv8l', 'true'),
(465, '5kjq27170', '189.46.111.136', '2019-10-04', '16:07:45', 4, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 8.0.0; ASUS_Z01KD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São Paulo', 'BR', '14024', 'Linux armv8l', 'true'),
(466, 'ki351wsmh', '179.242.188.186', '2019-10-04', '23:51:38', 4, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 8.1.0; SAMSUNG SM-J730G Build/M1AJQ) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/9.4 Chrome/67.0.3396.', 'São Paulo', 'SP', '', 'Linux armv8l', 'true'),
(467, '6h6aff4c4', '66.249.73.75', '2019-10-04', '22:00:00', 4, 10, 2019, 'https://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(468, '6h6aff4c4', '66.249.73.82', '2019-10-04', '22:00:00', 4, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(469, 'edas9aj8a', '66.249.73.82', '2019-10-04', '22:00:00', 4, 10, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(470, 'edas9aj8a', '66.249.73.82', '2019-10-04', '22:00:00', 4, 10, 2019, 'http://8pdev.studio/servicos/e_commerce', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(471, 'ejd0h7wax', '95.179.154.158', '2019-10-05', '16:52:20', 5, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36', 'Amsterdam', 'Amsterdam', 'NH', 'Linux x86_64', 'false'),
(472, 'uhw4svxql', '66.249.73.82', '2019-10-04', '22:00:00', 4, 10, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(473, 'edas9aj8a', '66.249.73.84', '2019-10-05', '22:00:00', 5, 10, 2019, 'http://8pdev.studio/projeto/okamed-web-site', '8p estúdio de desenvolvimento - OKAMED web site', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(474, 'z5faub2j9', '173.252.127.37', '2019-10-06', '08:50:14', 6, 10, 2019, 'http://8pdev.studio/?fbclid=IwAR28YkX17GriBVeVp_9qpbRvmSEHOzXJyW_9ew53j2yrgAjQGCz4HLygTvg', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(650, '7lg1qsxxx', '191.19.52.68', '2019-10-06', '22:54:19', 6, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(651, '7lg1qsxxx', '191.19.52.68', '2019-10-06', '22:55:12', 6, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(652, '7lg1qsxxx', '191.19.52.68', '2019-10-06', '22:55:16', 6, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(653, '7lg1qsxxx', '191.19.52.68', '2019-10-06', '22:57:59', 6, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(654, '7lg1qsxxx', '191.19.52.68', '2019-10-06', '22:58:37', 6, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(655, '7lg1qsxxx', '191.19.52.68', '2019-10-06', '22:59:18', 6, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(656, '7lg1qsxxx', '191.19.52.68', '2019-10-06', '22:59:33', 6, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(657, 'edas9aj8a', '66.249.73.82', '2019-10-06', '22:00:00', 6, 10, 2019, 'http://8pdev.studio/projeto/okamed_web_site', '8p estúdio de desenvolvimento - OKAMED web site', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(658, 'khkq9os37', '66.249.73.84', '2019-10-06', '22:00:00', 6, 10, 2019, 'http://8pdev.studio/?l=uz', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(659, 'qn4os3zz2', '66.249.73.84', '2019-10-07', '05:52:18', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(660, '6g99vxbr3', '69.171.251.25', '2019-10-07', '07:34:36', 7, 10, 2019, 'http://8pdev.studio/?fbclid=IwAR0inCVduwL4zz-Df5QyTlb3vHz1KUB7J-FLO4BnzlIbFr05N8Z72zs2bwE', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'Nagpur', 'Nagpur', 'MH', 'Linux x86_64', 'false'),
(661, 'ohw9mkq02', '104.132.119.65', '2019-10-07', '10:07:26', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'SP', 'BR', 'MacIntel', 'false'),
(662, 'khkq9os37', '66.249.73.84', '2019-10-06', '22:00:00', 6, 10, 2019, 'http://8pdev.studio/?l=uk', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(663, '4e6z54d41', '191.19.52.68', '2019-10-07', '12:10:10', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(664, '4e6z54d41', '191.19.52.68', '2019-10-07', '12:10:25', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(665, '4e6z54d41', '191.19.52.68', '2019-10-07', '12:11:24', 7, 10, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(666, '4e6z54d41', '191.19.52.68', '2019-10-07', '12:11:31', 7, 10, 2019, 'http://8pdev.studio/projeto/epics-landing-page', '8p estúdio de desenvolvimento - Epics Landing Page', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(667, '4e6z54d41', '191.19.52.68', '2019-10-07', '12:11:45', 7, 10, 2019, 'http://8pdev.studio/projeto/js_menu_web_site', '8p estúdio de desenvolvimento - JS Menu web site', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(668, '4e6z54d41', '191.19.52.68', '2019-10-07', '12:11:54', 7, 10, 2019, 'http://8pdev.studio/projeto/okamed_web_site', '8p estúdio de desenvolvimento - OKAMED web site', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(669, '4e6z54d41', '191.19.52.68', '2019-10-07', '12:12:07', 7, 10, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(670, '4e6z54d41', '191.19.52.68', '2019-10-07', '12:12:34', 7, 10, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(671, '4e6z54d41', '191.19.52.68', '2019-10-07', '12:13:05', 7, 10, 2019, 'http://8pdev.studio/contato', '8p estúdio de desenvolvimento - Contato', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(672, '4e6z54d41', '191.19.52.68', '2019-10-07', '12:13:14', 7, 10, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(673, 'qnq2ibz1x', '189.40.90.219', '2019-10-07', '13:27:50', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 8.0.0; ASUS_Z01KD Build/OPR1.170623.032; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865', 'São Paulo', 'SP', 'BR', 'Linux armv8l', 'true'),
(674, 'qnq2ibz1x', '189.40.90.219', '2019-10-07', '13:28:01', 7, 10, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Linux; Android 8.0.0; ASUS_Z01KD Build/OPR1.170623.032; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865', 'São Paulo', 'SP', 'BR', 'Linux armv8l', 'true'),
(675, 'qnq2ibz1x', '189.40.90.219', '2019-10-07', '13:28:05', 7, 10, 2019, 'http://8pdev.studio/projeto/js-menu-web-site', '8p estúdio de desenvolvimento - JS Menu web site', 'Mozilla/5.0 (Linux; Android 8.0.0; ASUS_Z01KD Build/OPR1.170623.032; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865', 'São Paulo', 'SP', 'BR', 'Linux armv8l', 'true'),
(676, '5kjq27170', '191.19.52.68', '2019-10-07', '13:30:08', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 8.0.0; ASUS_Z01KD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São Paulo', 'BR', '14024', 'Linux armv8l', 'true'),
(677, '5kjq27170', '191.19.52.68', '2019-10-07', '13:30:10', 7, 10, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Linux; Android 8.0.0; ASUS_Z01KD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São Paulo', 'BR', '14024', 'Linux armv8l', 'true'),
(678, '5kjq27170', '191.19.52.68', '2019-10-07', '13:30:20', 7, 10, 2019, 'http://8pdev.studio/projeto/js-menu-web-site', '8p estúdio de desenvolvimento - JS Menu web site', 'Mozilla/5.0 (Linux; Android 8.0.0; ASUS_Z01KD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São Paulo', 'BR', '14024', 'Linux armv8l', 'true'),
(679, '4e6z54d41', '191.19.52.68', '2019-10-07', '13:56:35', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(680, 'khkq9os37', '66.249.73.84', '2019-10-06', '22:00:00', 6, 10, 2019, 'http://8pdev.studio/?l=ur', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(681, 'khkq9os37', '66.249.73.82', '2019-10-06', '22:00:00', 6, 10, 2019, 'http://8pdev.studio/?l=af', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(682, '7lg1qsxxx', '191.19.52.68', '2019-10-07', '14:53:38', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(683, 'khkq9os37', '66.249.73.86', '2019-10-06', '22:00:00', 6, 10, 2019, 'http://8pdev.studio/?l=ar', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(684, 'khkq9os37', '66.249.73.82', '2019-10-06', '22:00:00', 6, 10, 2019, 'http://8pdev.studio/?l=nl', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(685, 'khkq9os37', '66.249.73.82', '2019-10-06', '22:00:00', 6, 10, 2019, 'http://8pdev.studio/?l=kk', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(686, '7lg1qsxxx', '191.19.52.68', '2019-10-07', '15:09:03', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(687, '59cne233w', '201.95.181.248', '2019-10-07', '15:14:28', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'SP', 'BR', 'MacIntel', 'false'),
(688, 'ecilg2rz0', '170.81.129.5', '2019-10-07', '15:26:32', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(689, 'ecilg2rz0', '170.81.129.5', '2019-10-07', '15:26:43', 7, 10, 2019, 'http://8pdev.studio/projeto/epics-landing-page', '8p estúdio de desenvolvimento - Epics Landing Page', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(690, 'ecilg2rz0', '170.81.129.5', '2019-10-07', '15:27:38', 7, 10, 2019, 'http://8pdev.studio/projeto/okamed_web_site', '8p estúdio de desenvolvimento - OKAMED web site', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(691, '7lg1qsxxx', '191.19.52.68', '2019-10-07', '15:33:26', 7, 10, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(692, '7lg1qsxxx', '191.19.52.68', '2019-10-07', '15:33:34', 7, 10, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(693, '7lg1qsxxx', '191.19.52.68', '2019-10-07', '15:33:39', 7, 10, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(694, '7lg1qsxxx', '191.19.52.68', '2019-10-07', '15:35:22', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(695, 'khkq9os37', '66.249.73.82', '2019-10-06', '22:00:00', 6, 10, 2019, 'http://8pdev.studio/?l=et', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(696, '7lg1qsxxx', '191.19.52.68', '2019-10-07', '15:39:19', 7, 10, 2019, 'http://8pdev.studio/painle', '8p estúdio de desenvolvimento', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(697, 'e23nfmz9j', '187.72.144.176', '2019-10-07', '16:00:20', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_1_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.1 Mobile/15E148 Safari/604.1', 'Rio de Janeiro', 'RJ', 'BR', 'iPhone', 'true'),
(698, '8yrsb9kss', '10.1.1.57', '2019-10-07', '17:08:06', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', '', '', '', 'Win32', 'false'),
(699, 'foroez7gf', '187.66.60.101', '2019-10-07', '16:14:01', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 7.1.1; Moto G (5S) Plus) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv7l', 'true'),
(700, 'foroez7gf', '187.66.60.101', '2019-10-07', '16:14:12', 7, 10, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Linux; Android 7.1.1; Moto G (5S) Plus) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv7l', 'true'),
(701, 'foroez7gf', '187.66.60.101', '2019-10-07', '16:14:31', 7, 10, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Linux; Android 7.1.1; Moto G (5S) Plus) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv7l', 'true'),
(702, 'wioqan1t3', '187.101.38.226', '2019-10-07', '16:15:04', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'Brazil', '14180-000', '', 'Win32', 'false'),
(703, '2bnmycafo', '177.189.240.127', '2019-10-07', '16:15:18', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Mobile/15E148 Safari/604.1', 'Brazil', '14940-000', '', 'iPhone', 'true'),
(704, '0k7y3zhch', '189.35.146.168', '2019-10-07', '16:15:57', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Mobile/15E148 Safari/604.1', 'São José do Rio Preto', 'SP', 'BR', 'iPhone', 'true'),
(705, '5kjq27170', '191.19.52.68', '2019-10-07', '16:33:53', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 8.0.0; ASUS_Z01KD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São Paulo', 'BR', '14024', 'Linux armv8l', 'true'),
(706, '5kjq27170', '191.19.52.68', '2019-10-07', '16:34:23', 7, 10, 2019, 'http://8pdev.studio/projeto/js-menu-web-site', '8p estúdio de desenvolvimento - JS Menu web site', 'Mozilla/5.0 (Linux; Android 8.0.0; ASUS_Z01KD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São Paulo', 'BR', '14024', 'Linux armv8l', 'true'),
(707, '0k7y3zhch', '189.35.146.168', '2019-10-07', '16:44:27', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Mobile/15E148 Safari/604.1', 'São José do Rio Preto', 'SP', 'BR', 'iPhone', 'true'),
(708, 'khkq9os37', '66.249.73.70', '2019-10-06', '22:00:00', 6, 10, 2019, 'http://8pdev.studio/?l=pt_br', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(709, 'kzdg5l4t7', '186.225.143.246', '2019-10-07', '17:01:28', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(710, 'kzdg5l4t7', '186.225.143.246', '2019-10-07', '17:02:18', 7, 10, 2019, 'http://8pdev.studio/servicos/e_commerce', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(711, 'kzdg5l4t7', '186.225.143.246', '2019-10-07', '17:02:33', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(712, 'kzdg5l4t7', '186.225.143.246', '2019-10-07', '17:03:10', 7, 10, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(713, 'kzdg5l4t7', '186.225.143.246', '2019-10-07', '17:03:16', 7, 10, 2019, 'http://8pdev.studio/projeto/js-menu-web-site', '8p estúdio de desenvolvimento - JS Menu web site', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(714, 'kzdg5l4t7', '186.225.143.246', '2019-10-07', '17:03:34', 7, 10, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(715, 'kzdg5l4t7', '186.225.143.246', '2019-10-07', '17:04:01', 7, 10, 2019, 'http://8pdev.studio/projeto/intera-skateshop-loja-online', '8p estúdio de desenvolvimento - Intera skateshop Loja Online', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(716, 'kzdg5l4t7', '186.225.143.246', '2019-10-07', '17:04:25', 7, 10, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(717, 'kzdg5l4t7', '186.225.143.246', '2019-10-07', '17:05:03', 7, 10, 2019, 'http://8pdev.studio/projeto/js-menu-web-site', '8p estúdio de desenvolvimento - JS Menu web site', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(718, 'kzdg5l4t7', '186.225.143.246', '2019-10-07', '17:06:16', 7, 10, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(719, 'kzdg5l4t7', '186.225.143.246', '2019-10-07', '17:08:28', 7, 10, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(720, 'kzdg5l4t7', '186.225.143.246', '2019-10-07', '17:09:57', 7, 10, 2019, 'http://8pdev.studio/inicio', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(721, 'w7540isri', '201.95.181.248', '2019-10-07', '17:15:03', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'SP', 'BR', 'MacIntel', 'false');
INSERT INTO `analytics` (`id`, `codigo`, `ip`, `data`, `hora`, `dia`, `mes`, `ano`, `url`, `pagina`, `navegador`, `cidade`, `estado`, `pais`, `sistema`, `dispositivo`) VALUES
(722, 'foroez7gf', '187.66.60.101', '2019-10-07', '17:15:07', 7, 10, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Linux; Android 7.1.1; Moto G (5S) Plus) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv7l', 'true'),
(723, 'kzdg5l4t7', '186.225.143.246', '2019-10-07', '17:17:13', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(724, 'kzdg5l4t7', '186.225.143.246', '2019-10-07', '17:17:15', 7, 10, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(725, 'kzdg5l4t7', '186.225.143.246', '2019-10-07', '17:17:17', 7, 10, 2019, 'http://8pdev.studio/servicos', '8p estúdio de desenvolvimento - Serviços', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(726, 'w7540isri', '201.95.181.248', '2019-10-07', '17:19:59', 7, 10, 2019, 'http://8pdev.studio/projeto/casa-verde-interiores', '8p estúdio de desenvolvimento - Casa Verde Interiores', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'SP', 'BR', 'MacIntel', 'false'),
(727, 'khkq9os37', '66.249.73.66', '2019-10-06', '22:00:00', 6, 10, 2019, 'http://8pdev.studio/?l=hi', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(728, 'khkq9os37', '66.249.73.69', '2019-10-06', '22:00:00', 6, 10, 2019, 'http://8pdev.studio/?l=ga', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(729, 'khkq9os37', '66.249.73.66', '2019-10-06', '22:00:00', 6, 10, 2019, 'http://8pdev.studio/?l=fr', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(730, '4e6z54d41', '191.19.52.68', '2019-10-07', '18:57:37', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(731, 'dnpxctxpk', '177.180.212.193', '2019-10-07', '19:20:08', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(732, 'dnpxctxpk', '177.180.212.193', '2019-10-07', '19:20:47', 7, 10, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(733, 'dnpxctxpk', '177.180.212.193', '2019-10-07', '19:21:03', 7, 10, 2019, 'http://8pdev.studio/projeto/casa-verde-interiores', '8p estúdio de desenvolvimento - Casa Verde Interiores', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Win32', 'false'),
(734, 'vu218jdke', '177.45.5.251', '2019-10-07', '19:23:41', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_1_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.1 Mobile/15E148 Safari/604.1', 'Brazil', '15600-000', '', 'iPhone', 'true'),
(735, 'khkq9os37', '66.249.73.66', '2019-10-06', '22:00:00', 6, 10, 2019, 'http://8pdev.studio/?l=zh_cn', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(736, 'khkq9os37', '66.249.73.66', '2019-10-06', '22:00:00', 6, 10, 2019, 'http://8pdev.studio/?l=es', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(737, 'khkq9os37', '66.249.73.66', '2019-10-06', '22:00:00', 6, 10, 2019, 'http://8pdev.studio/?l=sw', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(738, 'e23nfmz9j', '187.72.144.176', '2019-10-07', '20:27:53', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_1_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.1 Mobile/15E148 Safari/604.1', 'Rio de Janeiro', 'RJ', 'BR', 'iPhone', 'true'),
(739, '49nbnw9oz', '201.49.82.60', '2019-10-07', '20:41:37', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Mobile/15E148 Safari/604.1', 'São José do Rio Preto', 'SP', 'BR', 'iPhone', 'true'),
(740, 'khkq9os37', '66.249.73.70', '2019-10-06', '22:00:00', 6, 10, 2019, 'http://8pdev.studio/?l=he', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(741, 'gfmt2f8et', '66.249.73.69', '2019-10-01', '22:00:00', 1, 10, 2019, 'http://8pdev.studio/?l=he', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 AppleWebKit/537.36 (KHTML, like Gecko; compatible; Googlebot/2.1; +http://www.google.com/bot.html) Safari/537.36', 'United States', '', '', 'Linux x86_64', 'false'),
(742, '0dh8qs336', '177.27.221.160', '2019-10-07', '21:53:52', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 9; SAMSUNG SM-A305GT) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/10.1 Chrome/71.0.3578.99 Mobile Safa', 'São Paulo', 'SP', 'BR', 'Linux armv8l', 'true'),
(743, 'texpvg7ox', '187.181.198.107', '2019-10-07', '21:55:53', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 8.0.0; SM-A520F) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv8l', 'true'),
(744, 'vu218jdke', '177.45.5.251', '2019-10-07', '22:22:20', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_1_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.1 Mobile/15E148 Safari/604.1', 'Brazil', '15600-000', '', 'iPhone', 'true'),
(745, 'khkq9os37', '66.249.73.69', '2019-10-07', '22:00:00', 7, 10, 2019, 'http://8pdev.studio/?l=gu', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(746, 'khkq9os37', '66.249.73.69', '2019-10-07', '22:00:00', 7, 10, 2019, 'http://8pdev.studio/?l=ms', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(747, '4e6z54d41', '191.19.52.68', '2019-10-07', '22:52:07', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(748, 'gfazj9so0', '186.211.57.74', '2019-10-07', '23:39:52', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 9; SM-A305GT) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv8l', 'true'),
(749, '4e6z54d41', '191.19.52.68', '2019-10-07', '00:51:18', 7, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(750, 'u5cs30en9', '189.35.148.167', '2019-10-08', '01:31:02', 8, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Mobile/15E148 Safari/604.1', 'São José do Rio Preto', 'SP', 'BR', 'iPhone', 'true'),
(751, 'fjacvp26b', '179.99.24.62', '2019-10-08', '01:26:12', 8, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'SP', 'BR', 'Win32', 'false'),
(752, 'fjacvp26b', '179.99.24.62', '2019-10-08', '01:26:22', 8, 10, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'SP', 'BR', 'Win32', 'false'),
(753, '7lg1qsxxx', '191.19.52.68', '2019-10-08', '01:47:14', 8, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(754, 'foroez7gf', '187.66.60.101', '2019-10-08', '02:44:38', 8, 10, 2019, 'http://8pdev.studio/portfolio', '8p estúdio de desenvolvimento - Portfólio', 'Mozilla/5.0 (Linux; Android 7.1.1; Moto G (5S) Plus) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.92 Mobile Safari/537.36', 'São José do Rio Preto', 'SP', 'BR', 'Linux armv7l', 'true'),
(755, 'khkq9os37', '66.249.73.70', '2019-10-07', '22:00:00', 7, 10, 2019, 'http://8pdev.studio/?l=el', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 6.0.1; Nexus 5X Build/MMB29P) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.96 Mobile Safari/537.36 (c', 'United States', '', '', 'Linux armv8l', 'true'),
(756, 'jlsj10kbx', '179.110.92.33', '2019-10-08', '03:45:23', 8, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 9; MI 8 Lite Build/PKQ1.181007.001; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.92 M', 'São Paulo', 'SP', 'BR', 'Linux armv8l', 'true'),
(757, '39x40aigs', '186.237.139.42', '2019-10-08', '07:35:28', 8, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Mobile/15E148 Safari/604.1', 'São José do Rio Preto', 'SP', 'BR', 'iPhone', 'true'),
(758, '8eq11e32s', '186.237.139.42', '2019-10-08', '07:36:40', 8, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Mobile/15E148 Safari/604.1', 'São José do Rio Preto', 'SP', 'BR', 'iPhone', 'true'),
(759, 'i4a4kijp1', '66.220.149.33', '2019-10-08', '08:29:27', 8, 10, 2019, 'http://8pdev.studio/?fbclid=IwAR3twmxT-OXMpFgmK-l7gYbQVR0BwZKm_x0MMlmp0llS3fiUSEKwuaHlbC0', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36', 'Ottawa', 'Ottawa Division', 'ON', 'Linux x86_64', 'false'),
(760, 'djtidtu03', '177.79.26.231', '2019-10-08', '09:25:29', 8, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 9; SM-N9600 Build/PPR1.180610.011; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.92 Mo', 'São Paulo', 'SP', 'BR', 'Linux armv8l', 'true'),
(761, 'djtidtu03', '177.79.24.38', '2019-10-08', '09:25:53', 8, 10, 2019, 'http://8pdev.studio/quem-somos', '8p estúdio de desenvolvimento - Empresa', 'Mozilla/5.0 (Linux; Android 9; SM-N9600 Build/PPR1.180610.011; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/77.0.3865.92 Mo', 'São Paulo', 'SP', 'BR', 'Linux armv8l', 'true'),
(762, '49nbnw9oz', '201.49.82.60', '2019-10-08', '09:31:57', 8, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_4_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1.2 Mobile/15E148 Safari/604.1', 'São José do Rio Preto', 'SP', 'BR', 'iPhone', 'true'),
(763, 'ac85904u3', '192.168.0.99, 192.168.0.99, 127.0.0.1', '2019-10-08', '09:36:44', 8, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'Houston', '', '', 'Win32', 'false'),
(764, 'pxc6twtct', '189.78.216.122', '2019-10-08', '12:20:39', 8, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Linux; Android 8.0.0; ASUS_Z01KD) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.111 Mobile Safari/537.36', 'São Paulo', 'BR', '14024', 'Linux armv8l', 'true'),
(765, '7lg1qsxxx', '191.19.52.68', '2019-10-08', '12:21:47', 8, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36', 'São Paulo', 'BR', '14024', 'Win32', 'false'),
(766, '2bnmycafo', '177.189.240.127', '2019-10-08', '12:40:19', 8, 10, 2019, 'http://8pdev.studio/', '8p estúdio de desenvolvimento - site, aplicativo, sistema, ux/ui design', 'Mozilla/5.0 (iPhone; CPU iPhone OS 13_1_2 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.1 Mobile/15E148 Safari/604.1', 'Brazil', '14940-000', '', 'iPhone', 'true');

-- --------------------------------------------------------

--
-- Estrutura da tabela `area`
--

CREATE TABLE `area` (
  `id` int(11) NOT NULL,
  `titulo` varchar(60) NOT NULL,
  `slug` varchar(60) NOT NULL,
  `icone` varchar(24) NOT NULL,
  `controller` varchar(60) NOT NULL,
  `tipo` varchar(24) NOT NULL,
  `detalhes` text NOT NULL,
  `ordem` int(11) NOT NULL,
  `categoria` tinyint(1) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `area`
--

INSERT INTO `area` (`id`, `titulo`, `slug`, `icone`, `controller`, `tipo`, `detalhes`, `ordem`, `categoria`, `status`) VALUES
(19, 'Inicio', 'inicio', 'fe fe-home', 'inicio', '', '{\"plural\":\"Slides\",\"singular\":\"Slide\",\"imagem\":\"1\",\"conteudo\":\"1\",\"lista\":\"1\",\"blocos\":\"\",\"ativa\":\"1\"}', 0, 0, 1),
(20, 'Empresa', 'empresa', 'fe fe-award', 'empresa', '', '{\"plural\":\"\",\"singular\":\"\",\"imagem\":0,\"conteudo\":\"1\",\"lista\":\"\",\"blocos\":\"\",\"ativa\":\"\"}', 2, 0, 1),
(21, 'Serviços', 'servicos', 'fe fe-briefcase', 'servicos', '', '{\"plural\":\"Servi\\u00e7os\",\"singular\":\"Servi\\u00e7o\",\"imagem\":\"1\",\"conteudo\":\"1\",\"lista\":\"1\",\"blocos\":\"\",\"ativa\":\"1\"}', 3, 0, 1),
(23, 'Contato', 'contato', 'fe fe-phone-call', 'contato', '', '{\"plural\":\"\",\"singular\":\"\",\"imagem\":0,\"conteudo\":\"1\",\"lista\":\"\",\"blocos\":\"\",\"ativa\":\"1\"}', 6, 0, 1),
(25, 'Depoimentos', 'depoimentos', 'fe fe-award', 'depoimentos', '', '{\"plural\":\"Depoimentos\",\"singular\":\"Depoimento\",\"imagem\":\"1\",\"conteudo\":0,\"lista\":\"1\",\"blocos\":\"\",\"ativa\":\"1\"}', 4, 0, 1),
(27, 'Blog', 'blog', 'fe fe-list', 'blog', '', '{\"plural\":\"Not\\u00edcias\",\"singular\":\"Not\\u00edcia\",\"imagem\":\"1\",\"conteudo\":\"1\",\"lista\":\"1\",\"blocos\":\"1\",\"ativa\":\"1\"}', 5, 0, 1),
(30, 'Portfólio', 'portfolio', 'fe fe-book-open', 'portfolio', '', '{\"plural\":\"projetos\",\"singular\":\"projeto\",\"imagem\":\"1\",\"conteudo\":\"1\",\"lista\":\"1\",\"blocos\":\"\",\"ativa\":\"1\"}', 1, 1, 1),
(31, 'Propostas', 'propostas', 'fe fe-file-text', 'proposta', '', '{\"plural\":\"Propostas\",\"singular\":\"Proposta\",\"imagem\":\"1\",\"conteudo\":0,\"lista\":\"1\",\"blocos\":\"\",\"ativa\":\"1\"}', 7, 0, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `pai` int(11) NOT NULL,
  `cod_area` int(11) NOT NULL,
  `titulo` varchar(60) NOT NULL,
  `slug` varchar(60) NOT NULL,
  `conteudo` text NOT NULL,
  `controller` varchar(24) NOT NULL,
  `criado_em` date NOT NULL,
  `modificado_em` date NOT NULL,
  `ordem` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `lixeira` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`id`, `pai`, `cod_area`, `titulo`, `slug`, `conteudo`, `controller`, `criado_em`, `modificado_em`, `ordem`, `status`, `lixeira`) VALUES
(1, 0, 30, 'Site', 'site', 'null', 'portfolio_categorias', '2019-09-16', '2019-09-16', 0, 1, 0),
(6, 0, 30, 'Identidade Visual', 'identidade_visual', 'null', 'portfolio_categorias', '2019-09-16', '2019-09-16', 0, 1, 0),
(7, 0, 30, 'Aplicativo', 'aplicativo', 'null', 'portfolio_categorias', '2019-09-16', '2019-09-16', 0, 1, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias_rel`
--

CREATE TABLE `categorias_rel` (
  `id` int(11) NOT NULL,
  `cod_item` int(11) NOT NULL,
  `cod_categoria` int(11) NOT NULL,
  `cod_area` int(11) NOT NULL,
  `tipo` varchar(24) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `lixeira` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `categorias_rel`
--

INSERT INTO `categorias_rel` (`id`, `cod_item`, `cod_categoria`, `cod_area`, `tipo`, `status`, `lixeira`) VALUES
(4, 136, 2, 21, 'item', 1, 0),
(5, 137, 2, 21, 'item', 1, 0),
(6, 138, 2, 21, 'item', 1, 0),
(7, 176, 3, 21, 'item', 1, 0),
(27, 186, 1, 30, 'item', 1, 0),
(29, 184, 1, 30, 'item', 1, 0),
(45, 227, 1, 30, 'item', 1, 0),
(46, 226, 1, 30, 'item', 1, 0),
(47, 225, 1, 30, 'item', 1, 0),
(48, 224, 1, 30, 'item', 1, 0),
(50, 185, 1, 30, 'item', 1, 0),
(51, 188, 1, 30, 'item', 1, 0),
(52, 187, 1, 30, 'item', 1, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `configuracoes`
--

CREATE TABLE `configuracoes` (
  `id` int(11) NOT NULL,
  `titulo` varchar(60) NOT NULL,
  `descricao` varchar(240) NOT NULL,
  `tags` text NOT NULL,
  `logo` int(11) NOT NULL,
  `icone` int(11) NOT NULL,
  `img` int(11) NOT NULL,
  `css` text NOT NULL,
  `script` text NOT NULL,
  `sociais` text NOT NULL,
  `navegacao` text NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `configuracoes`
--

INSERT INTO `configuracoes` (`id`, `titulo`, `descricao`, `tags`, `logo`, `icone`, `img`, `css`, `script`, `sociais`, `navegacao`, `status`) VALUES
(1, '8p estúdio de desenvolvimento', 'Estúdio de desenvolvimento web de São José do Rio Preto com anos de experiência em criação de sites, design, criação de aplicativos, criação de logomarca e um profundo conhecimento dos processos internos de negócios que nos capacitam a dese', 'criação de site, programação, UX/UI design, criação de aplicativos, branding, desenvolvimento web, desenvolvimento de sites, criação de sites,  SEO, criação de aplicativos, criação de sites em rio preto, criação de site em rio preto, criação de site em rio preto, criação de sites, desenvolvimento de sites, criação de site, desenvolvimento de site, marketing digital, quanto custa um site, construção de site, construção de sites, site em rio preto, sites em rio preto, são josé do rio preto, site barato, sites baratos, webdesign, site no brasil, sites no brasil, anúncio no google, criação de logo, criação de logos, sites em rio preto,  ecommerce, landing page', 106, 51, 190, '', '<!-- Global site tag (gtag.js) - Google Analytics -->\r\n<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-135269765-1\"></script>\r\n<script>\r\n  window.dataLayer = window.dataLayer || [];\r\n  function gtag(){dataLayer.push(arguments);}\r\n  gtag(\'js\', new Date());\r\n\r\n  gtag(\'config\', \'UA-135269765-1\');\r\n</script>', '[{\"plataforma\":\"facebook\",\"link\":\"https:\\/\\/www.facebook.com\\/8pdev\\/\"},{\"plataforma\":\"instagram\",\"link\":\"https:\\/\\/www.instagram.com\\/8pdev\\/\"}]', '{\"menu_principal\":{\"titulo\":\"Menu principal\",\"status\":\"0\",\"itens\":[]}}', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `conteudo`
--

CREATE TABLE `conteudo` (
  `id` int(11) NOT NULL,
  `cod_pai` int(11) NOT NULL,
  `cod_usuario` int(11) NOT NULL,
  `titulo` varchar(280) NOT NULL,
  `slug` varchar(280) NOT NULL,
  `cod_area` int(11) NOT NULL,
  `controller` varchar(24) NOT NULL,
  `conteudo` text NOT NULL,
  `postagem_json` text NOT NULL,
  `img` int(11) NOT NULL,
  `tipo` varchar(60) NOT NULL,
  `criado_em` datetime NOT NULL,
  `modificado_em` datetime NOT NULL,
  `ordem` int(11) NOT NULL,
  `destaque` tinyint(4) NOT NULL,
  `views` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `lixeira` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `conteudo`
--

INSERT INTO `conteudo` (`id`, `cod_pai`, `cod_usuario`, `titulo`, `slug`, `cod_area`, `controller`, `conteudo`, `postagem_json`, `img`, `tipo`, `criado_em`, `modificado_em`, `ordem`, `destaque`, `views`, `status`, `lixeira`) VALUES
(106, 0, 0, 'site,  aplicativo, sistema, ux/ui design', 'site_aplicativo_sistema_uxui_design', 19, 'inicio', '{\"chamada\":\"Est\\u00fadio de cria\\u00e7\\u00e3o web\",\"texto_base_dos_termos_alternando\":\"Desenvolvemos:\",\"texto_curto\":\"Deixe seu contato e iremos te ajudar com todo nosso conhecimento em tecnologia e design.\",\"tituloservicos\":\"o que [entregamos para voc\\u00ea]\",\"chamadaservicos\":\"Usamos t\\u00e9cnicas de ux e ui design com tenologias de ponta para desenvolver uma aplica\\u00e7\\u00e3o que  realmente atenda sua necessidade, al\\u00e9m de ser bonito, claro!\",\"tituloportfolio\":\"Conhe\\u00e7a [nosso trabalho]\",\"chamadaportfolio\":\"\",\"tituloquemsomos\":\"A [8p]\",\"subtitulo\":\"\\\"Somos uma equipe<br>apaixonada por tecnologia.\\\"\",\"chamadaquemsomos\":\"<p>Com o tempo de experi\\u00eancia que temos na \\u00e1rea de<strong> cria\\u00e7\\u00e3o de sites<\\/strong>, <strong>aplicativos<\\/strong> e <strong>sistemas web<\\/strong>, vimos muitos trabalhos realizados sem o cuidado t\\u00e9cnico necess\\u00e1rio para se considerar algo minimamente bom, seja em programa\\u00e7\\u00e3o ou design.<\\/p><p>Por esse motivo, a<strong> 8pdev studio<\\/strong> foi formada por uma equipe capaz para entregar o que h\\u00e1 de mais atual e competente na \\u00e1rea de tecnologia.<\\/p>\",\"foto_quem_somos_1\":\"169\",\"foto_quem_somos_2\":\"170\",\"foto_quem_somos_3\":\"169\",\"foto_quem_somos_4\":\"171\",\"tituloblog\":\"Nosso [Blog]\",\"chamadablog\":\"\"}', '', 49, 'pagina', '2019-06-30 21:39:00', '2019-10-06 23:03:00', 0, 0, 0, 1, 0),
(107, 0, 0, 'Empresa', 'empresa', 20, 'empresa', '{\"chamada_empresa\":\"Est\\u00fadio de cria\\u00e7\\u00e3o e desenvolvimento.\",\"subtitulo_empresa\":\"programa\\u00e7\\u00e3o, UX\\/UI design, aplicativos, branding\",\"titulo_do_texto\":\"\\\"Somos uma equipe apaixonada por tecnologia.\\\"\",\"texto_empresa\":\"<p>A <strong>8P<\\/strong> \\u00e9 um est\\u00fadio de <strong>cria\\u00e7\\u00e3o de sites<\\/strong>, <strong>aplicativos <\\/strong>e <strong>sistemas web<\\/strong> que atende <strong>S\\u00e3o Jos\\u00e9 do Rio Preto<\\/strong> e regi\\u00e3o. Somos uma equipe apaixonada por tecnologia com especialistas de primeira linha capazes de enfrentar qualquer desafio na \\u00e1rea do desenvolvimento web.<\\/p><p>Adoramos o que fazemos, e o que realmente nos diferencia&nbsp;s\\u00e3o anos de experi\\u00eancia em programa\\u00e7\\u00e3o, design, marketing, branding e um profundo conhecimento dos processos internos de neg\\u00f3cios que nos capacitam a desenvolver produtos exatamente como deveriam. N\\u00f3s nunca seguimos o caminho mais f\\u00e1cil.<\\/p><p>Acompanhamos o tempo-agora, sempre aderindo \\u00e0s mais recentes tend\\u00eancias em design e desenvolvimento.<\\/p>\"}', '', 0, 'pagina', '2019-06-30 21:39:00', '2019-09-28 02:24:00', 0, 0, 0, 1, 0),
(108, 0, 0, 'Serviços', 'servicos', 21, 'servicos', '{\"chamada_servicos\":\"Cria\\u00e7\\u00e3o de sites,<br> aplicativos e mais...\",\"subtitulo_servicos\":\"UX\\/UI Design, Front-end, Back-end, E-commerce, Landing Page\",\"titulo_extra\":\"Outros servi\\u00e7os [que oferecemos!]\"}', '', 0, 'pagina', '2019-06-30 21:39:00', '2019-09-28 12:43:00', 0, 0, 0, 1, 0),
(110, 0, 0, 'Contato', 'contato', 23, 'contato', '{\"chamada_contato\":\"Envie sua mensagem.\",\"subtitulo_contato\":\"Tire suas d\\u00favidas ou fa\\u00e7a sugest\\u00f5es\",\"telefones\":\"(17) 3234-1426 |  (17) 98120-2314\",\"email\":\"oie@8pdev.studio\",\"endereco\":\"Rua Minas Gerais, 374, Vila Bom Jesus\\nCEP 15014-210\",\"cidade\":\"S\\u00e3o Jos\\u00e9 do Rio Preto | SP\",\"resumo_contato\":\"seg a sex das 10:00 \\u00e0s 18:00\\ns\\u00e1b das 10:00 \\u00e0s 14:00\"}', '', 0, 'pagina', '2019-06-30 21:40:00', '2019-09-27 00:31:00', 0, 0, 0, 1, 0),
(121, 0, 0, 'Depoimentos', 'depoimentos', 25, 'depoimentos', '{\"chamada_depoimentos\":\"O que dizem\",\"subtitulo_depoimentos\":\"Lorem ipsum dolor sit amet, consectetur\"}', '', 68, 'pagina', '2019-07-03 00:45:00', '2019-09-16 19:12:00', 0, 0, 0, 1, 0),
(136, 0, 1, 'Sistemas web', 'sistemas_web', 21, 'servicos', '{\"resumo_servico\":\"Uma nova maneira de organizar processos e minimizar custos da gest\\u00e3o dos neg\\u00f3cios.\",\"resumo_longo_servico\":\"<p>\\u00c9 muito comum que empresas possuam necessidades espec\\u00edficas, isso por que cada neg\\u00f3cio funciona de maneiras diferentes. Existem v\\u00e1rias solu\\u00e7\\u00f5es de<strong> sistemas web<\\/strong> para auxiliar nos processos de uma empresa, por\\u00e9m, nem sempre o <strong>software <\\/strong>traz as melhorias esperadas ou trabalha da forma que empresa est\\u00e1 habituada.<\\/p><p>Sendo assim, o <strong>desenvolvimento de sistema<\\/strong> sob medida, criado para atender as demandas e m\\u00e9todos espec\\u00edficos, pode ser a op\\u00e7\\u00e3o ideal para realmente automatizar suas tarefas e aperfei\\u00e7oar o m\\u00e9todo que sua empresa trabalha, tendo impacto consider\\u00e1vel na rotina de seus colaboradores.<\\/p>\",\"imagem_destaque\":\"131\"}', '', 131, 'item', '2019-08-05 20:34:00', '2019-09-27 13:51:00', 0, 1, 0, 1, 0),
(137, 0, 1, 'Site Catálogo', 'site_catalogo', 21, 'servicos', '{\"resumo_servico\":\"O formato de site que te possibilita descrever cada produto. \\u00d3timo para quem receber or\\u00e7amentos.\",\"resumo_longo_servico\":\"<p>Ao <strong>fazer um site<\\/strong> mesmo que simples, \\u00e9 preciso planejamento para que ele atenda a requisitos comerciais espec\\u00edficos. Com o <strong>site <\\/strong>do tipo <strong>cat\\u00e1logo <\\/strong>n\\u00e3o \\u00e9 diferente, pois aqui tamb\\u00e9m s\\u00e3o aplicadas as as mesmas caracter\\u00edsticas que formam uma<strong> loja online<\\/strong>.<\\/p><p>S\\u00e3o f\\u00e1ceis de navegar, r\\u00e1pidos de carregar, f\\u00e1ceis de rastrear, se integram perfeitamente a outros canais de neg\\u00f3cios existentes e possui <strong>ferramenta <\\/strong>de pedido de or\\u00e7amento integrado, tudo para proporcionar uma experi\\u00eancia eficiente ao seu cliente. A \\u00fanica diferen\\u00e7a fica para a quest\\u00e3o de fechamento e pagamento do produto\\/servi\\u00e7o, j\\u00e1 que neste caso esta etapa \\u00e9 feita por parte da empresa.<\\/p>\",\"imagem_destaque\":\"124\"}', '', 124, 'item', '2019-08-05 20:35:00', '2019-09-29 01:30:00', 0, 1, 0, 1, 0),
(138, 0, 1, 'Aplicativos', 'aplicativos', 21, 'servicos', '{\"resumo_servico\":\"Para a cria\\u00e7\\u00e3o de aplicativos \\u00e9 essencial buscar especialistas com experi\\u00eancia, t\\u00e9cnica e suporte necess\\u00e1rio.\",\"resumo_longo_servico\":\"<p>Para a <strong>cria\\u00e7\\u00e3o de aplicativos<\\/strong>, \\u00e9 importante que sua ideia possua um prop\\u00f3sito definido e que a utiliza\\u00e7\\u00e3o traga benef\\u00edcios ao usu\\u00e1rio e o incentive a utilizar. Para isso, na hora de escolher quem vai desenvolver e estruturar seu projeto \\u00e9 essencial buscar experi\\u00eancia, suporte e <strong>design<\\/strong>.<\\/p><p>N\\u00e3o \\u00e9 atoa que as <strong>aplica\\u00e7\\u00f5es <\\/strong>mais presentes nos <strong>Smartfones <\\/strong>s\\u00e3o as que possuem um \\u00f3timo desempenho no aparelho e oferecem aos usu\\u00e1rios uma experi\\u00eancia \\u00fanica. Tudo isso envolve muito estudo, planejamento e <strong>profissionais <\\/strong>competentes.<\\/p>\",\"imagem_destaque\":\"133\"}', '', 133, 'item', '2019-08-05 20:38:00', '2019-09-27 02:17:00', 0, 1, 0, 1, 0),
(143, 0, 0, 'Blog', 'blog', 27, 'blog', '{\"chamada_blog\":\"\\u00daltimas not\\u00edcias\",\"subtitulo_blog\":\"Lorem ipsum dolor sit amet, consectetur\"}', '', 0, 'pagina', '2019-08-17 11:59:00', '2019-08-29 17:02:00', 0, 0, 0, 1, 0),
(147, 0, 1, 'O que é e como fazer empréstimo consignado', 'o_que_e_e_como_fazer_emprestimo_consignado', 27, 'blog', '{\"resumo\":\"Tire suas d\\u00favidas sobre o empr\\u00e9stimo consignado e descubra se ele \\u00e9 mesmo a melhor op\\u00e7\\u00e3o para voc\\u00ea.\",\"tags\":\"empr\\u00e9stimos, financiamento, empr\\u00e9stimo consignado\"}', '[{\"id\":0,\"type\":\"text\",\"content\":\"<p>Você que precisa de dinheiro para pagar dívidas ou realizar sonhos, sabe o que é e como fazer empréstimo consignado?<br></p><p class=\\\"\\\">Caracterizado por ter sua parcela descontada diretamente da folha de pagamento, o empréstimo consignado é uma das linhas de crédito que mais cresce no Brasil, especialmente entre os aposentados. Facilidade na contratação e taxas de juros mais em conta estão entre as principais vantagens da modalidade. Já o valor liberado para empréstimo, prazo para pagamento, assim como os pré-requisitos necessários para aprovação, são alguns dos fatores que devem ser avaliados em comparação com outros tipos de crédito. A seguir, explicamos direitinho como ele funciona!</p>\"},{\"id\":1,\"type\":\"img\",\"content\":\"<img class=\\\"img-line img-block-0\\\" src=\\\"http://localhost/finnance/upload/2019/08/servicoscredito.png\\\">\"},{\"id\":2,\"type\":\"text\",\"content\":\"<p class=\\\"\\\">Independente da modalidade, contratar um empréstimo é assumir o compromisso de pagar as parcelas sempre em dia. Por isso, especialistas recomendam que as <b>parcelas</b> do empréstimo sejam pagas assim que o seu salário cair na conta. Mas, a gente sabe que, normalmente, não é assim que acontece na prática, não é mesmo? E, por isso, o número de inadimplentes acaba crescendo. Afinal, nada garante que vai sobrar <a href=\\\"www.google.com\\\">dinheiro</a> até o fim do mês!</p><p class=\\\"\\\">No caso do empréstimo consignado, é diferente. Como ele é feito por meio de parcerias entre bancos e órgãos públicos ou instituições financeiras e empresas do setor privado, é possível que o valor das parcelas seja descontado <i>diretamente</i> da folha de pagamento, seja ela referente ao salário ou a um <u>benefício</u>, como INSS. </p><p class=\\\"\\\">Assim, uma vez que o risco de a dívida não ser quitada pelo cliente é menor, as empresas costumam oferecer juros mais baixos para essa modalidade de crédito. No entanto, existem algumas regras para fazer o empréstimo consignado que podem dificultar o acesso a ele, começando pelos pré-requisitos para a realização do pedido!</p>\"}]', 36, 'item', '2019-08-17 13:49:00', '2019-08-23 19:32:00', 0, 1, 56, 1, 0),
(170, 0, 1, 'Vitae Lorem ipsum', 'vitae_lorem_ipsum', 27, 'blog', '{\"resumo\":\"Lorem ipsum dolor sit amet, consecte adipiscing elit. Vivamus tincidunt, nunc pharetra consectetur lacus dui.\",\"tags\":\"\"}', '[{\"id\":0,\"type\":\"text\",\"content\":\"<p>Lorem ipsum dolor sit amet, consecte adipiscing elit. Vivamus tincidunt, nunc pharetra consectetur lacus dui.&nbsp;Lorem ipsum dolor sit amet, consecte adipiscing elit. Vivamus tincidunt, nunc pharetra consectetur lacus dui.&nbsp;Lorem ipsum dolor sit amet, consecte adipiscing elit. Vivamus tincidunt, nunc pharetra consectetur lacus dui.</p>\"},{\"id\":1,\"type\":\"img\",\"content\":\"<img class=\\\"img-line img-block-0\\\" src=\\\"http://localhost/finnance/upload/2019/08/quemsomos.jpg\\\">\"},{\"id\":2,\"type\":\"text\",\"content\":\"<p>Lorem ipsum dolor sit amet, consecte adipiscing elit. Vivamus tincidunt, nunc pharetra consectetur lacus dui.&nbsp;Lorem ipsum dolor sit amet, consecte adipiscing elit. Vivamus tincidunt, nunc pharetra consectetur lacus dui. Lorem ipsum dolor sit amet, consecte adipiscing elit. Vivamus tincidunt, nunc pharetra consectetur lacus dui.</p>\"}]', 37, 'item', '2019-08-30 13:24:00', '2019-08-30 13:25:00', 0, 0, 4, 1, 0),
(174, 0, 0, 'Portfólio', 'portfolio', 30, 'portfolio', '{\"chamada_portfolio\":\"Trabalhos recentes.\",\"subtitulo_portfolio\":\"Um pouco das nossas cria\\u00e7\\u00f5es\"}', '', 0, 'pagina', '2019-09-16 14:58:00', '2019-09-28 12:43:00', 0, 0, 0, 1, 0),
(176, 0, 1, 'Site Institucional', 'site_institucional', 21, 'servicos', '{\"resumo_servico\":\"A forma mais simples e essencial da sua empresa transmitir autoridade e estar no mercado-alvo.\",\"resumo_longo_servico\":\"<p>A <strong>cria\\u00e7\\u00e3o de um site<\\/strong> \\u00e9 o primeiro e mais importante passo para estar na internet, <strong>transmitir autoridade<\\/strong> e estar em todos os lugares. N\\u00e3o \\u00e9 s\\u00f3 essencial, mas sim algo obrigat\\u00f3rio para qualquer neg\\u00f3cio.<\\/p><p>Nossa op\\u00e7\\u00e3o de<strong> site institucional <\\/strong>\\u00e9 a forma mais b\\u00e1sica para voc\\u00ea fincar sua bandeira no mundo online. Seguimos regras b\\u00e1sicas de <strong>SEO<\\/strong> que v\\u00e3o permitir o seu neg\\u00f3cio disputar, com vantagem, espa\\u00e7o nas ferramentas de busca mais utilizadas. Al\\u00e9m de aplicarmos estrategicamente<strong> t\\u00e9cnicas de design<\\/strong> que permite ao seu usu\\u00e1rio usufruir da sua aplica\\u00e7\\u00e3o da melhor forma.<\\/p>\",\"imagem_destaque\":\"128\"}', '', 128, 'item', '2019-09-16 16:42:00', '2019-09-27 13:52:00', 0, 1, 0, 1, 0),
(178, 0, 1, 'Maniele Dazetti', 'maniele_dazetti', 25, 'depoimentos', '{\"empresa\":\"Google\",\"texto\":\"<p>muito booooooooooooom bom mesmo amei nossa adorei muito massa se eu pudesse eu faria tamb\\u00e9m mas sabe como \\u00e9 n\\u00e9 enfim gente muito bom recomendo o bagulho \\u00e9 top demais 10 de 10 vai na f\\u00e9 que o neg\\u00f3cio vem<\\/p>\"}', '', 65, 'item', '2019-09-16 18:59:00', '2019-09-16 18:59:00', 0, 1, 0, 1, 0),
(179, 0, 1, 'Renan Breno', 'renan_breno', 25, 'depoimentos', '{\"empresa\":\"Facebook\",\"texto\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut a mollis lorem. Mauris lorem tortor, imperdiet sit amet mi egetrutrum elementum elit. Vivamus id libero diam. Maecenas variusante eu libero sollicitudin condimentum. Proin faucibus odioin.<\\/p>\"}', '', 66, 'item', '2019-09-16 19:00:00', '2019-09-16 19:00:00', 0, 1, 0, 1, 0),
(180, 0, 1, 'Elisa Austen', 'elisa_austen', 25, 'depoimentos', '{\"empresa\":\"Nubank\",\"texto\":\"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut a mollis lorem. Mauris lorem tortor, imperdiet sit amet mi egetrutrum elementum elit. Vivamus id libero diam. Maecenas variusante eu libero sollicitudin condimentum. Proin faucibus odioin.<\\/p>\"}', '', 67, 'item', '2019-09-16 19:02:00', '2019-09-16 19:02:00', 0, 1, 0, 1, 0),
(181, 107, 1, 'Rafael Nascimento', 'rafael_nascimento', 20, 'empresa', '{\"funcao_membro\":\"Back-end \\/ Programador\",\"resumo_membro\":\"\",\"facebook_membro\":\"https:\\/\\/www.facebook.com\\/rafael.nascimentodecarvalho.5\",\"instagram_membro\":\"https:\\/\\/www.instagram.com\\/rafael_btk\\/\",\"twitter_membro\":\"https:\\/\\/twitter.com\\/Billythekiid\",\"perfil_membro\":\"184\"}', '', 0, 'equipe', '2019-09-21 20:45:00', '2019-09-30 19:34:00', 1, 0, 0, 1, 0),
(184, 0, 1, 'Finnance web site', 'finnance_web_site', 30, 'portfolio', '{\"texto\":\"Site desenvolvido para a empresa de cr\\u00e9dito Finnance. Integrado ao nosso painel de gerenciamento de conte\\u00fado o projeto disp\\u00f5e de gest\\u00e3o de formul\\u00e1rios para capta\\u00e7\\u00e3o, controle de leads e landing page com desgin direcionado para prospec\\u00e7\\u00e3o.\",\"galeria\":\"[112,113]\",\"cliente\":\"Finnance\",\"link\":\"http:\\/\\/finnance.com.br\",\"tecnologias\":\"Codeigniter php, Adobe XD, Bootstrap4, jQuery, javascript, CSS3, HTML5\",\"tags_seo\":\"Finnance, Cria\\u00e7\\u00e3o de site, Codeigniter php, Adobe XD, Bootstrap4, jQuery, javascript, CSS3, HTML5\"}', '', 117, 'item', '2019-09-23 23:36:00', '2019-09-24 19:22:00', 0, 1, 0, 1, 0),
(185, 0, 1, 'BoxSupreme web site', 'boxsupreme_web_site', 30, 'portfolio', '{\"texto\":\"Site institucional desenvolvido para a empresa BoxSupreme Solu\\u00e7\\u00f5es Automotivas. Integrado ao framework Codeigniter, o projeto disp\\u00f5e de um design atual e com layout compat\\u00edvel a todos os dispositivos.\",\"galeria\":\"[122]\",\"cliente\":\"BoxSupreme\",\"link\":\"http:\\/\\/boxsupreme.com.br\",\"tecnologias\":\"Codeigniter php, Adobe XD, Bootstrap4, jQuery, javascript, CSS3, HTML5\",\"tags_seo\":\"BoxSupreme, Cria\\u00e7\\u00e3o de site, Codeigniter php, Adobe XD, Bootstrap4, jQuery, javascript, CSS3, HTML5\"}', '', 123, 'item', '2019-09-23 23:49:00', '2019-09-29 00:58:00', 0, 1, 0, 1, 0),
(186, 0, 1, 'Epics Landing Page', 'epics_landing_page', 30, 'portfolio', '{\"texto\":\"Sistema de cria\\u00e7\\u00e3o de propostas para convers\\u00e3o de poss\\u00edveis clientes, desenvolvido para a empresa de softwares web para fot\\u00f3grafos Epics. O projeto conta com integra\\u00e7\\u00e3o ao ERP da empresa, painel de gerenciamento de conte\\u00fado com tradu\\u00e7\\u00e3o e gest\\u00e3o de landing pages.\",\"galeria\":\"[114]\",\"cliente\":\"Epics\",\"link\":\"\",\"tecnologias\":\"Codeigniter php, Adobe XD, Bootstrap4, jQuery, javascript, CSS3, HTML5\",\"tags_seo\":\"Epics, Landing Page, Cria\\u00e7\\u00e3o de site, Codeigniter php, Adobe XD, Bootstrap4, jQuery, javascript, CSS3, HTML5, Cria\\u00e7\\u00e3o de Landing Page\"}', '', 115, 'item', '2019-09-24 00:02:00', '2019-09-24 19:22:00', 0, 1, 0, 1, 0),
(187, 0, 1, 'JS Menu web site', 'js_menu_web_site', 30, 'portfolio', '{\"texto\":\"Site desenvolvimento para a empresa de menu board JS Menu. Com layout institucional e design focado em argumentos de venda e informa\\u00e7\\u00f5es sobre os servi\\u00e7os prestados pela empresa.\",\"galeria\":\"[118]\",\"cliente\":\"JS Menu\",\"link\":\"\",\"tecnologias\":\"Codeigniter php, Adobe XD, Bootstrap4, jQuery, javascript, CSS3, HTML5\",\"tags_seo\":\"JS Menu, Landing Page, Cria\\u00e7\\u00e3o de site, Codeigniter php, Adobe XD, Bootstrap4, jQuery, javascript, CSS3, HTML5, Cria\\u00e7\\u00e3o de Landing Page\"}', '', 119, 'item', '2019-09-24 00:07:00', '2019-09-29 01:01:00', 0, 1, 0, 1, 0),
(188, 0, 1, 'OKAMED web site', 'okamed_web_site', 30, 'portfolio', '{\"texto\":\"Site institucional para a empresa OKAMED Tecnologia Hospitalar. Com layout atual e compat\\u00edvel a todos os dispositivos, junto a um design focado em mostrar os servi\\u00e7os prestados pela empresa. O projeto foi integrado ao nosso painel de gerenciamento de conte\\u00fado e conta tamb\\u00e9m com nossa ferramenta de or\\u00e7amento.\",\"galeria\":\"[120]\",\"cliente\":\"Okamed\",\"link\":\"http:\\/\\/www.okamed.com.br\\/\",\"tecnologias\":\"Codeigniter php, Adobe XD, Bootstrap4, jQuery, javascript, CSS3, HTML5\",\"tags_seo\":\"OKAMED, Cria\\u00e7\\u00e3o de site, Codeigniter php, Adobe XD, Bootstrap4, jQuery, javascript, CSS3, HTML5\"}', '', 121, 'item', '2019-09-24 00:13:00', '2019-09-29 01:01:00', 0, 1, 0, 1, 0),
(189, 106, 1, 'Site Institucional', 'site_institucional1', 19, 'inicio', 'null', '', 0, 'termos_alternando', '2019-09-24 13:41:00', '2019-09-27 02:14:00', -1, 0, 0, 1, 0),
(190, 106, 1, 'Sistemas web', 'sistemas_web1', 19, 'inicio', 'null', '', 0, 'termos_alternando', '2019-09-24 13:41:00', '2019-09-27 02:18:00', -1, 0, 0, 1, 0),
(191, 106, 1, 'Site Catálogo', 'site_catalogo1', 19, 'inicio', 'null', '', 0, 'termos_alternando', '2019-09-24 13:41:00', '2019-09-27 02:18:00', -1, 0, 0, 1, 0),
(193, 0, 1, 'E-commerce', 'e_commerce', 21, 'servicos', '{\"resumo_servico\":\"Uma alternativa para quem n\\u00e3o quer ter os custos de uma loja f\\u00edsica, utilizando a tecnologia para empreender.\",\"resumo_longo_servico\":\"<p>Para<strong> criar uma loja virtual<\\/strong> \\u00e9 preciso experi\\u00eancia e tempo de <strong>mercado.<\\/strong> N\\u00f3s da 8pdev temos o conhecimento necess\\u00e1rio para desenvolver seu<strong> e-commerce<\\/strong> utilizando das principais t\\u00e9cnicas, <strong>tecnologias <\\/strong>e <strong>plataformas de com\\u00e9rcio eletr\\u00f4nico<\\/strong> do mercado.<\\/p><p>Nossa <strong>equipe <\\/strong>possui a capacidade de entregar a melhorar <strong>experi\\u00eancia <\\/strong>de usu\\u00e1rio ao seu cliente, transformando e aumentando as chances de cada acesso ao seu site se tornarem uma venda. Al\\u00e9m de darmos <strong>suporte <\\/strong>em iniciativas relacionadas \\u00e0 estrat\\u00e9gia de <strong>convers\\u00e3o<\\/strong>.<\\/p>\",\"imagem_destaque\":\"125\"}', '', 125, 'item', '2019-09-24 18:43:00', '2019-09-28 02:04:00', 0, 1, 0, 1, 0),
(194, 0, 1, 'Landing Page', 'landing_page', 21, 'servicos', '{\"resumo_servico\":\"Uma ferramenta indispens\\u00e1vel em marketing digital para a convers\\u00e3o de visitas visitantes em lead.\",\"resumo_longo_servico\":\"<p><strong>Criar uma Landing Page<\\/strong> \\u00e9 fundamentais para uma estrat\\u00e9gia de <strong>Marketing digital<\\/strong>. Por isso, o layout possui elementos <strong>estrat\\u00e9gicos <\\/strong>voltados \\u00e0 <strong>convers\\u00e3o <\\/strong>do visitante em <strong>Lead<\\/strong>. Criando assim a oportunidade de construir um relacionamento mais pr\\u00f3ximo com seu potencial cliente, mostrar a import\\u00e2ncia da sua empresa no mercado, aumentar a confian\\u00e7a na qualidade de seus produtos e servi\\u00e7os diante o <strong>p\\u00fablico<\\/strong>.&nbsp;<\\/p><p>Com um bom uso dessa ferramenta voc\\u00ea \\u00e9 recompensado com o impulso para realiza\\u00e7\\u00e3o de vendas e uma rede forte de clientes em potencial.<\\/p>\",\"imagem_destaque\":\"130\"}', '', 130, 'item', '2019-09-24 18:44:00', '2019-09-29 01:11:00', 0, 1, 0, 1, 0),
(199, 107, 1, 'Renan Breno', 'renan_breno1', 20, 'empresa', '{\"funcao_membro\":\"FRONT-END \\/ Programador\",\"resumo_membro\":\"\",\"facebook_membro\":\"https:\\/\\/www.facebook.com\\/RenanBreeno\",\"instagram_membro\":\"https:\\/\\/www.instagram.com\\/brenanreno\\/\",\"twitter_membro\":\"https:\\/\\/twitter.com\\/brenanreno\",\"perfil_membro\":\"183\"}', '', 0, 'equipe', '2019-09-25 16:33:00', '2019-09-30 19:44:00', 2, 0, 0, 1, 0),
(200, 107, 1, 'Taiane Campos', 'taiane_campos', 20, 'empresa', '{\"funcao_membro\":\"UX \\/ UI DESIGNER\",\"resumo_membro\":\"\",\"facebook_membro\":\"https:\\/\\/www.facebook.com\\/taianecampos\",\"instagram_membro\":\"https:\\/\\/www.instagram.com\\/taianecampos\\/\",\"twitter_membro\":\"https:\\/\\/twitter.com\\/seiqlaseiqtem\",\"perfil_membro\":\"185\"}', '', 0, 'equipe', '2019-09-25 16:33:00', '2019-09-30 19:34:00', 0, 0, 0, 1, 0),
(210, 106, 1, 'Aplicativos', 'aplicativos1', 19, 'inicio', 'null', '', 0, 'termos_alternando', '2019-09-27 02:18:00', '2019-09-27 02:18:00', -1, 0, 0, 1, 0),
(211, 106, 1, 'E-commerce', 'e_commerce1', 19, 'inicio', 'null', '', 0, 'termos_alternando', '2019-09-27 02:18:00', '2019-09-27 02:18:00', -1, 0, 0, 1, 0),
(212, 106, 1, 'Landing Page', 'landing_page1', 19, 'inicio', 'null', '', 0, 'termos_alternando', '2019-09-27 02:18:00', '2019-09-27 02:18:00', -1, 0, 0, 1, 0),
(213, 107, 1, 'Codeigniter', 'codeigniter1', 20, 'empresa', '{\"imagem_tecnologia\":\"159\"}', '', 0, 'tecnologias_sobre', '2019-09-27 20:16:00', '2019-09-27 20:16:00', -1, 0, 0, 1, 0),
(214, 107, 1, 'Bootstrap 4', 'bootstrap_41', 20, 'empresa', '{\"imagem_tecnologia\":\"167\"}', '', 0, 'tecnologias_sobre', '2019-09-27 20:16:00', '2019-09-27 20:16:00', -1, 0, 0, 1, 0),
(215, 107, 1, 'Adobe XD', 'adobe_xd1', 20, 'empresa', '{\"imagem_tecnologia\":\"161\"}', '', 0, 'tecnologias_sobre', '2019-09-27 20:16:00', '2019-09-27 20:16:00', -1, 0, 0, 1, 0),
(216, 107, 1, 'CSS3', 'css31', 20, 'empresa', '{\"imagem_tecnologia\":\"165\"}', '', 0, 'tecnologias_sobre', '2019-09-27 20:16:00', '2019-09-27 20:16:00', -1, 0, 0, 1, 0),
(217, 107, 1, 'HTML5', 'html51', 20, 'empresa', '{\"imagem_tecnologia\":\"166\"}', '', 0, 'tecnologias_sobre', '2019-09-27 20:16:00', '2019-09-27 20:16:00', -1, 0, 0, 1, 0),
(218, 107, 1, 'React Native', 'react_native1', 20, 'empresa', '{\"imagem_tecnologia\":\"158\"}', '', 0, 'tecnologias_sobre', '2019-09-27 20:17:00', '2019-09-27 20:17:00', -1, 0, 0, 1, 0),
(219, 107, 1, 'Git', 'git1', 20, 'empresa', '{\"imagem_tecnologia\":\"164\"}', '', 0, 'tecnologias_sobre', '2019-09-27 20:17:00', '2019-09-27 20:17:00', -1, 0, 0, 1, 0),
(220, 108, 1, 'Hospedagem de site', 'hospedagem_de_site', 21, 'servicos', '{\"servico_extra\":\"Ideal para quem tem ou quer ter um site mas n\\u00e3o achou o suporte necess\\u00e1rio pra mant\\u00ea-lo online.\",\"imagem_extra\":\"149\"}', '', 0, 'outros_servicos', '2019-09-27 20:19:00', '2019-09-27 20:19:00', -1, 0, 0, 1, 0),
(221, 108, 1, 'Consultoria UX/UI Design', 'consultoria_uxui_design', 21, 'servicos', '{\"servico_extra\":\"Direcionado a quem n\\u00e3o est\\u00e1 satisfeito com a est\\u00e9tica de algum projeto e necessita de um olhar t\\u00e9cnico.\",\"imagem_extra\":\"136\"}', '', 0, 'outros_servicos', '2019-09-27 20:19:00', '2019-09-27 20:19:00', -1, 0, 0, 1, 0),
(222, 108, 1, 'Consultoria SEO', 'consultoria_seo', 21, 'servicos', '{\"servico_extra\":\"Para quem precisa reestruturar uma aplica\\u00e7\\u00e3o para que siga regras b\\u00e1sicas e avan\\u00e7adas de SEO.\",\"imagem_extra\":\"137\"}', '', 0, 'outros_servicos', '2019-09-27 20:19:00', '2019-09-27 20:19:00', -1, 0, 0, 1, 0),
(223, 108, 1, 'Identidade visual', 'identidade_visual', 21, 'servicos', '{\"servico_extra\":\"Para quem precisa de uma identidade marcante, que passe autoridade e confian\\u00e7a no seguimento que pertence.\",\"imagem_extra\":\"151\"}', '', 0, 'outros_servicos', '2019-09-27 20:20:00', '2019-09-27 20:20:00', -1, 0, 0, 1, 0),
(224, 0, 1, 'Intera skateshop Loja Online', 'intera_skateshop_loja_online', 30, 'portfolio', '{\"texto\":\"\",\"galeria\":\"[174]\",\"cliente\":\"Intera\",\"link\":\"https:\\/\\/interaskate.com.br\\/\",\"tecnologias\":\"wocommerce, wordpress\",\"tags_seo\":\"wocommerce, wordpress, E-commerce, Cria\\u00e7\\u00e3o de site, Codeigniter php, Adobe XD, Bootstrap4, jQuery, javascript, CSS3, HTML5\"}', '', 175, 'item', '2019-08-01 12:47:00', '2019-09-28 21:15:00', 0, 0, 0, 1, 0),
(225, 0, 1, 'Mar Aberto Loja Online', 'mar_aberto_loja_online', 30, 'portfolio', '{\"texto\":\"\",\"galeria\":\"[177]\",\"cliente\":\"Mar Aberto\",\"link\":\"http:\\/\\/marabertoloja.com.br\\/\",\"tecnologias\":\"wocommerce, wordpress\",\"tags_seo\":\"wocommerce, wordpress, E-commerce, Cria\\u00e7\\u00e3o de site, Codeigniter php, Adobe XD, Bootstrap4, jQuery, javascript, CSS3, HTML5\"}', '', 176, 'item', '2019-08-01 13:28:00', '2019-09-28 21:15:00', 0, 0, 0, 1, 0),
(226, 0, 1, 'Casa Verde Interiores', 'casa_verde_interiores', 30, 'portfolio', '{\"texto\":\"\",\"galeria\":\"[179]\",\"cliente\":\"Casa Verde\",\"link\":\"\",\"tecnologias\":\"Codeigniter php, Adobe XD, Bootstrap4, jQuery, javascript, CSS3, HTML5\",\"tags_seo\":\"Casa Verde Interiores, Cria\\u00e7\\u00e3o de site, Codeigniter php, Adobe XD, Bootstrap4, jQuery, javascript, CSS3, HTML5\"}', '', 178, 'item', '2019-09-01 15:19:00', '2019-09-28 21:15:00', 0, 0, 0, 1, 0),
(227, 0, 1, 'Aparato Interiores site', 'aparato_interiores_site', 30, 'portfolio', '{\"texto\":\"\",\"galeria\":\"[181]\",\"cliente\":\"Aparato Interiores\",\"link\":\"\",\"tecnologias\":\"Codeigniter php, Adobe XD, Bootstrap4, jQuery, javascript, CSS3, HTML5\",\"tags_seo\":\"Aparato Interiores, Cria\\u00e7\\u00e3o de site, Codeigniter php, Adobe XD, Bootstrap4, jQuery, javascript, CSS3, HTML5\"}', '', 180, 'item', '2019-09-01 16:27:00', '2019-09-28 21:15:00', 0, 0, 0, 1, 0),
(228, 0, 1, 'OKAMED', 'okamed', 19, 'inicio', '{\"cor\":\"#0084fe\"}', '', 121, 'item', '2019-10-06 21:18:00', '2019-10-06 21:21:00', 0, 0, 0, 1, 0),
(229, 0, 1, 'Finnance', 'finnance', 19, 'inicio', '{\"cor\":\"#1b1b4b\"}', '', 117, 'item', '2019-10-06 21:38:00', '2019-10-06 21:39:00', 0, 0, 0, 1, 0),
(230, 0, 1, 'BoxSupreme', 'boxsupreme', 19, 'inicio', '{\"cor\":\"#f9b200\"}', '', 123, 'item', '2019-10-06 21:38:00', '2019-10-06 21:39:00', 0, 0, 0, 1, 0),
(231, 0, 0, 'Propostas', 'propostas', 31, '', '', '', 0, 'pagina', '2019-10-08 13:23:00', '2019-10-08 13:23:00', 0, 0, 0, 1, 0),
(233, 0, 1, 'Proposta Finance', 'proposta_finance', 31, 'proposta', '{\"cliente\":\"Finnance\",\"servico\":\"site institucional \\u2022 landing page\",\"texto_investimento\":\"<p>O projeto ser\\u00e1 iniciado logo ap\\u00f3s a aprova\\u00e7\\u00e3o deste or\\u00e7amento e o pagamento da primeira parcela do valor de investimento.<\\/p><p>Valor:&nbsp;<strong style=\\\"color: rgb(226, 73, 134);\\\">R$3.450,00<\\/strong><\\/p><p>1 entrada de R$1.150,00 + 2 parcelas de R$1.150,00<\\/p>\",\"texto_projeto\":\"<p>Planejamento, estrutura\\u00e7\\u00e3o e desenvolvimento de um website institucional e landing page, com listagem de planos, parceiros, d\\u00favidas frequentes, unidades e not\\u00edcias, simulador de cons\\u00f3rcio e sistema de gerenciamento de conte\\u00fado.<\\/p><p>O custo do projeto ser\\u00e1 inteiramente cotado baseado por hora de desenvolvimento. Abaixo seguem as sess\\u00f5es que formam o website.<\\/p><ul><li>Home<\\/li><li>Quem Somos&nbsp;(p\\u00e1gina interna)<\\/li><li>Planos&nbsp;(p\\u00e1gina de listagem, p\\u00e1gina interna)<\\/li><li>Parceiros&nbsp;(p\\u00e1gina de listagem, link externo)<\\/li><li>D\\u00favidas&nbsp;(p\\u00e1gina de listagem)<\\/li><li>Seja Franqueado&nbsp;(link para landing)<\\/li><li>Unidades&nbsp;(p\\u00e1gina de listagem, p\\u00e1gina interna estilo landing)<\\/li><li>Not\\u00edcias&nbsp;(p\\u00e1gina de listagem, p\\u00e1gina interna)<\\/li><li>Contato&nbsp;(p\\u00e1gina interna)<\\/li><\\/ul><p><br><\\/p><p><strong>* P\\u00e1gina interna:<\\/strong>&nbsp;P\\u00e1gina com T\\u00edtulo, sub-t\\u00edtulo, texto e imagem<\\/p><p><strong>* P\\u00e1gina de listagem:<\\/strong>&nbsp;P\\u00e1gina com imagens e t\\u00edtulos dos itens<\\/p><p><strong>* P\\u00e1gina interna estilo landing:<\\/strong>&nbsp;P\\u00e1gina focada em convers\\u00e3o, com formul\\u00e1rio e informa\\u00e7\\u00f5es importantes<\\/p><p>Sobre a landing page \\u00e9 importate salientar que ela possui:<\\/p><ul><li>Foco total em&nbsp;<strong>convers\\u00e3o<\\/strong>, seguindo um modelo bem testado.<\\/li><li>P\\u00e1gina de sucesso com op\\u00e7\\u00e3o do lead preencher novos formul\\u00e1rios para&nbsp;<strong>filtrar melhor a qualidade<\\/strong>&nbsp;dele.<\\/li><li>Configur\\u00e1vel por um painel gerenciador, capaz de editar informa\\u00e7\\u00f5es e&nbsp;<strong>criar testes A\\/B<\\/strong>&nbsp;com facilidade.<\\/li><\\/ul>\",\"texto_etapas\":\"<p><strong>1\\u00aa etapa:<\\/strong>&nbsp;Wireframe<\\/p><ul><li>Arquitetura de informa\\u00e7\\u00e3o: mapa do site e navega\\u00e7\\u00e3o;<\\/li><li>Diagrama estrutural de cada p\\u00e1gina de acordo com o mapa do site;<\\/li><li>Organiza\\u00e7\\u00e3o do conte\\u00fado (estrutura\\u00e7\\u00e3o dos n\\u00edveis hier\\u00e1rquicos de informa\\u00e7\\u00e3o).<\\/li><\\/ul><p><br><\\/p><p><strong>2\\u00aa etapa:<\\/strong>&nbsp;Design<\\/p><ul><li>Defini\\u00e7\\u00e3o de identidade do projeto;<\\/li><li>Desenho das p\\u00e1ginas e estruturas de navega\\u00e7\\u00e3o;<\\/li><li>Estabelecimento de navega\\u00e7\\u00e3o e usabilidade do front\\u2010end.<\\/li><\\/ul><p><br><\\/p><p><strong>3\\u00aa etapa:<\\/strong>&nbsp;Implementa\\u00e7\\u00e3o<\\/p><ul><li>Configura\\u00e7\\u00e3o de gestor de conte\\u00fado;<\\/li><li>Desenvolvimento dos elementos html de interface;<\\/li><li>Aplica\\u00e7\\u00e3o dos m\\u00f3dulos de conte\\u00fado.<\\/li><\\/ul><p><br><\\/p><p><strong>4\\u00aa etapa:<\\/strong>&nbsp;Testes<\\/p><ul><li>Aplica\\u00e7\\u00e3o e supervis\\u00e3o de testes de usabilidade e valida\\u00e7\\u00e3o;<\\/li><li>Publica\\u00e7\\u00e3o do site numa \\u00e1rea de testes para revis\\u00e3o online;<\\/li><li>Publica\\u00e7\\u00e3o definitiva.<\\/li><\\/ul>\",\"prazo\":\"\"}', '', 0, 'item', '2019-10-08 14:50:00', '2019-10-08 14:50:00', 0, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `custom`
--

CREATE TABLE `custom` (
  `id` int(11) NOT NULL,
  `cod_area` int(11) NOT NULL,
  `ordem` int(11) NOT NULL,
  `label` varchar(60) NOT NULL,
  `name` varchar(60) NOT NULL,
  `tipo` varchar(24) NOT NULL,
  `placeholder` varchar(60) NOT NULL,
  `ajuda` varchar(140) NOT NULL,
  `classe` varchar(60) NOT NULL,
  `min` int(11) NOT NULL,
  `max` int(11) NOT NULL,
  `controller` varchar(24) NOT NULL,
  `coluna` tinyint(2) NOT NULL,
  `relacionado` int(11) NOT NULL,
  `bloco` varchar(60) NOT NULL,
  `options` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `lixeira` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `custom`
--

INSERT INTO `custom` (`id`, `cod_area`, `ordem`, `label`, `name`, `tipo`, `placeholder`, `ajuda`, `classe`, `min`, `max`, `controller`, `coluna`, `relacionado`, `bloco`, `options`, `status`, `lixeira`) VALUES
(102, 19, 0, 'Chamada', 'chamada', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(106, 21, 1, 'Subtítulo', 'subtitulo_servicos', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(121, 20, 0, 'Chamada', 'chamada_empresa', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(122, 20, 3, 'Texto', 'texto_empresa', 'textarea', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(132, 23, 1, 'Subtítulo', 'subtitulo_contato', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(133, 23, 0, 'Chamada', 'chamada_contato', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(134, 23, 2, 'Informações', 'informacoes', 'divisao', '', '', '', 0, 0, 'pagina', 0, 0, 'main', '', 1, 0),
(135, 23, 3, 'Telefones', 'telefones', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(136, 23, 4, 'E-mail', 'email', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(137, 23, 5, 'Endereço', 'endereco', 'textarea', '', '', 'no-quill', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(138, 23, 6, 'cidade', 'cidade', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(142, 25, 1, 'Texto', 'texto', 'textarea', '', '', '', 0, 0, 'item', 12, 0, 'main', '', 1, 0),
(144, 21, 0, 'Chamada', 'chamada_servicos', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(193, 27, 0, 'Chamada', 'chamada_blog', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(194, 27, 1, 'Subtítulo', 'subtitulo_blog', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(196, 27, 0, 'Resumo', 'resumo', 'input', '', '', '', 0, 0, 'item', 12, 0, 'main', '', 1, 0),
(198, 27, 1, 'Tags', 'tags', 'input', '', 'Separados por vírgula', 'select2', 0, 0, 'item', 12, 0, 'sidebar', '', 1, 0),
(213, 27, 0, 'Título', 'label_titulo', 'input', '', '', '', 0, 0, 'item', 0, 0, 'main', '', 1, 0),
(216, 20, 1, 'Subtítulo', 'subtitulo_empresa', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(217, 20, 2, 'Título do texto', 'titulo_do_texto', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(253, 25, 0, 'Título', 'label_titulo', 'input', '', '', '', 0, 0, 'pagina', 0, 0, 'main', '', 1, 0),
(254, 25, 0, 'Chamada', 'chamada_depoimentos', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(255, 25, 1, 'Subtítulo', 'subtitulo_depoimentos', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(264, 27, 0, 'Título', 'label_titulo', 'input', '', '', '', 0, 0, 'pagina', 0, 0, 'main', '', 1, 0),
(269, 19, 1, 'Texto base dos Termos alternando', 'texto_base_dos_termos_alternando', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(271, 19, 2, 'Texto curto', 'texto_curto', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(273, 19, 3, 'Seção Serviços', 'secao_servicos', 'divisao', '', '', '', 0, 0, 'pagina', 0, 0, 'main', '', 1, 0),
(274, 19, 4, 'Título', 'tituloservicos', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(275, 19, 5, 'Chamada', 'chamadaservicos', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(276, 19, 6, 'Seção Portfólio', 'secao_portfolio', 'divisao', '', '', '', 0, 0, 'pagina', 0, 0, 'main', '', 1, 0),
(277, 19, 7, 'Título', 'tituloportfolio', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(278, 19, 8, 'Chamada', 'chamadaportfolio', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(280, 19, 9, 'Seção Quem Somos', 'secao_quem_somo', 'divisao', '', '', '', 0, 0, 'pagina', 0, 0, 'main', '', 1, 0),
(281, 19, 10, 'Título', 'tituloquemsomos', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(282, 19, 12, 'Chamada', 'chamadaquemsomos', 'textarea', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(284, 19, 17, 'Seção Blog', 'secao_blog', 'divisao', '', '', '', 0, 0, 'pagina', 0, 0, 'main', '', 1, 0),
(285, 19, 18, 'Título', 'tituloblog', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(286, 19, 19, 'Chamada', 'chamadablog', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(291, 19, 13, 'Foto Quem Somos 1', 'foto_quem_somos_1', 'thumb', '', '', '', 0, 0, 'pagina', 6, 0, 'main', '', 1, 0),
(292, 19, 14, 'Foto Quem Somos 2', 'foto_quem_somos_2', 'thumb', '', '', '', 0, 0, 'pagina', 6, 0, 'main', '', 1, 0),
(293, 19, 15, 'Foto Quem Somos 3', 'foto_quem_somos_3', 'thumb', '', '', '', 0, 0, 'pagina', 6, 0, 'main', '', 1, 0),
(294, 19, 16, 'Foto Quem Somos 4', 'foto_quem_somos_4', 'thumb', '', '', '', 0, 0, 'pagina', 6, 0, 'main', '', 1, 0),
(308, 21, 0, 'Subtítulo', 'subtitulo1', 'input', '', '', '', 0, 0, 'categorias', 12, 0, 'main', '', 1, 0),
(310, 21, 0, 'Título', 'label_titulo', 'input', '', '', '', 0, 0, 'categorias', 0, 0, 'main', '', 1, 0),
(311, 21, 1, 'Imagem', 'imagem', 'image', '', '', '', 0, 0, 'categorias', 12, 0, 'main', '', 1, 0),
(313, 30, 0, 'Texto', 'texto', 'textarea', '', '', 'no-quill', 0, 0, 'item', 12, 0, 'main', '', 1, 0),
(315, 30, 2, 'Informações', 'informacoes', 'divisao', '', '', '', 0, 0, 'item', 0, 0, 'main', '', 1, 0),
(316, 30, 3, 'Cliente', 'cliente', 'input', '', '', '', 0, 0, 'item', 6, 0, 'main', '', 1, 0),
(317, 30, 4, 'Link', 'link', 'input', '', '', '', 0, 0, 'item', 6, 0, 'main', '', 1, 0),
(318, 30, 5, 'Tecnologias', 'tecnologias', 'input', '', '', '', 0, 0, 'item', 12, 0, 'main', '', 1, 0),
(320, 30, 1, 'Galeria', 'galeria', 'galeria', '', '', '', 0, 0, 'item', 12, 0, 'main', '', 1, 0),
(321, 25, 0, 'Título', 'label_titulo', 'input', '', '', '', 0, 0, 'item', 0, 0, 'main', '', 1, 0),
(322, 25, 0, 'Empresa', 'empresa', 'input', '', '', '', 0, 0, 'item', 12, 0, 'main', '', 1, 0),
(324, 19, 11, 'Subtítulo', 'subtitulo', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(327, 21, 0, 'Resumo', 'resumo_servico', 'textarea', '', '', 'no-quill', 0, 0, 'item', 12, 0, 'main', '', 1, 0),
(332, 30, 0, 'Chamada', 'chamada_portfolio', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(333, 30, 0, 'Título', 'label_titulo', 'input', '', '', '', 0, 0, 'pagina', 0, 0, 'main', '', 1, 0),
(334, 30, 1, 'Subtítulo', 'subtitulo_portfolio', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(349, 20, 0, 'Função', 'funcao_membro', 'input', '', '', '', 0, 0, 'equipe', 12, 0, 'main', '', 1, 0),
(350, 20, 1, 'Resumo', 'resumo_membro', 'input', '', '', '', 0, 0, 'equipe', 12, 0, 'main', '', 1, 0),
(351, 20, 2, 'Facebook', 'facebook_membro', 'input', '', '', '', 0, 0, 'equipe', 12, 0, 'main', '', 1, 0),
(352, 20, 3, 'Instagram', 'instagram_membro', 'input', '', '', '', 0, 0, 'equipe', 12, 0, 'main', '', 1, 0),
(353, 20, 4, 'Twitter', 'twitter_membro', 'input', '', '', '', 0, 0, 'equipe', 12, 0, 'main', '', 1, 0),
(354, 20, 5, 'Perfil', 'perfil_membro', 'thumb', '', '', '', 0, 0, 'equipe', 12, 0, 'main', '', 1, 0),
(359, 21, 2, 'Imagem destaque', 'imagem_destaque', 'image', '', '', '', 0, 0, 'item', 12, 0, 'main', '', 1, 0),
(361, 21, 1, 'Resumo longo', 'resumo_longo_servico', 'textarea', '', '', 'text--', 0, 0, 'item', 12, 0, 'main', '', 1, 0),
(365, 23, 7, 'Resumo', 'resumo_contato', 'textarea', '', '', 'no-quill', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(368, 30, 6, 'Tags SEO', 'tags_seo', 'input', '', '', '', 0, 0, 'item', 12, 0, 'sidebar', '', 1, 0),
(370, 30, 0, 'Título', 'label_titulo', 'input', '', '', '', 0, 0, 'item', 0, 0, 'main', '', 1, 0),
(383, 21, 0, 'Título', 'label_titulo', 'input', '', '', '', 0, 0, 'item', 0, 0, 'main', '', 1, 0),
(384, 20, 0, 'Título', 'label_titulo', 'input', '', '', '', 0, 0, 'pagina', 0, 0, 'main', '', 1, 0),
(387, 23, 0, 'Título', 'label_titulo', 'input', '', '', '', 0, 0, 'pagina', 0, 0, 'main', '', 1, 0),
(388, 20, 0, 'Imagem tecnologia', 'imagem_tecnologia', 'image', '', '', '', 0, 0, 'tecnologias_sobre', 12, 0, 'main', '', 1, 0),
(389, 21, 0, 'Serviço extra', 'servico_extra', 'textarea', '', '', 'no-quill', 0, 0, 'outros_servicos', 12, 0, 'main', '', 1, 0),
(390, 21, 1, 'Imagem extra', 'imagem_extra', 'image', '', '', '', 0, 0, 'outros_servicos', 12, 0, 'main', '', 1, 0),
(391, 19, 0, 'Título', 'label_titulo', 'input', '', '', '', 0, 0, 'pagina', 0, 0, 'main', '', 1, 0),
(392, 21, 0, 'Título', 'label_titulo', 'input', '', '', '', 0, 0, 'pagina', 0, 0, 'main', '', 1, 0),
(393, 21, 2, 'Título Serviço extra', 'titulo_extra', 'input', '', '', '', 0, 0, 'pagina', 12, 0, 'main', '', 1, 0),
(394, 19, 0, 'Título', 'label_titulo', 'input', '', '', '', 0, 0, 'item', 0, 0, 'main', '', 1, 0),
(395, 19, 0, 'Cor', 'cor', 'input', '', '', '', 0, 0, 'item', 12, 0, 'main', '', 1, 0),
(397, 31, 0, 'Cliente', 'cliente', 'input', '', '', '', 0, 0, 'item', 12, 0, 'main', '', 1, 0),
(398, 31, 1, 'Serviço', 'servico', 'input', '', '', '', 0, 0, 'item', 12, 0, 'main', '', 1, 0),
(399, 31, 2, 'Investimento', 'investimento', 'divisao', '', '', '', 0, 0, 'item', 0, 0, 'main', '', 1, 0),
(400, 31, 3, 'Texto Investimento', 'texto_investimento', 'textarea', '', '', '', 0, 0, 'item', 12, 0, 'main', '', 1, 0),
(401, 31, 4, 'Descrição do Projeto', 'descricao_do_projeto', 'divisao', '', '', '', 0, 0, 'item', 0, 0, 'main', '', 1, 0),
(402, 31, 5, 'Texto Projeto', 'texto_projeto', 'textarea', '', '', '', 0, 0, 'item', 12, 0, 'main', '', 1, 0),
(403, 31, 6, 'Etapas de Desenvolvimento', 'etapas_de_desenvolvimento', 'divisao', '', '', '', 0, 0, 'item', 0, 0, 'main', '', 1, 0),
(404, 31, 7, 'Texto Etapas', 'texto_etapas', 'textarea', '', '', '', 0, 0, 'item', 12, 0, 'main', '', 1, 0),
(407, 31, 8, 'Prazo', 'prazo', 'input', '', '', '', 0, 0, 'item', 12, 0, 'sidebar', '', 1, 0),
(413, 31, 0, 'Título', 'label_titulo', 'input', '', '', '', 0, 0, 'item', 0, 0, 'main', '', 1, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `lixo`
--

CREATE TABLE `lixo` (
  `id` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `cod_area` int(11) NOT NULL,
  `desc` text NOT NULL,
  `controller` varchar(60) NOT NULL,
  `img` int(11) NOT NULL,
  `usuario` int(11) NOT NULL,
  `origem` varchar(60) NOT NULL,
  `excluido_em` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `mensagens`
--

CREATE TABLE `mensagens` (
  `id` int(11) NOT NULL,
  `remetente` varchar(60) NOT NULL,
  `para` varchar(140) NOT NULL,
  `assunto` varchar(140) NOT NULL,
  `conteudo` text NOT NULL,
  `status` int(1) NOT NULL,
  `data` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `mensagens`
--

INSERT INTO `mensagens` (`id`, `remetente`, `para`, `assunto`, `conteudo`, `status`, `data`) VALUES
(7, 'rafaelcarvalhosjrp@gmail.com', 'contato@jsmenu.com.br', 'fffffff', 'contato@jsmenu.com.br<br>RAFAEL NASCIMENTO DE CARVALHO<br>rafaelcarvalhosjrp@gmail.com<br>17992427270<br>fffffff<br>sdfsdfsdf<br>', 2, '2019-06-04'),
(8, 'rafaelcarvalhosjrp@gmail.com', 'contato@jsmenu.com.br', 'ggggg', 'contato@jsmenu.com.br<br>RENAN B L JAQUETA<br>rafaelcarvalhosjrp@gmail.com<br>1733633474<br>ggggg<br>yyyyyy<br>', 1, '2019-06-04'),
(9, 'renanteste123@gmai.com', 'contato@jsmenu.com.br', 'kkkk', 'contato@jsmenu.com.br<br>RENAN B L JAQUETA<br>renanteste123@gmai.com<br>1733633474<br>kkkk<br>kkkkk<br>', 1, '2019-06-04'),
(10, 'taiteste@hmd.com', 'contato@jsmenu.com.br', 'sdfsdfsdf', 'Taiane teste<br>taiteste@hmd.com<br>1735195125<br>sdfsdfsdf<br>sdfsdfsdf<br>', 1, '2019-06-04'),
(11, 'rafaelcarvalhosjrp@gmail.com', 'oie@8pdev.studio', 'Contato via site', 'RAFAEL NASCIMENTO DE CARVALHO<br>rafaelcarvalhosjrp@gmail.com<br>(17) 99242-7270<br>tsteste<br>Contato via site<br>', -1, '2019-09-27'),
(12, 'RAFAEL NASCIMENTO DE CARVALHO', '', 'Pedido Via site', 'RAFAEL NASCIMENTO DE CARVALHO<br>(17) 99242-7270<br>oferta-especial<br>', -1, '2019-10-08'),
(13, 'RAFAEL NASCIMENTO DE CARVALHO', 'rafael@8pdev.studio', 'Pedido Via site', 'RAFAEL NASCIMENTO DE CARVALHO<br>(17) 99242-7270<br>oferta-especial<br>', -1, '2019-10-08'),
(14, 'RAFAEL NASCIMENTO DE CARVALHO', '', 'Lead Via site', 'RAFAEL NASCIMENTO DE CARVALHO<br>(17) 99242-7270<br>', -1, '2019-10-08'),
(15, 'RAFAEL NASCIMENTO DE CARVALHO', '', 'Lead Via site', 'RAFAEL NASCIMENTO DE CARVALHO<br>(17) 99242-7270<br>', -1, '2019-10-08'),
(16, 'RAFAEL NASCIMENTO DE CARVALHO', 'rafael@8pdev.studio', 'Lead Via site', 'RAFAEL NASCIMENTO DE CARVALHO<br>(17) 99242-7270<br>', -1, '2019-10-08');

-- --------------------------------------------------------

--
-- Estrutura da tabela `midia`
--

CREATE TABLE `midia` (
  `id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `arquivo` varchar(255) NOT NULL,
  `caminho` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `extensao` varchar(6) NOT NULL,
  `tipo` varchar(10) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `data` date NOT NULL,
  `lixeira` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `midia`
--

INSERT INTO `midia` (`id`, `titulo`, `arquivo`, `caminho`, `url`, `slug`, `extensao`, `tipo`, `descricao`, `data`, `lixeira`) VALUES
(51, 'favicon', 'favicon.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/favicon.png', 'favicon', '.png', 'img', '', '2019-09-12', 0),
(52, 'img1', 'img1.jpg', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/img1.jpg', 'img1', '.jpg', 'img', '', '2019-09-13', 0),
(53, 'img1_76', 'img1_76.jpg', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/img1_76.jpg', 'img1_76', '.jpg', 'img', '', '2019-09-13', 0),
(54, 'img1_66', 'img1_66.jpg', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/img1_66.jpg', 'img1_66', '.jpg', 'img', '', '2019-09-13', 0),
(55, 'img1_56', 'img1_56.jpg', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/img1_56.jpg', 'img1_56', '.jpg', 'img', '', '2019-09-13', 0),
(56, 'support-man', 'supportman.svg', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/supportman.svg', 'supportman', '.svg', '.svg', '', '2019-09-16', 0),
(57, 'support-man_90', 'supportman_90.svg', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/supportman_90.svg', 'supportman_90', '.svg', '.svg', '', '2019-09-16', 0),
(58, 'img2', 'img2.jpg', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/img2.jpg', 'img2', '.jpg', 'img', '', '2019-09-16', 0),
(59, 'img1_56', 'img1_56.jpg', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/img1_56.jpg', 'img1_56', '.jpg', 'img', '', '2019-09-16', 0),
(60, 'img4', 'img4.jpg', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/img4.jpg', 'img4', '.jpg', 'img', '', '2019-09-16', 0),
(61, 'img5', 'img5.jpg', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/img5.jpg', 'img5', '.jpg', 'img', '', '2019-09-16', 0),
(62, 'img3', 'img3.jpg', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/img3.jpg', 'img3', '.jpg', 'img', '', '2019-09-16', 0),
(63, 'img6', 'img6.jpg', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/img6.jpg', 'img6', '.jpg', 'img', '', '2019-09-16', 0),
(64, 'img1_18', 'img1_18.jpg', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/img1_18.jpg', 'img1_18', '.jpg', 'img', '', '2019-09-16', 0),
(65, 'img4_1', 'img4_1.jpg', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/img4_1.jpg', 'img4_1', '.jpg', 'img', '', '2019-09-16', 0),
(66, 'img2_91', 'img2_91.jpg', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/img2_91.jpg', 'img2_91', '.jpg', 'img', '', '2019-09-16', 0),
(67, 'img7', 'img7.jpg', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/img7.jpg', 'img7', '.jpg', 'img', '', '2019-09-16', 0),
(68, 'img4_96', 'img4_96.jpg', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/img4_96.jpg', 'img4_96', '.jpg', 'img', '', '2019-09-16', 0),
(69, 'Consultoria-em-UX-UI', 'consultoriaemuxui.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/consultoriaemuxui.png', 'consultoriaemuxui', '.png', 'img', '', '2019-09-21', 0),
(70, 'Aplicativos', 'aplicativos.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/aplicativos.png', 'aplicativos', '.png', 'img', '', '2019-09-21', 0),
(71, 'Sites', 'sites.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/sites.png', 'sites', '.png', 'img', '', '2019-09-21', 0),
(106, '8p', '8p.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/8p.png', '8p', '.png', 'img', '', '2019-09-24', 0),
(112, '8p_portfolio_finnance', '8p_portfolio_finnance.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/8p_portfolio_finnance.png', '8p_portfolio_finnance', '.png', 'img', '', '2019-09-24', 0),
(113, '8p_portfolio_finnance_landingpage', '8p_portfolio_finnance_landingpage.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/8p_portfolio_finnance_landingpage.png', '8p_portfolio_finnance_landingpage', '.png', 'img', '', '2019-09-24', 0),
(114, '8p_portfolio_epics', '8p_portfolio_epics.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/8p_portfolio_epics.png', '8p_portfolio_epics', '.png', 'img', '', '2019-09-24', 0),
(115, '8p_portfolio_epics_thumb', '8p_portfolio_epics_thumb.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/8p_portfolio_epics_thumb.png', '8p_portfolio_epics_thumb', '.png', 'img', '', '2019-09-24', 0),
(116, '8p_portfolio_finnance_landingpage_thumb', '8p_portfolio_finnance_landingpage_thumb.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/8p_portfolio_finnance_landingpage_thumb.png', '8p_portfolio_finnance_landingpage_thumb', '.png', 'img', '', '2019-09-24', 0),
(117, '8p_portfolio_finnance_thumb', '8p_portfolio_finnance_thumb.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/8p_portfolio_finnance_thumb.png', '8p_portfolio_finnance_thumb', '.png', 'img', '', '2019-09-24', 0),
(118, '8p_portfolio_jsmenu', '8p_portfolio_jsmenu.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/8p_portfolio_jsmenu.png', '8p_portfolio_jsmenu', '.png', 'img', '', '2019-09-24', 0),
(119, '8p_portfolio_jsmenu_thumb', '8p_portfolio_jsmenu_thumb.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/8p_portfolio_jsmenu_thumb.png', '8p_portfolio_jsmenu_thumb', '.png', 'img', '', '2019-09-24', 0),
(120, '8p_portfolio_okamed', '8p_portfolio_okamed.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/8p_portfolio_okamed.png', '8p_portfolio_okamed', '.png', 'img', '', '2019-09-24', 0),
(121, '8p_portfolio_okamed_thumb', '8p_portfolio_okamed_thumb.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/8p_portfolio_okamed_thumb.png', '8p_portfolio_okamed_thumb', '.png', 'img', '', '2019-09-24', 0),
(122, '8p_portfolio_boxsupreme', '8p_portfolio_boxsupreme.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/8p_portfolio_boxsupreme.png', '8p_portfolio_boxsupreme', '.png', 'img', '', '2019-09-24', 0),
(123, '8p_portfolio_boxsupreme_thumb', '8p_portfolio_boxsupreme_thumb.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/8p_portfolio_boxsupreme_thumb.png', '8p_portfolio_boxsupreme_thumb', '.png', 'img', '', '2019-09-24', 0),
(124, 'undraw_tabs_jf82', 'undraw_tabs_jf82.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_tabs_jf82.png', 'undraw_tabs_jf82', '.png', 'img', '', '2019-09-24', 0),
(125, 'undraw_about_me_wa29', 'undraw_about_me_wa29.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_about_me_wa29.png', 'undraw_about_me_wa29', '.png', 'img', '', '2019-09-24', 0),
(126, 'undraw_pair_programming_njlp', 'undraw_pair_programming_njlp.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_pair_programming_njlp.png', 'undraw_pair_programming_njlp', '.png', 'img', '', '2019-09-24', 0),
(127, 'undraw_usability_testing_2xs4', 'undraw_usability_testing_2xs4.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_usability_testing_2xs4.png', 'undraw_usability_testing_2xs4', '.png', 'img', '', '2019-09-24', 0),
(128, 'undraw_image_post_24iy', 'undraw_image_post_24iy.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_image_post_24iy.png', 'undraw_image_post_24iy', '.png', 'img', '', '2019-09-24', 0),
(129, 'undraw_noted_pc9f', 'undraw_noted_pc9f.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_noted_pc9f.png', 'undraw_noted_pc9f', '.png', 'img', '', '2019-09-24', 0),
(130, 'undraw_onboarding_o8mv', 'undraw_onboarding_o8mv.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_onboarding_o8mv.png', 'undraw_onboarding_o8mv', '.png', 'img', '', '2019-09-24', 0),
(131, 'undraw_code_review_l1q9', 'undraw_code_review_l1q9.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_code_review_l1q9.png', 'undraw_code_review_l1q9', '.png', 'img', '', '2019-09-24', 0),
(132, 'undraw_Taken_if77', 'undraw_taken_if77.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_taken_if77.png', 'undraw_taken_if77', '.png', 'img', '', '2019-09-24', 0),
(133, 'undraw_mobile_testing_reah', 'undraw_mobile_testing_reah.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_mobile_testing_reah.png', 'undraw_mobile_testing_reah', '.png', 'img', '', '2019-09-24', 0),
(134, 'undraw_interaction_design_odgc', 'undraw_interaction_design_odgc.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_interaction_design_odgc.png', 'undraw_interaction_design_odgc', '.png', 'img', '', '2019-09-24', 0),
(135, 'undraw_windows_q9m0', 'undraw_windows_q9m0.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_windows_q9m0.png', 'undraw_windows_q9m0', '.png', 'img', '', '2019-09-24', 0),
(136, 'undraw_user_flow_vr6w', 'undraw_user_flow_vr6w.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_user_flow_vr6w.png', 'undraw_user_flow_vr6w', '.png', 'img', '', '2019-09-24', 0),
(137, 'undraw_mobile_marketing_iqbr', 'undraw_mobile_marketing_iqbr.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_mobile_marketing_iqbr.png', 'undraw_mobile_marketing_iqbr', '.png', 'img', '', '2019-09-24', 0),
(138, 'undraw_Data_points_ubvs', 'undraw_data_points_ubvs.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_data_points_ubvs.png', 'undraw_data_points_ubvs', '.png', 'img', '', '2019-09-24', 0),
(139, 'undraw_bug_fixing_oc7a', 'undraw_bug_fixing_oc7a.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_bug_fixing_oc7a.png', 'undraw_bug_fixing_oc7a', '.png', 'img', '', '2019-09-24', 0),
(140, 'undraw_programmer_imem', 'undraw_programmer_imem.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_programmer_imem.png', 'undraw_programmer_imem', '.png', 'img', '', '2019-09-24', 0),
(141, 'undraw_operating_system_4lr6', 'undraw_operating_system_4lr6.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_operating_system_4lr6.png', 'undraw_operating_system_4lr6', '.png', 'img', '', '2019-09-24', 0),
(142, 'undraw_hacker_mind_6y85', 'undraw_hacker_mind_6y85.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_hacker_mind_6y85.png', 'undraw_hacker_mind_6y85', '.png', 'img', '', '2019-09-24', 0),
(143, 'undraw_instant_support_elxh', 'undraw_instant_support_elxh.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_instant_support_elxh.png', 'undraw_instant_support_elxh', '.png', 'img', '', '2019-09-24', 0),
(144, 'undraw_note_list_etto', 'undraw_note_list_etto.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_note_list_etto.png', 'undraw_note_list_etto', '.png', 'img', '', '2019-09-24', 0),
(145, 'undraw_post_online_dkuk', 'undraw_post_online_dkuk.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_post_online_dkuk.png', 'undraw_post_online_dkuk', '.png', 'img', '', '2019-09-24', 0),
(146, 'undraw_co-working_825n', 'undraw_coworking_825n.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_coworking_825n.png', 'undraw_coworking_825n', '.png', 'img', '', '2019-09-24', 0),
(147, 'undraw_may_the_force_bgdm', 'undraw_may_the_force_bgdm.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_may_the_force_bgdm.png', 'undraw_may_the_force_bgdm', '.png', 'img', '', '2019-09-24', 0),
(148, 'undraw_work_time_lhoj', 'undraw_work_time_lhoj.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_work_time_lhoj.png', 'undraw_work_time_lhoj', '.png', 'img', '', '2019-09-24', 0),
(149, 'undraw_programming_2svr', 'undraw_programming_2svr.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_programming_2svr.png', 'undraw_programming_2svr', '.png', 'img', '', '2019-09-24', 0),
(150, 'undraw_security_o890', 'undraw_security_o890.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_security_o890.png', 'undraw_security_o890', '.png', 'img', '', '2019-09-24', 0),
(151, 'undraw_creativity_wqmm', 'undraw_creativity_wqmm.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/undraw_creativity_wqmm.png', 'undraw_creativity_wqmm', '.png', 'img', '', '2019-09-24', 0),
(158, 'react-native', 'reactnative.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/reactnative.png', 'reactnative', '.png', 'img', '', '2019-09-26', 0),
(159, 'codeigniter', 'codeigniter.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/codeigniter.png', 'codeigniter', '.png', 'img', '', '2019-09-26', 0),
(161, 'adobexd', 'adobexd.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/adobexd.png', 'adobexd', '.png', 'img', '', '2019-09-26', 0),
(164, 'git', 'git.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/git.png', 'git', '.png', 'img', '', '2019-09-26', 0),
(165, 'css3', 'css3.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/css3.png', 'css3', '.png', 'img', '', '2019-09-26', 0),
(166, 'html5', 'html5.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/html5.png', 'html5', '.png', 'img', '', '2019-09-26', 0),
(167, 'bootstrap', 'bootstrap.png', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/bootstrap.png', 'bootstrap', '.png', 'img', '', '2019-09-26', 0),
(168, 'black-coffee-caffeine-coffee-383496', 'blackcoffeecaffeinecoffee383496.jpg', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/blackcoffeecaffeinecoffee383496.jpg', 'blackcoffeecaffeinecoffee383496', '.jpg', 'img', '', '2019-09-26', 0),
(169, '1b4e7b6a-fa15-414f-84c6-9fe5e1802642', '1b4e7b6afa15414f84c69fe5e1802642.jpg', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/1b4e7b6afa15414f84c69fe5e1802642.jpg', '1b4e7b6afa15414f84c69fe5e1802642', '.jpg', 'img', '', '2019-09-26', 0),
(170, 'work-in-progress-web-design-layout-ideas-picjumbo-com', 'workinprogresswebdesignlayoutideaspicjumbocom.jpg', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/workinprogresswebdesignlayoutideaspicjumbocom.jpg', 'workinprogresswebdesignlayoutideaspicjumbocom', '.jpg', 'img', '', '2019-09-26', 0),
(171, 'designer-holding-a-notebook-with-web-site-wireframes-layout-picjumbo-com', 'designerholdinganotebookwithwebsitewireframeslayoutpicjumbocom.jpg', 'upload/2019/09/', 'http://localhost/8pdev.studio/upload/2019/09/designerholdinganotebookwithwebsitewireframeslayoutpicjumbocom.jpg', 'designerholdinganotebookwithwebsitewireframeslayoutpicjumbocom', '.jpg', 'img', '', '2019-09-26', 0),
(174, 'intera', 'intera.jpg', 'upload/2019/09/', 'http://8pdev.studio/upload/2019/09/intera.jpg', 'intera', '.jpg', 'img', '', '2019-09-28', 0),
(175, 'thumb-intera', 'thumb_intera.jpg', 'upload/2019/09/', 'http://8pdev.studio/upload/2019/09/thumb_intera.jpg', 'thumb_intera', '.jpg', 'img', '', '2019-09-28', 0),
(176, 'mar-aberto-thumb', 'mar_aberto_thumb.jpg', 'upload/2019/09/', 'http://8pdev.studio/upload/2019/09/mar_aberto_thumb.jpg', 'mar_aberto_thumb', '.jpg', 'img', '', '2019-09-28', 0),
(177, 'maraberto', 'maraberto.jpg', 'upload/2019/09/', 'http://8pdev.studio/upload/2019/09/maraberto.jpg', 'maraberto', '.jpg', 'img', '', '2019-09-28', 0),
(178, 'casaverdethumb', 'casaverdethumb.jpg', 'upload/2019/09/', 'http://8pdev.studio/upload/2019/09/casaverdethumb.jpg', 'casaverdethumb', '.jpg', 'img', '', '2019-09-28', 0),
(179, 'casaverde', 'casaverde.jpg', 'upload/2019/09/', 'http://8pdev.studio/upload/2019/09/casaverde.jpg', 'casaverde', '.jpg', 'img', '', '2019-09-28', 0),
(180, 'thumb-aparato', 'thumb_aparato.jpg', 'upload/2019/09/', 'http://8pdev.studio/upload/2019/09/thumb_aparato.jpg', 'thumb_aparato', '.jpg', 'img', '', '2019-09-28', 0),
(181, 'aparato', 'aparato.jpg', 'upload/2019/09/', 'http://8pdev.studio/upload/2019/09/aparato.jpg', 'aparato', '.jpg', 'img', '', '2019-09-28', 0),
(183, 'renan', 'renan.gif', 'upload/2019/09/', 'http://8pdev.studio/upload/2019/09/renan.gif', 'renan', '.gif', 'img', '', '2019-09-30', 0),
(184, 'rafa', 'rafa.gif', 'upload/2019/09/', 'http://8pdev.studio/upload/2019/09/rafa.gif', 'rafa', '.gif', 'img', '', '2019-09-30', 0),
(185, 'tai', 'tai.gif', 'upload/2019/09/', 'http://8pdev.studio/upload/2019/09/tai.gif', 'tai', '.gif', 'img', '', '2019-09-30', 0),
(190, 'site-thumb', 'site_thumb.png', 'upload/2019/10/', 'http://8pdev.studio/upload/2019/10/site_thumb.png', 'site_thumb', '.png', 'img', '', '2019-10-07', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `midia_rel`
--

CREATE TABLE `midia_rel` (
  `id` int(11) NOT NULL,
  `midia` int(11) NOT NULL,
  `item` int(11) NOT NULL,
  `tipo` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `relacional`
--

CREATE TABLE `relacional` (
  `id` int(11) NOT NULL,
  `sessao_principal` int(11) NOT NULL,
  `cod_principal` int(11) NOT NULL,
  `controller_principal` varchar(60) NOT NULL,
  `sessao_origem` int(11) NOT NULL,
  `cod_item` int(11) NOT NULL,
  `controller_origem` varchar(60) NOT NULL,
  `tipo_principal` varchar(24) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sublista`
--

CREATE TABLE `sublista` (
  `id` int(11) NOT NULL,
  `cod_area` int(11) NOT NULL,
  `titulo` varchar(60) NOT NULL,
  `slug` varchar(60) NOT NULL,
  `tipo` varchar(14) NOT NULL,
  `ordem` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `sublista`
--

INSERT INTO `sublista` (`id`, `cod_area`, `titulo`, `slug`, `tipo`, `ordem`) VALUES
(42, 1, 'Portfolio1ggg', 'portfolio1ggg', 'item', 0),
(48, 1, 'Cliente', 'cliente', 'pagina', 0),
(49, 9, 'Soluções', 'solucoes', 'pagina', 0),
(50, 10, 'Segmentos', 'segmentos', 'pagina', 0),
(51, 11, 'Formatos', 'formatos', 'pagina', 0),
(52, 12, 'Vantagens', 'vantagens', 'pagina', 0),
(54, 13, 'Depoimentos', 'depoimentos', 'pagina', 0),
(55, 14, 'Campos do formulário', 'campos_do_formulario', 'pagina', 0),
(57, 15, 'Campos de solicitação', 'campos_de_solicitacao', 'pagina', 0),
(65, 29, 'Unidades', 'unidades', 'pagina', 0),
(67, 19, 'Termos alternando', 'termos_alternando', 'pagina', 0),
(68, 20, 'Equipe', 'equipe', 'pagina', 0),
(71, 20, 'Tecnologias Sobre', 'tecnologias_sobre', 'pagina', 0),
(72, 21, 'Outros serviços', 'outros_servicos', 'pagina', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nome` varchar(60) NOT NULL,
  `login` varchar(60) NOT NULL,
  `email` varchar(140) NOT NULL,
  `senha` varchar(60) NOT NULL,
  `img` int(11) NOT NULL,
  `link` varchar(140) NOT NULL,
  `acessos` varchar(140) NOT NULL,
  `criado_em` date NOT NULL,
  `modificado_em` date NOT NULL,
  `tipo` varchar(10) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `login`, `email`, `senha`, `img`, `link`, `acessos`, `criado_em`, `modificado_em`, `tipo`, `status`) VALUES
(1, 'Rafael Nascimento de Carvalho', 'rafael', 'rafaelcarvalhosjrp@gmail.com', 'D1063FC523EF316FBA32322382B78830', 214, 'https://www.facebook.com/rafael.nascimentodecarvalho.5', '', '2018-08-26', '2019-03-11', 'master', 1),
(2, 'Marnei de Jesus', 'marnei', 'marnei@sjrp.com.br', 'D1063FC523EF316FBA32322382B78830', 0, '', '[\"5\",\"3\",\"2\",\"1\"]', '2019-01-05', '2019-05-16', 'usuario', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `analytics`
--
ALTER TABLE `analytics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categorias_rel`
--
ALTER TABLE `categorias_rel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `configuracoes`
--
ALTER TABLE `configuracoes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `id_2` (`id`);

--
-- Indexes for table `conteudo`
--
ALTER TABLE `conteudo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom`
--
ALTER TABLE `custom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lixo`
--
ALTER TABLE `lixo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mensagens`
--
ALTER TABLE `mensagens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `midia`
--
ALTER TABLE `midia`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `midia_rel`
--
ALTER TABLE `midia_rel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `relacional`
--
ALTER TABLE `relacional`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sublista`
--
ALTER TABLE `sublista`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `analytics`
--
ALTER TABLE `analytics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=768;

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `categorias_rel`
--
ALTER TABLE `categorias_rel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `conteudo`
--
ALTER TABLE `conteudo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=234;

--
-- AUTO_INCREMENT for table `custom`
--
ALTER TABLE `custom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=414;

--
-- AUTO_INCREMENT for table `lixo`
--
ALTER TABLE `lixo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `mensagens`
--
ALTER TABLE `mensagens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `midia`
--
ALTER TABLE `midia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;

--
-- AUTO_INCREMENT for table `midia_rel`
--
ALTER TABLE `midia_rel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `relacional`
--
ALTER TABLE `relacional`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sublista`
--
ALTER TABLE `sublista`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
