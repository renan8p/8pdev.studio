var options = {
    buttonLabels: 'fontawesome',
    imageDragging: "true",
    toolbar: {
        buttons: ['bold', 'italic', 'underline', 'anchor', 'h2', 'h3']
    },
    autoLink: true,
    paste: {
        forcePlainText: true,
        cleanPastedHTML: false,
        cleanReplacements: [],
        cleanAttrs: ['style', 'dir'],
        cleanTags: ['meta', 'span'],
        unwrapTags: []
    }
};


// Carrega modo Medium de edição de texto
var editor = new MediumEditor('.editable', options);

// Remove bloco de imagem vazios
$(".midia-footer .btn, .midia-btn-close").on("click", function(){
    block_remove();
});

$("html").on("blur", '.editable', function(){
    block_remove();
});
// -----

// Ativa botão de opções
$("#blog-editor").on("click", ".box-options", function(){
    $(this).toggleClass("active");
});

$("#blog-editor").on("click", ".editor-btn-remove", function(){
    $(this).closest(".editor-block").remove();
    pack_content();
});
// -----

// Função q insere caixa de opções em blocos
block_options();

// Ativa reordenação de blocos
$( "#blog-editor" ).sortable({
    connectWith: "#blog-editor",
    handle: ".editor-btn-handle",
    stop: function( event, ui ) {
        pack_content()
    }
});
// -----

// Insere bloco de texto
$(".btn-text").bind("click", function(){

    var editable = $('textarea.editable').length;

    var append_ = '<div class="editor-block text-box"><textarea class="textarea'+editable+' editable"></textarea></div>';

    $("#blog-editor").append(append_);
    editor = new MediumEditor('.editable', options);

    block_options();
});
// -----

// Insere bloco de imagem
$(".btn-image").bind("click", function(){

    var img_count = $('img.img-line').length;

    $(this).attr("data-target", '.img-block-'+img_count);

    var append_ = '<div class="editor-block img-box"><figure><img class="img-line img-block-'+img_count+'" src="assets/painel/img/px-hor.png"/></figure></div>';

    $("#blog-editor").append(append_);

    block_options();
});
// -----

// Insere bloco de externo
$(".btn-youtube").bind("click", function(){

    var video_count = $('.youtube-box').length;

    var append_ = '<div class="editor-block youtube-box"><input placeholder="Cole a url do vídeo aqui..." class="youtube-input youtube-'+video_count+'"></div>';

    $("#blog-editor").append(append_);

    block_options();
});

$("#blog-editor").on("blur", "input.youtube-input", function(){
    
    var url = $(this).val();
    $(this).next().remove();

    if(url.indexOf("youtube") > -1){
        link = url.split("watch?v=");
        insert = '<object><iframe src="https://www.youtube.com/embed/'+link[1]+'" frameborder="0"></iframe></object>';

        $(this).closest(".youtube-box").addClass("loaded");
        $(this).after(insert);
        $(this).remove();
    }

    if(url.indexOf("vimeo") > -1){
        link = url.split("vimeo.com/");
        insert = '<object><iframe src="https://player.vimeo.com/video/'+link[1]+'" frameborder="0"></iframe></object>';

        $(this).after(insert);
        $(this).remove();
    }

    block_options();
});
// -----

$( "#blog-editor" ).keyup(function(){
    pack_content();
});

// Empacota conteudo para envio
function pack_content(){

    var conteudo = "";
    var conteudo_block = [];

    $('.editor-block').each(function( index ){

        var conteudo_obj = {};

        if($(this).hasClass("img-box")){
            html = $(this).find("figure").html();
            conteudo_obj.id  = index;
            conteudo_obj.type = "img";
            conteudo_obj.content = html;

            valid = true;
        }
        if($(this).hasClass("text-box")){
            html = $(this).find("div.editable").html();
            conteudo_obj.id  = index;
            conteudo_obj.type = "text";
            conteudo_obj.content = html;

            valid = ($(this).find("div.editable").text() != "") ? true : false;
        }
        if($(this).hasClass("youtube-box")){
            html = $(this).find("object").html();
            conteudo_obj.id  = index;
            conteudo_obj.type = "youtube";
            conteudo_obj.content = html;

            valid = ($(this).find("object").length > 0) ? true : false;
        }

        if(valid){
            // Conteudo que vai pro site
            conteudo += html;
            // Conteudo que monta blocos no painel
            conteudo_block.push(conteudo_obj);
        }
    });

    var conteudo_json = JSON.stringify(conteudo_block);

    $(".editable_blocks").val(conteudo_json);
    //$(".editable_content").val(conteudo);
}

// Função que remove bloco de imagens
function block_remove(){
    setTimeout(function(){ // O setTime é pra trasar e esperar a execução de outro script
        $('.editor-block').each(function(){
            var attr = $(this).find(".img-line").attr("src");
            if( attr == "assets/painel/img/px-hor.png" ){
                $(this).remove();
            }
        });
        pack_content();
    }, 10);
}

// Função q insere caixa de opções
function block_options(){
    var box_options = '<div class="box-options"><span class="btn btn-primary editor-btn-handle"><i class="fe fe-move"></i></span><span class="btn btn-primary editor-btn-remove"><i class="fe fe-trash-2"></i></span><span class="btn editor-btn-plus"><i class="fe fe-x"></i></span></div>';
    $('.editor-block').each(function(){
        var length_ = $(this).find(".box-options").length;
        if( length_ == 0 ){
            $(this).append(box_options);
        }
    });
    pack_content();
}