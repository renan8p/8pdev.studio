$(document).ready(function(){

	//  Reordena areas
	$('#reorder-navegacao').sortable({
		handle: '.btn-move'
	});

	$("form#new-link input").focus(function(){$(this).removeClass('is-invalid')});
	$("form#new-link").submit( function(){

		var link   = $("#link_url").val();
		var target = ($('#link_target').is(":checked")) ? "_blank" : "_self";
		var titulo = $("#link_label").val();

		var badge = "badge-info";

		if(link != "" && titulo != ""){
			$(".to-copy").attr('data-titulo', titulo);
			$(".to-copy").attr('data-link', link);
			$(".to-copy").attr('data-tipo', "link");
			$(".to-copy").attr('data-target', target);
			$(".to-copy").attr('data-area', false);
			$(".to-copy").attr('data-submenu', false);

			$(".to-copy").find('.nav-titulo').text(titulo);
			$(".to-copy").find('.nav-tipo').text("link").addClass(badge);
			$(".to-copy").find('.nav-link').text(link);
			$(".to-copy").find('.submenu-check').remove();

			if(target == "_blank"){ $(".to-copy").find(".nav-tipo").append(" (nova aba)"); }

			$(".to-copy").clone().appendTo("#reorder-navegacao");
			$("#reorder-navegacao li").last().removeClass("to-copy opacity-0");

			$('input', this).val("");
			$('input[type="checkbox"]', this).prop('checked', false);
		}
		else{
			if(titulo == ""){ $("#link_label").addClass('is-invalid'); }
			if(link == ""){ $("#link_url").addClass('is-invalid'); }
		}

		return false;
	});

	$("form#new-menu").submit( function(){

		var titulo = $("#titulo_menu").val();
		var status = ($("#status_menu").is("checked")) ? 1 : 0;

		$.ajax({
			url: "painel/configuracoes/salvar_menu",
			type: 'POST',
			dataType: 'json',
			data: {titulo: titulo, status: status},
			success: function(response){
				if(response.status == 200){
					window.location.href = 'painel/configuracoes/navegacao/' + response.slug;
				}
			}
		});

		return false;
	});

	$("#menu").bind('change', function(){
		window.location.href = 'painel/configuracoes/navegacao/' + $(this).val();
	});

	$('#envia_menu').bind('submit', function(){

		var menu = $('select', this).val();
		var itens = new Array();		
		$('#reorder-navegacao li').each(function(){

			var titulo  = $(this).data('titulo');
			var link    = $(this).data('link');
			var area    = $(this).data('area');
			var slug    = $(this).data('slug');
			var tipo    = $(this).data('tipo');
			var target  = $(this).data('target');
			var submenu = ($(this).find('input[name="link_target"]').is(':checked')) ? true : false;

			itens.push({titulo: titulo, area: area, link: link, slug: slug, tipo: tipo, target: target, submenu: submenu});
		});

		$.ajax({
			url: "painel/configuracoes/salvar_navegacao",
			type: 'POST',
			dataType: 'json',
			data: {menu: menu, itens: itens},
            beforeSend: function(){
        		$('#feedback').html('Carregando...').feedback('show');
            },
            success: function(response){
				if(response.status == 200){
					var msg = 'Enviado com sucesso!';
					$('#feedback').html(msg).feedback('hide', 200, response.url);
				}
            },
            error: function(response){
            	$('#feedback').html('Ops! Algo deu errado...').feedback('hide', 0);
            }
        });

		return false;
	});

	$('form.list-nav').bind('submit', function(){

		$('.custom-checkbox', this).each(function(){
			if($('input[type="checkbox"]', this).is(':checked')){

				var slug   = $(this).data('slug');
				var target = "_self";
				var titulo = $(this).data('titulo');
				var tipo   = $(this).data('tipo');				
				var area   = $(this).data('area');

				var badge = "badge-secondary";
					badge = (tipo == "pagina") ? "badge-primary" : badge;

				$(".to-copy").attr('data-titulo', titulo);
				$(".to-copy").attr('data-slug', slug);
				$(".to-copy").attr('data-tipo', tipo);
				$(".to-copy").attr('data-area', area);
				$(".to-copy").attr('data-target', target);

				$(".to-copy").find('.nav-titulo').text(titulo);
				$(".to-copy").find('.nav-tipo').text(tipo).addClass(badge);
				$(".to-copy").find('.nav-link').text('/'+slug);

				$(".to-copy").clone().appendTo("#reorder-navegacao");
				$("#reorder-navegacao li").last().removeClass("to-copy opacity-0");

				$('input[type="checkbox"]', this).prop('checked', false);
			}
		});

		return false;
	});

	$("#reorder-social").on('click', '.btn-remove', function(){
		$(this).closest("li").remove();
		save();
	});

	$('#reorder-navegacao').on('click', '.btn-remove', function(){
		$(this).closest('li.list-group-item').remove();
	});

	function save(){
		var itens = [];
		$("#reorder-social li").each(function(index, key){
			var link = $(this).find('.social-link').text();
			var plataforma = $(this).find('.social-plataforma').text();
			itens.push({'plataforma': plataforma, 'link': link});
		});

		$.post("painel/configuracoes/salvar_navegacao", {itens: itens});
	}
});