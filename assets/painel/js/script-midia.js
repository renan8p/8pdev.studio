var target   = "";
var multiple = "single";

$(document).ready(function($) {

	$("body").on("click", ".midia-submit", function(){
		var modal_return = $(this).attr('data-return');

		if(modal_return == 'list'){
			var checks = [];
			var checkeds = "";			

			$(".midia-item.active").each(function(){
				var midia_data = $(this).find('.midia-data');
				checks.push(midia_data.data('midia'));

				content = "<img src='" + midia_data.data('url') + "'/>";
				content = (midia_data.data('tipo') == "audio") ? '<audio controls><source src="' + midia_data.data('url') + '" type="audio/mpeg"></audio>' : content ;
				content = (midia_data.data('tipo') == "video") ? '<video controls><source src="' + midia_data.data('url') + '" type="video/mp4"></video>' : content;				
				content = (midia_data.data('tipo') == "documento") ? '<div class="pdf"><div class="pdf-desc"><p>' + midia_data.data('desc') + '</p><span class="badge badge-soft-primary p-2">' + midia_data.data('tipo') + '</span></div></div>' : content;

				checkeds += "<div class='midia-list' data-midia='"+midia_data.data('midia')+"'><div class='midia-delete' data-target='"+target+"'><i class='fe fe-x'></i></div>"+content+"</div>";		
			});

			checks = (multiple == "multiple") ? JSON.stringify(checks) : checks[0];

			console.log(target);

			$(target).val(checks);
			$(target + "-load").html(checkeds);
		}

		if(modal_return == 'img'){
			var midia_data = $(".midia-item.active").find('.midia-data');
			$(target).attr('src', midia_data.data('url-full'));
		}

		$("#midia").midia_modal('hide');
	});

	$("body").on("click", ".midia-list", function(e){
		var $tgt = $(e.target);		

		if(!$tgt.closest('.midia-delete').length) {
			var id = $(this).attr("data-midia");
			$("input[value='"+id+"']").next().trigger("click");
		} 

	});

	$("body").on("click", ".btn-midia", function(event){
		event.preventDefault();

		$(".midia-filtro").show();
		$("#midia").midia_modal('show');

		target   = $(this).attr("data-target");
		gerencia = $(this).attr("data-gerencia");
		multiple = $(this).attr("data-multiple");
		filtro   = $(this).attr("data-filter");
		filtro   = (filtro != undefined) ? filtro : 'todos';

		$(".midia-filtro").attr("data-filter", filtro);
		if(filtro != 'todos'){ $(".midia-filtro").hide(); }

		if(gerencia == "gerencia"){
			$('.midia-footer .midia-submit').hide();
		}
		else{
			$('.midia-footer .midia-submit').show();
		}

		$(".midia-submit").attr('data-return', 'list');

		if(gerencia == "url"){
			$(".midia-submit").attr('data-return', 'img');
		}

		$("#midia-lista").midia_lista();
	});

	$("body").on('click', '.midia-delete', function(){
		$(this).closest(".midia-list").remove();
		var target_input = $(this).data("target");
		var each_midia   = $(target_input+"-load .midia-list");
		var checks       = $(each_midia).return_midia();

		console.log(target_input);

		$(target_input).val(checks);
	});

	$("#midia").on('click', '.midia-data', function(){
		var midia = $(this).data("midia");
		if(multiple == "multiple"){
			$(this).parent('.midia-item').toggleClass("active");	
		}
		else{
			$('.midia-data').not(this).parent('.midia-item').removeClass("active");
			$(this).parent('.midia-item').toggleClass("active");
		}
	});

	$('#fileupload').fileupload({
		url: "painel/midias/midia_upload",
		dataType: 'json',
		type: 'POST',
		processData: false,
		contentType: false,
		cache: false,
		limitMultiFileUploads: 20,
		send: function (e, data) {
			$("#upload-tab").trigger("click");
			$('.midia-header').addClass('disabled');

			var upload = parseInt($('.counter span b').text());
			$('.counter span b').text(upload+1);

			$('#loader-upload').show();
		},
		success: function(response, data){			

			if( response.return_list != undefined ) {
				response.return_list.forEach(function(key, index){

					var uploaded = parseInt($('.counter span number').text());
					$('.counter span number').text(uploaded+1);

					$('.midia-feedback').append('<div class="d-block mb-3 feedback-' + index + '"><div class="alert alert-'+ index +' alert-'+key.alert+'" style="display: none;" data-index="'+key.file+'"><b>'+key.file+'</b> '+key.feedback+'<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button></div></div>');
					$('[data-index="'+key.file+'"]').fadeIn();
					window.setTimeout(function(){
						$('.feedback-' + index).fadeOut(function(){
							$(this).remove();
						});
					}, 8000);
				});
			}

			if (response.status == 0) {
				$('.midia-feedback').append('<div class="status-0 alert alert-danger" style="display: none;">' + response.msg + '</div><br>');
				$('.alert-0').fadeIn();
			}
		},
		stop: function(response){
			window.setTimeout(function(){
				$('.midia-header').removeClass('disabled');
				$('#loader-upload').hide(0);
				$("#arquivos-tab").trigger("click");
				$("#midia-lista").midia_lista();
				$('.counter span b, .counter span number').text(0);
			}, 5000);
		}
	}).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
	// --

	$(".midia-btn-close").bind("click", function(){
		$("#midia").midia_modal('hide');
	});

	$("html").keyup(function(e) {
		if (e.key === "Escape") {
			$("#midia").midia_modal('hide');
		}
	});

	$("#midia-lista").scroll(function(){
		$(this).lazyload();
	});

	// Seta midia na modal lateral
	$("#midia").on('click', '.btn-midia-edit', function(){
		$(".midia-edit").html("").removeClass('video audio documento');

		var midia_id = $(this).attr('data-midia');

		$.post( "painel/midias/midia_data", {id : midia_id}, function( data ) {

			$('.edit-midia-id').val(midia_id);
			$('.edit-midia-img').attr('src', data.url);			
			$('.edit-midia-ver').attr('href', data.url);
			$('.edit-midia-titulo').val(data.titulo);
			$('.edit-midia-slug').val(data.slug);
			$('.edit-midia-extensao').val(data.extensao);
			$('.edit-midia-desc').val(data.descricao);

			if (data.tipo == "img") {
				element = '<img class="edit-midia-img" src="' + data.url + '">';
			}

			if (data.tipo == "audio") {
				$('.midia-edit').addClass('audio');
				element = '<audio controls><source src="' + data.url + '" type="audio/mpeg"></audio>';
			}

			if (data.tipo == "video") {
				$('.midia-edit').addClass('video');
				element = '<video width="320" height="240" controls><source src="' + data.url + '" type="video/mp4"></video>';			
			}

			if (data.tipo == "documento") {
				$('.midia-edit').addClass('documento');		
			}

			setTimeout(function () { $(".midia-edit").html(element); }, 300);	

			$('.edit-midia-delete').attr('data-remove', '#midia-item-'+ midia_id);
			$('.edit-midia-delete').attr('data-target', 'painel/midias/lixeira/'+ midia_id);

			$("#edit-sidebar").toggleClass('open');

		}, "json");
	});

	$(".btn-close-sidebar").bind('click', function(){
		$("#edit-sidebar").removeClass('open');
		$(".midia-edit").html("");
	});

	// fechar sidebar de info de imagem
	$('body').click(function(event){
	    if (!$(event.target).closest(".edit-sidebar").length) {
	        $("#edit-sidebar").removeClass('open');
			$(".midia-edit").html("");
	    }
	});

	$("#edit-sidebar form.midia-desc").submit(function(){

		var midia_id       = $('.edit-midia-id', this).val();
		var midia_titulo   = $('.edit-midia-titulo', this).val();
		var midia_slug     = $('.edit-midia-slug', this).val();
		var midia_extensao = $('.edit-midia-extensao', this).val();
		var midia_desc     = $('.edit-midia-desc', this).val();

		$.ajax({
			url: 'painel/midias/midia_data_save',
			data: {id: midia_id, titulo: midia_titulo, slug: midia_slug, extensao: midia_extensao, descricao: midia_desc},
			type: 'POST',
			dataType: 'json',
			beforeSend: function(){
				$('#confirm').modal('hide');
        		$('#feedback').html('Carregando...').feedback('show');
			},
			success: function(response){
				if(response.status == 200){
					$('#feedback').html('Realizado com sucesso!').feedback('hide', 200);
				}
				else{
					$('#feedback').html('Ops! Ocorreu algo.').feedback('hide', 0);
				}
			},
			error: function(response){
				$('#feedback').html('Carregando...').feedback('hide', 0);
			}
		});

		return false;
	});

	$("#dropdownFiltro .dropdown-item").bind("click", function(){
		
		$("#arquivos-tab").trigger("click");

		var filtro = $(this).attr("data-value");
		$(".midia-filtro").attr("data-filter", filtro);

		$("#midia-lista").midia_lista();
		$("#dropdownFiltro .dropdown-item").removeClass('d-none');
		$(this).addClass('d-none');

		$(".midia-filtro").html( $(this).html() );

		return false;
	});

	$(".midia-buscar").bind("keyup", function(){
		$("#arquivos-tab").trigger("click");
		$("#midia-lista").midia_lista();
		return false
	});

	$(".midia-box.multiple").sortable({
		stop: function( event, ui ) {
			multiple = 'multiple';
			$(this).set_galeria();
		}
	});

});

$.fn.extend({
	lazyload: function(){
		if($("#midia-lista .midia-item.lazyload").first().length > 0){

			var scroll_top = $("#midia-lista").scrollTop() + $('#midia-lista').height();
			var midia_top  = $(".midia-item.lazyload").first().offset().top;
			var footer_top = ($(".midia-footer").is(':visible')) ? $(".midia-footer").offset().top : $(window).height() - 57;

			if(midia_top < footer_top || scroll_top <= 0){

				var midia_item = $("#midia-lista .midia-item.lazyload").first();
				var tipo_item  = midia_item.closest('.midia-list-item');

				if(tipo_item.hasClass("img")){

					var midia_data = midia_item.find('.midia-data');
					var lazy_item  = midia_data.attr('data-url');

					var img = new Image();
					img.onload = function() {
						midia_item.css({'background-image': 'url('+this.src+')'});
						midia_item.removeClass('lazyload');
						$("#midia-lista").lazyload();

					}
					img.src = lazy_item;
				}
				else{
					midia_item.removeClass('lazyload');
					$("#midia-lista").lazyload();
				}
			}
		}
	},
	set_galeria: function(){

		var midia_box_id = '#'+$(this).attr('id');

		var target_input = midia_box_id.replace("-load", "");
		var each_midia   = $(midia_box_id + " .midia-list");				

		var checks = [];
		each_midia.each(function(){
			checks.push($(this).data("midia"));
		});

		checks = (multiple == "multiple") ? JSON.stringify(checks) : checks[0];

		console.log(checks);

		$(target_input).val(checks);
	},
	return_midia: function(){		

		var checks = [];
		$(this).each(function(){
			checks.push($(this).data("midia"));
		});
		checks = JSON.stringify(checks);

		return checks;
	},
	midia_active: function(checks){

		if(checks){
			checks = JSON.parse(checks);
			checks = (typeof checks == 'object') ? checks.reverse() : [checks];

			checks.forEach(function(element, index, array) {

				console.log("a[" + index + "] = " + element);

				var midia_first = $('#midia-item-'+element);

				midia_first.addClass('clone');
				midia_first.clone().prependTo("#midia-lista");
				midia_first.remove();
			});
		}
	},
	midia_lista: function(){

		$("#midia-lista").html('<div class="loader loader-primary"></div>');

		var checks = $(target).val();
		var filtro = $(".midia-filtro").attr("data-filter");
		var buscar = $(".midia-buscar").val();

		$.post('painel/midias/lista', { checks: checks, filtro: filtro, buscar: buscar }, function (data) {
			$("#midia-lista").html(data);

			if(data == "fail"){
				console.log(data);
				$("#midia-lista").html("<h2 class='header-title p-4'>Sem resultados de mídia.</h2>");
			}

			$(this).midia_active(checks);
			$(this).lazyload();
		});
	},
	midia_modal: function(action){

		if(action == 'show'){
			$('body, html').css('overflow', 'hidden');
			$("#midia").fadeIn(250);			
			$(document).off('focusin.modal');
		}
		else{
			$("#midia").fadeOut(250, function(){
				$('body, html').css('overflow', 'visible');
				$("#midia-lista").html("");
			});
		}
	}
});