$(document).ready(function(){

	$('body').on('click', '[data-sublista]', function(){

		var item  = $(this).attr('data-item');
		var slug  = $(this).attr('data-sublista');
		var form  = (item == undefined) ? "#form-" + slug : "#form-" + item;
		var lista = ".sublista-" + slug;
		var post_tipo = $('#tipo').val();
        var serialize = $(this).attr('data-serialize');

		var post = $.fn.serializeForm(form, serialize);

		post['post_tipo'] = post_tipo;
		post['post_pai']  = $('#id').val();
		post['cod_area']  = $('#cod_area').val();
		post['item_tipo'] = slug;
		post['item_id']   = $('.item_id', form).val();
		post['titulo']    = $("input[name='titulo_sublista']", form).val();

		if(post['titulo'] != ""){

			var custom = {};
			if($(".custom-field", form).length > 0){
				$(".custom-field", form).each(function(){
					var name  = $(this).attr('name');
					var value = ($(this).hasClass('get-html')) ? $(".ql-editor", this).html() : $(this).val();
						custom[name] = value;
				});
			}

			$.ajax({
		        url: "painel/sublistas/salvar_subitem",
		        type: 'POST',
		        dataType: 'json',
		        data: {post: post, custom: custom},
		        beforeSend: function(){
		    		$('#feedback').html('Carregando...').feedback('show');
		        },
		        success: function(response){

					if(response.status == 200){
						$('.modal').modal('hide');

						$('#id').val(response.item.cod_pai);
						setTimeout(function(){
							$.fn.sublista_load(lista, function(){
								$("#form-" + slug).reset_form();
								$('#feedback').html('Item inserido!').feedback('hide', 200);								
							});
						}, 100);							
					}
					else{
						$('#feedback').html('Ops! Algo deu errado...').feedback('hide', 0);
					}
					$(this).validate(response);
		        },
		        error: function(response){
		        	$('.modal').modal('hide');
		        	$('#feedback').html('Ops! Algo deu errado...').feedback('hide', 0);
		        }
		    });
		}
		else{
			console.log(form);
			$("input[name='titulo_sublista']", form).addClass('is-invalid');
		}		
	});

	$('body').on('click', '[data-subitem-excluir]', function(){

		var item   = $(this).attr("data-subitem-excluir");
		var target = $(this).closest('[data-sublista-load]');

		$.ajax({
	        url: "painel/sublistas/deleta_subitem/"+item,
	        type: 'POST',
	        dataType: 'json',
	        data: {item: item},
	        beforeSend: function(){
	    		$('#feedback').html('Carregando...').feedback('show');
	        },
	        success: function(response){
				if(response.status == 200){
					$('#feedback').html('Item excluído!').feedback('hide', 200);
					$(this).sublista_load(target);
				}
				else{
					$('#feedback').html('Ops! Algo deu errado...').feedback('hide', 0);
				}
	        },
	        error: function(response){
	        	$('#feedback').html('Ops! Algo deu errado...').feedback('hide', 0);
	        }
	    });

		return false;	
	});

	$('[data-sublista-load]').each(function(){
		$(this).sublista_load(this);
	});

	$('[data-sublista-load]').sortable({		
		handle: '.handle-move',
		stop: function( event, ui ){

			var ordem = [];
			$('.subitem', this).each(function(index, key){
				var item = $(this).data('item');
				ordem.push(item);
			});

			$.post("painel/sublistas/reordena_subitem", {ordem: ordem});
		}
	});

	$('#nav-sublistas').sortable({
		items: ".nav-item:not(.unsortable)",
		handle: '.btn-move-sublist',
		stop: function( event, ui ){

			var ordem = [];
			$('.nav-item:not(.unsortable)', this).each(function(index, key){
				var item = $(this).data('item');
				ordem.push(item);
			});

			$.post("painel/sublistas/reordena_sublista", {ordem: ordem});
		}
	});
});

$.fn.extend({
	sublista_load: function(target, callback){

		var sublista = $(target).attr('data-sublista-load');

		var tipo      = sublista;
		var cod_pai   = $('#id').val();
		var post_tipo = $('#tipo').val();
		var cod_area  = $('#cod_area').val();

		$(target).load( "painel/sublistas/sublista", {post: {cod_area: cod_area, cod_pai: cod_pai, tipo: tipo, post_tipo: post_tipo}}, function(){
			$().select_icon();
			$('[data-toggle="select"]').Select2();
			$('[data-toggle="quill"]').Quill();
			if(callback != undefined) callback();
		});
	},
	reset_form: function(){
		var form = $(this);

		$('.custom-field, input', form).val("");

		$('.custom-field[data-toggle="quill-loaded"]', form).find('.ql-editor').html('');

		$('select.custom-field', form).each(function(){
			$('option:first', this).prop('selected', true);

			$(this).Select2({
	            minimumResultsForSearch: -1
	        });
		});

		$('.midia-list', form).remove();
	}
});