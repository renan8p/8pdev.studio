$(document).ready(function(){

	//  Reordena areas
	$('#reorder-social').sortable({
		handle: '.btn-move',
		stop: function(){ save(); }
	});

	$('.btn-inserir-social').bind('click', function(){

		var link = $('#link').val();
		var plataforma = $('#plataforma').val();

		if(link != ""){
			$('#reorder-social h2').remove();

			$('#link').val("");

			$(".to-copy").find('.social-link').text(link);
			$(".to-copy").find('.social-plataforma').text(plataforma);

			$(".to-copy").clone().appendTo("#reorder-social");
			$("#reorder-social li").last().removeClass("to-copy opacity-0");

			save();
		}

	});

	$("#reorder-social").on('click', '.btn-remove', function(){
		$(this).closest("li").remove();
		save();
	});

	function save(){
		var itens = [];
		$("#reorder-social li").each(function(index, key){
			var link = $(this).find('.social-link').text();
			var plataforma = $(this).find('.social-plataforma').text();
			itens.push({'plataforma': plataforma, 'link': link});
		});

		$.post("painel/configuracoes/salvar_social", {itens: itens});
	}

});