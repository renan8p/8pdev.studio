$(document).ready(function(){

	var insert_in = "";

	$("body").on("click", '.btn-field', function(){
		insert_in = $(this).attr('data-insert');
	});

	// Chamada para tipo de campo a ser inserido	
	$(".field-insert").bind("click", function(){

		var field = $(".custom-list .field").length;
		var tipo  = $(this).data("tipo");
		var bloco = (insert_in == "#custom-bloco") ? 'true' : 'false';

		$.post( "painel/cms/custom_fields", {field: field, tipo: tipo, bloco: bloco}, function( html ) {
			$(insert_in).append(html);

			$('.add-tags').tagsInput({'defaultText':'value:option'});			
			$('.custom-list').sortable({handle: '.btn-move'});
		});
	});

	$('[data-submit-fields]').bind('click', function(){

		var father   = $(this).attr('data-submit-fields');
		var validate = true;
		var area       = $('#area').val();

		var label      = $("[name='label_titulo']", father).val();
		var controller = $("[name='controller']", father).val();

		var fields = [];
		var post = {};

		$("form.field", father).each(function(){

			var post = {};
			$(".custom-field", this).each(function(){
				var name  = $(this).attr('name');
				var value = $(this).val().toString();
					post[name] = value;
			});

			if(post['label'] == ""){
				$(this).parent().addClass('is-invalid');
				validate = false;
			}

			fields.push(post);
		});

		if(validate){

			$.ajax({
	            url: "painel/cms/custom_salvar",
	            type: 'POST',
	            dataType: 'json',
	            data: {area: area, fields: fields, controller: controller, label: label},
	            beforeSend: function(){
	        		$('#feedback').html('Carregando...').feedback('show');
	            },
	            success: function(response){
					if(response.status == 200){
						$('#feedback').html('Publicado com sucesso!').feedback('hide', 200, true);
					}
					else{
						$('#feedback').html('Ops! Ocorreu algo.').feedback('hide', 0);
					}
	            },
	            error: function(response){
	            	$('#feedback').html('Carregando...').feedback('hide', 0);
	            }
	        });			
		}

		return false;
	});

	$(".form-custom-fields").on('keydown', 'input', function(){
		$(this).closest(".custom-field-control").removeClass('is-invalid');
		$(this).removeClass('is-invalid');
	});

	$('.add-tags').tagsInput({'defaultText':'value:option'});

	$('.custom-list').sortable({handle: '.btn-move'});
});