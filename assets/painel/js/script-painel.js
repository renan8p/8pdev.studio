$(document).ready(function($) {

	$("body").on('click', '.confirm', function(){

		var target = $(this).attr('data-target');
		var remove = $(this).attr('data-remove');

		console.log(target, remove);

		$('#confirm').modal('show');
		$("#confirm .btn-confirm").data('confirm', target);
		$("#confirm .btn-confirm").data('remove', remove);

		return false;
	});

	$("#confirm .btn-confirm").bind('click', function(){

		var target = $(this).data('confirm');
		var remove = $(this).data('remove');		
		var post   = $(this).data('post');
		    post   = (post != "") ? {post: $(this).data('post')} : {};

		$('#confirm').modal('hide');

		if(target != "false"){
			$.ajax({
				url: target,
				data: post,
				type: 'POST',
				dataType: 'json',
				beforeSend: function(){
					$('#confirm').modal('hide');
	        		$('#feedback').html('Carregando...').feedback('show');
				},
				success: function(response){
					if(response.status == 200){
						console.log(remove);
						$(remove).remove();
						$('#feedback').html('Realizado com sucesso!').feedback('hide', 200, response.url);
					}
					else{
						var msg = (response.msg != "") ? response.msg : 'Ops! Ocorreu algo.';
						$('#feedback').html(msg).feedback('hide', 0);
					}
				},
				error: function(response){
					var msg = (response.msg != "") ? response.msg : 'Carregando...';
					$('#feedback').html(msg).feedback('hide', 0);
				}
			});
		}
		else{
			$(remove).remove();
		}
	});

	$(".btn-action").bind('click', function(){

		var target = $(this).data('target');
		var remove = $(this).data('remove');

		$.ajax({
			url: target,
			data: {},
			type: 'POST',
			dataType: 'json',
			beforeSend: function(){
				$(this).attr("disable", true);
        		$('#feedback').html('Carregando...').feedback('show');
			},
			success: function(response){
				if(response.status == 200){
					$(remove).remove();
					$('#feedback').html('Realizado com sucesso!').feedback('hide', 200);
				}
				else{
					var msg = (response.msg != "") ? response.msg : 'Ops! Ocorreu algo.';
					$('#feedback').html(msg).feedback('hide', 0);
				}
			},
			error: function(response){
				var msg = (response.msg != "") ? response.msg : 'Carregando...';
				$('#feedback').html(msg).feedback('hide', 0);
			}
		});
	});

	$("#acao-coletiva button.colecao").bind('click', function(){

		var target = $(this).data('target');
		var confim = $(this).data('confirm');

		var colecao = [];
		$('td .custom-checkbox input[type="checkbox"]:checked').each(function(){
			var myJSON = $(this).val();
			var myObj  = JSON.parse(myJSON);

			colecao.push(myObj);
		});

		if(confim == true){
			$('#confirm').modal('show');
			$("#confirm .btn-confirm").data('confirm', target);
			$("#confirm .btn-confirm").data('post', colecao);
		}
		else{

			$.ajax({
				url: target,
				data: {colecao: colecao},
				type: 'POST',
				dataType: 'json',
				beforeSend: function(){
					$(this).attr("disable", true);
	        		$('#feedback').html('Carregando...').feedback('show');
				},
				success: function(response){
					if(response.status == 200){
						$('#feedback').html('Realizado com sucesso!').feedback('hide', 200, response.url);
					}
					else{
						var msg = (response.msg != "") ? response.msg : 'Ops! Ocorreu algo.';
						$('#feedback').html(msg).feedback('hide', 0);
					}
				},
				error: function(response){
					var msg = (response.msg != "") ? response.msg : 'Carregando...';
					$('#feedback').html(msg).feedback('hide', 0);
				}
			});
		}
	});

	$( ".formulario" ).submit(function(){

        var url       = $(this).attr('action');
        var serialize = $(this).attr('data-serialize');
        var post      = $.fn.serializeForm(this, serialize);

		var custom = {};
		if($(".custom-field", this).length > 0){
			$(".custom-field", this).each(function(){
				var name  = $(this).attr('name');
				var value = ($(this).hasClass('get-html')) ? $(".ql-editor", this).html() : $(this).val();
					custom[name] = value;
			});
		}

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: {post: post, custom: custom},
            beforeSend: function(){
        		$('#feedback').html('Carregando...').feedback('show');
            },
            success: function(response){
				if(response.status == 200){
					var msg = (response.msg != undefined) ? response.msg : 'Publicado com sucesso!';
					$(".formulario #id").val(response.id);
					$('#feedback').html(msg).feedback('hide', 200, response.url);
				}
				else{
					var msg = (response.msg != undefined) ? response.msg : 'Ops! Algo deu errado...';
					$('#feedback').html(msg).feedback('hide', 0);
				}
				$(this).validate(response);
            },
            error: function(response){
            	$('#feedback').html('Ops! Algo deu errado...').feedback('hide', 0);
            }
        });

        return false;
	});

	$("[data-reset-form]").on("click", function(){
		$(".form-control").removeClass('is-invalid');
	});

	$('body').on("keydown change blur", ".form-control[data-required='true'], .is-invalid", function(){

		if($(this).val() != ""){
			$(this).removeClass('is-invalid');
		}
		else{
			$(this).addClass('is-invalid');
		}
	});

	// plugin icon
	$().select_icon();

	$("#mensagens tr.new, .status-mensagem").bind('click', function(){
		$(this).removeClass('new');

		var ref = $(this).attr('data-ref');
		var data = {post: {status: 1}};

		if($(this).hasClass('btn')){
			status = $('.form-mensagem-'+ref+' select').val();
			data = {post: {status: status}};
			$('#mensagem-'+ref).modal('toggle');
		}

		$.post( "painel/dashboard/mensagem/"+ref, data, function( data ) {			
			if(data.status == 200){
				$('tr[data-ref="'+data.item+'"] .item-status span.badge').removeClass('badge-soft-success badge-soft-info badge-soft-warning badge-soft-danger badge-soft-light badge-soft-dark').addClass(data.badge).html(data.texto);
				if(data.badge == 'badge-soft-dark') $('tr[data-ref="'+data.item+'"]').remove();
			}
		}, "json");
	});

	//  Reordena areas
	$('.reorder-area').sortable({
		stop: function( event, ui ){

			var ordem = [];
			$('li.card', this).each(function(index, key){
				var item = $(this).data('item');
				ordem.push(item);
			});

			$.post("painel/cms/reorder", {ordem: ordem});
		}
	});

	var placeholder = $('.input-tags').attr('placeholder');
	$('.input-tags').tagsInput({'defaultText': placeholder});
});

$.fn.extend({
	serializeForm: function(form, serialize){

		if (serialize == undefined){
			var post = {};
			var serialize = $('input:not(.custom-field), select:not(.custom-field), textarea:not(.custom-field)', form).serializeArray();

			$.each(serialize, function(i, field){
				post[field.name] = field.value;
			});
		}
		else{
			var post = $(form).serialize();
		}

		return post;
	},
	feedback: function(action, callback, refresh){
		$("button[type=submit]").prop('disabled', true);
		if(action == 'hide'){

			if(callback == 200){
				$('#feedback').addClass('check');
			}
			else{
				$('#feedback').addClass('erro');
			}

			setTimeout(function(){
				$('#feedback').removeClass();
				$("button[type=submit]").prop('disabled', false);

				if(typeof refresh !== 'undefined'){
					if(typeof(refresh) === "boolean"){
						location.reload();
					}
					else{
						window.location.href = refresh;
					}
				}

			}, 3000);
		}
		else{
			$('#feedback').removeClass('check erro').addClass('visible load');
		}
	},
	validate: function(response){		

		var fails    = "";
		var feedback = "";

		if(fails != undefined){		
			fails = response.fails;
		}

		$('.invalid-feedback').remove();
		$('.form-control').removeClass('is-invalid');		

		$('.form-control').each(function(index, el) {

			if($(this).attr("data-required") == "true"){

				var value    = $(this).val();
				var name     = $(this).attr('name');
				var type     = $(this).attr('type');
				var validate = $(this).attr('data-required');				

				if(fails != undefined){	
					fails.forEach(function(currentValue, ind, arr){
						if(fails[ind][name] != undefined){
							console.log( fails[ind][name] );
							$('[name="'+name+'"]').addClass('is-invalid').after('<small class="invalid-feedback">'+fails[ind][name]+'</small>');
						}
					});
				}

				if(validate == 'true' && value == ""){
					$(this).addClass('is-invalid');
				}

				if(type == "email"){
					var regra = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
					if(!regra.test(value)){
						$(this).addClass('is-invalid');
					}
				}

				if($(this).attr("data-pass") != undefined){

					var pass_repeat = $(this).attr("data-pass");

					if($(this).val() == $(pass_repeat).val() && $(this).val() != "" && $(this).val().length > 5){
						$(this).removeClass('is-invalid input-true');
						$(pass_repeat).removeClass('is-invalid input-true');
					}
					else{
						$(pass_repeat).addClass('is-invalid');
						$(this).addClass('is-invalid');
					}
				}
			}
		});

		return ($('.is-invalid').length > 0) ? false : true;
	},
	select_icon: function(){		
		if($(".select-icon").length > 0){
			var icons = ["fe fe-activity", "fe fe-airplay", "fe fe-alert-circle", "fe fe-alert-octagon", "fe fe-alert-triangle", "fe fe-align-center", "fe fe-align-justify", "fe fe-align-left", "fe fe-align-right", "fe fe-anchor", "fe fe-aperture", "fe fe-archive", "fe fe-arrow-down", "fe fe-arrow-down-circle", "fe fe-arrow-down-left", "fe fe-arrow-down-right", "fe fe-arrow-left", "fe fe-arrow-left-circle", "fe fe-arrow-right", "fe fe-arrow-right-circle", "fe fe-arrow-up", "fe fe-arrow-up-circle", "fe fe-arrow-up-left", "fe fe-arrow-up-right", "fe fe-at-sign", "fe fe-award", "fe fe-bar-chart", "fe fe-bar-chart-2", "fe fe-battery", "fe fe-battery-charging", "fe fe-bell", "fe fe-bell-off", "fe fe-bluetooth", "fe fe-bold", "fe fe-book", "fe fe-book-open", "fe fe-bookmark", "fe fe-box", "fe fe-briefcase", "fe fe-calendar", "fe fe-camera", "fe fe-camera-off", "fe fe-cast", "fe fe-check", "fe fe-check-circle", "fe fe-check-square", "fe fe-chevron-down", "fe fe-chevron-left", "fe fe-chevron-right", "fe fe-chevron-up", "fe fe-chevrons-down", "fe fe-chevrons-left", "fe fe-chevrons-right", "fe fe-chevrons-up", "fe fe-chrome", "fe fe-circle", "fe fe-clipboard", "fe fe-clock", "fe fe-cloud", "fe fe-cloud-drizzle", "fe fe-cloud-lightning", "fe fe-cloud-off", "fe fe-cloud-rain", "fe fe-cloud-snow", "fe fe-code", "fe fe-command", "fe fe-compass", "fe fe-copy", "fe fe-corner-down-left", "fe fe-corner-down-right", "fe fe-corner-left-down", "fe fe-corner-left-up", "fe fe-corner-right-down", "fe fe-corner-right-up", "fe fe-corner-up-left", "fe fe-corner-up-right", "fe fe-cpu", "fe fe-credit-card", "fe fe-crop", "fe fe-crosshair", "fe fe-database", "fe fe-delete", "fe fe-disc", "fe fe-dollar-sign", "fe fe-download", "fe fe-download-cloud", "fe fe-droplet", "fe fe-edit", "fe fe-edit-2", "fe fe-edit-3", "fe fe-external-link", "fe fe-eye", "fe fe-eye-off", "fe fe-facebook", "fe fe-fast-forward", "fe fe-feather", "fe fe-file", "fe fe-file-minus", "fe fe-file-plus", "fe fe-file-text", "fe fe-film", "fe fe-filter", "fe fe-flag", "fe fe-folder", "fe fe-folder-minus", "fe fe-folder-plus", "fe fe-gift", "fe fe-git-branch", "fe fe-git-commit", "fe fe-git-merge", "fe fe-git-pull-request", "fe fe-github", "fe fe-gitlab", "fe fe-globe", "fe fe-grid", "fe fe-hard-drive", "fe fe-hash", "fe fe-headphones", "fe fe-heart", "fe fe-help-circle", "fe fe-home", "fe fe-image", "fe fe-inbox", "fe fe-info", "fe fe-instagram", "fe fe-italic", "fe fe-layers", "fe fe-layout", "fe fe-life-buoy", "fe fe-link", "fe fe-link-2", "fe fe-linkedin", "fe fe-list", "fe fe-loader", "fe fe-lock", "fe fe-log-in", "fe fe-log-out", "fe fe-mail", "fe fe-map", "fe fe-map-pin", "fe fe-maximize", "fe fe-maximize-2", "fe fe-menu", "fe fe-message-circle", "fe fe-message-square", "fe fe-mic", "fe fe-mic-off", "fe fe-minimize", "fe fe-minimize-2", "fe fe-minus", "fe fe-minus-circle", "fe fe-minus-square", "fe fe-monitor", "fe fe-moon", "fe fe-more-horizontal", "fe fe-more-vertical", "fe fe-move", "fe fe-music", "fe fe-navigation", "fe fe-navigation-2", "fe fe-octagon", "fe fe-package", "fe fe-paperclip", "fe fe-pause", "fe fe-pause-circle", "fe fe-percent", "fe fe-phone", "fe fe-phone-call", "fe fe-phone-forwarded", "fe fe-phone-incoming", "fe fe-phone-missed", "fe fe-phone-off", "fe fe-phone-outgoing", "fe fe-pie-chart", "fe fe-play", "fe fe-play-circle", "fe fe-plus", "fe fe-plus-circle", "fe fe-plus-square", "fe fe-pocket", "fe fe-power", "fe fe-printer", "fe fe-radio", "fe fe-refresh-ccw", "fe fe-refresh-cw", "fe fe-repeat", "fe fe-rewind", "fe fe-rotate-ccw", "fe fe-rotate-cw", "fe fe-rss", "fe fe-save", "fe fe-scissors", "fe fe-search", "fe fe-send", "fe fe-server", "fe fe-settings", "fe fe-share", "fe fe-share-2", "fe fe-shield", "fe fe-shield-off", "fe fe-shopping-bag", "fe fe-shopping-cart", "fe fe-shuffle", "fe fe-sidebar", "fe fe-skip-back", "fe fe-skip-forward", "fe fe-slack", "fe fe-slash", "fe fe-sliders", "fe fe-smartphone", "fe fe-speaker", "fe fe-square", "fe fe-star", "fe fe-stop-circle", "fe fe-sun", "fe fe-sunrise", "fe fe-sunset", "fe fe-tablet", "fe fe-tag", "fe fe-target", "fe fe-terminal", "fe fe-thermometer", "fe fe-thumbs-down", "fe fe-thumbs-up", "fe fe-toggle-left", "fe fe-toggle-right", "fe fe-trash", "fe fe-trash-2", "fe fe-trending-down", "fe fe-trending-up", "fe fe-triangle", "fe fe-truck", "fe fe-tv", "fe fe-twitter", "fe fe-type", "fe fe-umbrella", "fe fe-underline", "fe fe-unlock", "fe fe-upload", "fe fe-upload-cloud", "fe fe-user", "fe fe-user-check", "fe fe-user-minus", "fe fe-user-plus", "fe fe-user-x", "fe fe-users", "fe fe-video", "fe fe-video-off", "fe fe-voicemail", "fe fe-volume", "fe fe-volume-1", "fe fe-volume-2", "fe fe-volume-x", "fe fe-watch", "fe fe-wifi", "fe fe-wifi-off", "fe fe-wind", "fe fe-x", "fe fe-x-circle", "fe fe-x-square", "fe fe-youtube", "fe fe-zap", "fe fe-zap-off", "fe fe-zoom-in", "fe fe-zoom-out"];
			var box_icons = "";
			icons.forEach(function(icon){
				box_icons += "<span class='item-icon' data-icon='"+icon+"'><i class='"+icon+"'></i></span>";
			});

			$(".select-icon").after("<div class='select-icon-box'>"+box_icons+"</div>");

			$(".select-icon-box").on("click", ".item-icon", function(){
				var icon = $(this).data('icon');
				$(".select-icon").val(icon);
			});

			$(".select-icon").focus(function(){
				$(".select-icon-box").addClass("active");
			});

			$('body').click(function(event){
				var elem1 = $(event.target).closest(".select-icon-box").length;
				var elem2 = $(event.target).closest(".select-icon").length;
			    if ((elem1 == 1 && elem2 == 0) || (elem1 == 0 && elem2 == 0)) {
			        $(".select-icon-box").removeClass("active");
			    }
			});
		}
	}
});