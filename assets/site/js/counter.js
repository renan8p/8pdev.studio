const saleDate = new Date($('.timer').data('time')).getTime();
var timerHours = $('.timer').find('.hours strong');
var timerMinutes = $('.timer').find('.minutes strong');
var timerSeconds = $('.timer').find('.seconds strong');

// countdown
function timer(){
  // get today's date
  const today = new Date().getTime();

  // get the difference
  const diff = saleDate - today;

  // math
  let days = Math.floor(diff / (1000 * 60 * 60 * 24));
  //let hours = Math.floor((diff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  let hours = Math.floor((diff % (1000 * 60 * 60 * 24 * 24)) / (1000 * 60 * 60)); // Added the quantity of days in hours
  let minutes = ('0' + Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60))).slice(-2);
  let seconds = ('0' + Math.floor((diff % (1000 * 60)) / 1000)).slice(-2);
  // can't use slice cause it can be more than 100
  if (hours < 10) {
    hours = '0'+hours;
  }

  // if sale has less than 1sec or ended
  if(diff <= 999){
    days = '00';
    hours = '00';
    minutes = '00';
    seconds = '00';
    $('.timer').addClass('inactive');
    $('.btn-sale').addClass('inactive').removeAttr('href');
  }

  // display
  timerHours.html(hours);
  timerMinutes.html(minutes);
  timerSeconds.html(seconds);
  //console.log(days, hours, minutes, seconds);
}

timer();
setInterval(timer, 1000);