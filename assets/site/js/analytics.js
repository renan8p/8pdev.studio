$(document).ready(function(){
    setVisita();

    function setVisita(){

        var visit = {
            codigo: getCodigo(),
            data: getData(),
            hora: getHorario(),
            dia: getDia(),
            mes: getMes(),
            ano: getAno(),
            url: getUrl(),
            pagina: getPagina(),
            navegador: getNavegador(),
            cidade: getCidade(),
            estado: getEstado(),
            pais: getPais(),
            sistema: getSistema(),
            dispositivo: getDispositivo()
        }

        $.ajax({
            url: "async/analytics",
            data: visit,
            type: 'POST',
            dataType: 'json',
            success: function(response){
                console.log(response);
            },
            error: function(response){

            }
        });
    }

    function getCodigo(){

        if(getCookie("visitante") == "") {
            codigo = Math.random().toString(36).substr(2, 9);
            setCookie("visitante", codigo);
        }

        return getCookie("visitante");
    }

    function getData(){

        return getAno()+"-"+getMes()+"-"+getDia();
    }

    function getHorario(){

        var data = new Date( new Date().getTime() + -2 * 3600 * 1000 ).toUTCString();
            data = data.split(" ");

        return data[4];
    }

    function getHora(value){

        var data = getHorario();
            data = data.split(":");

        data = data[0];
        data = (value == "minuto")  ? data[1] : data;
        data = (value == "segundo") ? data[2] : data;

        return data;
    }

    function getDia(){

        var data = new Date();
        dia = data.getDate();
        dia = (dia < 10) ? '0'+dia : dia;

        return dia;
    }

    function getMes(){

        var data = new Date();

        return data.getMonth() + 1;
    }

    function getAno(){

        var data = new Date();

        return data.getFullYear();
    }

    function getUrl(){

        var pagina = window.location;

        return pagina.href;
    }

    function getPagina(){

        var pagina = document.title;

        return pagina;
    }

    function getNavegador(){
        return navigator.userAgent
    }

    function getCidade(){
        return null;
    }

    function showPosition(position) {
      return "Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude;
    }

    function getEstado(){
        return null;
    }

    function getPais(){
        return navigator.language;
    }

    function getSistema(){
        return navigator.platform;
    }

    function getDispositivo(){

        var dispositivo = false;
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            dispositivo = true;
        }

        return dispositivo;
    }

    function getCookie(cname) {
      var name = cname + "=";
      var decodedCookie = decodeURIComponent(document.cookie);
      var ca = decodedCookie.split(';');
      for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }
      return "";
    }

    function setCookie(cname, cvalue) {
        var exdays = 45;
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/" + ";" + "SameSite=Lax";
    }
});