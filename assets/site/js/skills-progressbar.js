
$(window).scroll(function(event){
    if ($('.slick-team').hasClass('active')) {
    	setTimeout(moveProgressBar,800);
    }
});

// TEAM SKILLS PROGRESS
function moveProgressBar() {
    var progressBarWrap = $('.skills'),
        progressBars = progressBarWrap.find('.progress-bar');

    progressBars.each(function(index, el) {
        var progressBarPercentage = $(el).data('percentage');
        $(el).find('span').css({width: progressBarPercentage + '%'});
    });
}