// Hide header on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 5;
var header = $('#topo')
var navbarHeight = 0; //header.outerHeight();

$(window).scroll(function(event){
    didScroll = true;
    //hasScrolled();
});

setInterval(function() {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();
    // Make scroll more than delta
    if(Math.abs(lastScrollTop - st) <= delta)
        return;
    // If scrolled down and past the navbar, add class .nav-up.
    if (st > lastScrollTop && st > navbarHeight){
        // Scroll Down
        header.removeClass('nav-down').addClass('nav-up');
    } else {
        // Scroll Up
        if(st + $(window).height() < $(document).height()) {
            header.removeClass('nav-up').addClass('nav-down');
        }
    }
    if(st >= 2){
        // Scroll not at page top
        header.addClass('nav-fixed');
    }
    else{
        // Scroll at page top
        header.removeClass('nav-fixed');
    }
    lastScrollTop = st;
}