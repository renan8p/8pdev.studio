$(window).on('load', function () {
  // Page preloader
  setTimeout(function() {
    $('#jsPreloader').fadeOut(800)
  }, 400)

  // initialization of svg injector module
  $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
});

$(document).ready(function() {
  setTimeout(function(){
    $('[data-whatsapp-number]').attr('data-number', '1');
  }, 1000 * 8);
  setTimeout(function(){
    $('[data-whatsapp-number]').attr('data-number', '2');
  }, 1000 * 10);
  setTimeout(function(){
    $('[data-whatsapp-number]').attr('data-number', '3');
    var old_title = document.title;
    setInterval(function(){
      if (document.title == old_title) {
        document.title = '(3) Novas Mensagens - ' + old_title;
      }
      else{
        document.title = old_title;
      }
    },2000);
  }, 1000 * 11);
  
  if (Cookies.get('beforeUnloadModal') == null) {
    setTimeout(function(){
      var count = 0,
          target = $('#beforeUnloadModal'),
          timeoutId;

      window.addEventListener('mousemove', function(e) {
        if(timeoutId) clearTimeout(timeoutId);
        timeoutId = setTimeout(function() {
          if (e.clientY < 10 && !count) {
            count++;
            target.modal();
            Cookies.set('beforeUnloadModal', 'true', {expires: 7, domain: '8pdev.studio', sameSite: 'lax'});
          }
        }, 10);
      });
    }, 1000 * 15);
  }

  // initialization of header
  $.HSCore.components.HSHeader.init($('#header'));

  // initialization of form validation
  $.HSCore.components.HSValidation.init('.js-validate');

  // initialization of show animations
  $.HSCore.components.HSShowAnimation.init('.js-animation-link');

  // initialization of animation
  $.HSCore.components.HSOnScrollAnimation.init('[data-animation]');

  // initialization of go to
  $.HSCore.components.HSGoTo.init('.js-go-to');

  $('input[type="tel"]').focusout(function(){
      var phone, element;
      element = $(this);
      element.unmask();
      phone = element.val().replace(/\D/g, '');
      if(phone.length > 10) {
          element.mask("(99) 99999-999?9");
      } else {
          element.mask("(99) 9999-9999?9");
      }
  }).trigger('focusout');

  // Nav link active
  var pathname = window.location.pathname,
      page = pathname.substring(pathname.lastIndexOf('/') + 1).substring(pathname.lastIndexOf('#') + 1),
      navLinks = $('#header .nav-link');
      
  navLinks.removeClass('active');
  if(page == ''){
    $(navLinks[0]).addClass('active');
  }
  else{
    $(navLinks).each(function(index, el) {
      if (page.includes($(el).attr('href').replace('.php',''))) {
        $(el).addClass('active');
      }
    });
  }

  // Title
  $('.title-theme').each(function(index, el) {
    var text = $(el).text(),
        hasDataHighlight = $(el).data('title-highlight') > 0,
        hasHighlight = text.split('[')[1];
    
    if(hasDataHighlight){
      var dataHighlight = $(el).data('title-highlight'),
          textDefault = text.split(' '),
          textHighlight = text.split(' ').splice(-1 * dataHighlight,dataHighlight);
      
      for(var i = 0; i < dataHighlight; i++){
        textDefault.pop();
      }

      $(el).html(textDefault.join(' ') + ' <span class="text-primary">' + textHighlight.join(' ') + '</span>');
    }
    else if(hasHighlight){
      var textDefault = text.split('[')[0],
          textHighlight = text.split('[').pop().split(']')[0];
      $(el).html(textDefault + '<span class="text-primary">' + textHighlight + '</span>');
    }

  });

  // SMOOTH SCROLLING
  var scrollOffset = 72;
  $('a.scroll-link').on('click', function(e) {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top - scrollOffset //ALTURA DO TOPO
        }, 800);
        return false;
      }
    }
  });

  // Stick in parent
  if($('.fixed').length > 0 && $(window).width() > 992){
    $('.fixed').stick_in_parent({offset_top: 128});
  }

  $('.lead-form').on('submit', function(){

    var form = $( this );    
    var data = form.serialize();

    var val = $('[required]', this).validation();

    if(val){
      $.ajax({
        url: "async/lead",
        data: data,
        type: 'POST',
        dataType: 'json',
        beforeSend: function(){
          form.find('button[type="submit"]').html('<span class="spinner-border spinner-border-sm"></span>');
        },
        success: function(response){
          if(response.status == 200){
            $('.modal').modal('hide');
            $('#feedbackModal').modal('show');
            form.find('button').html('Pronto!');
            form.trigger("reset");
          }
          else{
            console.log(response.msg);
            form.find('button').html('Tente novamente');
          }
        },
        error: function(){
          form.find('button').html('Tente novamente');
        }
      });
    }

    return false;
  });

  $('[data-tipo-projeto]').bind('click', function(){
    var tipo = $(this).attr('data-tipo-projeto');    

    $('input[name="nome_pedido"]').focus();
    $(".tipo_projeto option[value='"+tipo+"']").attr('selected', 'selected');    
  });

  $('form[name="pedido"]').submit(function(){
    
    var form = $( this );    
    var data = form.serialize();

    var val = $('[required]', this).validation();

    if(val){
      $.ajax({
        url: "async/pedido",
        data: data,
        type: 'POST',
        dataType: 'json',
        beforeSend: function(){
          form.find('button[type="submit"]').html('<span class="spinner-border spinner-border-sm"></span>');
        },
        success: function(response){
          if(response.status == 200){
            $('.modal').modal('hide');
            $('#feedbackModal').modal('show');
            form.find('button').html('Pronto!');
            form.trigger("reset");
          }
          else{
            console.log(response.msg);
            form.find('button').html('Tente novamente');
          }
        },
        error: function(){
          form.find('button').html('Tente novamente');
        }
      });
    }

    return false;
  });

  $('#contato-form').on('submit', function(){

    var form = $( this );    
    var data = form.serialize();

    var val = $('[required]', this).validation();

    if(val){
      $.ajax({
        type: "POST",
        url: "async/enviar",
        async: true,
        data: data,
        dataType: 'json',
        beforeSend: function(){
          form.find('button[type="submit"]').html('<span class="spinner-border spinner-border-sm"></span>');
        },
        success: function(response){
          if(response.status == 200){
            $('.modal').modal('hide');
            $('#feedbackModal').modal('show');
            form.find('button').html('Pronto!');
            form.trigger("reset");
          }
          else{
            console.log(response.msg);
            form.find('button').html('Tente novamente');
          }
        },
        error: function(){
          form.find('button').html('Tente novamente');
        }
      });
    }
    else{
      form.find('button').html('Enviar');
    }

    return false;
  });

}); 

$.fn.extend({
  validation: function(){

    var valida = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    var val = true;

    $(this).each(function(){

      if($(this).val() == ""){
        alert();
        val = false;
        $(this).addClass('is-invalid');
      }
      else{
        $(this).removeClass('is-invalid');        
      }

      if($(this).attr("type") == "email" && !valida.test($(this).val())) {          
        val = false;
        $(this).addClass('is-invalid');
      }
    });

    return val;
  }
});