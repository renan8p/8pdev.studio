<div class="container-fluid bg-light p-0">
  <div class="row justify-content-between vh-100 m-0">

    <div class="col-12 col-md-auto">
      <div class="row align-items-center vh-100 bg-white">
        <div class="col align-self-center max-500 px-4 px-lg-6 mb-5">

          <div class="text-center my-6 d-block d-md-none">
            <img src="assets/painel/img/logo.svg" width="150">
          </div>

          <h1 class="display-4 text-center mb-2">
            Painel de usuário
          </h1>
          <p class="text-muted text-center mb-6">
            Insira seu login e senha para acessar sua conta
          </p>

          <form class="formulario" action="painel/login/accesso" method="POST">

            <div class="form-group">
              <label>Login ou E-mail</label>
              <div class="input-group input-group-merge">
                <input type="text" class="form-control form-control-prepended" name="login" id="login" data-required="true">
                <div class="input-group-prepend">
                  <div class="input-group-text">
                    <span class="fe fe-user"></span>
                  </div>
                </div>
              </div>
            </div>

            <!-- Password -->
            <div class="form-group">
              <div class="row">
                <div class="col">
                  <label>Sua senha</label>
                </div>
              </div>
              <div class="input-group input-group-merge">
                <input type="password" name="senha" id="senha" class="form-control form-control-prepended" data-required="true">
                <div class="input-group-prepend">
                  <div class="input-group-text">
                    <span class="fe fe-lock"></span>
                  </div>
                </div>
              </div>
            </div>

            <button class="btn btn-lg btn-block btn-primary mb-3">
              Entrar
            </button>

            <!-- Link -->
            <p class="mt-4">
              <small class="text-muted">             
                <a href="password-reset-cover.html" class="form-text small text-muted">
                  Esqueceu seu acesso?
                </a>
              </small>
            </p>
          </form>

          <div class="text-center font-14 mb-4 d-block d-md-none mt-6 font-14">
              <b>
                <a href="mailto:oie@8pdev.studio" class="color-one">
                  <i class="fa fa-envelope-o mr-2"></i> oie@8pdev.studio
                </a>
                <span class="mx-3">|</span>
                <a href="tel:+551732341426" class="color-one">
                  <i class="fa fa-phone mr-2"></i> (17) 3234-1426
                </a>
                <br>
                Rua Minas Gerais, 374 - São José do Rio Preto SP
              </b>
          </div>

        </div>
      </div>
    </div>

    <div class="col-12 col-md d-none d-lg-block p-0 text-center">
      <div class="row vh-100 m-0">

        <div class="col-12 align-self-center">
          <div class="text-center mb-6">
            <img src="assets/painel/img/logo.svg" width="200">
          </div>
          <div class="max-550 d-inline-block">
            <div id="svg8p"></div>
          </div>
        </div>

        <div class="col-12 align-self-end">
          <div class="w-100 mb-5 font-14">
            <div class="d-block text-center">
              <b>
                <a href="mailto:oie@8pdev.studio" class="color-one">
                  <i class="fa fa-envelope-o mr-2"></i> oie@8pdev.studio
                </a>
                <span class="mx-3">|</span>
                <a href="tel:+551732341426" class="color-one">
                  <i class="fa fa-phone mr-2"></i> (17) 3234-1426
                </a>
                <br>
                Rua Minas Gerais, 374 - São José do Rio Preto SP
              </b>
            </div>
          </div>
        </div>

      </div>      
    </div>

  </div>
</div>