<form class="formulario" action="painel/configuracoes/salvar_scripts" method="POST">
	<div class="row">
		<div class="col-12 col-md-6">
			<div class="card">
				<div class="card-header">
					<h4 class="card-header-title">
						Javascript
					</h4>
				</div>
				<div class="card-body p-0">
					<textarea name="script" id="script" class="flask-code"><?php echo (isset($config->script)) ? $config->script : NULL; ?></textarea>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-6">
			<div class="card">
				<div class="card-header">
					<h4 class="card-header-title">
						Css
					</h4>
				</div>
				<div class="card-body p-0">
					<textarea name="css" id="css" class="flask-code"><?php echo (isset($config->css)) ? $config->css : NULL; ?></textarea>
				</div>
			</div>
		</div>
		<div class="col-12 text-center">
			<hr class="mt-0 mb-4">
			<button type="submit" class="btn btn-primary px-5">Salvar códigos</button>
		</div>
	</div>	
</form>