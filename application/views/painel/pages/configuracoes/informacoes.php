<form class="formulario" action="painel/configuracoes/salvar_informacoes" method="POST">
	<div class="row">
		<div class="col-12 col-md-8">
			<div class="card">
				<div class="card-body pb-0">
					<div class="form-group">
						<label for="titulo">Título principal</label>
						<input type="text" class="form-control" id="titulo" name="titulo" value="<?php echo (isset($config->titulo)) ? $config->titulo : NULL; ?>" data-required="true" placeholder="">
					</div>
					<div class="form-group">
						<label for="descricao">Descrição principal</label>
						<textarea class="form-control" name="descricao" id="descricao"><?php echo (isset($config->descricao)) ? $config->descricao : NULL; ?></textarea>
					</div>
					<div class="form-group mb-5">
						<label for="tags">Tags SEO</label>
						<input type="text" class="form-control" id="tags" name="tags" value="<?php echo (isset($config->tags)) ? $config->tags : NULL; ?>" placeholder="Ex: Sorveteria, Barbearia, Advogados...">
					</div>

					<div class="row">
						<div class="col-12 col-sm-6">
							<div class="form-group">
					            <label for="logo">Logo</label>
								<input type="hidden" class="form-post" name="logo" id="logo" value="<?php echo (isset($config->logo)) ? $config->logo : NULL; ?>">
								<button type="button" class="btn btn-white btn-block btn-midia mb-3" data-filter="img" data-target="#logo">
									<i class="fe fe-paperclip"></i> Selecionar
								</button>
					            <div id="logo-load" class="row midia-box single">
									<?php

										if(isset($config->logo) && $config->logo != 0){
											?>
						            			<div class="midia-list" data-midia="<?php echo $config->logo; ?>">
						            				<div class="midia-delete" data-target="#logo">
							            				<i class="fe fe-x"></i>
							            			</div>
													<img src="<?php echo $config->get_img('sm', "logo"); ?>"/>
							            		</div>
						            		<?php
						            	}

						            ?>
					        	</div>
					        </div>
						</div>
						<div class="col-12 col-sm-6">
							<div class="form-group">
					            <label for="icone">Ícone</label>
					            <input type="hidden" class="form-post" name="icone" id="icone" value="<?php echo (isset($config->icone)) ? $config->icone : NULL; ?>">
								<button type="button" class="btn btn-white btn-block btn-midia mb-3" data-filter="img" data-target="#icone">
									<i class="fe fe-paperclip"></i> Selecionar
								</button>
					            <div id="icone-load" class="row midia-box single">
									<?php

										if(isset($config->icone) && $config->icone != 0){
											?>
						            			<div class="midia-list" data-midia="<?php echo $config->icone; ?>">
						            				<div class="midia-delete" data-target="#icone">
							            				<i class="fe fe-x"></i>
							            			</div>
													<img src="<?php echo $config->get_img('sm', "icone"); ?>"/>
							            		</div>
						            		<?php
						            	}

						            ?>
					        	</div>								
					        </div>
						</div>
					</div>
				</div>
			</div>

		</div>
		<div class="col-12 col-md-4">
			<div class="card">
				<div class="card-body">
					<div class="form-group">
			            <label for="img">Imagem principal:</label>
			            <div id="img-load" class="row midia-box single">
							<?php

								if(isset($config->img) && $config->img != 0){
									?>
				            			<div class="midia-list" data-midia="<?php echo $config->img; ?>">
				            				<div class="midia-delete" data-target="#img">
					            				<i class="fe fe-x"></i>
					            			</div>
											<img src="<?php echo $config->get_img('sm'); ?>"/>
					            		</div>
				            		<?php
				            	}

				            ?>
			        	</div>
			            <input type="hidden" class="form-post" name="img" id="img" value="<?php echo (isset($config->img)) ? $config->img : NULL; ?>">
			        	<button type="button" class="btn btn-white btn-block btn-midia" data-filter="img" data-target="#img">
			        		<i class="fe fe-paperclip"></i> Selecionar
			        	</button>
			        </div>

			        <div class="form-group">

						<div class="custom-switch d-inline-block mr-3 ">
							<input type="checkbox" class="form-post custom-control-input" name="status" id="status" <?php echo (isset($config->status) && $config->status) ? "checked" : NULL; ?>>
							<label class="custom-control-label" for="status"></label>
						</div>
			            <label for="status" class="m-0">Online</label>

			        </div>
			        <hr>
					<button type="submit" class="btn btn-primary btn-block">Enviar</button>

				</div>
			</div>
		</div>
	</div>
</form>