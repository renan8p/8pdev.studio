<div class="row">

	<div class="col-12 col-md-8">
		<form id="envia_menu" name="envia_menu" class="row">
			<div class="col-9">
				<select class="form-control" data-toggle="select" data-minimum-results-for-search="-1" id="menu">
					<?php

						if(isset($menus)){
							foreach ($menus as $key => $option) {
								?>
									<option value='<?php echo $key; ?>' <?php echo ($option['status'] == 1) ? "disabled" : NULL; ?> <?php echo ($key == $select_menu) ? "selected" : NULL; ?>>
										<?php

											echo $option['titulo'];
											echo ($option['status'] == 1) ? " (Automático)" : NULL;

										?>
									</option>
								<?php
							}
						}

					?>
				</select>
			</div>
			<div class="col-3 pl-0">
				<button type="submit" class="btn btn-primary btn-block">Salvar menu</button>
			</div>
		</form>
		<ul class="list-group list-group-lg mt-4 ui-sortable" id="reorder-navegacao">
			<?php

				if($navegacao){
					foreach ($navegacao as $key => $item) {

						$badge = "primary";
						$badge = ($item['tipo'] == "link") ? "info" : $badge;
						$badge = ($item['tipo'] == "categoria") ? "secondary" : $badge;

						?>

						<li class="list-group-item rounded mb-3 shadow-lg"
								data-titulo="<?php echo $item['titulo']; ?>"
								data-link="<?php echo $item['link']; ?>"
								data-area="<?php echo $item['area']; ?>"
								data-target="<?php echo $item['target']; ?>" 
								data-slug="<?php echo $item['slug']; ?>"
								data-tipo="<?php echo $item['tipo']; ?>">

							<div class="row align-items-center">
								<div class="col-7">

									<!-- Title -->
									<h3 class="nav-titulo">
										<?php echo $item['titulo']; ?>
									</h3>

									<!-- Time -->
									<p class="mb-0">
										<span class="nav-tipo badge badge-<?php echo $badge; ?>"><?php echo $item['tipo']; echo ($item['target'] == "_blank") ? " (nova aba)" : NULL; ?></span>
										<span class="nav-link badge badge-soft-primary"><?php echo ($item['link']) ? $item['link'] : '/'.$item['slug']; ?></span>										
									</p>

			                    </div>
								<div class="col-5 text-right">
									<?php

										if($item['tipo'] != "link"){
											?>
											<span class="d-inline-block custom-control custom-checkbox mr-3">
												<input type="checkbox" value="<?php echo $item['submenu']; ?>" <?php echo ($item['submenu'] != "false") ? "checked" : NULL; ?> class="custom-control-input" name="link_target" id="link_target<?php echo $item['area']; ?>">
												<label class="custom-control-label" for="link_target<?php echo $item['area']; ?>">Ativar submenu</label>
											</span>
											<?php
										}

									?>									
									<span class="btn btn-light btn-move btn-sm">
										<i class="fe fe-move"></i>
									</span>
									<button class="btn btn-white btn-remove btn-sm">
										<i class="fe fe-trash-2"></i>
									</button>
								</div>
							</div>
						</li>

						<?php
					}
				}

			?>			

        </ul>
	</div>

	<div class="col-12 col-md-4">

		<div class="list-group accordion mb-4 shadow" id="accordionMenu">			

			<div class="list-group-item" id="headPaginas" data-toggle="collapse" data-target="#form-paginas" aria-expanded="true" aria-controls="form-paginas">
				Páginas
			</div>

			<div id="form-paginas" class="collapse show" aria-labelledby="headPaginas" data-parent="#accordionMenu">
				<div class="card mb-0">
					<form class="list-nav card-body">
						<?php
						
							foreach ($lista_paginas as $key => $pag) {
								?>
								<div class="d-block custom-control custom-checkbox mb-2" data-area="<?php echo $pag->cod_area; ?>" data-slug="<?php echo $pag->slug; ?>" data-titulo="<?php echo $pag->titulo; ?>" data-tipo="pagina">
									<input type="checkbox" value="<?php echo $pag->slug; ?>" class="custom-control-input" name="paginas[]" id="paginas<?php echo $pag->id; ?>">
									<label class="custom-control-label" for="paginas<?php echo $pag->id; ?>"><?php echo $pag->titulo; ?></label>
								</div>
								<?php
							}

						?>
						<button type="submit" class="btn btn-white btn-block mt-4">
							Adicionar
						</button>
					</form>
				</div>
			</div>

			<div class="list-group-item" id="headCategorias" data-toggle="collapse" data-target="#form-categorias" aria-expanded="true" aria-controls="form-categorias">
				Categorias
			</div>
			<div id="form-categorias" class="collapse" aria-labelledby="headCategorias" data-parent="#accordionMenu">
				<div class="card mb-0">
					<form class="list-nav card-body">
						<?php

							if($lista_categorias){
								foreach ($lista_categorias as $key => $cat) {
									?>
									<div class="d-block custom-control custom-checkbox mb-2" data-area="<?php echo $cat->cod_area; ?>" data-slug="<?php echo $cat->get_current().'/'.$cat->slug; ?>" data-titulo="<?php echo $cat->titulo; ?>" data-tipo="categoria">
										<input type="checkbox" value="<?php echo $cat->slug; ?>" class="custom-control-input" name="categorias[]" id="categorias<?php echo $cat->id; ?>">
										<label class="custom-control-label" for="categorias<?php echo $cat->id; ?>"><?php echo $cat->titulo; ?></label>
									</div>
									<?php
								}
							}
							else{
								echo "<b>Sem categorias para listar</b>";
							}

						?>
					</form>
				</div>				
			</div>

			<div class="list-group-item" id="headLink" data-toggle="collapse" data-target="#form-link" aria-expanded="true" aria-controls="form-link">
				Link Personalizado
			</div>
			<div id="form-link" class="collapse" aria-labelledby="headLink" data-parent="#accordionMenu">
				<div class="card mb-0">
					<div class="card-body">
						<form id="new-link" name="new-link" method="POST">
							<div class="form-group">
								<input type="text" class="form-control" name="link_label" id="link_label" placeholder="Título">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" name="link_url" id="link_url" placeholder="http://">
							</div>
							<div class="d-block custom-control custom-checkbox">
								<input type="checkbox" value="_blank" class="custom-control-input" name="link_target" id="link_target">
								<label class="custom-control-label" for="link_target">Abrir em nova aba</label>
							</div>
							<button type="submit" class="btn btn-white btn-block mt-4">
								Adicionar
							</button>
						</form>
					</div>
				</div>				
			</div>

		</div>

		<?php if($this->session->userdata('tipo') == 'master'){ ?>
			<div class="card">
				<div class="card-body">
					<form id="new-menu">
						<label>Título</label>
						<input type="text" class="form-control mb-3" placeholder="Nome do menu" name="titulo_menu" id="titulo_menu"/>									
						<input type="checkbox" name="status_menu" id="status_menu">
						<label for="status_menu">Bloqueado</label>
						<button type="submit" class="btn btn-primary btn-block mt-3">Criar menu</button>
					</form>
				</div>
			</div>
		<?php } ?>

	</div>

</div>

<li class="to-copy list-group-item rounded mb-3 shadow-lg opacity-0" data-area="" data-titulo="" data-link="" data-target="" data-slug="" data-tipo="">				
	<div class="row align-items-center">
		<div class="col-7">

			<!-- Title -->
			<h3 class="nav-titulo"></h3>

			<!-- Time -->
			<p class="mb-0">
				<span class="nav-tipo badge"></span>
				<span class="nav-link badge badge-soft-primary"></span>
			</p>

        </div>
		<div class="col-5 text-right">
			<span class="submenu-check d-inline-block custom-control custom-checkbox mr-3">
				<input type="checkbox" value="" class="custom-control-input" name="link_target" id="link_target<?php echo $key; ?>">
				<label class="custom-control-label" for="link_target<?php echo $key; ?>">Ativar submenu</label>
			</span>
			<span class="btn btn-light btn-move btn-sm">
				<i class="fe fe-move"></i>
			</span>
			<button class="btn btn-white btn-remove btn-sm">
				<i class="fe fe-trash-2"></i>
			</button>
		</div>
	</div>
</li>