<div class="row">

	<div class="col-12 col-md-8">

		<ul class="list-group list-group-lg" id="reorder-social">
			<?php

				if($sociais){
					foreach ($sociais as $key => $social) {
						?>
						<li class="list-group-item rounded mb-3 shadow-lg">
							<div class="row align-items-center">
								<div class="col-12 col-sm-9">						
									<span class="social-plataforma badge badge-soft-primary p-2 px-3 mr-3"><?php echo $social->plataforma; ?></span>
									<p class="social-link d-inline-block mb-1"><?php echo $social->link; ?></p>
								</div>
								<div class="col-12 col-sm-3 text-right">
									<span class="btn btn-light btn-move btn-sm">
										<i class="fe fe-move"></i>
									</span>
									<button class="btn btn-white btn-remove btn-sm">
										<i class="fe fe-trash-2"></i>
									</button>
								</div>
							</div>
						</li>
						<?php
					}
				}
				else{
					?>
					<h2>Não há rede sociais.</h2>
					<?php
				}
			?>
			
        </ul>

	</div>

		<div class="col-12 col-md-4">

		<div class="card">
			<div class="card-body">
				<div class="form-group">						
					<select class="form-control" id="plataforma" name="plataforma" data-toggle="select" data-minimum-results-for-search="-1" >
						<option value="facebook">Facebook</option>
						<option value="instagram">Instagram</option>
						<option value="twitter">Twitter</option>
						<option value="youtube">YouTube</option>
						<option value="pinterest">Pinterest</option>
						<option value="behance">Behance</option>
						<option value="dribbble">Dribbble</option>
					</select>
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="link" name="link" placeholder="Link do perfil">
				</div>
				<div class="form-group m-0">
					<button type="button" class="btn-inserir-social btn btn-primary btn-block">
						Adicionar rede
					</button>
				</div>
			</div>
		</div>

	</div>
</div>

<li class="to-copy list-group-item rounded mb-3 shadow-lg opacity-0">
	<div class="row align-items-center">
		<div class="col-12 col-sm-9">
			<span class="social-plataforma badge badge-soft-primary p-2 px-3 mr-3"></span>
			<p class="social-link d-inline-block mb-1"></p>
		</div>
		<div class="col-12 col-sm-3 text-right">
			<span class="btn btn-light btn-move btn-sm">
				<i class="fe fe-move"></i>
			</span>
			<button class="btn btn-white btn-remove btn-sm">
				<i class="fe fe-trash-2"></i>
			</button>
		</div>
	</div>
</li>