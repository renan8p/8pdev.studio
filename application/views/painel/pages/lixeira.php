<div class="header">
	<div class="header-body">
	  <div class="row align-items-end">
	    <div class="col">

	      <h6 class="header-pretitle">
	        Área
	      </h6>
	      
	      <h1 class="header-title">
	        Lixeira
	      </h1>
	      
	    </div>
	    <div class="col-auto">
	      
	      <ul class="nav nav-tabs header-tabs">
			<li class="nav-item">
				<a href="#" class="nav-link text-center active">
					<h6 class="header-pretitle text-secondary">
						Lixo
					</h6>
					<h3 class="mb-0">
						<?php echo (count($lixos) > 1) ? count($lixos)." itens" : count($lixos)." item"; ?>
					</h3>
				</a>
			</li>
	      </ul>

	    </div>
	  </div> 

	</div>
</div>

<div class="card" id="table-list" data-toggle="lists" data-lists-values='["item-titulo", "item-origem", "item-data"]'>
  
  <div class="card-header">
    <div class="row align-items-center justify-content-between">

      <div class="col col-sm-6">

        <form class="row text align-items-center">  
          <label class="col-auto mb-0" for="busca">
              <span class="fe fe-search text-muted"></span>
          </label>                    
          <div class="col">
              <input type="search" id="busca" class="form-control form-control-flush search" placeholder="Pesquisar...">
          </div>
        </form>
      </div>

      <div class="col-auto text-right">

        <div class="dropdown d-inline mr-2">
          <button class="btn btn-white dropdown-toggle" type="button" id="opcoes" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Opções
          </button>
          <div id="acao-coletiva" class="dropdown-menu dropdown-menu-right" aria-labelledby="opcoes">
            <button type="button" class="dropdown-item colecao" data-target="painel/lixeira/recuperar"> <span class="fe fe-repeat mr-2"></span> Recuperar</button>
            <button type="button" class="dropdown-item colecao" data-confirm="true" data-target="painel/lixeira/excluir"> <span class="fe fe-x mr-2"></span> Excluir</button>
          </div>
        </div>
      </div>

    </div> 
  </div>

  <div class="table-responsive">
    <table class="table table-sm table-nowrap card-table">
      <thead>
        <tr>
          <th width="40">
            <div class="custom-control custom-checkbox table-checkbox">
              <input type="checkbox" class="custom-control-input" name="checkColecao" id="checkColecao">
              <label class="custom-control-label" for="checkColecao"></label>
            </div>
          </th>
          <th width="200" colspan="2">
            <a href="#" class="text-muted sort" data-sort="item-titulo">
              Título
            </a>
          </th>
          <th>
            <a href="#" class="text-muted sort" data-sort="item-data">
              Excluído em
            </a>
          </th>
          <th>
            <a href="#" class="text-muted sort" data-sort="item-origem">
              Origem
            </a>
          </th>
          <th>
            <a href="#" class="text-muted sort" data-sort="item-origem">
              Excluído por
            </a>
          </th>
        </tr>
      </thead>
      <tbody class="list">

        <?php 

          foreach ($lixos as $key => $item) {
            ?>

            <tr>
              <td>
                <div class="custom-control custom-checkbox table-checkbox">
                  <input type="checkbox" value='{"id": <?php echo $item->item; ?>, "cod_area": <?php echo $item->cod_area; ?>, "controller": "<?php echo $item->controller; ?>", "origem": "<?php echo $item->origem; ?>"}' class="custom-control-input" name="checkColecao<?php echo $item->id;?>" id="checkColecao<?php echo $item->id;?>">
                  <label class="custom-control-label" for="checkColecao<?php echo $item->id;?>"></label>
                </div>
              </td>
              <td class="item-titulo">
              	<?php echo $item->desc; ?>
              </td>
              <td>
              	<?php
              	
              		if($item->get_img('xs')){
              			?>
		                <a href="#!" class="avatar background rounded mr-3" style="background-image: url(<?php echo $item->get_img('xs'); ?>)">
		                  <img src="assets/painel/img/pixel.png" width="50">
		                </a>
            			<?php
            		}
            	?>
              </td>
              <td class="item-data">
                <time datetime="<?php echo $item->excluido_em;?>">
                  <?php echo Lazy::get_data_extensa($item->excluido_em); ?>
                </time>
              </td>
              <td class="item-origem">
                <?php echo $item->getOrigem(); ?>
              </td>
              <td class="item-usuario">
                <?php echo $item->getUsuario(); ?>
              </td>
            </tr>

            <?php

          }

        ?>

      </tbody>
    </table>
  </div>
</div>