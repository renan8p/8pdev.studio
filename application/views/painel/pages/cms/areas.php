<div class="row justify-content-center">
  <div class="col-12 col-lg-10 col-xl-8">

      <div class="header">
        <div class="header-body">
          <div class="row align-items-center">
            <div class="col">
              <h6 class="header-pretitle">
                Restrito
              </h6>
              <h1 class="header-title">Áreas
              </h1>
              
            </div>
            <div class="col-auto">
              <a class="btn btn-primary" href="painel/cms/form">
                Cadastrar nova área <span class="fe fe-chevron-right"></span>
              </a>
            </div>
          </div>

        </div>
      </div>

      <ul class="list-group list-group-flush list reorder-area">

        <?php 

          foreach ($areas as $key => $area) {
          ?>
            <li class="card p-4 mb-3" data-item="<?php echo $area->id; ?>">
          
              <div class="d-flex justify-content-between align-items-center">
                
                <div class="d-flex">
                  <i class="<?php echo $area->icone; ?> mr-3 align-self-start" ></i>
                  <h4 class="mb-1 name d-inline-block">
                    <p class="mb-3"><?php echo $area->titulo; ?></p>
                    <p class="small mb-0">
                      <span class="text-<?php echo ($area->status) ? "success" : "danger"; ?>">●</span>
                      <?php echo ($area->status) ? "Online" : "Offline"; ?>
                    </p>
                  </h4>                  
                </div>


                  <a href="painel/cms/form/<?php echo $area->id; ?>" class="btn btn-sm btn-white">
                    <span class="fe fe-edit-2"></span> Editar
                  </a>

              </div>

            </li>
          <?php
          }

        ?>
        
      </ul>

  </div>
</div>
