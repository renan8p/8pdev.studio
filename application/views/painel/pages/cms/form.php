
<form class="formulario" method="POST" name="formulario" action="painel/cms/salvar">
  <div class="row justify-content-center">
    <div class="col-12 col-lg-10 col-xl-8 mb-5">

      <div class="header">
        <div class="header-body">
          <div class="row align-items-center">
            <div class="col">
              <h6 class="header-pretitle">
                Restrito
              </h6>
              <h1 class="header-title">
                Áreas
              </h1>
            </div>
            <div class="col-auto">
              <a class="btn btn-white" href="painel/cms">
                <span class="fe fe-chevron-left"></span> Voltar
              </a>
            </div>
          </div>
        </div>
      </div>

      <div class="card">
        <div class="card-header">
          <div class="row align-items-center justify-content-between">
            <div class="col">
              <h4 class="card-header-title">
                <?php echo (isset($area->id)) ? "Editando" : "Cadastrando"; ?> área
              </h4>    
            </div>
            <div class="col-auto">

              <label for="status" class="m-0">Online</label>
              <div class="custom-switch d-inline-block ml-3">
                <input type="checkbox" class="custom-control-input" name="status" id="status" <?php echo (isset($area->id) && $area->status) ? "checked" : NULL; ?>>
                <label class="custom-control-label" for="status"></label>
              </div>

            </div>
          </div>
        </div>

        <div class="card-body">

            <input type="hidden" name="id" id="id" class="form-post form-control" value="<?php echo (isset($area->id)) ? $area->id : NULL; ?>">

            <div class="row align-items-center">

              <div class="col-12">
                <div class="form-group">
                  <label>Titulo</label>
                  <input type="text" name="titulo" class="form-post form-control" value="<?php echo (isset($area->titulo)) ? $area->titulo : NULL; ?>" data-required="true">
                </div>
              </div>

              <div class="col-6">
                <div class="form-group">
                  <label>Ícone</label>
                  <input type="text" name="icone" autocomplete="off" class="form-post form-control select-icon" value="<?php echo (isset($area->icone)) ? $area->icone : NULL; ?>">
                </div>
              </div>
              <div class="col-6">
                <div class="form-group">
                  <label>Controller</label>
                  <input type="text" name="controller" class="form-post form-control" data-required="true" value="<?php echo (isset($area->controller)) ? $area->controller : NULL; ?>" <?php echo (isset($area->id)) ? "disabled" : NULL; ?>>
                </div>
              </div>

              <div class="d-flex justify-content-between mt-3">

                <div class="col-auto">
                  <div class="form-group m-0">
                    <div class="custom-switch d-inline-block mr-3">
                      <?php

                        $status = "checked";
                        $status = (isset($area->id) && $area->getDetalhes('conteudo') == 0) ? NULL : $status;

                      ?>
                      <input type="checkbox" class="custom-control-input" name="conteudo" id="conteudo" value="1" <?php echo $status; ?>>
                      <label class="custom-control-label" for="conteudo"></label>
                    </div>
                    <label for="conteudo" class="m-0">Área de conteúdo</label>
                  </div>
                </div>

                <div class="col-auto">
                  <div class="form-group m-0 ml-5">
                    <div class="custom-switch d-inline-block mr-3">
                      <?php

                        $status = "checked";
                        $status = (isset($area->id) && $area->getDetalhes('ativa') == 0) ? NULL : $status;

                      ?>
                      <input type="checkbox" class="custom-control-input" name="ativa" id="ativa" value="1" <?php echo $status; ?>>
                      <label class="custom-control-label" for="ativa"></label>
                    </div>
                    <label for="ativa" class="m-0">Ativa para usuario</label>
                  </div>
                </div>
              </div>

            </div>
        </div>
      </div>

      <div class="card">
        <div class="card-header">
          
          <div class="form-group m-0">
            <div class="custom-switch d-inline-block mr-3 ">
              <input type="checkbox" class="custom-control-input" name="lista" id="lista" value="1" <?php echo (isset($area->id) && $area->getDetalhes('lista') == 1) ? "checked" : NULL; ?>>
              <label class="custom-control-label" for="lista"></label>
            </div>
            <label for="lista" class="m-0">Cadastro de itens</label>
            <script type="text/javascript">
              $(document).ready(function(){

                $("#lista:checkbox").change(function(){
                  if (this.checked) {
                    $('.lista').show();
                  }
                  else{
                    $('.lista').hide();
                  }
                });

              });
            </script>
          </div>

        </div>
        <div class="card-body lista" style="display: <?php echo (isset($area->id) && $area->getDetalhes('lista') == 1) ? "block" : "none"; ?>">
          <div class="row">


            <div class="col-6">
              <div class="form-group">
                <label>Título do item (Plural)</label>
                <input type="text" name="plural" class="form-post form-control" placeholder="Ex: Serviços, Slides, Testemunhos, Cursos..." value="<?php echo (isset($area->id)) ? $area->getDetalhes('plural') : NULL; ?>">
              </div>
            </div>

            <div class="col-6">
              <div class="form-group">
                <label>Título do item (Singular)</label>
                <input type="text" name="singular" class="form-post form-control" placeholder="Ex: Serviço, Slide, Testemunho, Curso" value="<?php echo (isset($area->id)) ? $area->getDetalhes('singular') : NULL; ?>">
              </div>
            </div>

            <div class="col-6">
              <label class="check-control m-0">
                <input type="checkbox" value="1" name="imagem" id="imagem" <?php echo (isset($area->id) && $area->getDetalhes('imagem') == 1) ? "checked" : NULL; ?>/>
                Item com imagem
              </label>
            </div>

            <div class="col-6">
              <label class="check-control m-0">
                <input type="checkbox" value="1" name="categoria" id="categoria" <?php echo (isset($area->categoria) && $area->categoria == 1) ? "checked" : NULL; ?>/>
                Categorias e segmentos
              </label>
            </div>

            <div class="col-6">
              <label class="check-control m-0">
                <input type="checkbox" value="1" name="blocos" id="blocos" <?php echo (isset($area->id) && $area->getDetalhes('blocos')) ? "checked" : NULL; ?>/>
                Ativar Bloco de Blog
              </label>
            </div>

          </div>
        </div>
      </div>

      <div class="d-flex justify-content-between">

        <?php

          if(isset($area->id)){
            ?>
            <button type="button" class="btn btn-white confirm" data-target="painel/cms/excluir_sessao/<?php echo $area->id ?>">
              Excluir
            </button>
            <?php
          }

        ?>

        <button type="submit" class="btn btn-primary px-4 float-right">Enviar</button>

      </div>

    </div>

  </div>
</form>
