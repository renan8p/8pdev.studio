<input type="hidden" value="<?php echo $area->id;?>" name="area" id="area">

  <div class="card">

    <!-- Header tabs -->
    <div class="card-header pb-0">
      <div class="row">
        <div class="col">
          <ul id="nav-sublistas" class="nav nav-tabs">
            <li class="nav-item mr-3 unsortable">
              <a class="nav-link active" href="#main" data-toggle="pill">Form principal</a>
            </li>
            <?php

              foreach ($sublistas as $key => $sublista) {
                ?>
                <li class="nav-item ml-3" data-item="<?php echo $sublista->id; ?>">
                  <a class="nav-link btn-move-sublist" href="#<?php echo $sublista->slug; ?>" data-toggle="pill"><?php echo $sublista->titulo; ?></a>
                  <a class="text-muted pl-1" data-toggle="dropdown">
                    <span class="fe fe-chevron-down"></span>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right text-left">
                    <a href="#" class="dropdown-item" data-toggle="modal" data-target="#editarSublistaModal<?php echo $key; ?>">
                      <span class="fe fe-edit-2 mr-2"></span> Editar sublista
                    </a>
                    <a href="#" class="dropdown-item confirm" data-target="painel/sublistas/deleta_sublista/<?php echo $sublista->id; ?>">
                      <span class="fe fe-x mr-2"></span> Excluir sublista
                    </a>
                  </div>
                </li>
                <?php
              }

            ?>
          </ul>
        </div>

        <div class="col-auto pt-1">
          <button type="button" class="btn btn-white px-4" data-toggle="modal" data-target="#sublistaModal">
            Add Sublista <i class="fe fe-list ml-2"></i>  
          </button>
        </div>

      </div>
    </div>
    <!--  -->

    <div class="card-body">
      <div class="tab-content" id="pills-tabContent">

        <!-- Formulario principal -->
        <div class="tab-pane show active" id="main">
          <div id="form-primario" class="form-custom-fields">
            <input type="hidden" value="<?php echo $controller;?>" name="controller">

            <div class="form-group">
              <input type="text" name="label_titulo" value="<?php echo $label_titulo; ?>" placeholder="Título" class="form-control">
            </div>

            <div id="custom-primario" class="custom-list">
              <?php

                print_r($fields);

              ?>
            </div>

            <div class="d-flex justify-content-between">
              <button type="button" class="btn btn-white px-4 btn-field" data-insert="#custom-primario" data-toggle="modal" data-target="#fieldsModal">
                Novo campo
              </button>
              <button type="submit" data-submit-fields="#form-primario" class="btn btn-primary px-4">
                Salvar
              </button>
            </div>

          </div>
        </div>
        <!-- Fim form principal -->

        <!-- Sublistas -->
        <?php

          foreach ($sublistas as $key => $sublista) {
            ?>
            <div class="tab-pane" id="<?php echo $sublista->slug; ?>">
              <div id="form-<?php echo $sublista->slug; ?>" class="form-custom-fields">
                <input type="hidden" value="<?php echo $sublista->slug; ?>" name="controller">

                <div class="form-group">
                  <input type="text" placeholder="Descrição" readonly="true" class="form-control">
                </div>

                <div id="custom-<?php echo $sublista->slug; ?>" class="custom-list">
                  <?php

                    print_r($sublista->fields);                

                  ?>
                </div>

                <div class="d-flex justify-content-between">
                  <button type="button" class="btn btn-white px-4 btn-field" data-insert="#custom-<?php echo $sublista->slug; ?>" data-toggle="modal" data-target="#fieldsModal">
                    Novo campo
                  </button>
                  <button type="submit" data-submit-fields="#form-<?php echo $sublista->slug; ?>" class="btn btn-primary px-4">
                    Salvar
                  </button>
                </div>

              </div>
            </div>
            <?php
          }

        ?>
        <!--  -->

      </div>

    </div>
  </div>

<div id="sublistas">
  <?php

    //if(isset($sublist_blocos)) print_r($sublist_blocos);

  ?>
</div>

<!-- Modal sublista -->
<div class="modal fade" id="sublistaModal" >
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">

      <div class="modal-header pb-0">
        <h4 class="modal-title">Adicionar sublista</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <form class="formulario" action="painel/sublistas/salvar_sublista/<?php echo $area->id;?>/<?php echo $controller;?>">
          <div class="form-group">
            <input type="text" placeholder="Título" name="titulo" class="form-control">
          </div>
          <button type="submit" class="btn btn-block btn-primary px-4">
            Enviar
          </button>
        </form>
      </div>

    </div>
  </div>
</div>

<?php

  foreach ($sublistas as $key => $sublista){
    ?>
    <!-- Modal sublista -->
    <div class="modal fade" id="editarSublistaModal<?php echo $key; ?>" >
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

          <div class="modal-header pb-0">
            <h4 class="modal-title">Adicionar sublista</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>

          <div class="modal-body">
            <form class="formulario" action="painel/sublistas/salvar_sublista/<?php echo $area->id;?>/<?php echo $controller;?>">
              <input type="hidden" value="<?php echo $sublista->id; ?>" name="id">
              <div class="form-group">
                <input type="text" value="<?php echo $sublista->titulo; ?>" name="titulo" class="form-control">
              </div>
              <button type="submit" class="btn btn-block btn-primary px-4">
                Enviar
              </button>
            </form>
          </div>

        </div>
      </div>
    </div>
    <?php
  }

?>

<!-- Modal fields -->
<div class="modal fade" id="fieldsModal" tabindex="-1" role="dialog" aria-labelledby="fieldsModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header pb-0">
        <h4 class="modal-title" id="fieldsModalLabel">Adicionar campos</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <ul class="list-group">
          <li class="list-group-item">
            <i class="fe fe-align-left mr-2"></i>
            Campo

            <button data-tipo="text" type="button" data-dismiss="modal" class="field-insert btn btn-primary float-right btn-sm px-4">          
              Add
            </button>
          </li>
          <li class="list-group-item">
            <i class="fe fe-minus mr-2"></i>
            Divisão

            <button data-tipo="divisao" type="button" data-dismiss="modal" class="field-insert btn btn-primary float-right btn-sm px-4">          
              Add
            </button>
          </li>
          <li class="list-group-item">
            <i class="fe fe-paperclip mr-2"></i>
            Mídia

            <button data-tipo="midia" type="button" data-dismiss="modal" class="field-insert btn btn-primary float-right btn-sm px-4">          
              Add
            </button>
          </li>
          <li class="list-group-item">
            <i class="fe fe-plus-square mr-2"></i>
            Seleção

            <button data-tipo="select" type="button" data-dismiss="modal" class="field-insert btn btn-primary float-right btn-sm px-4">
              Add
            </button>
          </li>
          <li class="list-group-item">
            <i class="fe fe-shuffle mr-2"></i>
            Relacionado

            <button data-tipo="relacionado" type="button" data-dismiss="modal" class="field-insert btn btn-primary float-right btn-sm px-4">
              Add
            </button>
          </li>
        </ul>        

      </div>
    </div>
  </div>
</div>

