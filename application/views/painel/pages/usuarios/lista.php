<div class="card" id="table-list" data-toggle="lists" data-lists-values='["item-titulo", "item-data", "item-status"]'>
  
  <div class="card-header">
    <div class="row align-items-center justify-content-between">

      <div class="col col-sm-6">
        <!-- Search -->
        <form class="row text align-items-center">  
          <label class="col-auto mb-0" for="busca">
            <span class="fe fe-search text-muted"></span>
          </label>
          <div class="col">
            <input type="search" id="busca" class="form-control form-control-flush search" placeholder="Pesquisar...">
          </div>
        </form>                    
      </div>

      <div class="col-auto text-right">

        <div class="dropdown d-inline mr-2">
          <button class="btn btn-white dropdown-toggle" type="button" id="opcoes" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Opções
          </button>
          <div id="acao-coletiva" class="dropdown-menu dropdown-menu-right" aria-labelledby="opcoes">
            <button type="button" class="dropdown-item colecao" data-target="painel/usuarios/colecao/bloquear"> <span class="fe fe-user-minus mr-2"></span> Bloquear</button>            
            <button type="button" class="dropdown-item colecao" data-target="painel/usuarios/colecao/desbloquear"> <span class="fe fe-user-check mr-2"></span> Desbloquear</button>
            <button type="button" class="dropdown-item colecao" data-confirm="true" data-target="painel/usuarios/colecao/excluir"> <span class="fe fe-user-x mr-2"></span> Excluir</button>
          </div>
        </div>
      </div>

    </div> <!-- / .row -->
  </div>

  <div class="table-responsive">
    <table class="table table-sm table-nowrap card-table">
      <thead>
        <tr>
          <th width="40">
            <div class="custom-control custom-checkbox table-checkbox">
              <input type="checkbox" class="custom-control-input" name="ordersSelect" id="ordersSelectAll">
              <label class="custom-control-label" for="ordersSelectAll"></label>
            </div>
          </th>
          <th>
            <a href="#" class="text-muted sort" data-sort="item-titulo">
              Título
            </a>
          </th>
          <th>
            <a href="#" class="text-muted sort" data-sort="item-data">
              Atualizado em
            </a>
          </th>
          <th colspan="2">
            <a href="#" class="text-muted sort" data-sort="item-status">
              Status
            </a>
          </th>
        </tr>
      </thead>
      <tbody class="list">

        <?php 

          foreach ($usuarios as $key => $usuario) {
            ?>

            <tr>
              <td>
                <div class="custom-control custom-checkbox table-checkbox">
                  <input type="checkbox" value='{"id": <?php echo $usuario->id; ?>}' class="custom-control-input" name="ordersSelect" id="ordersSelect<?php echo $usuario->id;?>">
                  <label class="custom-control-label" for="ordersSelect<?php echo $usuario->id;?>"></label>
                </div>
              </td>
              <td class="item-titulo">
                <?php echo $usuario->nome;?>
              </td>
              <td class="item-data">
                <time datetime="<?php echo $usuario->modificado_em;?>">
                  <?php echo Lazy::get_data_extensa($usuario->modificado_em);?>
                </time>
              </td>
              <td class="item-status">
                <div class="badge badge-soft-<?php echo ($usuario->status == 1) ? "success" : "danger"; ?>">
                  <?php echo ($usuario->status == 1) ? "Ativo" : "Bloqueado";?>
                </div>
              </td>
              <td class="text-right">
                <a href="painel/usuarios/usuario/<?php echo $usuario->id;?>" class="btn btn-sm btn-white">
                  <i class="fe fe-edit-2"></i>
                  Editar
                </a>
              </td>
            </tr>

            <?php

          }

        ?>

      </tbody>
    </table>
  </div>
</div>