<form class="formulario" action="painel/usuarios/salvar_usuario" method="POST" data-serialize="false">
	<input type="hidden" class="form-post" id="id" name="id" value="<?php echo (isset($usuario->id)) ? $usuario->id : NULL; ?>">

	<div class="row">
		<div class="col-12 col-xl-8">
			<div class="card">
				<div class="d-flex justify-content-between card-header">
					<h4 class="card-header-title d-inline-block">
						<?php echo (isset($usuario->id)) ? "Editando " : "Cadastrando "; ?> usuário
					</h4>                    
                </div>
				<div class="card-body">

					<div class="form-group">
						<label>
							Nome
						</label>
						<input type="text" name="nome" id="nome" value="<?php echo (isset($usuario->nome)) ? $usuario->nome : NULL; ?>" class="form-post form-control" data-required="true">
					</div>

					<div class="form-group">
						<label>
							E-mail
						</label>
						<input type="email" name="email" id="email" value="<?php echo (isset($usuario->email)) ? $usuario->email : NULL; ?>" <?php echo ($this->session->userdata('tipo') != "master") ? 'readonly' : NULL; ?> class="form-post form-control" data-required="true">
					</div>

					<div class="form-group mb-5">
						<label>
							Login
						</label>
						<input type="text" name="login" id="login" value="<?php echo (isset($usuario->login)) ? $usuario->login : NULL; ?>" class="form-post form-control" data-required="true">
					</div>	

					<?php 

						if((isset($usuario->login) && $usuario->login == $this->session->userdata('login')) || (isset($usuario->login) && $this->session->userdata('tipo') == "master")){
							?>
							<button type="button" class="btn btn-white" data-toggle="modal" data-target="#passModal">
								<i class="fe fe-unlock"></i> Redefinir senha
							</button>
							<?php
						}

						if(!isset($usuario->login) && $this->session->userdata('tipo') == "master"){
							?>
							<div class="form-group mb-5">
								<label>
									senha
								</label>
								<input type="text" name="senha" id="senha" class="form-post form-control" data-required="true">
							</div>
							<?php
						}

					?>

				</div>
			</div>
		</div>
		<div class="col-12 col-xl-4">

			<?php

				if($this->session->userdata('tipo') != 'usuario'){

					$novo_usuario = (!isset($usuario->login)) ? true : false;
					$meu_usuario  = (isset($usuario->login) && $usuario->login == $this->session->userdata('login')) ? false : true;

					if($novo_usuario || $meu_usuario){
					?>
					<div class="card">
						<div class="d-flex justify-content-between card-header">
							<h4 class="card-header-title d-inline-block">
								Permissões
							</h4>
		                </div>
						<div class="card-body">
							<div class="row m-0">
			            	<?php

			            		foreach ($areas as $key => $area) {
			            			?>
			            			<div class="col-12 col-sm-6 custom-control custom-checkbox table-checkbox mb-2">
					                  <input type="checkbox" value="<?php echo $area->id; ?>" <?php echo (in_array($area->id, $acessos)) ? 'checked' : NULL;?> class="custom-control-input" name="acessos[]" id="acesso<?php echo $area->id; ?>">
					                  <label class="custom-control-label" for="acesso<?php echo $area->id; ?>"><?php echo $area->titulo; ?></label>
					                </div>
			            			<?php
			            		}

			            	?>
			            	</div>
						</div>
					</div>
					<?php
					}
				}
			?>			

			<div class="card">
				<div class="card-body">

					<div class="form-group">
			            <label for="img">Imagem de Perfil:</label>
			            <div id="img-load" class="row midia-box single">
							<?php

								if(isset($usuario->img) && $usuario->img != 0){
									?>
				            			<div class="midia-list" data-midia="<?php echo $usuario->img; ?>">
				            				<div class="midia-delete" data-target="#img">
					            				<i class="fe fe-x"></i>
					            			</div>
											<img src="<?php echo $usuario->get_img('sm'); ?>"/>
					            		</div>
				            		<?php
				            	}

				            ?>
			        	</div>
			            <input type="hidden" class="form-post" name="img" id="img" value="<?php echo (isset($usuario->img)) ? $usuario->img : NULL; ?>">
			        	<button type="button" class="btn btn-white btn-block btn-midia" data-filter="img" data-target="#img">
			        		<i class="fe fe-image"></i> Selecionar
			        	</button>
			        </div>

			        <div class="form-group">
						<label for="link">
							Link
						</label>
						<input type="text" name="link" id="link" value="<?php echo (isset($usuario->link)) ? $usuario->link : NULL; ?>" class="form-post form-control">
					</div>

					<?php

						$tipo = (isset($usuario->id) && $this->session->userdata('id') != $usuario->id) ? true : false;
						if($this->session->userdata('tipo') != 'usuario' && $tipo){
							?>
							<div class="form-group">
					            <label for="tipo">Tipo:</label>
					        	<select class="form-post form-control" name="tipo" id="tipo" data-toggle="select" data-minimum-results-for-search="-1">
			                        <!-- <option value="1" <?php echo (isset($usuario->tipo) && $usuario->tipo == 'master') ? 'selected' : NULL; ?>>Desenvolvedor</option> -->
			                        <option value="admin" <?php echo (isset($usuario->tipo) && $usuario->tipo == 'admin') ? 'selected' : NULL; ?>>Admin</option>
			                        <option value="usuario" <?php echo (isset($usuario->tipo) && $usuario->tipo == 'usuario') ? 'selected' : NULL; ?>>Usuário</option>
			                    </select>
					        </div>
							<?php
						}

						if($tipo){
							?>
						        <div class="form-group mt-5">

									<div class="custom-switch d-inline-block mr-3 ">
										<input type="checkbox" class="form-post custom-control-input" name="status" id="status" <?php echo ($usuario->status) ? "checked" : NULL; ?>>
										<label class="custom-control-label" for="status"></label>
									</div>
						            <label for="status" class="m-0">Ativo</label>

						        </div>
							<?php
						}
					?>

			        <hr class="my-4">
			        <button type="submit" class="btn btn-primary btn-block">Enviar</button>

				</div>
			</div>

		</div>
	</div>
</form>

<?php 

	if((isset($usuario->login) && $usuario->login == $this->session->userdata('login')) || $this->session->userdata('tipo') == "master"){
		?>
		<div class="modal fade" id="passModal" tabindex="-1" role="dialog" aria-labelledby="passModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="passModalLabel">Redefinir Senha</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<form class="formulario modal-body" action="painel/usuarios/reset_senha" method="POST">
						<input type="hidden" name="usuario" id="usuario" value="<?php echo $usuario->login; ?>">
						<div class="form-group">
							<label>
								Nova senha
							</label>
							<input type="password" name="senha" id="senha" class="form-post form-control" data-required="true" data-pass="#reset_senha">
						</div>
						<div class="form-group mb-5">
							<label>
								Repetir senha
							</label>
							<input type="password" name="reset_senha" id="reset_senha" class="form-post form-control" data-required="true">
						</div>
						<div class="d-flex justify-content-between">
							<button type="button" class="btn btn-white" data-dismiss="modal" data-reset-form>Cancelar</button>
							<button type="submit" class="btn btn-primary px-4">Redefinir</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<?php
	}

?>



