<div class="header">
  <div class="header-body">
    <div class="row align-items-center">
      <div class="col">

        <h6 class="header-pretitle">
          Área
        </h6>
        
        <h1 class="header-title">
          Dashboard
        </h1>
        
      </div>
      <div class="col-12 col-md-4 text-right">

        <div class="dropdown">
          <button type="button" class="dropdown-toggle btn btn-white px-4" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <?php echo Lazy::getNomeMes($mes); ?>
          </button>
          <div class="dropdown-menu dropdown-menu-right p-0">
            <form action="painel/dashboard" method="POST" class="p-3">
              <div class="form-row align-items-center">

                <div class="form-group col-12 mb-3">
                  <select name="mes" class="form-control form-control-lg" data-toggle="select" data-minimum-results-for-search="-1">
                    <?php

                      for ($i=1; $i <= 12; $i++) { 

                        ?>
                          <option value="<?php echo $i; ?>" <?php echo ($i == $mes) ? "selected" : null; ?>>
                            <?php echo Lazy::getNomeMes($i); ?>
                          </option>
                        <?php

                      }

                    ?>
                  </select>
                </div>

                <div class="form-group col-12 mb-3">
                  <select name="ano" class="form-control form-control-lg" data-toggle="select" data-minimum-results-for-search="-1">
                    <?php

                      for ($i = date("Y")-2; $i < date("Y")+4; $i++) { 

                        ?>
                          <option value="<?php echo $i; ?>" <?php echo ($i == $ano) ? "selected" : null; ?>>
                            <?php echo $i; ?>
                          </option>
                        <?php

                      }

                    ?>
                  </select>
                </div>

                <div class="form-group col-12 m-0">
                  <button type="submit" class="btn btn-primary btn-block">
                    Filtrar
                  </button>
                </div>

              </div>
            </form>
          </div>
        </div>


      </div>
    </div>
  </div>
</div>

<div class="row mt-4">

  <div class="col-12">
    <div class="card" id="table-list" data-toggle="lists" data-lists-values='["item-enviado", "item-assunto", "item-recebido"]'>
      <div class="card-header">

        <div class="row align-items-center">
          <div class="col-auto pr-0">
            <span class="fe fe-mail text-muted font-20 mb-0"></span>
          </div>
          <div class="col">
            <h6 class="card-title text-uppercase text-muted mb-0">
              Mensagens
            </h6>
          </div>
          <div class="col-auto">
            <span class="h2 mb-0">
              <?php echo count($mensagens) ?>
            </span>
            <span class="h4 mr-2 mb-0">
              mensagens
            </span>
          </div>
        </div>

      </div>

      <div class="card-body p-0">
        <div id="mensagens" class="table-responsive">
          <table class="table table-sm table-nowrap card-table">
            <thead>
              <tr>
                <th class="px-0">
                  <h4 class="m-0"><span class="badge badge-soft-<?php echo ($novos > 0) ? 'success' : 'secondary'; ?>"><?php echo $novos; ?></span></h4>
                </th>
                <th class="pr-0">
                  <a href="#!" class="text-muted sort" data-sort="item-recebido">
                    Status
                  </a>
                </th>
                <th>
                  <a href="#!" class="text-muted sort" data-sort="item-enviado">
                    Enviado por
                  </a>
                </th>
                <th>
                  <a href="#!" class="text-muted sort" data-sort="item-assunto">
                    Assunto
                  </a>
                </th>
                <th>
                  <a href="#!" class="text-muted sort" data-sort="item-recebido">
                    Recebido em
                  </a>
                </th>
                <th>
                    &nbsp;
                </th>
              </tr>
            </thead>
            <tbody class="list">

              <?php 

                  foreach ($mensagens as $key => $msg) {
                    ?>
                      <tr <?php echo ($msg->status == 0) ? 'class="new"' : NULL; ?> data-ref="<?php echo $msg->id; ?>" data-toggle="modal" data-target="#mensagem-<?php echo $msg->id; ?>">
                        <td class="item-novo pr-0">
                          <span>●</span>
                        </td>
                        <td class="item-status pr-0">
                          <h4 class="m-0">
                            <span class="badge badge-soft-<?php echo $msg->getBadge(); ?>">
                              <?php echo $msg->getStatus(); ?>                          
                            </span>
                          </h4>
                        </td>
                        <td class="item-enviado">
                          <span>
                            <?php echo $msg->remetente; ?>
                          </span>
                        </td>
                        <td class="item-assunto">
                          <span>
                            <?php echo $msg->assunto; ?>
                          </span>
                        </td>
                        <td class="item-recebido">
                          <span class="text-muted">
                            <?php echo Lazy::get_data_extensa($msg->data); ?>
                          </span>
                        </td>
                        <td class="text-right">
                          <a class="mensagem-<?php echo $msg->id; ?> btn btn-sm btn-white"> Ver </a>
                        </td>
                      </tr>
                    <?php
                }

              ?>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <div class="col-12">
      <div class="card">
        <div class="card-header">

          <div class="row align-items-center">
            <div class="col-auto pr-0">
              <span class="fe fe-trending-up text-muted font-20 mb-0"></span>
            </div>
            <div class="col">
              <h6 class="card-title text-uppercase text-muted mb-0">
                Acessos
              </h6>
            </div>
            <div class="col-auto">
              <span class="h2 mb-0">
                <?php echo $acessos; ?>
              </span>
              <span class="h4 mr-2 mb-0">
                visita<?php echo ($acessos == 1) ? NULL : "s"; ?>
              </span>
            </div>
          </div>

        </div>
        <div class="card-body pt-5">
          <div class="chart">
            <canvas id="performanceChartAlias" data-values='<?php echo $grafico; ?>'></canvas>
          </div>
        </div>
    </div>
  </div>

  <div class="col-12">
    <div class="row d-none">
      <div class="col-12">

        <!-- Card -->
        <div class="card">
          <div class="card-body">

            <div class="row align-items-center mb-5">
              <div class="col-auto">
                <span class="fe fe-pie-chart text-muted font-24 mb-0"></span>
              </div>
              <div class="col">
                <h6 class="card-title text-uppercase text-muted mb-2">
                  Novos usuários
                </h6>
                <div class="row align-items-center no-gutters">
                  <div class="col-auto">
                    <span class="h2 mr-2 mb-0">
                      84.5%
                    </span>
                  </div>
                  <div class="col">
                    <div class="progress progress-sm">
                      <div class="progress-bar" role="progressbar" style="width: 85%" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                </div> 
              </div>
            </div> 

            <div class="row align-items-center mb-5">
              <div class="col-auto">
                <span class="fe fe-clipboard text-muted font-24 mb-0"></span>
              </div>
              <div class="col">
                <h6 class="card-title text-uppercase text-muted mb-2">
                  Taxa de rejeição
                </h6>
                <div class="row align-items-center no-gutters">
                  <div class="col-auto">
                    <span class="h2 mr-2 mb-0">
                      84.5%
                    </span>
                  </div>
                  <div class="col">
                    <div class="progress progress-sm">
                      <div class="progress-bar bg-warning" role="progressbar" style="width: 85%" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div> 

            <div class="row align-items-center">
              <div class="col-auto">
                <span class="fe fe-clock text-muted font-24 mb-0"></span>
              </div>
              <div class="col pr-0">
                <h6 class="card-title text-uppercase text-muted mb-2">
                  Taxa de permanência
                </h6>
                <div class="row align-items-center no-gutters">
                  <div class="col-auto">
                    <span class="h2 mr-2 mb-0">
                      25 min
                    </span>
                  </div>
                  <div class="col">
                    <span class="h3 mr-2 mb-0">
                    </span>
                  </div>
                </div> 
              </div>
            </div> 

          </div>
        </div>
          
      </div>
    </div>

    <div class="card mb-6">
      <div class="card-header">
        <div class="row align-items-center">
          <div class="col-auto pr-0">
            <span class="fe fe-layout text-muted font-20 mb-0"></span>
          </div>
          <div class="col">
            <h6 class="card-title text-uppercase text-muted mb-0">
              Mais acessados
            </h6>
          </div>
        </div> 
      </div>
      <div class="card-body">

        <?php 

          foreach ($ranking as $key => $rank) {
            ?>

              <div class="row align-items-center">
                <div class="col-12 col-md-3">
                  <h4 class="card-title mb-1">
                      <?php echo $rank->pagina; ?>
                  </h4>
                </div>
                <div class="col">                  
                  <p class="card-text small text-muted">
                    <a target="_blank" href="<?php echo $rank->url; ?>">
                      <?php echo $rank->url; ?>
                    </a>
                  </p>
                </div>
                <div class="col-auto">
                  <span class="badge badge-soft-secondary">
                    <?php echo $rank->total; ?>
                  </span>
                </div>

              </div>

            <?php
            
            echo ($key+1 < count($ranking)) ? "<hr>" : NULL;

          }

        ?>
      </div>
    </div>
  </div>

</div>

<?php 

  foreach ($mensagens as $key => $msg) {
    ?>
    <!-- Modal -->
    <div class="modal fade" id="mensagem-<?php echo $msg->id; ?>" tabindex="-1" role="dialog" aria-labelledby="mensagem-<?php echo $msg->id; ?>" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header d-block position-absolute" style="right: 0;z-index: 10;">        
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">

            <div class="row">
              <span class="col-2 pr-0 text-right text-muted">de:</span>
              <span class="col-10"><?php echo $msg->remetente; ?></span>
              <span class="col-2 pr-0 text-right text-muted">para:</span>
              <span class="col-10"><?php echo $msg->para; ?></span>
              <span class="col-2 pr-0 text-right text-muted">data:</span>
              <span class="col-10"><?php echo Lazy::get_data_extensa($msg->data); ?></span>
              <span class="col-2 pr-0 text-right text-muted">assunto:</span>
              <span class="col-10"><?php echo $msg->assunto; ?></span>

              <div class="col-12 mt-5">
                <p>
                  <b>Conteúdo</b>
                </p>
                <p>
                  <?php echo $msg->conteudo; ?>
                </p>
              </div>
            </div>

            <form class="form-mensagem-<?php echo $msg->id; ?>" method="POST">
              <div class="d-flex justify-content-between mt-5">
                <div class="col-auto">
                  <div class="row align-items-center">
                    <div class="col-md-auto d-none d-md-block text-muted">Marcar como:</div>
                    <div class="col p-0" style="width: 150px">
                      <select name="status" id="status" class="form-control form-control-sm" data-toggle="select" data-minimum-results-for-search="-1">
                        <option value="1" <?php echo ($msg->status == 0) ? 'selected' : NULL; ?>>Em espera</option>
                        <option value="2" <?php echo ($msg->status == 2) ? 'selected' : NULL; ?>>Pendente</option>
                        <option value="3" <?php echo ($msg->status == 3) ? 'selected' : NULL; ?>>Atrasado</option>
                        <option value="4" <?php echo ($msg->status == 4) ? 'selected' : NULL; ?>>Finalizado</option>
                        <option value="-1">Excluir</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-auto col-md-auto p-0 text-right">
                  <button type="button" class="status-mensagem btn btn-primary btn-sm" data-ref="<?php echo $msg->id; ?>">Pronto</button>
                </div>
              </div>
            </form>

          </div>
        </div>
      </div>
    </div>
    <?php
  }

?>
