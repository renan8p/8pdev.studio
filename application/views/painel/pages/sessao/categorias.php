<div class="card" id="table-list" data-toggle="lists" data-lists-values='["item-titulo", "item-data", "item-status"]'>
  
  <div class="card-header">
    <div class="row align-items-center justify-content-between">

      <div class="col col-sm-6">
        <!-- Search -->
        <form class="row text align-items-center">  
          <label class="col-auto mb-0" for="busca">
              <span class="fe fe-search text-muted"></span>
          </label>                    
          <div class="col">
              <input type="search" id="busca" class="form-control form-control-flush search" placeholder="Pesquisar...">
          </div>
        </form>                    
      </div>

      <div class="col-auto text-right">                   
        <!-- Button -->
        <div class="dropdown d-inline mr-2">
          <button class="btn btn-white dropdown-toggle" type="button" id="opcoes" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Opções
          </button>
          <div id="acao-coletiva" class="dropdown-menu dropdown-menu-right" aria-labelledby="opcoes">
            <button type="button" class="dropdown-item colecao" data-target="painel/sessao/colecao/publicar/categoria"> <span class="fe fe-check-circle mr-2"></span> Publicar</button>
            <button type="button" class="dropdown-item colecao" data-target="painel/sessao/colecao/despublicar/categoria"> <span class="fe fe-x-circle mr-2"></span> Despublicar</button>
            <button type="button" class="dropdown-item colecao" data-target="painel/sessao/colecao/lixeira/categoria"> <span class="fe fe-trash-2 mr-2"></span> Lixeira</button>
          </div>
        </div>

        <a href="painel/sessao/categoria/<?php echo $area->id; ?>" class="btn btn-primary">
          Cadastrar categoria <span class="fe fe-chevron-right"></span>
        </a>
      </div>

    </div> <!-- / .row -->
  </div>

  <div class="table-responsive">
    <table class="table table-sm table-nowrap card-table">
      <thead>
        <tr>
          <th width="40">
            <div class="custom-control custom-checkbox table-checkbox">
              <input type="checkbox" class="custom-control-input" name="ordersSelect" id="ordersSelectAll">
              <label class="custom-control-label" for="ordersSelectAll"></label>
            </div>
          </th>
          <th>
            <a href="#" class="text-muted sort" data-sort="item-titulo">
              Título
            </a>
          </th>
          <th>
            <a href="#" class="text-muted sort" data-sort="item-data">
              Atualizado em
            </a>
          </th>
          <th colspan="2">
            <a href="#" class="text-muted sort" data-sort="item-status">
              Status
            </a>
          </th>
        </tr>
      </thead>
      <tbody class="list">

        <?php 

          foreach ($categorias as $key => $categoria) {
            ?>

            <tr>
              <td>
                <div class="custom-control custom-checkbox table-checkbox">
                  <input type="checkbox"  value='{"id": <?php echo $categoria->id; ?>, "cod_area": <?php echo $categoria->cod_area; ?>, "controller": "<?php echo $categoria->controller; ?>"}' class="custom-control-input" name="ordersSelect" id="ordersSelect<?php echo $categoria->id;?>">
                  <label class="custom-control-label" for="ordersSelect<?php echo $categoria->id;?>"></label>
                </div>
              </td>
              <td class="item-titulo">
                <?php echo $categoria->titulo; ?>
              </td>
              <td class="item-data">
                <time datetime="<?php echo $categoria->modificado_em;?>">
                  <?php echo Lazy::get_data_extensa($categoria->modificado_em); ?>
                </time>
              </td>
              <td class="item-status">
                <div class="badge badge-soft-<?php echo ($categoria->status == 1) ? "success" : "secondary"; ?>">
                  <?php echo ($categoria->status == 1) ? "publicado" : "privado"; ?>
                </div>
              </td>
              <td class="text-right">
                <a href="painel/sessao/categoria/<?php echo $categoria->cod_area."/".$categoria->id;?>" class="btn btn-sm btn-white">
                  <i class="fe fe-edit-2"></i>
                  Editar
                </a>
              </td>
            </tr>

            <?php

          }

        ?>

      </tbody>
    </table>
  </div>
</div>