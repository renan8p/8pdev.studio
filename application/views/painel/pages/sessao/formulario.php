<form class="formulario" action="painel/sessao/salvar_post" method="POST" data-serialize="false">
	<input type="hidden" class="form-post" id="tipo" name="tipo" value="<?php echo (isset($conteudo_post->tipo)) ? $conteudo_post->tipo : $nav_tab; ?>">
	<input type="hidden" class="form-post" id="id" name="id" value="<?php echo (isset($conteudo_post->id)) ? $conteudo_post->id : NULL; ?>">
	<input type="hidden" class="form-post" id="cod_area" name="cod_area" value="<?php echo (isset($area->id)) ? $area->id : NULL; ?>">

	<div class="row">
		<div class="col-12 col-xl-8">
			<div class="card">
				<div class="card-body pb-2">
					<div class="form-group">
						<label>
							<?php echo $label_titulo; ?>
						</label>
						<input type="text" name="titulo" value="<?php echo (isset($conteudo_post->titulo)) ? $conteudo_post->titulo : NULL; ?>" class="form-post form-control form-control-lg" data-required="true">
						<input type="hidden" value="<?php echo (isset($conteudo_post->slug)) ? $conteudo_post->slug : NULL; ?>">
					</div>
					<?php

						if(isset($fields_main)) Custom_fields::custom_list($fields_main, $values);

					?>
				</div>
			</div>
			<?php

				if($area->getDetalhes("blocos") && $conteudo_post->tipo == 'item'){
					Custom_fields::custom_blog($editable_blocks);
				}

			?>
		</div>
		<div class="col-12 col-xl-4">
			<div class="position-xl-absolute">
				<div class="card">
					<div class="card-body">

				        <button type="submit" class="btn btn-primary btn-block">
							<?php echo (isset($conteudo_post->id)) ? "Salvar" : "Publicar"; ?>
				        </button>

						<hr class="my-4">

				        <div class="form-group mb-4">
							<div class="custom-switch d-inline-block mr-3">
								<?php

									$status = (isset($conteudo_post->status)) ? $conteudo_post->status : 'checked';
									$status = ($status == 1) ? 'checked' : $status;
									$status = ($status == 2) ? NULL : $status;

								?>
								<input type="checkbox" class="form-post custom-control-input" name="status" id="status" value="<?php echo (isset($conteudo_post->status)) ? $conteudo_post->status : NULL; ?>" <?php echo $status; ?>>
								<label class="custom-control-label" for="status"></label>
							</div>
							<label for="status" class="m-0">Deixar conteúdo público</label>
				        </div>

				        <?php

				        	if($conteudo_post->tipo == "item"){
				        		?>
								<div class="form-group mb-4">
									<div class="custom-switch d-inline-block mr-3">
										<?php

											$destaque = (isset($conteudo_post->destaque)) ? $conteudo_post->destaque : NULL;
											$destaque = ($destaque == 1) ? 'checked' : NULL;

										?>
										<input type="checkbox" class="form-post custom-control-input" name="destaque" id="destaque" value="1" <?php echo $destaque; ?>>
										<label class="custom-control-label" for="destaque"></label>
									</div>	
									<label for="destaque" class="m-0">Destacar</label>
								</div>
								<hr class="my-4">
								<?php
							}
						?>


				        <div class="form-group m-0">
				        	<div class="row align-items-center">
				        		<div class="col-auto">
									<label class="m-0">Postado em:</label>
								</div>
								<div class="col">
									<input type="text" name="criado_em" class="form-post form-control d-inline" data-toggle="flatpickr" data-flatpickr-time value="<?php echo (isset($conteudo_post->criado_em)) ? Lazy::getDateTimeDb($conteudo_post->criado_em) : NULL; ?>">
				        		</div>
				        	</div>
				        </div>

					</div>
				</div>
				<?php

					if($area->categoria && $categorias){
						?>
						<div class="card">
							<div class="card-header">
								<h4 class="card-header-title">
									Categorias:
								</h4>
							</div>
							<div class="card-body">
								<div class="row m-0">
				            	<?php

				            		foreach ($categorias as $key => $categoria) {
				            			?>
				            			<div class="col-12 col-sm-6 custom-control custom-checkbox table-checkbox mb-2">
						                  <input type="checkbox" value="<?php echo $categoria->id; ?>" <?php echo (isset($conteudo_post->id) && $conteudo_post->get_categoria_check($categoria->id)) ? "checked" : NULL; ?> class="custom-control-input" name="categorias[]" id="categorias<?php echo $categoria->id; ?>">
						                  <label class="custom-control-label" for="categorias<?php echo $categoria->id; ?>"><?php echo $categoria->titulo; ?></label>
						                </div>
				            			<?php
				            		}

				            	?>
				            	</div>
							</div>
						</div>
						<?php
					}

					if($fields_sidebar){
						?>
						<div class="card">
							<div class="card-body pb-1">
								<?php
									Custom_fields::custom_list($fields_sidebar, $values);
								?>
							</div>
						</div>
						<?php
					}

					if($area->getDetalhes("imagem") || (isset($conteudo_post->tipo) && $conteudo_post->tipo == 'pagina' )){
						?>
						<div class="card">
							<div class="card-body">
								<div class="form-group mb-0">
						            <label for="img">Imagem destaque</label>
						            <div id="img-load" class="row midia-box single">
							            <?php

							            	if(isset($conteudo_post->img) && $conteudo_post->img != 0){
							            		?>
							            			<div class="midia-list" data-midia="<?php echo $conteudo_post->img; ?>">
							            				<div class="midia-delete" data-target="#img">
								            				<i class="fe fe-x"></i>
								            			</div>
								            			<?php echo $conteudo_post->get_html_midia('sm'); ?>
								            		</div>
							            		<?php
							            	}

							            ?>
						        	</div>
						            <input type="hidden" class="form-post" name="img" id="img" value="<?php echo (isset($conteudo_post->img)) ? $conteudo_post->img : NULL; ?>">
						        	<button type="button" class="btn btn-white btn-block btn-midia" data-filter="img" data-target="#img">
						        		<i class="fe fe-image"></i> Selecionar
						        	</button>
						        </div>
						    </div>
						</div>
				        <?php
			    	}
				?>
			</div>
		</div>
	</div>
</form>

<div class="row">
	<div class="col-12 col-xl-8">
	<?php

		if($sublistas){
			foreach($sublistas as $sublista){
				Custom_fields::custom_sublist($sublista, $sublista->get_sublista_fields());
			}
		}

	?>
	</div>
</div>
