<form class="formulario" action="painel/sessao/salvar_categoria" method="POST">
	<input type="hidden" class="form-post" id="id" name="id" value="<?php echo (isset($categoria->id)) ? $categoria->id : NULL; ?>">
	<input type="hidden" class="form-post" id="cod_area" name="cod_area" value="<?php echo $area->id; ?>">
	<input type="hidden" class="form-post" id="controller" name="controller" value="<?php echo $area->controller; ?>_categorias">

	<div class="row">
		<div class="col-12 col-xl-8">
			<div class="card">			
				<div class="card-header">
					<h4 class="card-header-title">
						<?php echo (isset($categoria->id)) ? "Editando" : "Cadastrando"; ?> categoria
					</h4>                      
                </div>
				<div class="card-body">

					<div class="form-group">
						<label>
							Título
						</label>
						<input type="text" name="titulo" value="<?php echo (isset($categoria->titulo)) ? $categoria->titulo : NULL; ?>" class="form-post form-control" data-required="true">
					</div>
					<?php

						Custom_fields::custom_list($fields_main, $conteudo);

					?>
				</div>
			</div>
		</div>
		<div class="col-12 col-xl-4">
			<div class="card">
				<div class="card-body">

					<div class="form-group">
						<label for="pai">Categorias pai:</label>
			        	<select class="form-post form-control" data-toggle="select" name="pai" id="pai">
	                        <option>Nenhuma</option>
				        <?php 

				          foreach ($categorias_pai as $key => $pai) {
				            ?>
	                        <option value="<?php echo $pai->id;?>"><?php echo $pai->titulo;?></option>
				            <?php

				          }

				        ?>
				    	</select>
					</div>

					<div class="form-group">
			            <label for="status">Status:</label>
			        	<select class="form-post form-control" data-toggle="select" data-minimum-results-for-search="-1" name="status" id="status">
	                        <option value="1" <?php echo (isset($categoria->status) && $categoria->status == 1) ? 'selected' : NULL; ?>>Público</option>
	                        <option value="2" <?php echo (isset($categoria->status) && $categoria->status == 2) ? 'selected' : NULL; ?>>Privado</option>
	                    </select>
			        </div>

			        <hr class="my-4">

			        <button type="submit" class="btn btn-primary btn-block">Enviar</button>

				</div>
			</div>
		</div>
	</div>
</form>

