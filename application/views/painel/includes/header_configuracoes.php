<div class="header mb-5">
  <div class="header-body">
    <div class="row align-items-end">
      <div class="col">

        <h6 class="header-pretitle">
          Área
        </h6>

        <h1 class="header-title">
          Configurações
        </h1>

      </div>
      <div class="col-auto">

        <!-- Nav -->
        <ul class="nav nav-tabs nav-overflow header-tabs">        

              <li class="nav-item">
                <a href="painel/configuracoes" class="nav-link <?php echo ($nav_tab == 'informacoes') ? 'active' : NULL; ?>">
                  Informações
                </a>
              </li>

              <!--
              <li class="nav-item">
                <a href="painel/configuracoes/navegacao" class="nav-link <?php echo ($nav_tab == 'navegacao') ? 'active' : NULL; ?>">
                  Navegação
                </a>
              </li>
              -->

              <li class="nav-item">
                <a href="painel/configuracoes/social" class="nav-link <?php echo ($nav_tab == 'social') ? 'active' : NULL; ?>">
                  Redes Sociais
                </a>
              </li>

              <li class="nav-item">
                <a href="painel/configuracoes/scripts" class="nav-link <?php echo ($nav_tab == 'scripts') ? 'active' : NULL; ?>">
                  Scripts
                </a>
              </li>

        </ul>
      </div>
      
    </div>
  </div>
</div>