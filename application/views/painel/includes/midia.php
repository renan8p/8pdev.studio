<?php	

	foreach($midias as $key => $midia){		
		?>
			<div id="midia-item-<?php echo $midia->id; ?>" class="midia-list-item <?php echo $midia->tipo; ?> col-6 col-sm-4 col-lg-3 col-xl-2">
				<div class="midia-item <?php echo (in_array($midia->id, $checks)) ? 'active' : NULL; ?> lazyload">
					<img class="w-100" src="assets/painel/img/px-hor.png">
					<div class="midia-descricao">
						<p class="m-0"><?php echo $midia->titulo; ?></p>
						<span class="badge badge-soft-primary p-2"><?php echo $midia->extensao; ?></span>
					</div>
					<span class="midia-data" data-midia="<?php echo $midia->id; ?>" data-desc="<?php echo $midia->titulo; ?>" data-tipo="<?php echo $midia->tipo; ?>" data-url="<?php echo $midia->get_midia('xs'); ?>" data-url-full="<?php echo $midia->get_midia(); ?>"></span>
					<button class="btn btn-white btn-midia-edit" data-midia="<?php echo $midia->id; ?>"><i class="fe fe-settings"></i></button>
				</div>
			</div>
		<?php
	}

?>