<div class="header mb-5">
  <div class="header-body">
    <div class="row align-items-end">
      <div class="col">

	        <h6 class="header-pretitle">
				<?php echo (isset($area->titulo) && isset($atividade)) ? $area->titulo : 'ÁREA'; ?>
	        </h6>

	        <h1 class="header-title d-inline-block">
				<?php echo (isset($atividade)) ? $atividade.' '.$area->getDetalhes('singular') : $area->titulo; ?>
			</h1>
        
      </div>
      <div class="col-auto">

        <ul class="nav nav-tabs header-tabs">
          <?php

	  		if($area->getDetalhes('conteudo') == 1){
	  			$active = ($nav_tab == 'pagina' || $nav_tab == 'pagina_custom') ? 'active' : NULL;
	  			?>
		  		<li class="nav-item">
					<a href="painel/sessao/formulario/pagina/<?php echo $area->id; ?>" class="nav-link <?php echo $active ?>">
						Conteúdo
					</a>
					<?php
						if($this->session->userdata('tipo') == "master")
						{
							?>
								<a href="painel/cms/custom/<?php echo $area->id; ?>/pagina" class="btn btn-outline-secondary p-1 ml-2">
									<span class="fe fe-corner-right-down"></span>
								</a>
							<?php
						}
					?>							
				</li>
				<?php
			}

			if($area->getDetalhes('lista') == 1){
	  			$active = ($nav_tab == 'lista' || $nav_tab == 'item') ? 'active' : NULL;
				?>
				<li class="nav-item">
					<a href="painel/sessao/lista/<?php echo $area->id; ?>" class="nav-link <?php echo $active ?>">
						Lista de <?php echo ($area->getDetalhes('plural')) ? $area->getDetalhes('plural') : "Itens"; ?>
					</a>
					<?php														
						if($this->session->userdata('tipo') == "master")
						{
							?>
								<a href="painel/cms/custom/<?php echo $area->id; ?>/item" class="btn btn-outline-secondary p-1 ml-2">
									<span class="fe fe-corner-right-down"></span>
								</a>
							<?php
						}
					?>							
				</li>
				<?php
			}

			if($area->categoria && $area->getDetalhes('lista') == 1){
				$active = ($nav_tab == 'categorias' || $nav_tab == 'categorias_custom') ? 'active' : NULL;
				?>
				<li class="nav-item">
					<a href="painel/sessao/categorias/<?php echo $area->id; ?>" class="nav-link <?php echo $active ?>">
						Categorias
					</a>
					<?php
						if($this->session->userdata('tipo') == "master")
						{
							?>
								<a href="painel/cms/custom/<?php echo $area->id; ?>/categorias" class="btn btn-outline-secondary p-1 ml-2">
									<span class="fe fe-corner-right-down"></span>
								</a>
							<?php
						}
					?>							
				</li>
				<?php
			}

		?>
        </ul>

      </div>
    </div> 

  </div>
</div>
