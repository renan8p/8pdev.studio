<nav class="navbar navbar-expand-md navbar-light bg-white d-none d-md-flex mb-3">
  <div class="container-fluid">

    <!-- Brand -->
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <?php 

          if(isset($breadcrumb)){
              ?>
              <li class="breadcrumb-item"><a href="painel/dashboard">Dashboard</a></li>
              <?php
              foreach ($breadcrumb as $key => $item) {
                echo '<li class="breadcrumb-item"><a href="painel/'.$key.'">'.$item.'</a></li>';
              }
          }
          else{
            ?>
            <li class="breadcrumb-item">
              <a href="painel/dashboard">
                Bem-vindo <b><?php echo Lazy::getPrimeiroNome($this->session->userdata("nome")); ?></b>
              </a>
            </li>
            <?php
          }
        ?>
      </ol>
    </nav>

    <!-- User -->
    <div class="navbar-user">

      <!-- Dropdown -->
      <div class="dropdown mr-4 d-none d-md-flex">

        <?php 

          if(isset($controller)){
            ?>
            <a href="<?php echo $controller; ?>" target="_blank" class="btn btn-sm btn-outline-secondary px-3">
              ver site
            </a>
            <?php
          }

        ?>

        <!-- Toggle -->
        <!-- <a href="#" class="text-muted" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <span class="icon active">
            <i class="fe fe-bell"></i>
          </span>
        </a> -->

        <!-- Menu -->
        <div class="dropdown-menu dropdown-menu-right dropdown-menu-card">
          <div class="card-header">
            <div class="row align-items-center">
              <div class="col">
          
                <!-- Title -->
                <h5 class="card-header-title">
                  Notificações
                </h5>

              </div>
              <div class="col-auto">
          
                <!-- Link -->
                <a href="#!" class="small">
                  Ver todas
                </a>

              </div>
            </div> <!-- / .row -->
          </div> <!-- / .card-header -->
          <div class="card-body">

            <!-- List group -->
            <div class="list-group list-group-flush my--3">
              <a class="list-group-item px-0" href="#!">
        
                <div class="row">              
                  <div class="col-12 ml--2">
              
                    <!-- Content -->
                    <div class="small text-muted">
                      <strong class="text-body">Dianna Smiley</strong> shared your post with <strong class="text-body">Ab Hadley</strong>, <strong class="text-body">Adolfo Hess</strong>, and <strong class="text-body">3 others</strong>.
                    </div>

                  </div>
                  <div class="col-auto">

                    <small class="text-muted">
                      2m
                    </small>
              
                  </div>
                </div> <!-- / .row -->

              </a>
              <a class="list-group-item px-0" href="#!">

                <div class="row">              
                  <div class="col-12 ml--2">
              
                    <!-- Content -->
                    <div class="small text-muted">
                      <strong class="text-body">Ab Hadley</strong> reacted to your post with a 😍
                    </div>

                  </div>
                  <div class="col-auto">

                    <small class="text-muted">
                      2m
                    </small>
              
                  </div>
                </div> <!-- / .row -->

              </a>
            </div>
      
          </div>
        </div>

      </div>

      <!-- Dropdown -->
      <div class="dropdown">
  
        <!-- Toggle -->
        <a href="#" class="avatar background avatar-sm rounded-circle avatar-online dropdown-toggle" style="background-image: url('<?php echo $this->session->userdata('img'); ?>');" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>

        <!-- Menu -->
        <div class="dropdown-menu dropdown-menu-right">
          <a href="painel/usuarios/usuario/<?php echo $this->session->userdata('id'); ?>" class="dropdown-item">
            <p class="m-0"><?php echo $this->session->userdata('nome'); ?></p>
            <small class="text-muted"><?php echo $this->session->userdata('email'); ?></small>
          </a>
          <!-- <a href="settings.html" class="dropdown-item">Settings</a> -->
          <hr class="dropdown-divider">
          <a href="painel/login/logout" class="dropdown-item">
            <span class="fe fe-power"></span> Sair
          </a>
        </div>

      </div>

    </div>

  </div> <!-- / .container-fluid -->
</nav>