<div class="header">
	<div class="header-body">
	    <div class="row justify-content-between align-items-center">
          <div class="col">

            <!-- Pretitle -->
            <h6 class="header-pretitle">
              Área
            </h6>

            <!-- Title -->
            <h1 class="header-title">
              <?php echo (isset($usuario->id) && $usuario->id == $this->session->userdata('id')) ? "Meu perfil" : "Usuários"; ?>
            </h1>

          </div>
          <div class="col text-right">
          	<?php

              if($this->session->userdata('tipo') != "usuario"){
                if(isset($usuario)){
                  ?>
            		    <a href="painel/usuarios/lista" class="btn btn-white d-inline-block">
                      Listar usuários
                    </a>
                  <?php
                }

                ?>
                  <a href="painel/usuarios/usuario" class="btn btn-primary ml-2 px-4">
                    Cadastrar usuário <span class="ml-2 fe fe-user-plus"></span>
                  </a>
                <?php
              }

            ?>

          </div>
        </div>
	</div>
</div>