<div class="subitem" data-item="<?php echo $item->id; ?>">
	<div class="row py-3">
		<div class="col-auto">
			<div class="handle-move pl-4">
				<i class="fe fe-menu"></i>
			</div>
		</div>
		<div class="col-12 col-md">
			<?php echo $item->titulo; ?>
		</div>
		<div class="col-auto pr-5">				
			<div class="dropdown">
				<a href="#" class="btn btn-white py-0 px-2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<i class="fe fe-more-horizontal"></i>
				</a>
				<div class="dropdown-menu dropdown-menu-right">
					<a href="#" class="dropdown-item" data-toggle="modal" data-target="#modal-<?php echo $item->id; ?>">
						<span class="fe fe-edit-3 mr-2"></span> Editar
					</a>
					<a href="#" class="dropdown-item" data-subitem-excluir="<?php echo $item->id; ?>">
						<span class="fe fe-x mr-2"></span> Excluir
					</a>
				</div>
			</div>
		</div>
		<div class="modal fade" id="modal-<?php echo $item->id; ?>" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-body pb-3">
						<div class="form-sublista" id="form-<?php echo $item->id; ?>">
							<input type="hidden" name="item_id" class="item_id" value="<?php echo $item->id; ?>">							
							
							<div class="form-group">
								<label>
									Descrição
								</label>
								<input type="text" name="titulo_sublista" value="<?php echo (isset($item->titulo)) ? $item->titulo : NULL; ?>" class="form-post form-control">
								<input type="hidden" value="<?php echo (isset($item->slug)) ? $item->slug : NULL; ?>">
							</div>
							<?php

								Custom_fields::custom_list($fields, $conteudo, $item->id);

							?>
						</div>
					</div>
					<div class="modal-footer justify-content-between bd-top py-3 pl-3">
						<button type="button" class="btn btn-link" data-dismiss="modal">Cancelar</button>
						<button type="button" class="btn btn-primary px-4 btn-sublista" data-dismiss="modal" data-sublista="<?php echo $item->tipo; ?>" data-item="<?php echo $item->id; ?>">Enviar</button>
					</div>			
				</div>
			</div>
		</div>
	</div>
</div>
