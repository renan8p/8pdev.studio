<div id="midia">
	<div class="midia-header pt-1 bg-white">
        <div class="d-flex justify-content-between">
    		<div class="col-auto">
	            <ul class="nav nav-tabs" id="myTab" role="tablist">
	                <li class="nav-item">
	                    <a class="nav-link active" id="arquivos-tab" data-toggle="tab" href="#arquivos" role="tab" aria-controls="arquivos" aria-selected="true"><b>Arquivos</b></a>
	                </li>
	                <li class="nav-item">
	                    <a class="nav-link" id="upload-tab" data-toggle="tab" href="#upload" role="tab" aria-controls="upload" aria-selected="false"><b>Upload</b></a>
	                </li>
	            </ul>
			</div>
			<div class="col-auto pt-2">
				<div class="form-inline">
					<div class="form-group m-0 mr-3">
						<div class="nav-item dropdown pl-3" id="dropdownFiltro" >
							<a class="nav-link dropdown-toggle midia-filtro" data-filter="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="fe fe-folder mr-2"></span> Todos
							</a>
							<div class="dropdown-menu" aria-labelledby="dropdownFiltro">
								<a class="dropdown-item d-none" data-value="todos" href="#">
									<span class="fe fe-folder mr-2"></span> Todos
								</a>
								<a class="dropdown-item" data-value="img" href="#">
									<span class="fe fe-image mr-2"></span> Imagens
								</a>
								<a class="dropdown-item" data-value="video" href="#">
									<span class="fe fe-play-circle mr-2"></span> Vídeos
								</a>
								<a class="dropdown-item" data-value="audio" href="#">
									<span class="fe fe-headphones mr-2"></span> Áudio
								</a>
								<a class="dropdown-item" data-value="documento" href="#">
									<span class="fe fe-file-text mr-2"></span> Documentos
								</a>
							</div>
						</div>
					</div>
					<div class="form-group m-0">
						<input type="text" class="form-control midia-buscar" id="input-buscar" placeholder="Buscar...">
					</div>
				</div>
			</div>
        </div>
        <!--end of col-->
	</div>

    <div class="tab-content">
        <div class="tab-pane container-fluid fade active show" id="arquivos" role="tabpanel" aria-labelledby="arquivos-tab">
        	<div id="midia-lista"></div>
    	    <div class="midia-footer">
				<div class="d-flex justify-content-end px-3">
					<button class="btn btn-white midia-btn-close mr-3">Cancelar</button>
					<button class="btn btn-primary midia-submit px-4" data-return="list">
						Selecionar <span class="fe fe-check ml-2"></span>
					</button>
				</div>
			</div>
        </div>

        <form class="tab-pane container-fluid fade" id="upload" role="tabpanel" method="POST" enctype="multipart/form-data" arialabelledby="upload-tab">
            <input id="fileupload" type="file" name="arquivo[]" multiple />
            <div class="row midia-input text-center">
            	<div class="col-12" disable>
	                <h3>
	                	Arraste aqui
	                	<small class="text-muted">ou</small>
	                </h3>
	            	<button class="btn btn-white px-4 btn-lg">
	            		<span class="fe fe-share mr-3"></span> Selecione os arquivos
	            	</button>
            	</div>
            	<div id="loader-upload" class="col-12 text-center pt-4">
            		<p class="counter"><span><number>0</number> / <b>0</b></span><br>
            			Enviando... Pode ser que leve alguns minutos
            		</p>
		            <div id="progress">
						<div class="loader loader-primary"></div>
					</div>
				</div>
            </div>
		</form>
        <div class="midia-feedback"></div>
    </div>

    <div id="edit-sidebar">
    	<div class="edit-sidebar">
			<button class="btn btn-link btn-close-sidebar">
				<i class="fe fe-arrow-right"></i>
			</button>

			<a href="" target="_blank" class="edit-midia-ver btn btn-link btn-close-sidebar float-right">
				Ver mídia
			</a>

			<div class="midia-edit"></div>
			
			<form class="midia-desc" name="midia-desc" method="POST">
				<input type="hidden" name="id" class="edit-midia-id form-control">
				<div class="form-group">
					<label>Nome</label>
					<input type="text" name="titulo" class="edit-midia-titulo form-control">
				</div>

				<div class="row">
					<div class="col-9">
					<div class="form-group">
						<label>Slug</label>
						<input type="text" name="slug" readonly class="edit-midia-slug form-control">
					</div>
					</div>	
					<div class="col-3 pl-0">
						<div class="form-group">
							<label>.ext</label>
							<input type="text" name="extensao" readonly class="edit-midia-extensao form-control">
						</div>
					</div>	
				</div>

				<div class="form-group">
					<label>Descrição</label>
					<textarea name="descricao" class="edit-midia-desc form-control" rows="3"></textarea>
				</div>
				<button type="button" class="btn btn-white edit-midia-delete confirm" data-remove="" data-target="">
					Excluir
				</button>
				<button type="submit" class="btn btn-primary px-4 float-right">Salvar</button>
			</form>
		</div>
    </div>

</div>