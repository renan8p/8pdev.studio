<div id="<?php echo (isset($sublista->slug)) ? $sublista->slug : NULL; ?>" data-slug="sublista-<?php echo (isset($sublista->id)) ? $sublista->slug : NULL; ?>" class="custom-sublista form-custom-fields">
	<div class="card">

		<div class="card-header p-3">
			<h4 class="card-header-title">
				<div class="input-group input-group-merge">
					<input type="text" class="form-control form-control-prepended titulo_form" name="titulo_form" value="<?php echo (isset($sublista->titulo)) ? $sublista->titulo : NULL; ?>" placeholder="Título da sublista. Ex: Horários disponíveis" >
					<div class="input-group-prepend">
						<div class="input-group-text">
							<span class="fe fe-list"></span>
						</div>
					</div>
				</div>
			</h4>
			<div class="text-right">
				<a role="button" class="btn confirm" data-remove="#<?php echo (isset($sublista->slug)) ? $sublista->slug : NULL; ?>" data-target="false">
					<i class="fe fe-trash-2"></i>
				</a>
				<span class="btn btn-white btn-move-sublist">
					<i class="fe fe-move"></i> 
				</span>
				<button type="button" class="add-campo btn btn-white px-4 new-field" data-insert="#insert-<?php echo (isset($sublista->slug)) ? $sublista->slug : NULL; ?>" data-toggle="modal" data-target="#fieldsModal">
					Novo campo
				</button>
			</div>
		</div>

		<div class="card-body">
			<div class="form-group">
				<input type="text" placeholder="Descrição" readonly="true" class="form-control">
			</div>
			<div id="insert-<?php echo (isset($sublista->slug)) ? $sublista->slug : NULL; ?>" class="custom-list">
				<?php

					if(isset($sublista_fields)) print_r($sublista_fields)

				?>
			</div>
		</div>

	</div>
</div>