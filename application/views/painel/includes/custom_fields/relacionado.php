<div id="item-field-<?php echo (is_object($field)) ? $field->controller.'-'.$field->ordem : $field; ?>" class="custom-field-control">
  <form class="field">
    <div class="row">
      <input type="hidden" class="custom-field" name="id" value="<?php echo (is_object($field)) ? $field->id : NULL; ?>">      
      <input type="hidden" name="tipo" class="custom-field" value="relacionado"/>

      <!-- Label -->
      <div class="col pr-0">
        <div class="input-group input-group-merge">
          <input type="text" class="custom-field form-control form-control-prepended" placeholder="Label..." name="label" value="<?php echo (is_object($field)) ? $field->label : NULL; ?>">
          <div class="input-group-prepend">
            <div class="input-group-text">
              <span class="fe fe-shuffle"></span>
            </div>
          </div>
        </div>
      </div>

      <!-- Opções -->
      <div class="col-auto">
        <span class="btn btn-white btn-move">
          <i class="fe fe-move"></i>
        </span>
        <a class="btn btn-white" data-toggle="modal" data-target="#item-<?php echo (is_object($field)) ? $field->controller.'-'.$field->ordem : $field; ?>" role="button">
          <i class="fe fe-more-vertical"></i>
        </a>
        <a role="button" class="btn confirm" data-remove="#item-field-<?php echo (is_object($field)) ? $field->controller.'-'.$field->ordem : $field; ?>" data-target="false">
          <i class="fe fe-trash-2"></i>
        </a>
      </div>

    </div>

    <div class="modal fade" id="item-<?php echo (is_object($field)) ? $field->controller.'-'.$field->ordem : $field; ?>" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">

          <div class="modal-header align-items-center bd-bottom py-3">
            <h4 class="modal-title">Configuração de campos</h4>
            <button type="button" class="btn btn-link float-right" data-dismiss="modal">Ok</button>
          </div>

          <div class="modal-body pb-2">
            <div class="row">

              <div class="col-12">
                <div class="form-group">
                  <label>
                    Name / Id
                  </label>
                  <input type="text" class="custom-field form-control" name="name"  value="<?php echo (is_object($field)) ? $field->name : NULL; ?>">
                </div>
              </div>

              <div class="col-12">
                <div class="form-group">
                  <label>
                    Relacionar com
                  </label>
                  <select class="custom-field form-control" name="relacionado">
                    <?php

                      foreach ($relacionado as $key => $rel) {
                        ?>
                        <option value="<?php echo $rel->id; ?>" <?php echo (is_object($field) && $field->relacionado == $rel->id) ? "selected" : NULL; ?>>
                          <?php echo $rel->titulo; ?>
                        </option>
                        <?php
                      }

                    ?>
                  </select>
                </div>
              </div>

              <div class="col-12 col-md-6">
                <div class="form-group">
                  <label>
                    Tipo
                  </label>
                  <select class="custom-field form-control" name="options">
                      <option value="multiple"  <?php echo (is_object($field) && $field->options == "multiple") ? "selected" : NULL; ?>>Múltiplo</option>
                      <option value="single"    <?php echo (is_object($field) && $field->options == "single") ? "selected" : NULL; ?>>Único</option>
                      <option value="categorias"  <?php echo (is_object($field) && $field->options == "categorias") ? "selected" : NULL; ?>>Categorias</option>
                  </select>
                </div>
              </div>

              <div class="col-12 col-md-6">
                <div class="form-group">
                  <label>
                    Ocupar
                  </label>
                  <select class="custom-field form-control" name="coluna">
                      <option value="12" <?php echo (is_object($field) && $field->coluna == "12") ? "selected" : NULL; ?>>Linha toda</option>
                      <option value="6"  <?php echo (is_object($field) && $field->coluna == "6") ? "selected" : NULL; ?>>1 coluna</option>
                  </select>
                </div>
              </div>            

              <div class="col">
                <div class="form-group">
                  <label>
                    Texto de ajuda
                  </label>
                  <input type="text" class="custom-field form-control" name="ajuda" value="<?php echo (is_object($field)) ? $field->ajuda : NULL; ?>">
                </div>
              </div>

              <div class="col-12 col-md-6 <?php echo ($bloco == 'true') ? NULL : "d-none"; ?>">
                <div class="form-group">
                  <label>
                    Bloco
                  </label>
                  <select class="custom-field form-control" name="bloco">
                    <option value="main"    <?php echo (is_object($field) && $field->bloco == "main")    ? "selected" : NULL; ?>>Main</option>
                    <option value="sidebar" <?php echo (is_object($field) && $field->bloco == "sidebar") ? "selected" : NULL; ?>>Sidebar</option>
                  </select>
                </div>
              </div>

            </div>
          </div>

        </div>
      </div>
    </div>

  </form>
</div>