<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white">
  <div class="container-fluid">

    <!-- Toggler -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidebarCollapse" aria-controls="sidebarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <!-- User (xs) -->
    <div class="navbar-user d-md-none">

      <!-- Dropdown -->
      <div class="dropdown">
    
        <!-- Toggle -->
        <a href="#!" id="sidebarIcon" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <div class="avatar avatar-sm avatar-online">
            Nome
          </div>
        </a>

        <!-- Menu -->
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="sidebarIcon">
          <a href="profile-posts.html" class="dropdown-item">Profile</a>
          <a href="settings.html" class="dropdown-item">Settings</a>
          <hr class="dropdown-divider">
          <a href="sign-in.html" class="dropdown-item">Logout</a>
        </div>

      </div>

    </div>

    <!-- Collapse -->
    <div class="collapse navbar-collapse pt-3" id="sidebarCollapse">
      <?php if($this->session->userdata('tipo') == "master"){ ?>
        <h6 class="navbar-heading text-muted">
          Restrito
        </h6>

        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" href="painel/cms">
              <i class="fe fe-unlock"></i> Áreas
            </a>
          </li>
        </ul>  
        
        <hr class="my-3">  
      <?php } ?>

      <h6 class="navbar-heading text-muted">
        Sessões
      </h6>
      <!-- Navigation -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link <?php echo (isset($nav_dashboard)) ? 'active' : NULL; ?>" href="painel/dashboard">
            <i class="fe fe-flag"></i> Dashboard
          </a>
        </li>
        <?php

          foreach ($menu as $key => $m) {

            $link = "painel/sessao/formulario/pagina/" . $m->id;            
            $link = ($m->tipo == "lista" || $m->getDetalhes('conteudo') == 0) ? "painel/sessao/lista/" . $m->id : $link;

            ?>
              <li class="nav-item">
                <a class="nav-link <?php echo (isset($area->id) && $area->id == $m->id) ? 'active' : NULL; ?>" href="<?php echo $link; ?>">
                  <i class="<?php echo $m->icone; ?>"></i> <?php echo $m->titulo; ?>
                </a>
              </li>
            <?php
          }

        ?>
      </ul>

      <hr class="my-3">
      
      <h6 class="navbar-heading text-muted">
        Avançado
      </h6>

      <ul class="navbar-nav">

        <li class="nav-item">
          <a class="nav-link <?php echo (isset($nav_configuracoes)) ? 'active' : NULL; ?>" href="painel/configuracoes">
            <i class="fe fe-settings"></i> Configurações
          </a>
        </li>

        <?php

          if($this->session->userdata('tipo') != 'usuario'){
            ?>
            <li class="nav-item">
              <a class="nav-link <?php echo (isset($nav_usuarios)) ? 'active' : NULL; ?>" href="painel/usuarios/lista">
                <i class="fe fe-users"></i> Usuários
              </a>
            </li>
            <?php
          }

        ?>

        <li class="nav-item btn-midia" data-gerencia="gerencia" data-multiple="multiple">
          <span class="nav-link">
            <i class="fe fe-inbox"></i> Mídias
          </span>
        </li>

        <li class="nav-item">
          <a class="nav-link <?php echo (isset($nav_lixeira)) ? 'active' : NULL; ?>" href="painel/lixeira">
            <i class="fe fe-trash-2"></i> Lixeira
          </a>
        </li>

      </ul> 
      <!-- Heading -->
    </div> <!-- / .navbar-collapse -->

  </div> <!-- / .container-fluid -->
</nav>