<?php

    // echo "<pre>";
    // print_r($contato);
    // exit();

?>

<div class="modal fade" id="servicoModal" tabindex="-1" role="dialog" aria-labelledby="servicoModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header d-flex justify-content-end p-0">
        <button type="button" class="btn close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body pt-0">
        <div class="text-center">
          <i class="far fa-grin-wink font-40 text-success color-2 mb-3"></i>
          <h4 class="color-2">Estamos quase lá</h4>
          <p>Informe seus dados para entrarmos em contato.</p>
        </div>
        <form method="POST" class="mt-4" name="pedido" action="async/pedido">
          <input type="hidden" name="email_contato" value="<?php echo $contato->content('email'); ?>">
          <div class="row">
            <div class="form-group col-md-6 pr-md-2">
              <input type="text" class="form-control" name="nome_pedido" placeholder="Seu nome">
            </div>
            <div class="form-group col-md-6 pl-md-2">
              <input type="tel" class="form-control" name="whats_email_pedido" required placeholder="Whatsapp">
            </div>
          </div>
          <div class="form-group">
            <select class="form-control tipo_projeto">
              <option>Tipo de projeto...</option>
              <?php

                foreach ($servicos as $key => $item) {
                  ?>
                  <option value="<?php echo $item->content('slug'); ?>"><?php echo $item->content('titulo'); ?></option>
                  <?php
                }

              ?>
              <option>Outro</option>
            </select>
          </div>

          <div class="text-center">
            <button type="submit" class="btn btn-primary btn-block">
              Tudo pronto!
            </button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="feedbackModal" tabindex="-1" role="dialog" aria-labelledby="feedbackModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header justify-content-end">
        <button type="button" class="btn close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body pt-0">
        <div class="text-center">
          <i class="far fa-handshake font-40 text-success color-2 mb-3"></i>
          <h4 class="color-2">Tudo certo!</h4>
          <p>Fique esperto que vamos entrar em contato o mais breve possível para falar do seu projeto :D</p>
          <div class="text-center mt-4">
            <button type="button" class="btn btn-primary d-inline-block px-4" data-dismiss="modal">
              Voltar
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="beforeUnloadModal" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header justify-content-end">
        <button type="button" class="btn close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body pt-0">
        <div class="text-center">
          <i class="far fa-grin-beam-sweat font-40 text-success color-2 mb-3"></i>
          <h4 class="color-2">Calma aí!!!</h4>
          <p>Temos uma oferta especial de lançamento da <b class="text-primary">8p dev studio</b>. Preencha o formulário abaixo e receba essa oferta exclusiva.</p>
          <form method="POST" class="mt-4" name="pedido" action="async/pedido">
            <input type="hidden" name="email_contato" value="<?php echo $contato->content('email'); ?>">
            <div class="row">
              <div class="form-group col-md-6 pr-md-2">
                <input type="text" class="form-control" name="nome_pedido" placeholder="Seu nome">
              </div>
              <div class="form-group col-md-6 pl-md-2">
                <input type="tel" class="form-control" name="whats_email_pedido" required placeholder="Whatsapp">
              </div>
            </div>
            <div class="text-center">
              <input type="hidden" name="origem" value="oferta-especial">
              <button type="submit" class="btn btn-primary btn-block">
                Tudo pronto!
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="contato" class="position-relative to-front">
  <div class="gradient-half-primary-v2">
    <div class="container pb-7 pb-md-75 pt-4 pt-md-6">
      <div class="row justify-content-center align-items-center">

        <div class="col mb-4 mb-lg-5 text-center">
            <h2 class="display-4 font-40 mb-2">
              Vamos fazer algo incrível <span class="text-primary">para você!</span>
            </h2>
            <p>Deixe seu contato para combinarmos uma conversa</p>
        </div>

        <div class="col-12">
          <form class="lead-form" method="POST" name="lead_form">

            <input type="hidden" name="email_contato" value="<?php echo $contato->content('email'); ?>">

            <div class="row">
              <div class="col-12 col-lg pr-lg-0">                  
                <div class="form-group mb-lg-0">
                  <input type="text" class="form-control" name="nome" id="banner-nome" placeholder="Digite aqui seu Nome..." aria-label="Seu Whatsapp" required="" data-msg="Campo obrigatório." data-error-class="u-has-error" data-success-class="u-has-success">
                </div>
              </div>
              <div class="col-12 col-lg px-lg-2">
                <div class="form-group mb-lg-0">
                  <input type="text" class="form-control" name="whatsapp" id="banner-whatsapp" placeholder="Digite aqui seu whatsapp..." aria-label="Seu Whatsapp" required="" data-msg="Campo obrigatório." data-error-class="u-has-error" data-success-class="u-has-success">
                </div>
              </div>
              <div class="col-12 col-lg-auto pl-lg-0">                
                <div class="form-group mb-0 text-right">
                  <button type="submit" class="btn btn-primary btn-block px-lg-4 transition-3d-hover">
                    Eu quero!
                    <span class="fas fa-arrow-right small ml-2"></span>
                  </button>
                </div>
              </div>
            </div>

          </form>
        </div>

      </div>
    </div>

    <figure class="position-absolute right-0 bottom-0 mb--4 left-0">
      <img src="assets/site/svg/components/wave-1-bottom-sm.svg" alt="Image Description" class="w-100" height="150">
    </figure>
  </div>
</div>

<footer class="position-relative bg-white to-front">
  <div class="container mt-4 mb-md-5">
    <div class="row justify-content-end">
      <div class="col-sm-6 col-lg mb-5 mb-sm-0 text-center text-md-left">
        <a class="mb-5" href="inicio" aria-label="<?php echo $configuracao->titulo; ?>">
          <img width="160" src="<?php echo $configuracao->get_img('auto', 'logo'); ?>" alt="<?php echo $configuracao->titulo; ?>" title="<?php echo $configuracao->titulo; ?>" class="img-fluid">    
        </a>
      </div>

      <div class="col-sm-6 col-lg-auto mb-5 mb-lg-0 text-center text-md-left">
        <h4 class="h6 font-weight-semi-bold">Links</h4>
        <ul class="list-group list-group-flush list-group-borderless list-group-horizontal mb-0">
          <li class="col-12 p-0"><a class="list-group-item list-group-item-action" href="quem-somos">A empresa</a></li>
          <li class="col-12 p-0"><a class="list-group-item list-group-item-action" href="servicos">Serviços</a></li>
          <li class="col-12 p-0"><a class="list-group-item list-group-item-action" href="portfolio">Portfólio</a></li>
          <li class="col-12 p-0"><a class="list-group-item list-group-item-action" href="contato">Contato</a></li>
        </ul>
      </div>

      <div class="col-sm-6 col-lg-4 mb-5 mb-md-0 text-center text-md-left">
        <h4 class="h6 font-weight-semi-bold">Informações</h4>
        <div class="pt-2 font-14 text-secondary">
          <?php echo $contato->content('cidade'); ?><br><?php echo nl2br($contato->content('endereco')); ?><br>
          <?php echo $contato->content('telefones'); ?><br>
          <b><a href="mail:<?php echo $contato->content('email'); ?>"><?php echo $contato->content('email'); ?></a></b>
          <br><br>
          <?php echo nl2br($contato->content('resumo_contato')); ?>
        </div>
      </div>
    </div>
  </div>
  <div class="container space-1">
    <div class="row justify-content-end">
      <div class="col-md-5 pb-5 pb-md-0 text-center text-md-right to-front">
        <ul class="list-inline mb-0">
          <li class="list-inline-item mx-0">
            <a class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent" target="_blank" href="https://www.facebook.com/8pdev/">
              <span class="fab fa-facebook-f btn-icon__inner"></span>
            </a>
          </li>
          <li class="list-inline-item mx-0">
            <a class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent" target="_blank" href="https://www.twitter.com/8pdev/">
              <span class="fab fa-twitter btn-icon__inner"></span>
            </a>
          </li><!-- 
          <li class="list-inline-item mx-0">
            <a class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent" target="_blank" href="https://www.instagram.com/8pdev/">
              <span class="fab fa-github btn-icon__inner"></span>
            </a>
          </li> -->
          <li class="list-inline-item mx-0">
            <a class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent" target="_blank" href="https://www.instagram.com/8pdev/">
              <span class="fab fa-instagram btn-icon__inner"></span>
            </a>
          </li>
          <li class="list-inline-item mx-0">
            <a class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent" target="_blank" href="https://www.behance.net/8pdev/">
              <span class="fab fa-behance btn-icon__inner"></span>
            </a>
          </li>
          <!-- <li class="list-inline-item mx-0">
            <a class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent" target="_blank" href="https://www.dribbble.com/8pdev/">
              <span class="fab fa-dribbble btn-icon__inner"></span>
            </a>
          </li> -->
        </ul>
      </div>
    </div>
  </div>
  <figure class="w-80 w-md-65 w-lg-50 position-absolute bottom-0 left-0">
    <img src="assets/site/img/elemento-footer.svg" class="ml--1">
  </figure>
</footer>

<div class="position-fixed to-front bottom-0 right-0 pb-4 pr-4">
  <a target="_blank" class="btn d-flex btn-success btn-pill px-md-4" data-whatsapp-number="5517981202314" href="https://api.whatsapp.com/send?phone=5517981202314&amp;text=Oi, quero um orçamento! :D">  
    <span class="d-none d-sm-inline-block pt-1">Chama no whats </span> <i class="fab fa-whatsapp font-weight-normal font-24 ml-sm-3"></i>
  </a>
</div>
