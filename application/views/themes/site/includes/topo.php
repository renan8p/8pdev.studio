<header id="header" class="u-header u-header--abs-top u-header--show-hide header-transparent" data-header-fix-moment="300" data-header-fix-effect="slide">
  <div class="u-header__section">
    <div id="logoAndNav" class="container-fluid py-md-2 px-4">

      <nav class="js-mega-menu navbar navbar-expand-md u-header__navbar u-header__navbar--no-space">
        
        <a class="navbar-brand u-header__navbar-brand" href="inicio" aria-label="<?php echo $configuracao->titulo; ?>">
          <img src="<?php echo $configuracao->get_img('auto', 'logo'); ?>" alt="<?php echo $configuracao->titulo; ?>" title="<?php echo $configuracao->titulo; ?>">
        </a>

        <button type="button" class="navbar-toggler btn u-hamburger"
                aria-label="Toggle navigation"
                aria-expanded="false"
                aria-controls="navBar"
                data-toggle="collapse"
                data-target="#navBar">
          <span id="hamburgerTrigger" class="u-hamburger__box">
            <span class="u-hamburger__inner"></span>
          </span>
        </button>

        <div id="navBar" class="collapse py-2 py-md-0 navbar-collapse u-header__navbar-collapse justify-content-end">
          <ul class="navbar-nav u-header__navbar-nav">
            <li class="nav-item u-header__nav-item">
              <a href="inicio"     class="nav-link u-header__nav-link <?php echo ($page == 'inicio') ? 'active' : NULL; ?>">Home</a>
            </li>            
            <li class="nav-item u-header__nav-item">
              <a href="portfolio"  class="nav-link u-header__nav-link <?php echo ($page == 'portfolio') ? 'active' : NULL; ?>">Portfólio</a>
            </li>
            <li class="nav-item u-header__nav-item">
              <a href="servicos"   class="nav-link u-header__nav-link <?php echo ($page == 'servicos') ? 'active' : NULL; ?>">Serviços</a>
            </li>
            <li class="nav-item u-header__nav-item">
              <a href="quem-somos" class="nav-link u-header__nav-link <?php echo ($page == 'empresa') ? 'active' : NULL; ?>">A empresa</a>
            </li>
            <!-- <li class="nav-item u-header__nav-item">
              <a href="depoimentos" class="nav-link u-header__nav-link scroll-link">Depoimentos</a>
            </li> -->
            <!-- <li class="nav-item u-header__nav-item">
              <a href="blog" class="nav-link u-header__nav-link">Blog</a>
            </li> -->
            <li class="nav-item u-header__nav-item mb-3 mb-md-0 ml-0 ml-md-4 mt-3 mt-md-0">
              <a href="contato" class="btn btn-sm btn-primary transition-3d-hover">Contato</a>
            </li>
          </ul>
        </div>

      </nav>

    </div>
  </div>
</header>

<ul class="m-0 p-4 position-fixed left-0 bottom-0 to-front d-none d-lg-block">
  <li class="list-inline-item d-block mx-0 mt-3">
    <a class="btn btn-sm font-18 btn-icon btn-soft-secondary btn-bg-transparent" target="_blank" href="https://www.facebook.com/8pdev/">
      <span class="fab fa-facebook-f btn-icon__inner"></span>
    </a>
  </li>
  <li class="list-inline-item d-block mx-0 mt-3">
    <a class="btn btn-sm font-18 btn-icon btn-soft-secondary btn-bg-transparent" target="_blank" href="https://www.twitter.com/8pdev/">
      <span class="fab fa-twitter btn-icon__inner"></span>
    </a>
  </li>
  <li class="list-inline-item d-block mx-0 mt-3">
    <a class="btn btn-sm font-18 btn-icon btn-soft-secondary btn-bg-transparent" target="_blank" href="https://www.instagram.com/8pdev/">
      <span class="fab fa-instagram btn-icon__inner"></span>
    </a>
  </li>
  <li class="list-inline-item d-block mx-0 mt-3">
    <a class="btn btn-sm font-18 btn-icon btn-soft-secondary btn-bg-transparent" target="_blank" href="https://www.behance.net/8pdev/">
      <span class="fab fa-behance btn-icon__inner"></span>
    </a>
  </li>
</ul>