<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="theme-color" content="#E24986">
  <meta name="msapplication-navbutton-color" content="#E24986">
  <meta name="apple-mobile-web-app-status-bar-style" content="#E24986">
  <title><?php echo $seo['titulo']; ?></title>
  <base href="<?php echo base_url(); ?>">
  
  <meta name="description" content="<?php echo $seo['description']; ?>">
  <meta name="title" content="<?php echo $seo['titulo']; ?>">

  <meta property="og:type" content="website">
  <meta property="og:title" content="<?php echo $seo['titulo']; ?>">
  <meta property="og:url" content="<?php echo $seo['url']; ?>">
  <meta property="og:description" content="<?php echo $seo['description']; ?>">
  <meta property="og:image" content="<?php echo $seo['image']; ?>">
  <meta property="og:site_name" content="<?php echo $seo['titulo']; ?>">
  <meta property="og:tag" content="<?php echo $seo['keywords']; ?>"/>
  <meta property="og:locale" content="pt_BR">
  <meta name="rating" content="general" />
  <meta name="googlebot" content="index, follow" /> 
  <meta name="audience" content="all" />  
  <meta name="reply-to" content="<?php echo $contato->content('email'); ?>" />
  <meta name="company" content="<?php echo $configuracao->titulo; ?>" />

  <meta itemprop="name" content="<?php echo $seo['titulo']; ?>">
  <meta itemprop="url" content="<?php echo $seo['url']; ?>">
  <meta itemprop="description" content="<?php echo $seo['description']; ?>">
  <meta itemprop="image" content="<?php echo $seo['image']; ?>">
  <meta itemprop="keywords" name="keywords" content="<?php echo $seo['keywords']; ?>"/>

  <!-- Marcação JSON-LD gerada pelo Assistente de marcação para dados estruturados do Google. -->
  <script type="application/ld+json">
  {
    "@context" : "http://schema.org",
    "@type" : "LocalBusiness",
    "priceRange" : "BRL",
    "name" : "8pdev studio",
    "image" : "http://8pdev.studio/upload/2019/09/8p.png",
    "telephone" : "(17) 3234-1426",
    "email" : "oie@8pdev.studio",
    "address" : {
      "@type" : "PostalAddress",
      "streetAddress" : "Rua Minas Gerais, 374, Vila Bom Jesus",
      "addressLocality" : "São José do Rio Preto",
      "addressRegion" : "SP",
      "addressCountry" : "Brasil",
      "postalCode" : "15014-210"
    },
    "openingHoursSpecification": [
    {
      "@type": "OpeningHoursSpecification",
      "dayOfWeek": [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday"
      ],
      "opens": "09:00",
      "closes": "18:00"
    }
  ],
    "url" : "http://8pdev.studio/"
  }
  </script>

  <link rel="shortcut icon" href="<?php echo $configuracao->get_img('lg', 'icone'); ?>">

  <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700,900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="assets/site/fonts/font-awesome.css?v=1">
  <link rel="stylesheet" href="assets/site/vendor/animate.css/animate.min.css?v=1">
  <link rel="stylesheet" href="assets/site/vendor/fancybox/jquery.fancybox.min.css?v=1">
  <link rel="stylesheet" href="assets/site/vendor/slick-carousel/slick/slick.min.css?v=1">
  <link rel="stylesheet" href="assets/site/vendor/cubeportfolio/css/cubeportfolio.min.css?v=1">
  <link rel="stylesheet" href="assets/site/css/theme.css?v=1">
  <link rel="stylesheet" href="assets/site/css/custom.css?v=1">

  <?php echo $configuracao->css; ?>

</head>
<body id="<?php echo $id_body ?>" class="loading <?php echo $page ?>">
  <?php

      print_r($topo);
      print_r($conteudo);
      print_r($rodape);

  ?>    
  <script src="assets/site/vendor/jquery/dist/jquery.min.js?v=1"></script>
  <script src="assets/site/vendor/jquery/dist/jquery.easing.min.js?v=1"></script>
  <script src="assets/site/vendor/jquery-migrate/dist/jquery-migrate.min.js?v=1"></script>
  <script src="assets/site/vendor/popper.js/dist/umd/popper.min.js?v=1"></script>
  <script src="assets/site/vendor/bootstrap/bootstrap.min.js?v=1"></script>
  <script src="assets/site/vendor/svg-injector/dist/svg-injector.min.js?v=1"></script>
  <script src="assets/site/vendor/jquery-validation/dist/jquery.validate.min.js?v=1"></script>
  <script src="assets/site/vendor/fancybox/jquery.fancybox.min.js?v=1"></script>
  <script src="assets/site/vendor/slick-carousel/slick/slick.js?v=1"></script>
  <script src="assets/site/vendor/cubeportfolio/js/jquery.cubeportfolio.min.js?v=1"></script>
  <script src="assets/site/vendor/appear.js?v=1"></script>

  <script src="assets/site/js/hs.core.js?v=1"></script>

  <script src="assets/site/js/components/hs.header.js?v=1"></script>
  <script src="assets/site/js/components/hs.validation.js?v=1"></script>
  <script src="assets/site/js/components/hs.show-animation.js?v=1"></script>
  <script src="assets/site/js/components/hs.onscroll-animation.js?v=1"></script>
  <script src="assets/site/js/components/hs.svg-injector.js?v=1"></script>
  <script src="assets/site/js/components/hs.go-to.js?v=1"></script>
  <script src="assets/site/js/components/jquery.maskedinput.min.js?v=1"></script>
  <script src="assets/site/js/jquery.sticky-kit.min.js?v=1"></script>
  <script src="assets/site/js/js.cookie.js?v=1"></script>
  <script src="assets/site/js/main.js?v=1"></script>
  
  <script src="assets/site/vendor/typed.js/lib/typed.min.js?v=1"></script>
  <script src="assets/site/js/components/hs.cubeportfolio.js?v=1"></script>
  <script src="assets/site/js/components/hs.fancybox.js?v=1"></script>
  <script src="assets/site/js/components/hs.slick-carousel.js?v=1"></script>
  <!-- JS Plugins Init. -->
  <script>      
      $(document).on('ready', function () {

        if($('.cbp').length > 0){
          // initialization of cubeportfolio
          $.HSCore.components.HSCubeportfolio.init('.cbp');
        }

        if($('.js-fancybox').length > 0){
          // initialization of fancybox
          $.HSCore.components.HSFancyBox.init('.js-fancybox');
        }

        if($('#textos-alternando').length > 0){
          var textos = JSON.parse($('#textos-alternando').html());
          $('#textos-alternando').remove();
        }

        if($('.u-text-animation.u-text-animation--typing').length > 0){
          // initialization of text animation (typing)
          var typed = new Typed(".u-text-animation.u-text-animation--typing", {
            strings: textos,
            typeSpeed: 60,
            loop: true,
            backSpeed: 25,
            backDelay: 1500
          });
        }

        if($('.js-slick-carousel').length > 0){
          // initialization of slick carousel
          $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');
        }

      });
  </script>

  <script type="text/javascript" src="assets/site/js/analytics.js?v=1"></script>

  <?php echo (isset($script)) ? $script : NULL; ?>

  <?php echo (isset($contato->codigo)) ? $contato->codigo : NULL; ?>

  <?php echo $configuracao->script; ?>
</body>
</html>