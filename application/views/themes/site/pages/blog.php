  <main id="content" role="main" class="mt-10">
    <div class="bg-light py-3">
      <div class="container">
        <ol class="breadcrumb pl-0">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Blog</li>
        </ol>
      </div>
    </div>
    <div class="container space-2">
      <div class="row">
        <div class="col-lg-9 mb-9 mb-lg-0">
          <div class="space-bottom-2 w-75">
            <h1 class="display-3">Nosso <span class="text-primary">blog</span></h1>
            <p class="w-65">Our duty towards you is to share our experience we're reaching in our work path with you.</p>
          </div>
          <div class="row mx-gutters-2">
            <div class="col-md-7 mb-3" data-animation="fadeInUp">
              <a class="d-flex align-items-end bg-img-hero gradient-overlay-half-primary-v5 transition-blog-hover height-450 rounded-pseudo" href="blog-single.php" style="background-image: url(assets/img/400x500/img8.jpg);">
                <article class="w-100 text-center p-6">
                  <h2 class="h4 text-white">Front &amp; envelope</h2>
                  <div class="mt-4">
                    <div class="u-avatar mx-auto">
                      <img class="img-fluid rounded-circle" src="assets/img/100x100/img3.jpg" alt="Image Description">
                    </div>
                    <strong class="d-block text-white-70 show-on-hover mt-2">Neyton Burchie</strong>
                  </div>
                </article>
              </a>
            </div>
            <div class="col-md-5 mb-3" data-animation="fadeInUp" data-animation-delay="200">
              <a class="d-flex align-items-end bg-img-hero gradient-overlay-half-primary-v5 transition-blog-hover height-450 rounded-pseudo" href="blog-single.php" style="background-image: url(assets/img/400x500/img8.jpg);">
                <article class="w-100 text-center p-6">
                  <h2 class="h4 text-white">Front &amp; envelope</h2>
                  <div class="mt-4">
                    <div class="u-avatar mx-auto">
                      <img class="img-fluid rounded-circle" src="assets/img/100x100/img3.jpg" alt="Image Description">
                    </div>
                    <strong class="d-block text-white-70 show-on-hover mt-2">Neyton Burchie</strong>
                  </div>
                </article>
              </a>
            </div>
            <div class="col-md-6 mb-3" data-animation="fadeInUp">
              <a class="d-flex align-items-end bg-img-hero gradient-overlay-half-primary-v5 transition-blog-hover height-450 rounded-pseudo" href="blog-single.php" style="background-image: url(assets/img/400x500/img8.jpg);">
                <article class="w-100 text-center p-6">
                  <h2 class="h4 text-white">Front &amp; envelope</h2>
                  <div class="mt-4">
                    <div class="u-avatar mx-auto">
                      <img class="img-fluid rounded-circle" src="assets/img/100x100/img3.jpg" alt="Image Description">
                    </div>
                    <strong class="d-block text-white-70 show-on-hover mt-2">Neyton Burchie</strong>
                  </div>
                </article>
              </a>
            </div>
            <div class="col-md-6 mb-3" data-animation="fadeInUp" data-animation-delay="200">
              <a class="d-flex align-items-end bg-img-hero gradient-overlay-half-primary-v5 transition-blog-hover height-450 rounded-pseudo" href="blog-single.php" style="background-image: url(assets/img/400x500/img8.jpg);">
                <article class="w-100 text-center p-6">
                  <h2 class="h4 text-white">Front &amp; envelope</h2>
                  <div class="mt-4">
                    <div class="u-avatar mx-auto">
                      <img class="img-fluid rounded-circle" src="assets/img/100x100/img3.jpg" alt="Image Description">
                    </div>
                    <strong class="d-block text-white-70 show-on-hover mt-2">Neyton Burchie</strong>
                  </div>
                </article>
              </a>
            </div>
            <div class="col-md-5 mb-3" data-animation="fadeInUp">
              <a class="d-flex align-items-end bg-img-hero gradient-overlay-half-primary-v5 transition-blog-hover height-450 rounded-pseudo" href="blog-single.php" style="background-image: url(assets/img/400x500/img8.jpg);">
                <article class="w-100 text-center p-6">
                  <h2 class="h4 text-white">Front &amp; envelope</h2>
                  <div class="mt-4">
                    <div class="u-avatar mx-auto">
                      <img class="img-fluid rounded-circle" src="assets/img/100x100/img3.jpg" alt="Image Description">
                    </div>
                    <strong class="d-block text-white-70 show-on-hover mt-2">Neyton Burchie</strong>
                  </div>
                </article>
              </a>
            </div>
            <div class="col-md-7 mb-3" data-animation="fadeInUp" data-animation-delay="200">
              <a class="d-flex align-items-end bg-img-hero gradient-overlay-half-primary-v5 transition-blog-hover height-450 rounded-pseudo" href="blog-single.php" style="background-image: url(assets/img/400x500/img8.jpg);">
                <article class="w-100 text-center p-6">
                  <h2 class="h4 text-white">Front &amp; envelope</h2>
                  <div class="mt-4">
                    <div class="u-avatar mx-auto">
                      <img class="img-fluid rounded-circle" src="assets/img/100x100/img3.jpg" alt="Image Description">
                    </div>
                    <strong class="d-block text-white-70 show-on-hover mt-2">Neyton Burchie</strong>
                  </div>
                </article>
              </a>
            </div>
            <div class="col-md-6 mb-3" data-animation="fadeInUp">
              <a class="d-flex align-items-end bg-img-hero gradient-overlay-half-primary-v5 transition-blog-hover height-450 rounded-pseudo" href="blog-single.php" style="background-image: url(assets/img/400x500/img8.jpg);">
                <article class="w-100 text-center p-6">
                  <h2 class="h4 text-white">Front &amp; envelope</h2>
                  <div class="mt-4">
                    <div class="u-avatar mx-auto">
                      <img class="img-fluid rounded-circle" src="assets/img/100x100/img3.jpg" alt="Image Description">
                    </div>
                    <strong class="d-block text-white-70 show-on-hover mt-2">Neyton Burchie</strong>
                  </div>
                </article>
              </a>
            </div>
            <div class="col-md-6 mb-3" data-animation="fadeInUp" data-animation-delay="200">
              <a class="d-flex align-items-end bg-img-hero gradient-overlay-half-primary-v5 transition-blog-hover height-450 rounded-pseudo" href="blog-single.php" style="background-image: url(assets/img/400x500/img8.jpg);">
                <article class="w-100 text-center p-6">
                  <h2 class="h4 text-white">Front &amp; envelope</h2>
                  <div class="mt-4">
                    <div class="u-avatar mx-auto">
                      <img class="img-fluid rounded-circle" src="assets/img/100x100/img3.jpg" alt="Image Description">
                    </div>
                    <strong class="d-block text-white-70 show-on-hover mt-2">Neyton Burchie</strong>
                  </div>
                </article>
              </a>
            </div>
            <div class="col-12 mb-3" data-animation="fadeInUp">
              <a class="d-flex align-items-end bg-img-hero gradient-overlay-half-primary-v5 transition-blog-hover height-450 rounded-pseudo" href="blog-single.php" style="background-image: url(assets/img/400x500/img8.jpg);">
                <article class="w-100 text-center p-6">
                  <h2 class="h4 text-white">Front &amp; envelope</h2>
                  <div class="mt-4">
                    <div class="u-avatar mx-auto">
                      <img class="img-fluid rounded-circle" src="assets/img/100x100/img3.jpg" alt="Image Description">
                    </div>
                    <strong class="d-block text-white-70 show-on-hover mt-2">Neyton Burchie</strong>
                  </div>
                </article>
              </a>
            </div>
            <div class="col-md-6 mb-3" data-animation="fadeInUp">
              <a class="d-flex align-items-end bg-img-hero gradient-overlay-half-primary-v5 transition-blog-hover height-450 rounded-pseudo" href="blog-single.php" style="background-image: url(assets/img/400x500/img8.jpg);">
                <article class="w-100 text-center p-6">
                  <h2 class="h4 text-white">Front &amp; envelope</h2>
                  <div class="mt-4">
                    <div class="u-avatar mx-auto">
                      <img class="img-fluid rounded-circle" src="assets/img/100x100/img3.jpg" alt="Image Description">
                    </div>
                    <strong class="d-block text-white-70 show-on-hover mt-2">Neyton Burchie</strong>
                  </div>
                </article>
              </a>
            </div>
            <div class="col-md-6 mb-3" data-animation="fadeInUp" data-animation-delay="200">
              <a class="d-flex align-items-end bg-img-hero gradient-overlay-half-primary-v5 transition-blog-hover height-450 rounded-pseudo" href="blog-single.php" style="background-image: url(assets/img/400x500/img8.jpg);">
                <article class="w-100 text-center p-6">
                  <h2 class="h4 text-white">Front &amp; envelope</h2>
                  <div class="mt-4">
                    <div class="u-avatar mx-auto">
                      <img class="img-fluid rounded-circle" src="assets/img/100x100/img3.jpg" alt="Image Description">
                    </div>
                    <strong class="d-block text-white-70 show-on-hover mt-2">Neyton Burchie</strong>
                  </div>
                </article>
              </a>
            </div>
          </div>
          <div class="space-bottom-2"></div>
          <nav>
            <ul class="pagination mb-0 justify-content-center">
              <li class="page-item ml-0">
                <a class="page-link" href="#" aria-label="Anterior">
                  <span aria-hidden="true"><i class="far fa-angle-left"></i></span>
                  <span class="sr-only">Anterior</span>
                </a>
              </li>
              <li class="page-item active"><a class="page-link" href="#">1</a></li>
              <li class="page-item"><a class="page-link" href="#">2</a></li>
              <li class="page-item disabled"><a class="page-link" href="#">...</a></li>
              <li class="page-item"><a class="page-link" href="#">12</a></li>
              <li class="page-item">
                <a class="page-link" href="#" aria-label="Próximo">
                  <span aria-hidden="true"><i class="far fa-angle-right"></i></span>
                  <span class="sr-only">Próximo</span>
                </a>
              </li>
            </ul>
          </nav>
        </div>
        <div id="stickyBlockStartPoint" class="col-lg-3 order-lg-1">
          <div class="js-sticky-block"
               data-offset-target="#logoAndNav"
               data-parent="#stickyBlockStartPoint"
               data-sticky-view="lg"
               data-start-point="#stickyBlockStartPoint"
               data-end-point="#stickyBlockEndPoint"
               data-offset-top="32"
               data-offset-bottom="170">
            <h3 class="h5 text-primary mb-4">News</h3>
            <article class="card border-0 mb-5">
              <div class="card-body p-0">
                <div class="media">
                  <div class="u-avatar mr-3">
                    <img class="img-fluid rounded" src="assets/img/100x100/img5.jpg" alt="Image Description">
                  </div>
                  <div class="media-body">
                    <h4 class="h6 font-weight-normal mb-0">
                      <a href="#">
                        Homemade Clabatta donuts
                      </a>
                    </h4>
                  </div>
                </div>
              </div>
            </article>
            <article class="card border-0 mb-5">
              <div class="card-body p-0">
                <div class="media">
                  <div class="u-avatar mr-3">
                    <img class="img-fluid rounded" src="assets/img/100x100/img7.jpg" alt="Image Description">
                  </div>
                  <div class="media-body">
                    <h4 class="h6 font-weight-normal mb-0">
                      <a href="#">
                        Spruce up your coffee table
                      </a>
                    </h4>
                  </div>
                </div>
              </div>
            </article>
            <article class="card border-0 mb-5">
              <div class="card-body p-0">
                <div class="media">
                  <div class="u-avatar mr-3">
                    <img class="img-fluid rounded" src="assets/img/100x100/img6.jpg" alt="Image Description">
                  </div>
                  <div class="media-body">
                    <h4 class="h6 font-weight-normal mb-0">
                      <a href="#">
                        Breating in the crisp air of cozy place
                      </a>
                    </h4>
                  </div>
                </div>
              </div>
            </article>
            <hr class="my-7">
            <h3 class="h5 text-primary mb-4">Tags</h3>
            <ul class="list-inline mb-0">
              <li class="list-inline-item p-1 m-0">
                <a class="btn btn-xs btn-soft-secondary btn-pill" href="#">Design</a>
              </li>
              <li class="list-inline-item p-1 m-0">
                <a class="btn btn-xs btn-soft-secondary btn-pill" href="#">Art</a>
              </li>
              <li class="list-inline-item p-1 m-0">
                <a class="btn btn-xs btn-soft-secondary btn-pill" href="#">Graphic</a>
              </li>
              <li class="list-inline-item p-1 m-0">
                <a class="btn btn-xs btn-soft-secondary btn-pill" href="#">CSS</a>
              </li>
              <li class="list-inline-item p-1 m-0">
                <a class="btn btn-xs btn-soft-secondary btn-pill" href="#">HTML</a>
              </li>
              <li class="list-inline-item p-1 m-0">
                <a class="btn btn-xs btn-soft-secondary btn-pill" href="#">SASS</a>
              </li>
              <li class="list-inline-item p-1 m-0">
                <a class="btn btn-xs btn-soft-secondary btn-pill" href="#">WordPress</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div id="stickyBlockEndPoint"></div>
  </main>