<!--   <div class="position-relative bg-img-hero-center" style="background-image: url(<?php echo $servico->get_img('auto'); ?>);">
    <div class="container space-bottom-3 space-top-2">
      <div class="w-md-65 w-lg-50">

        <ol class="breadcrumb pl-0 mb-2">
          <li class="breadcrumb-item"><a href="/">Início</a></li>
          <li class="breadcrumb-item active" aria-current="page"><?php echo $servico->content('titulo'); ?></li>
        </ol>

        <h1><?php echo $servico->content('chamada_servicos'); ?></h1>
        <h2 class="h1 text-primary font-weight-medium"><?php echo $servico->content('subtitulo_servicos'); ?></h2>
      </div>
    </div>

    <figure class="position-absolute right-0 bottom-0 left-0">
      <img src="assets/site/svg/components/wave-1-bottom-sm.svg" alt="Image Description" class="w-100" height="150">
    </figure>
  </div> -->

  <div class="container pt-75 pb-0 pb-md-5">
    <div class="w-md-65 w-lg-50">

      <ol class="breadcrumb pl-0 mb-2">
        <li class="breadcrumb-item"><a href="/">Início</a></li>
        <li class="breadcrumb-item active" aria-current="page"><?php echo $servico->content('titulo'); ?></li>
      </ol>

      <h1><?php echo $servico->content('chamada_servicos'); ?></h1>
      <h2 class="h1 text-primary font-weight-medium"><?php echo $servico->content('subtitulo_servicos'); ?></h2>

    </div>
  </div>

  <main id="content" role="main">
    <div id="servicos" class="position-relative pb-5 pb-md-6">

      <?php

        foreach ($servicos as $key => $item) {
          ?>
            <div class="container" id="<?php echo $item->content('slug'); ?>">
              <div class="row align-items-center justify-content-between <?php echo ($key%2 == 0) ? NULL : 'flex-md-row-reverse'; ?>">
                <div class="col-md-5 space-1 text-center">
                  <img src="<?php echo $item->get_img('auto', 'imagem_destaque'); ?>" alt="<?php echo $item->content('titulo'); ?>" class="img-fluid">
                </div>
                <div class="col-md-5 space-1">
                  <h3 class="display-3 mb-4"><?php echo $item->content('titulo'); ?></h3>
                  <div class="mb-5"><?php echo $item->content('resumo_longo_servico'); ?></div>
                  <div class="d-flex justify-content-between">
                    <button type="button" class="btn btn-primary px-5 transition-3d-hover" data-tipo-projeto="<?php echo $item->content('slug'); ?>" data-toggle="modal" data-target="#servicoModal">
                      Quero este!
                      <!-- <span class="fas fa-angle-right ml-2"></span> -->
                    </button>
                  </div>
                </div>
              </div>
            </div>

          <?php

            if($key+1 < count($servicos)){
              ?>
              <div class="container">
                <div class="w-lg-65 mx-lg-auto">
                  <hr class="my-3 my-md-6">
                </div>
              </div>
              <?php
            }
        }

      ?>

      </div>
    </div>

    <section id="servicos" class="pb-5 pb-lg-7 pt-6">
      <div class="container">
        <div class="mb-9">
          <div class="w-md-80 text-center mx-md-auto mb-3">
            <h2 class="display-3 title-theme"><?php echo $servico->content('titulo_extra'); ?></h2>
          </div>
        </div>
        <div class="row">
        <?php

            foreach ($servico->sublistas['outros_servicos']->items as $key => $item) {
              ?>
                <div class="col-lg-6 p-3">
                  <div class="card card-hover-border h-100 p-2">
                    <div class="row align-items-center pr-md-4">
                      <div class="col-md-4 pr-md-0">
                        <img class="img-fluid" src="<?php echo $item->get_img('xs', 'imagem_extra'); ?>" alt="<?php echo $item->content('titulo') ?>">
                      </div>
                      <div class="col-md pl-0">
                        <div class="px-4 py-3 py-md-4">
                          <h3 class="h4"><?php echo $item->content('titulo') ?></h3>
                          <p class="mb-0 mb-md-0 1 text-body"><?php echo $item->content('servico_extra') ?></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              <?php
            }

        ?>
        </div>
      </div>
    </section>

    <section class="pb-5 pb-md-6 pb-lg-7">
      <div class="container">
        <div class="row">

            <div class="col-12 col-lg-4">
              <article class="card border-0 bg-primary text-white shadow-primary-lg transition-3d-hover mb-4 mb-lg-0">
                <div class="card-body text-white p-5">
                  <small class="text-white-70 mb-3">O que você vê:</small>
                  <h3 class="h5 mb-3 text-white">UX/UI DESIGN</h3>
                  <p class="opacity-75 mb-4">Usamos técnicas de ux e ui design para desenvolver uma aplicação que realmente atenda sua necessidade, além de ser lindo, claro!</p>
                  <a class="btn btn-sm btn-soft-white transition-3d-hover" href="servicos/ux-ux-design">Ler mais</a>
                </div>
              </article>
            </div>

            <div class="col-12 col-lg-4">
              <article class="card border-0 shadow-sm transition-3d-hover mb-4 mb-lg-0">
                <div class="card-body p-5">
                  <small class="text-primary mb-1">O que você não vê:</small>
                  <h3 class="h5 mb-3">FRONT / BACK</h3>
                  <p class="mb-4">Somos profissionais jovens, porém, com a experiência necessária para transformar a sua necessidade em código e solução!</p>
                  <a class="btn btn-sm btn-soft-primary transition-3d-hover" href="servicos/front-end-back-end">Ler mais</a>
                </div>
              </article>
            </div>

            <div class="col-12 col-lg-4">
              <article class="card border-0 shadow-sm transition-3d-hover mb-4 mb-lg-0">
                <div class="card-body p-5">
                  <small class="text-primary mb-1">O que entregamos:</small>
                  <h3 class="h5 mb-3">Website / Sistema / App</h3>
                  <p class="mb-4">Sua aplicação web será desenvolvida e estruturada estratégicamente para entrar no mercado e competir com o que tiver de mais atual.</p>
                  <a class="btn btn-sm btn-soft-primary transition-3d-hover" href="servicos/web-site-cracao-de-aplicativos">Ler mais</a>
                </div>
              </article>
            </div>

        </div>
      </div>
    </section>

  </main>