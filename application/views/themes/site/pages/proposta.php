<style type="text/css">
	body{
		font-size: 1.2rem;
	}
	p strong {
		color: inherit;
	}
	.investimento p strong{
		color: #e24986;
		font-size: 1.6rem;
	}
</style>

  <main id="content" role="main">
  	<a class="position-absolute p-4" href="index.php" aria-label="8p dev studio">
		<img src="assets/site/img/logo.svg" alt="8p dev studio" title="8p dev studio" width="200" class="img-fluid">
	</a>
    <div id="banner" class="d-lg-flex position-relative min-height-100vh d-flex align-items-center">		
      <div class="container">
        <div class="row align-items-center justify-content-between">
          <div class="col-lg d-none d-lg-block">
            <div id="svg8p"></div>    
          </div>
          <div class="col-lg-5 offset-lg-1 mt-5 mt-lg-0">
            <div class="mb-5 text-center text-lg-left">              
              <h1 class="display-1 font-48 font-size-md-down-5 mb-2">Proposta para <?php echo $proposta->content('cliente'); ?></h1>
              <h5 class="font-size-2 text-primary font-weight-black"><?php echo $proposta->content('servico'); ?></h5>
            </div>
            <p>Índice:</p>
            <ol>
              <li class="mb-2"><a href="#investimento" class="js-go-to" data-target="#investimento">Investimento</a></li>
              <li class="mb-2"><a href="#descricao" class="js-go-to" data-target="#descricao">Descrição do Projeto</a></li>
              <li class="mb-2"><a href="#desenvolvimento" class="js-go-to" data-target="#desenvolvimento">Etapas de Desenvolvimento</a></li>
              <li class="mb-2"><a href="#condicoes" class="js-go-to" data-target="#condicoes">Condições Gerais</a></li>
              <li class="mb-2"><a href="#aprovacao" class="js-go-to" data-target="#aprovacao">Aprovação da proposta</a></li>
              <li class="mb-2"><a href="#parceria" class="js-go-to" data-target="#parceria">Parceria 8p</a></li>
            </ol>
          </div>
        </div>
      </div>
    </div>
    <div id="conteudo" class="space-bottom-2">
      <div class="container">
        <div class="w-lg-50 mx-auto">
			<h2 id="investimento" class="display-2 mb-5">1. Investimento</h2>         
			<div class="investimento">
				<?php echo $proposta->content('texto_investimento'); ?>
			</div>
			<hr class="my-10">

			<h2 id="desenvolvimento" class="display-2 mb-5">2. Descrição do Projeto</h2>
			<?php echo $proposta->content('texto_projeto'); ?>

          <hr class="my-10">  

          <h2 id="desenvolvimento" class="display-2 mb-5">3. Etapas de Desenvolvimento</h2>

          <?php echo $proposta->content('texto_etapas'); ?>

          <h3 class="display-4 mt-6 mb-4">Produtividade</h3>
          <p>Todo o andamento das etapas de desenvolvimento do projeto serão atualizadas e informadas através da ferramenta estabelecida pelo cliente.</p>
          <p>É recomendado realizar reuniões entre as etapas para servir de revisão, acompanhamento e solicitação de alterações.</p>
          <h3 class="display-4 mt-6 mb-4">Manutenção</h3>
          <p>Trabalhos com duas possibilidades que o cliente pode escolher qual melhor convém:</p>
          <ul>
            <li>Após a publicação do projeto, fica definido um prazo limite de <b>20 dias</b> úteis com direito a <b>40 horas de desenvolvimento</b> para solicitação de ajustes que não fujam do escopo inicial, sem gerar nenhum custo extra. Após este período os ajustes serão reconsiderados e orçados.</li>
          </ul>
          <strong class="pl-6 my-4 d-block">ou</strong>
          <ul>
            <li>Será cobrada uma taxa mensal de <b>R$240,00</b> pela manutenção do site onde as alterações sempre estarão inclusas se forem possíveis e viáveis. Com contrato de no mínimo 6 meses.</li>
          </ul>
          <hr class="my-10">
          <h2 id="condicoes" class="display-2 mb-5">4. Condições Gerais</h2>
          <h3 class="display-4 mb-4 mt-6">Prazo</h3>
          <p>O prazo para finalização do projeto é de <b><?php echo $proposta->content('prazo'); ?></b>.</p>
          <p>Os prazos para desenvolvimento do projeto estão atrelados ao fornecimento das informações pelo cliente e o escopo inicial. Não estão contabilizados atrasos decorrentes da demora de aprovações ou revisões.</p>
          <h3 class="display-4 mb-4 mt-6">Serviços de terceiros</h3>
          <p>Não estão incluídos serviços de terceiros como tradução, revisão, compra de imagens, ilustrações, hospedagem ou domínios. A contratação e pagamento destes serviços deve ser feita pelo cliente.</p>
          <h3 class="display-4 mb-4 mt-6">Valores</h3>
          <p>O valor desta proposta poderá ser revisto caso ocorram alterações no briefing ou na complexidade do trabalho; alterações de prazos estabelecidos; atraso por parte do cliente na entrega de materiais; atraso nas aprovações necessárias ao desenvolvimento do trabalho; aplicação do projeto em outras peças não especificadas nesta proposta; ou alteração e aperfeiçoamento de um item do projeto que extrapole a concepção original do projeto.</p>
          <h2 class="display-4 mb-4 mt-6">Treinamento</h2>
          <p>Após a entrega do projeto, realizaremos uma reunião para ensinar a gerenciar os painéis de gerenciamento e tirar quiasquer dúvidas que ainda restarem.</p>
          <h3 class="display-4 mb-4 mt-6">Cancelamento durante o projeto</h3>
          <p>Se por alguma razão o cliente escolher cancelar o projeto durante sua execução, onde ideias e propostas já tiverem sido discutidas e desenvolvidas, será cobrada uma porcentagem proporcional a parte do projeto que o cliente recebeu.</p>
          <p>Se a <b>8pdev.studio</b> por alguma razão for impossibilitada de continuar o projeto, devolverá ao cliente o valor proporcional a parte do projeto que o cliente não recebeu. Estas opções podem ser negociadas e estão abertas a sugestões.</p>
          <h3 class="display-4 mb-4 mt-6">Suspensão do projeto</h3>
          <p>O projeto pode ser suspenso caso o cliente: interfira com exigências incoerentes com o briefing aprovado e não aceite renegociar o valor; atrase excessivamente a entrega de materiais para desenvolvimento do projeto ou atrase o pagamento sem justificativa. Caso não seja possível contornar estas situações a suspensão do projeto não estará sujeita a reembolso.</p>
          <h3 class="display-4 mb-4 mt-6">Registro de patente</h3>
          <p>A <b>8pdev.studio</b> não é responsável por nenhum tipo de registro legal de marca, projeto ou nome. Estas questões são de inteira responsabilidade do cliente.</p>
          <h3 class="display-4 mb-4 mt-6">Propriedade</h3>
          <p>Eventuais materiais que forem fornecidos pelo cliente para referência ou utilização no projeto são de inteira responsabilidade do cliente e não serão utilizadas para outros fins.</p>
          <p>Todos os materiais de preparação, pesquisas, rascunhos e imagens finais do projeto permanecem sob propriedade da <b>8pdev.studio</b> e poderão ser usados em seu portfólio. As artes-finais/arquivos digitais passarão a ser propriedade do cliente somente após o pagamento final.</p>
          <p>Aplica-se ao projeto ora citado as normas dispostas na Lei nº 9.610/98 (Lei de Direito de Autor), pelo que o crédito autoral sobre o trabalho objeto deste contrato deve ser sempre indicado.</p>
          <hr class="my-10">
          <h2 id="aprovacao" class="display-2 mb-5">5. Aprovação da Proposta</h2>
          <p>Caso esteja de acordo com as condições acima o cliente deverá validar a aprovação formal por e-mail, em resposta ao envio desta proposta. Após aprovação o cliente deve providenciar o primeiro pagamento para que o projeto possa ser iniciado.</p>
          <h2 class="display-4 mb-4 mt-6">Finalizando</h2>
          <p>Agradecemos pela oportunidade de preparar esta proposta. Se chegamos até aqui é porque acredita que podemos ajudá-lo. Caso esta seja aprovada garantimos que será feito o melhor para alcançar todas as expectativas.</p>
          <br>
          <small>Att, Renan Breno | <a href="http://8pdev.studio" target="_blank">8pdev.studio</a></small>
          <br>
          <small><a href="tel:+551732341426" target="_blank">(17) 3234-1426</a> ou <a target="_blank" href="https://api.whatsapp.com/send?phone=5517981202314&text=Olá! Quero um orçamento :D">(17) 99242-7270 (Whatsapp)</a> - <a href="mailto:renan@8pdev.studio">renan@8pdev.studio</a></small>
        </div>
      </div>
    </div>
    <div id="parceria" class="svg-preloader position-relative d-none">
      <figure class="position-absolute right-0 bottom-0 left-0">
        <img class="js-svg-injector" src="assets/svg/components/wave-1-bottom-sm.svg" alt="Image Description" data-parent="#parceria">
      </figure>
      <div class="gradient-half-primary-v3">
        <div class="container space-top-2 space-top-md-3 space-bottom-1">
          <div class="w-lg-75 mx-auto text-lg-center space-bottom-2">
            <h2 class="display-2 mb-5">Parceria <span class="text-primary">8p</span></h2>
            <p>A <b>8p</b> está apenas começando e queremos criar fortes alianças com nossos parceiros, para isso acreditamos numa parceria recompasadora para ambos os lados.</p>
          </div>
          <div class="row justify-content-lg-between align-items-center align-items-xl-start">
            <div class="col-lg-6 mb-7 mb-lg-0 space-bottom-lg-2 order-lg-1">
              <p>Como funciona:</p>
              <ol>
                <li>Indique a <b>8p</b> para seu círculo de parceiros</li>
                <li>No primeiro parceiro indicado que criar um site, app ou sistema com a gente você recebe <strong>15% de desconto</strong> no seu projeto atual ou em outros serviços futuros</li>
                <li>No segundo, você recebe <strong>20% de desconto</strong></li>
                <li>E no terceiro, você recebe <strong>25% de desconto</strong></li>
              </ol>
            </div>
            <div class="col-lg-6 mt-xl-auto">
              <figure class="ie-working-men">
                <img class="js-svg-injector" src="assets/svg/illustrations/app-development.svg" alt="SVG Illustration" data-parent="#parceria">
              </figure>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>

<div class="position-fixed to-front bottom-0 right-0 pb-4 pr-4">
  <a target="_blank" class="btn d-flex btn-success btn-pill px-md-4" data-whatsapp-number="5517981202314" href="https://api.whatsapp.com/send?phone=5517981202314&amp;text=Oi, tenho algumas dúvidas!">  
    <span class="d-none d-sm-inline-block pt-1">Chama no whats </span> <i class="fab fa-whatsapp font-weight-normal font-24 ml-sm-3"></i>
  </a>
</div>