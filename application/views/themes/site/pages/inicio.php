  <?php
    /*
    echo "<pre>";
    echo print_r($inicio);
    echo "</pre>";
    */
  ?>

  <main id="content" role="main" class="pt-10 pt-lg-0">
    <section id="banner" class="position-relative">
      <div class="container-fluid overflow-hidden">
        <div class="row flex-row-reverse">

          <div class="col-12 col-lg-7 p-0">

            <div class="js-slick-carousel u-slick"
              data-numbered-pagination="#slickPaging"
              data-infinite="true"
              data-autoplay="true"
              data-autoplay-speed="3000">

              <?php 

                foreach ($home as $key => $item) {
                  ?>
                  <div class="js-slide">
                    <div class="d-flex vh-lg-100 justify-content-center align-items-center" style="background: <?php echo $item->content('cor'); ?>;">
                      <div class="col-10 col-xl-7">
                        <img src="<?php echo $item->get_img('lg', 'img'); ?>" class="img-fluid">
                      </div>
                    </div>
                  </div>
                  <?php
                }

              ?>

            </div>
            <div id="slickPaging" class="u-slick__paging pb-lg-6 pr-sm-4"></div>
          </div>

          <div class="col col-lg-5 shadow-right z-index-2">
            <div class="d-flex vh-lg-100 align-items-center justify-content-center justify-content-lg-end">

              <div class="max-550 d-inline-block text-left py-5 py-lg-0 px-md-4 px-xl-6">

                <div class="mb-md-4">
                  <h1 class="display-1 font-48 mb-2"><?php echo $inicio->content('chamada'); ?></h1>
                </div>
                <div>
                  <div id="textos-alternando" class="d-none">
                    <?php
                      $termos = array();
                      foreach ($inicio->sublistas['termos_alternando']->items as $key => $item) {
                        array_push($termos, $item->content('titulo'));
                      }
                      echo json_encode($termos);
                    ?>                
                  </div>
                  <h3 class="font-20 mb-4 my-md-4 d-none">
                    <?php echo $inicio->content('texto_base_dos_termos_alternando'); ?>
                    <span class="text-primary">
                      <strong class="u-text-animation u-text-animation--typing"></strong>
                    </span>
                  </h3>

                  <?php echo $inicio->content('texto_curto'); ?>
                </div>
                <form class="lead-form mt-4" method="POST" name="lead_form">
                  <input type="hidden" name="email_contato" value="<?php echo $contato->content('email'); ?>">

                  <div class="form-group mb-3">
                    <input type="text" class="form-control" name="nome" id="banner-nome" placeholder="Digite seu nome..." aria-label="Seu Nome" required="" data-msg="Campo obrigatório." data-error-class="u-has-error" data-success-class="u-has-success">
                  </div>

                  <div class="form-group mb-3">
                    <input type="tel" class="form-control" name="whatsapp" id="banner-whatsapp" placeholder="Digite aqui seu whatsapp..." aria-label="Seu Whatsapp" required="" data-msg="Campo obrigatório." data-error-class="u-has-error" data-success-class="u-has-success">
                  </div>

                  <div class="form-group mb-0">
                    <button type="submit" class="btn btn-primary btn-block transition-3d-hover">Quero um orçamento <span class="fas fa-arrow-right small ml-2"></span></button>
                  </div>
                </form>
              </div>

            </div>
          </div>

        </div>
      </div>
      <a class="js-go-to u-go-to-ver-arrow to-front" href="javascript:;" data-target="#quem-somos" data-position='{"bottom": "1.5rem", "left": "calc(50% - 20px)"}' data-type="absolute" data-offset-top="100" data-compensation="#header" data-show-effect="fadeOut" data-hide-effect="fadeIn">
        <span class="fal fa-long-arrow-down u-go-to-ver-arrow__inner animated fadeOutDown infinite"></span>
      </a>
    </section>

    <hr class="mt-0 mb-6 d-none d-lg-block">

    <section id="quem-somos" class="py-5 py-lg-7">
      <div class="container">
        <div class="row justify-content-between align-items-center">

          <div class="col-lg-5 mb-5 mb-lg-0">
            <div class="pr-md-4">
              <h2 class="display-2 title-theme"><?php echo $inicio->content('tituloquemsomos'); ?></h2>
              <h4 class="text-secondary my-4"><?php echo $inicio->content('subtitulo'); ?></h4>
              <div class="mb-4">
                <?php echo $inicio->content('chamadaquemsomos'); ?>
              </div>              
              <div class="d-flex justify-content-between">
                <a class="btn btn-success btn-wide px-4 transition-3d-hover" href="contato">Quero conversar!</a>              
                <a class="btn btn-link btn-wide transition-3d-hover" href="quem-somos"><b>Saiba mais</b> <i class="fa fa-angle-right ml-2"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-6 position-relative">
            <div id="svg8p"></div>
            <?php /* <div class="row mx-gutters-2 justify-content-center">
              <div class="col-5 align-self-end px-2 mb-3 d-none">                
                  <img class="img-fluid rounded" src="<?php echo $inicio->get_img('xs','foto_quem_somos_1'); ?>" alt="<?php echo $inicio->content('tituloquemsomos'); ?>">
                  <span class="u-media-viewer__container">
                    <span class="u-media-viewer__icon">
                      <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                    </span>
                  </span>
              </div>
              <div class="col-7 px-2 mb-3">                
                  <img class="img-fluid rounded" height="288" src="<?php echo $inicio->get_img('xs','foto_quem_somos_2'); ?>" alt="<?php echo $inicio->content('tituloquemsomos'); ?>">
                  <span class="u-media-viewer__container">
                    <span class="u-media-viewer__icon">
                      <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                    </span>
                  </span>
              </div>
              <div class="col-5 offset-1 px-2 mb-3">                
                  <img class="img-fluid rounded" height="288" src="<?php echo $inicio->get_img('xs','foto_quem_somos_3'); ?>" alt="<?php echo $inicio->content('tituloquemsomos'); ?>">
                  <span class="u-media-viewer__container">
                    <span class="u-media-viewer__icon">
                      <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                    </span>
                  </span>
              </div>
              <div class="col-5 px-2 mb-3">                
                  <img class="img-fluid rounded" height="288" src="<?php echo $inicio->get_img('xs','foto_quem_somos_4'); ?>" alt="<?php echo $inicio->content('tituloquemsomos'); ?>">
                  <span class="u-media-viewer__container">
                    <span class="u-media-viewer__icon">
                      <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                    </span>
                  </span>
              </div>
            </div>
            <div class="w-100 content-centered-y z-index-n1">
              <figure class="ie-soft-triangle-shape">
                <img src="assets/site/img/fundo_1.png" class="img-fluid">
              </figure>
            </div> */ ?>
          </div>

        </div>
      </div>
    </section>

    <section class="container py-5 py-lg-7">
      <!-- Title -->
      <div class="text-center mb-4 mb-md-9">        
        <div class="mb-3 d-inline-block">
          <h2 class="display-3 title-theme"><?php echo $inicio->content('tituloservicos'); ?></h2>
        </div>
        <!-- 
        <div class="w-md-80 w-lg-60 text-center mx-md-auto mb-9">
          <p><?php echo $inicio->content('chamadaservicos'); ?></p>
        </div>
        -->
      </div>
      <!-- End Title -->

      <div class="row justify-content-center mx-gutters-2">
        <?php

            foreach ($servicos as $key => $item) {
              ?>
                <div class="col-sm-6 col-lg-4 mb-6">
                  <div class="text-center">

                    <figure class="mb-4">
                      <img src="<?php echo $item->get_img('xs'); ?>" height="200" alt="<?php echo $item->content('titulo'); ?>">
                    </figure>

                    <div class="px-5">
                      <div class="mb-4">
                        <h3 class="h5"><?php echo $item->content('titulo'); ?></h3>
                        <p class="mb-md-0"><?php echo $item->content('resumo_servico'); ?>
                        <a class="text-primary" href="servicos/<?php echo Lazy::getSlug($item->content('titulo')); ?>">Ler mais</a></p>
                      </div>
                      <button type="button" class="btn btn-soft-primary px-4 transition-3d-hover" data-tipo-projeto="<?php echo $item->content('slug'); ?>" data-toggle="modal" data-target="#servicoModal">
                        Quero este!
                      </button>
                    </div>

                  </div>
                </div>
              <?php
            }

        ?>
      </div>
    </section>

    <section id="portfolio" class="py-5 pb-md-6 overflow-hidden">
      <div class="container">

        <div class="mb-9">
          <div class="w-md-80 w-lg-60 text-center mx-md-auto mb-3">
            <h2 class="display-3 title-theme"><?php echo $inicio->content('tituloportfolio'); ?></h2>
          </div>
          <!-- <div class="w-md-80 w-lg-40 text-center mx-md-auto">
            <p><?php echo $inicio->content('chamadaportfolio'); ?></p>
          </div> -->
        </div>

        <div class="js-slick-carousel slick-opacity u-slick u-slick--gutters-1"
          data-slides-show="3"
          data-slides-scroll="1"
          data-arrows-classes="d-inline-block u-slick__arrow u-slick__arrow-centered--y rounded-circle"
          data-arrow-left-classes="fas fa-arrow-left u-slick__arrow-inner u-slick__arrow-inner--left ml-md--5"
          data-arrow-right-classes="fas fa-arrow-right u-slick__arrow-inner u-slick__arrow-inner--right mr-md--5"
          data-responsive='[{
            "breakpoint": 992,
              "settings": {
                "slidesToShow": 2
              }
            }, {
            "breakpoint": 768,
              "settings": {
                "slidesToShow": 1
              }
            }]'>
          <?php 

            foreach ($portfolio as $key => $item){
              ?>
              <div class="js-slide bg-img-hero-center">
                <div class="cbp-item graphic px-3 m-0">
                  <div class="cbp-item-wrapper">
                    <a class="cbp-caption" href="projeto/<?php echo Lazy::getSlug($item->content('titulo')); ?>">
                      <img class="rounded img-fluid" src="<?php echo $item->get_img('sm'); ?>" alt="<?php echo $item->content('titulo'); ?>">
                      <div class="py-3">
                        <h4 class="h5 text-dark mt-4 mb-2"><?php echo $item->content('titulo'); ?></h4>
                        <p class="small mb-0">
                          <?php echo $item->content('cliente'); ?>
                        </p>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
              <?php
            }

          ?>
      </div>

        <div class="text-center py-5">
          <a href="portfolio" class="btn btn-soft-primary px-4">
            Todos os trabalhos
            <span class="fas fa-arrow-right small ml-4"></span>
          </a>
        </div>

      </div>
    </section>

    <?php /* <section id="depoimentos" class="container-fluid d-none bg-img-hero" style="background-image: url(<?php echo $depoimento->get_img(); ?>);">
      <div class="container">
        <div class="row">
          <div class="col-md-6 space-3">
            <div class="card border-0 shadow-sm rounded ovh">
              <div class="card-body p-0 pb-5">
                <div class="js-slick-carousel u-slick slick-gradient" data-pagi-classes="text-right u-slick__pagination mt-4 mb-0">
                  <?php
                    foreach ($depoimentos as $key => $item) {
                  ?>
                    <div class="js-slide px-5 pt-5 pb-0">
                      <div class="p-md-3">
                        <figure class="mb-4">
                          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="40px" height="40px"
                             viewBox="0 0 8 8" style="enable-background:new 0 0 8 8;" xml:space="preserve">
                            <path class="fill-primary" d="M3,1.3C2,1.7,1.2,2.7,1.2,3.6c0,0.2,0,0.4,0.1,0.5c0.2-0.2,0.5-0.3,0.9-0.3c0.8,0,1.5,0.6,1.5,1.5c0,0.9-0.7,1.5-1.5,1.5
                              C1.4,6.9,1,6.6,0.7,6.1C0.4,5.6,0.3,4.9,0.3,4.5c0-1.6,0.8-2.9,2.5-3.7L3,1.3z M7.1,1.3c-1,0.4-1.8,1.4-1.8,2.3
                              c0,0.2,0,0.4,0.1,0.5c0.2-0.2,0.5-0.3,0.9-0.3c0.8,0,1.5,0.6,1.5,1.5c0,0.9-0.7,1.5-1.5,1.5c-0.7,0-1.1-0.3-1.4-0.8
                              C4.4,5.6,4.4,4.9,4.4,4.5c0-1.6,0.8-2.9,2.5-3.7L7.1,1.3z"/>
                          </svg>
                        </figure>
                        <blockquote class="h6 font-weight-normal text-lh-md mb-4"><?php echo $item->content('texto'); ?></blockquote>
                        <div class="media">
                          <div class="u-sm-avatar mr-3">
                            <img class="img-fluid rounded-circle" src="<?php echo $item->get_img(''); ?>" alt="<?php echo $item->content('titulo'); ?>">
                          </div>
                          <div class="media-body">
                            <h4 class="h6 mb-0"><?php echo $item->content('titulo'); ?></h4>
                            <p class="small"><?php echo $item->content('empresa'); ?></p>
                          </div>
                        </div>
                      </div>
                    </div>

                  <?php
                    }
                  ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="blog" class="d-none py-5 pb-md-7 pt-md-75">
      <div class="container">   

        <div class="row justify-content-md-between align-items-center mb-5">
          <div class="col-lg-5 mb-7 mb-lg-0">
            <h2 class="display-4 title-theme"><?php echo $inicio->content('tituloblog'); ?></h2>
            <p>Start knowing what your attendees value, and win more business with Front template.</p>
          </div>

          <div class="col-lg-6 text-lg-right mt-lg-auto">
            <a href="#" class="text-primary">
              Todas as postagens
              <span class="fas fa-arrow-right small ml-2"></span>
            </a>
          </div>
        </div>

        <div class="row">
          <div class="col-12">
            <div class="row">
              <div class="col-md-5">
                <a class="d-flex align-items-end bg-img-hero gradient-overlay-half-primary-v5 transition-blog-hover height-450 rounded-pseudo" href="blog-single.php" style="background-image: url(assets/site/img/400x500/img8.jpg);">
                  <article class="w-100 text-center py-5 px-3 px-md-4">
                    <h2 class="h4 text-white">Front &amp; envelope</h2>
                    <div class="mt-4">
                      <div class="u-avatar mx-auto">
                        <img class="img-fluid rounded-circle" src="assets/site/img/100x100/img3.jpg" alt="Image Description">
                      </div>
                      <strong class="d-block text-white-70 show-on-hover mt-2">Neyton Burchie</strong>
                    </div>
                  </article>
                </a>
              </div>
              <div class="col-md-4">
                <a class="d-flex align-items-end bg-img-hero gradient-overlay-half-primary-v5 transition-blog-hover height-450 rounded-pseudo" href="blog-single.php" style="background-image: url(assets/site/img/400x500/img9.jpg);">
                  <article class="w-100 text-center py-5 px-3 px-md-4">
                    <h3 class="h4 text-white">Gimme that Awwward!</h3>
                    <div class="mt-4">
                      <div class="u-avatar mx-auto">
                        <img class="img-fluid rounded-circle" src="assets/site/img/100x100/img1.jpg" alt="Image Description">
                      </div>
                      <strong class="d-block text-white-70 show-on-hover mt-2">Tina Krueger</strong>
                    </div>
                  </article>
                </a>
              </div>
              <div class="col-md-3">
                <a class="d-flex align-items-end bg-img-hero gradient-overlay-half-primary-v5 transition-blog-hover height-450 rounded-pseudo" href="blog-single.php" style="background-image: url(assets/site/img/400x500/img9.jpg);">
                  <article class="w-100 text-center py-5 px-3 px-md-4">
                    <h3 class="h4 text-white">Gimme that Awwward!</h3>
                    <div class="mt-4">
                      <div class="u-avatar mx-auto">
                        <img class="img-fluid rounded-circle" src="assets/site/img/100x100/img1.jpg" alt="Image Description">
                      </div>
                      <strong class="d-block text-white-70 show-on-hover mt-2">Tina Krueger</strong>
                    </div>
                  </article>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> */ ?>
  </main>
  <script src="http://www.8pdev.studio/animation/8p-svg-animation.js?v=1"></script>
