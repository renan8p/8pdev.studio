<div class="container pt-75 pb-5 pb-md-5">
  <div class="w-md-65 w-lg-50">

    <ol class="breadcrumb pl-0 mb-2">
      <li class="breadcrumb-item"><a href="/">Início</a></li>
      <li class="breadcrumb-item active" aria-current="page"><?php echo $portfolio->content('titulo'); ?></li>
    </ol>

    <h1><?php echo $portfolio->content('chamada_portfolio'); ?></h1>
    <h2 class="h1 text-primary font-weight-medium"><?php echo $portfolio->content('subtitulo_portfolio'); ?></h2>
  </div>
</div>

<main id="content" role="main">    
  <section id="portfolio" class="pt-md-5 pb-md-6 u-cubeportfolio">
    <div class="container">
      <div class="row">
        <?php 

          foreach ($pecas as $key => $item){
            ?>
            <div class="col-12 col-sm-4 pb-5">
              <div class="cbp-item graphic">
                <div class="cbp-item-wrapper">
                  <a class="cbp-caption" href="projeto/<?php echo Lazy::getSlug($item->content('titulo')); ?>">
                    <img class="rounded img-fluid" src="<?php echo $item->get_img('sm'); ?>" alt="<?php echo $item->content('titulo'); ?>">
                    <div class="py-3">
                      <h4 class="h5 text-dark mt-3 mb-2"><?php echo $item->content('titulo'); ?></h4>
                      <p class="small mb-0">
                        <?php echo $item->content('cliente'); ?>
                      </p>
                    </div>
                  </a>
                </div>
              </div>
            </div>
            <?php
          }

        ?>
      </div>
    </div>
  </section>
</main>