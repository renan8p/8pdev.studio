<div class="container space-2 space-md-3">
    <div class="row justify-content-center">
        <div class="col-auto text-center">
         	<h2 class="display-2 mb-3">Ops! :/</h2>
        	<p><img src="assets/site/img/error_404.png" width="200"></p>
            <p class="max-250">Não encontramos o conteúdo que você acessou.</p><br>
	        <a href="" class="btn btn-soft-primary transition-3d-hover">Voltar para a home</a>
        </div>
    </div>
</div>