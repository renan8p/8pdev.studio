<div class="container pt-75 py-5">
  <div class="w-md-65 w-lg-50">

    <ol class="breadcrumb pl-0 mb-2">
      <li class="breadcrumb-item"><a href="/">Início</a></li>
      <li class="breadcrumb-item active" aria-current="page"><?php echo $contato->content('titulo'); ?></li>
    </ol>

    <h1><?php echo $contato->content('chamada_contato'); ?></h1>
    <h2 class="h1 text-primary font-weight-medium"><?php echo $contato->content('subtitulo_contato'); ?></h2>
  </div>
</div>

<main id="contato-page" class="gradient-half-primary-v2" role="main">
  <div class="container">

    <form id="contato-form" name="contato-form" method="POST" class="pt-4">
      <input type="hidden" name="to" value="<?php echo $contato->content('email'); ?>">
      <div class="row">

        <div class="col-12 col-md-4">
          <div class="form-group">
            <div class="js-form-message">
              <label class="sr-only" for="contato-nome">Seu nome</label>
              <div class="input-group">
                <input type="text" class="form-control" name="nome" id="nome" placeholder="Seu nome" required />
              </div>
            </div>
          </div>
        </div>

        <div class="col-12 col-md-4">
          <div class="form-group">
            <div class="js-form-message">
              <label class="sr-only" for="contato-email">Seu e-mail</label>
              <div class="input-group">
                <input type="email" class="form-control" name="email" id="email" placeholder="Seu e-mail" required />
              </div>
            </div>
          </div>
        </div>

        <div class="col-12 col-md-4">
          <div class="form-group">
            <div class="js-form-message">
              <label class="sr-only" for="contato-whatsapp">Seu Whatsapp</label>
              <div class="input-group">
                <input type="tel" class="form-control" name="whatsapp" id="whatsapp" placeholder="Seu Whatsapp" />
              </div>
            </div>
          </div>
        </div>

        <div class="col-12">
          <div class="form-group">
            <div class="js-form-message">
              <label class="sr-only" for="contato-mensagem">Mensagem</label>
              <div class="input-group">
                <textarea class="form-control" name="mensagem" id="mensagem" rows="7" placeholder="Digite sua mensagem" aria-label="Digite sua mensagem" required /></textarea>
              </div>
            </div>
          </div>
        </div>

        <div class="col-12">
          <div class="form-group m-0">
            <button type="submit" class="btn btn-primary btn-block d-md-inline-block px-5 transition-3d-hover">Enviar</button>                
          </div>
        </div>

      </div>
    </form>

    <!-- Modal -->
    <div class="modal fade" id="leadModal" tabindex="-1" role="dialog" aria-labelledby="leadModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="text-center">
              <i class="fa fa-thumbs-up font-40 text-success color-2 mb-3"></i><h4 class="color-2">Recebemos sua mensagem!</h4><p>Agradecemos seu interesse e retornaremos o mais breve possível. Obrigado pelo contato :D</p>

              <div class="text-center">
                <a href="/" class="btn btn-primary d-inline-block px-4">
                  Voltar
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container pt-5 pb-7 pt-md-6 pb-md-7">
    <div class="row no-gutters justify-content-center">

      <div class="col-sm-6 col-lg-4 u-ver-divider u-ver-divider--none-lg">        
        <div class="text-center py-3 py-md-5">
          <figure class="ie-height-56 max-width-8 mx-auto mb-4">
            <i class="fas fa-map-marker-alt font-32"></i>
          </figure>
          <h2 class="h6 mb-0">Endereço</h2>
          <p class="mb-0"><?php echo $contato->content('cidade'); ?><br><?php echo nl2br($contato->content('endereco')); ?></p>
        </div>
      </div>

      <div class="col-sm-6 col-lg-4 u-ver-divider u-ver-divider--none-lg">
        <div class="text-center py-3 py-md-5">
          <figure class="ie-height-56 max-width-8 mx-auto mb-4">
            <i class="fas fa-envelope-open-text font-32"></i>
          </figure>
          <h3 class="h6 mb-0">E-mail</h3>
          <p class="mb-0"><a href="mail:<?php echo $contato->content('email'); ?>"><?php echo $contato->content('email'); ?></a></p>
        </div>
      </div>

      <div class="col-sm-6 col-lg-4">
        <div class="text-center py-3 py-md-5">
          <figure class="ie-height-56 max-width-8 mx-auto mb-4">
            <i class="fas fa-headphones-alt font-32"></i>
          </figure>
          <h3 class="h6 mb-0">Telefones</h3>
          <p class="mb-0"><?php echo $contato->content('telefones'); ?><br><?php echo nl2br($contato->content('resumo_contato')); ?></p>
        </div>
      </div>

    </div>    
  </div>

  <figure class="position-absolute right-0 bottom-0 mb--4 left-0">
    <img src="assets/site/svg/components/wave-1-bottom-sm.svg" alt="Contato" class="w-100" height="150">
  </figure>
</main>