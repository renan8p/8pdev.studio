  <!-- <div class="position-relative bg-img-hero-center" style="background-image: url(<?php echo $empresa->get_img('auto'); ?>);">
    <div class="container space-bottom-3 space-top-2">
      <div class="w-md-65 w-lg-50">

        <ol class="breadcrumb pl-0 mb-2">
          <li class="breadcrumb-item"><a href="/">Início</a></li>
          <li class="breadcrumb-item active" aria-current="page"><?php echo $empresa->content('titulo'); ?></li>
        </ol>

        <h1><?php echo $empresa->content('chamada_empresa'); ?></h1>
        <h2 class="h1 text-primary font-weight-medium"><?php echo $empresa->content('subtitulo_empresa'); ?></h2>
      </div>
    </div>
 
    <figure class="position-absolute right-0 bottom-0 left-0">
      <img src="assets/site/svg/components/wave-1-bottom-sm.svg" alt="Image Description" class="w-100" height="150">
    </figure>
  </div> -->

  <div class="container pt-75 pb-0 pb-md-5">
    <div class="w-md-65 w-lg-50">

      <ol class="breadcrumb pl-0 mb-2">
        <li class="breadcrumb-item"><a href="/">Início</a></li>
        <li class="breadcrumb-item active" aria-current="page"><?php echo $empresa->content('titulo'); ?></li>
      </ol>

      <h1><?php echo $empresa->content('chamada_empresa'); ?></h1>
      <h2 class="h1 text-primary font-weight-medium"><?php echo $empresa->content('subtitulo_empresa'); ?></h2>
    </div>
  </div>

  <main id="content" role="main">
    <div id="quem-somos">

      <div class="container space-2">
        <div class="row justify-content-lg-between">
          <div class="col-lg-5 mb-5 mb-lg-0 text-center text-md-left">
            <h2 class="font-weight-bold"><?php echo $empresa->content('titulo_do_texto'); ?></h2>
          </div>
          <div class="col-lg-6">            
            <?php echo $empresa->content('texto_empresa'); ?>
          </div>
        </div>
      </div>

    </div>

    <div id="time" class="mb-6 mt-md-6">
      <div class="container">

        <div class="row">
          <?php

            foreach ($empresa->sublistas['equipe']->items as $key => $item) {
              ?>
              <div class="col-md-4 mb-3">
                <div class="card shadow transition-3d-hover  overflow-hidden">
                  <?php

                    if($item->get_img('md', 'perfil_membro')){
                    ?>
                      <picture>
                        <img class="w-100" src="<?php echo $item->get_img('md', 'perfil_membro'); ?>" alt="<?php echo $item->content('titulo'); ?>">
                      </picture>
                    <?php
                    }

                  ?>
                  <div class="p-4">
                    <h4 class="h5 text-lh-sm"><?php echo $item->content('titulo'); ?></h4>
                    <small class="d-block text-secondary text-uppercase pb-3 opacity-75"><?php echo $item->content('funcao_membro'); ?></small>

                    <p class="font-size-1"><?php echo $item->content('resumo_membro'); ?></p>

                    <ul class="list-inline pt-2 m-0">
                      <li class="list-inline-item">
                        <a class="btn btn-sm btn-icon btn-soft-secondary rounded" target="_blank" href="<?php echo $item->content('facebook_membro'); ?>">
                          <span class="fab fa-facebook-f btn-icon__inner"></span>
                        </a>
                      </li>
                      <li class="list-inline-item">
                        <a class="btn btn-sm btn-icon btn-soft-secondary rounded" target="_blank" href="<?php echo $item->content('instagram_membro'); ?>">
                          <span class="fab fa-instagram btn-icon__inner"></span>
                        </a>
                      </li>
                      <li class="list-inline-item">
                        <a class="btn btn-sm btn-icon btn-soft-secondary rounded" target="_blank" href="<?php echo $item->content('twitter_membro'); ?>">
                          <span class="fab fa-twitter btn-icon__inner"></span>
                        </a>
                      </li>
                    </ul>
                  </div>

                </div>
              </div>
              <?php
            }

          ?>

        </div>
      </div>
    </div>

    <section id="tecnologias" class="pt-4 pb-5 pb-lg-7">
      <div class="container">
        <div class="row">
          <div class="col px-0 px-md-3">
            <div class="js-slick-carousel slick-gradient u-slick u-slick--gutters-1"
                data-infinite="true"
                data-slides-show="5"
                data-center-mode="true"
                data-autoplay="true"
                data-variable-width="true"
                data-autoplay-speed="5000"
                data-slides-scroll="1"
                data-slick='{"swipeToSlide": true, "focusOnSelect": true, "centerPadding": "3.5rem"}'
                data-responsive='[{
                  "breakpoint": 768,
                  "settings": {
                    "slidesToShow": 3
                  }
              }]'>
              <?php

                  foreach ($empresa->sublistas['tecnologias_sobre']->items as $key => $item) {
                    ?>
                    <div class="js-slide bg-img-hero-center">
                      <div class="text-center px-3">
                        <img src="<?php echo $item->get_img('xs', 'imagem_tecnologia'); ?>" width="200">
                      </div>
                    </div>
                    <?php
                  }
                  
              ?>
            </div>
          </div>
        </div>
      </div>
    </section>

  </main>
