<!DOCTYPE html>
<html lang="pt-br">

<head>
  <title>Blog - 8p estúdio de desenvolvimento</title>
  
  <?php include('inc_head.php'); ?>
</head>
<body>

  <?php include('inc_topo.php'); ?>

  <main id="content" role="main" class="ovh mt-10">
    <div class="bg-light py-3">
      <div class="container">
        <div class="w-lg-60 mx-auto">
          <ol class="breadcrumb pl-0">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item"><a href="blog.php">Blog</a></li>
            <li class="breadcrumb-item active" aria-current="page">Announcing a free plan for small teams</li>
          </ol>
        </div>
      </div>
    </div>
    <div class="container space-top-2">
      <div class="w-lg-60 mx-auto">
        <div class="media align-items-center mb-4">
          <div class="u-sm-avatar mr-3">
            <img class="img-fluid rounded-circle" src="assets/img/100x100/img1.jpg" alt="Image Description">
          </div>
          <div class="media-body">
            <h4 class="d-inline-block h6 mb-0">Andrea Gard</h4>
          </div>
        </div>
        <h1 class="display-3 mb-3 text-lh-sm">Announcing a free plan for small teams</h1>
        <h2 class="font-size-2 mb-2 font-weight-normal">lorem ipsum dolor sit amet loret comij tuodap loasdr</h2>
        <div class="mb-8">
          <span class="text-muted">April 15, 2018</span>
        </div>
        <div class="mb-5">
          <p>At Front, our mission has always been focused on bringing openness and transparency to the design process. We've always believed that by providing a space where designers can share ongoing work not only empowers them to make better products, it also helps them grow. We're proud to be a part of creating a more open culture and to continue building a product that supports this vision.</p>
          <p>As we've grown, we've seen how Front has helped companies such as Spotify, Microsoft, Airbnb, Facebook, and Intercom bring their designers closer together to create amazing things. We've also learned that when the culture of sharing is brought in earlier, the better teams adapt and communicate with one another.</p>
          <p>That's why we are excited to share that we now have a <a href="#">free version of Front</a>, which will allow individual designers, startups and other small teams a chance to create a culture of openness early on.</p>
        </div>
        <h2 class="h5 mb-3">Bringing the culture of sharing to everyone.</h2>
        <p>Small teams and individual designers need a space where they can watch the design process unfold, both for themselves and for the people they work with – no matter if it's a fellow designer, product manager, developer or client. Front allows you to invite more people into the process, creating a central place for conversation around design. As those teams grow, transparency and collaboration becomes integrated in how they communicate and work together.</p>
      </div>
    </div>
    <div class="container">
      <div class="w-lg-80 mx-auto">
        <div class="js-slick-carousel u-slick u-slick--gutters-2 space-2 slick-opac-25 slick-visible"
             data-slides-show="1"
             data-slides-scroll="1"
             data-focus-on-select="true"
             data-center-mode="true"
             data-center-padding="200px"
             data-pagi-classes="text-center u-slick__pagination mt-5 mb-0"
             data-responsive='[{
               "breakpoint": 992,
               "settings": {
                 "centerPadding": "120px"
               }
             }, {
               "breakpoint": 768,
               "settings": {
                 "centerPadding": "80px"
               }
             }, {
               "breakpoint": 554,
               "settings": {
                 "centerPadding": "50px"
               }
             }]'>
          <div class="js-slide rounded bg-img-hero min-height-300" style="background-image: url(assets/img/900x450/img6.jpg);"></div>
          <div class="js-slide rounded bg-img-hero min-height-300" style="background-image: url(assets/img/900x450/img7.jpg);"></div>
          <div class="js-slide rounded bg-img-hero min-height-300" style="background-image: url(assets/img/900x450/img8.jpg);"></div>
          <div class="js-slide rounded bg-img-hero min-height-300" style="background-image: url(assets/img/900x450/img9.jpg);"></div>
          <div class="js-slide rounded bg-img-hero min-height-300" style="background-image: url(assets/img/900x450/img10.jpg);"></div>
        </div>
      </div>
    </div>
    <div class="container space-bottom-2">
      <div class="w-lg-60 mx-auto">
        <div class="mb-7">
          <p>"Front allows us to collaborate in real time and is a really great way for leadership on the team to stay up-to-date with what everybody is working on," <a href="#">said</a> Stewart Scott-Curran, Intercom's Director of Brand Design.</p>
          <p>We know the power of sharing is real, and we want to create an opportunity for everyone to try Front and explore how transformative open communication can be. Now you can have a team of one or two designers and unlimited spectators (think PMs, management, marketing, etc.) share work and explore the design process earlier.</p>
          <p>"Front opened a new way of sharing. It's a persistent way for everyone to see and absorb each other's work," said David Scott, Creative Director at <a href="#">Eventbrite</a>.</p>
        </div>
      </div>
    </div>
    <div id="SVGwave1BottomSMShape" class="svg-preloader gradient-half-theme position-relative">
      <div class="container space-2 space-md-3 position-relative">
        <div class="text-center space-bottom-1">
          <div class="u-avatar mx-auto mb-4">
            <img class="img-fluid rounded-circle" src="assets/img/100x100/img2.jpg" alt="Image Description">
          </div>
          <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-7">
            <blockquote class="h4 text-white font-weight-light mb-0">The template is really nice and offers quite a large set of options. It's beautiful and the coding is done quickly and seamlessly. Thank you!</blockquote>
          </div>
          <h4 class="h5 text-white mb-0">Maria Muszynska</h4>
        </div>
        <figure class="w-35 position-absolute top-0 right-0 left-0 left-15x">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             viewBox="0 0 8 8" style="enable-background:new 0 0 8 8;" xml:space="preserve">
            <path class="fill-white" opacity=".075" d="M3,1.3C2,1.7,1.2,2.7,1.2,3.6c0,0.2,0,0.4,0.1,0.5c0.2-0.2,0.5-0.3,0.9-0.3c0.8,0,1.5,0.6,1.5,1.5c0,0.9-0.7,1.5-1.5,1.5
              C1.4,6.9,1,6.6,0.7,6.1C0.4,5.6,0.3,4.9,0.3,4.5c0-1.6,0.8-2.9,2.5-3.7L3,1.3z M7.1,1.3c-1,0.4-1.8,1.4-1.8,2.3
              c0,0.2,0,0.4,0.1,0.5c0.2-0.2,0.5-0.3,0.9-0.3c0.8,0,1.5,0.6,1.5,1.5c0,0.9-0.7,1.5-1.5,1.5c-0.7,0-1.1-0.3-1.4-0.8
              C4.4,5.6,4.4,4.9,4.4,4.5c0-1.6,0.8-2.9,2.5-3.7L7.1,1.3z"/>
          </svg>
        </figure>
      </div>
      <figure class="position-absolute right-0 bottom-0 left-0">
        <img class="js-svg-injector" src="assets/svg/components/wave-1-bottom-sm.svg" alt="Image Description"
             data-parent="#SVGwave1BottomSMShape">
      </figure>
    </div>
    <div class="container space-2">
      <div class="w-lg-60 mx-auto">
        <p>"Front allows us to collaborate in real time and is a really great way for leadership on the team to stay up-to-date with what everybody is working on," <a href="#">said</a> Stewart Scott-Curran, Intercom's Director of Brand Design.</p>
        <p>We know the power of sharing is real, and we want to create an opportunity for everyone to try Front and explore how transformative open communication can be. Now you can have a team of one or two designers and unlimited spectators (think PMs, management, marketing, etc.) share work and explore the design process earlier.</p>
        <p>"Front opened a new way of sharing. It's a persistent way for everyone to see and absorb each other's work," said David Scott, Creative Director at <a href="#">Eventbrite</a>.</p>
      </div>
    </div>
    <div class="container space-bottom-2">
      <div class="w-lg-60 mx-auto">
        <ul class="list-inline text-center mb-0">
          <li class="list-inline-item pb-3">
            <a class="btn btn-xs btn-soft-secondary btn-pill" href="#">Business</a>
          </li>
          <li class="list-inline-item pb-3">
            <a class="btn btn-xs btn-soft-secondary btn-pill" href="#">Start-Up</a>
          </li>
          <li class="list-inline-item pb-3">
            <a class="btn btn-xs btn-soft-secondary btn-pill" href="#">Plan</a>
          </li>
        </ul>
        <hr class="my-7">
        <ul class="list-inline text-center mb-0">
          <li class="list-inline-item">
            <a class="btn btn-icon btn-soft-secondary btn-bg-transparent" href="#">
              <span class="fab fa-facebook-f btn-icon__inner"></span>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="btn btn-icon btn-soft-secondary btn-bg-transparent" href="#">
              <span class="fab fa-google btn-icon__inner"></span>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="btn btn-icon btn-soft-secondary btn-bg-transparent" href="#">
              <span class="fab fa-twitter btn-icon__inner"></span>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="btn btn-icon btn-soft-secondary btn-bg-transparent" href="#">
              <span class="fab fa-pinterest btn-icon__inner"></span>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="btn btn-icon btn-soft-secondary btn-bg-transparent" href="#">
              <span class="fab fa-get-pocket btn-icon__inner"></span>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="btn btn-icon btn-soft-secondary btn-bg-transparent" href="#">
              <span class="fab fa-telegram-plane btn-icon__inner"></span>
            </a>
          </li>
          <li class="list-inline-item">
            <a class="btn btn-icon btn-soft-secondary btn-bg-transparent" href="#">
              <span class="fab fa-slack-hash btn-icon__inner"></span>
            </a>
          </li>
        </ul>
        <hr class="my-7">
        <div class="media">
          <div class="u-lg-avatar mr-3">
            <img class="img-fluid rounded-circle" src="assets/img/100x100/img1.jpg" alt="Image Description">
          </div>
          <div class="media-body">
            <div class="row mb-3 mb-sm-0">
              <div class="col-sm-9 mb-2">
                <h4 class="d-inline-block mb-0">
                  <a class="d-block h6 mb-0" href="#">Andrea Gard</a>
                </h4>
                <small class="d-block text-muted">Best Author of the year Award Winner.</small>
              </div>
            </div>
            <p class="small">Andrea Gard is the author of two story collections and two novels, most recently Eat Only When You're Hungry. She lives in Chicago.</p>
          </div>
        </div>
      </div>
      <div class="container space-top-3">
        <div class="w-lg-80 mx-auto">
          <div class="js-slick-carousel u-slick u-slick--gutters-2 slick-opac-25 slick-visible"
               data-slides-show="1"
               data-slides-scroll="1"
               data-focus-on-select="true"
               data-center-mode="true"
               data-center-padding="200px"
               data-pagi-classes="text-center u-slick__pagination mt-5 mb-0"
               data-responsive='[{
                 "breakpoint": 992,
                 "settings": {
                   "centerPadding": "120px"
                 }
               }, {
                 "breakpoint": 768,
                 "settings": {
                   "centerPadding": "80px"
                 }
               }, {
                 "breakpoint": 554,
                 "settings": {
                   "centerPadding": "50px"
                 }
               }]'>
            <div class="js-slide">
              <a class="min-width-15 d-flex align-items-end bg-img-hero gradient-overlay-half-primary-v5 transition-blog-hover height-450 rounded-pseudo" href="blog-single.php" style="background-image: url(assets/img/400x500/img8.jpg);">
                <article class="w-100 text-center p-6">
                  <h2 class="h4 text-white">Front &amp; envelope</h2>
                  <div class="mt-4">
                    <div class="u-avatar mx-auto">
                      <img class="img-fluid rounded-circle" src="assets/img/100x100/img3.jpg" alt="Image Description">
                    </div>
                    <strong class="d-block text-white-70 show-on-hover mt-2">Neyton Burchie</strong>
                  </div>
                </article>
              </a>
            </div>
            <div class="js-slide">
              <a class="min-width-15 d-flex align-items-end bg-img-hero gradient-overlay-half-primary-v5 transition-blog-hover height-450 rounded-pseudo" href="blog-single.php" style="background-image: url(assets/img/400x500/img8.jpg);">
                <article class="w-100 text-center p-6">
                  <h2 class="h4 text-white">Front &amp; envelope</h2>
                  <div class="mt-4">
                    <div class="u-avatar mx-auto">
                      <img class="img-fluid rounded-circle" src="assets/img/100x100/img3.jpg" alt="Image Description">
                    </div>
                    <strong class="d-block text-white-70 show-on-hover mt-2">Neyton Burchie</strong>
                  </div>
                </article>
              </a>
            </div>
            <div class="js-slide">
              <a class="min-width-15 d-flex align-items-end bg-img-hero gradient-overlay-half-primary-v5 transition-blog-hover height-450 rounded-pseudo" href="blog-single.php" style="background-image: url(assets/img/400x500/img8.jpg);">
                <article class="w-100 text-center p-6">
                  <h2 class="h4 text-white">Front &amp; envelope</h2>
                  <div class="mt-4">
                    <div class="u-avatar mx-auto">
                      <img class="img-fluid rounded-circle" src="assets/img/100x100/img3.jpg" alt="Image Description">
                    </div>
                    <strong class="d-block text-white-70 show-on-hover mt-2">Neyton Burchie</strong>
                  </div>
                </article>
              </a>
            </div>
            <div class="js-slide">
              <a class="min-width-15 d-flex align-items-end bg-img-hero gradient-overlay-half-primary-v5 transition-blog-hover height-450 rounded-pseudo" href="blog-single.php" style="background-image: url(assets/img/400x500/img8.jpg);">
                <article class="w-100 text-center p-6">
                  <h2 class="h4 text-white">Front &amp; envelope</h2>
                  <div class="mt-4">
                    <div class="u-avatar mx-auto">
                      <img class="img-fluid rounded-circle" src="assets/img/100x100/img3.jpg" alt="Image Description">
                    </div>
                    <strong class="d-block text-white-70 show-on-hover mt-2">Neyton Burchie</strong>
                  </div>
                </article>
              </a>
            </div>
            <div class="js-slide">
              <a class="min-width-15 d-flex align-items-end bg-img-hero gradient-overlay-half-primary-v5 transition-blog-hover height-450 rounded-pseudo" href="blog-single.php" style="background-image: url(assets/img/400x500/img8.jpg);">
                <article class="w-100 text-center p-6">
                  <h2 class="h4 text-white">Front &amp; envelope</h2>
                  <div class="mt-4">
                    <div class="u-avatar mx-auto">
                      <img class="img-fluid rounded-circle" src="assets/img/100x100/img3.jpg" alt="Image Description">
                    </div>
                    <strong class="d-block text-white-70 show-on-hover mt-2">Neyton Burchie</strong>
                  </div>
                </article>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>

  <?php include('inc_rodape.php'); ?>
  <script src="assets/js/components/hs.slick-carousel.js"></script>

  <script type="text/javascript">
    $(document).on('ready', function () {

      // initialization of slick carousel
      $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');
    });
  </script>
</body>

</html>