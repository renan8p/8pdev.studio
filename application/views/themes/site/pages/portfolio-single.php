  <div class="position-relative bg-img-hero-center">
    <div class="container pt-75 py-5">
      <div class="w-md-65 w-lg-50">

        <ol class="breadcrumb pl-0 mb-2">
          <li class="breadcrumb-item"><a href="/">Início</a></li>
          <li class="breadcrumb-item"><a href="portfolio">Portfólio</a></li>
          <li class="breadcrumb-item active" aria-current="page"><?php echo $projeto->content('titulo'); ?></li>
        </ol>

        <h1><?php echo $projeto->content('chamada_portfolio'); ?></h1>
        <h2 class="h1 text-primary font-weight-medium"><?php echo $projeto->content('subtitulo_portfolio'); ?></h2>
      </div>
    </div>
  </div>

  <main id="content" role="main">
    <div class="container">
      <div class="row align-items-stretch flex-md-row-reverse">

        <div class="col-lg-5 pl-lg-6 position-relative">
          <div class="fixed">
            <div class="mb-4">
              <h1 class="display-3 mb-4"><?php echo $projeto->content('titulo'); ?></h1>
              <p class="mb-0"><?php echo $projeto->content('texto'); ?></p>
            </div>
            <hr class="my-5">
            <ul class="list-unstyled mb-0">            
              <li class="media mb-3">
                <div class="w-40 w-sm-30">
                  <b class="font-16 text-secondary m-0">Acessar</b>
                </div>
                <div class="media-body ">
                  <small>
                    <?php

                      if($projeto->content('link')){
                        ?>
                        <a href="<?php echo $projeto->content('link'); ?>" target="_blank" class="text-primary">
                          <?php echo $projeto->content('link'); ?>
                        </a>
                        <?php
                      }
                      else{
                        ?>Em breve<?php
                      }
                    
                    ?>
                  </small>
                </div>
              </li>
              <li class="media mb-3">
                <div class="w-40 w-sm-30">
                  <b class="font-16 text-secondary m-0">Cliente</b>
                </div>
                <div class="media-body ">
                  <small class="text-muted">
                   <?php echo $projeto->content('cliente'); ?>
                  </small>
                </div>
              </li>
              <li class="media mb-3">
                <div class="w-40 w-sm-30">
                  <b class="font-16 text-secondary m-0">Tecnologias</b>
                </div>
                <div class="media-body ">
                  <small class="text-muted">
                    <?php echo $projeto->content('tecnologias'); ?>
                  </small>
                </div>
              </li>
            </ul>
            <hr class="my-5">
            <div class="media d-none d-lg-block">
              <div class="w-40 w-sm-30">
                <h6 class="pt-2">Compartilhe</h6>
              </div>
              <div class="media-body ">
                <?php echo Lazy::get_social_share('projeto/'.$projeto->content('slug')); ?>              
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-7">
          <?php 

            foreach($projeto->get_gallery('auto', 'galeria') as $key => $item){
              ?>
              <img class="img-fluid" src="<?php echo $item; ?>" alt="<?php echo $projeto->content('titulo'); ?>">
              <?php
            }

          ?>          
        </div>

      </div>

      <div class="media d-block d-lg-none text-center">
        <h6 class="pt-2">Compartilhe</h6>
        <div class="media-body ">
          <?php echo Lazy::get_social_share('projeto/'.$projeto->content('slug')); ?>              
        </div>
      </div>
    </div>

    <div class="container mt-5 mt-md-7">
      <div class="w-lg-65 mx-lg-auto">
        <hr class="my-0">
      </div>
    </div>

    <div class="container mt-5 mt-md-6">
      <div class="row justify-content-between">

        <?php

          foreach($pecas as $key => $item){
            ?>
            <div class="col-lg-5 mb-5 mb-lg-0">
              <div class="row">
                <div class="col-sm-6 mb-4">
                  <a href="projeto/<?php echo $item->content('slug'); ?>">
                    <img class="img-fluid rounded mx-auto" src="<?php echo $item->get_img('md'); ?>" alt="<?php echo $item->content('titulo'); ?>">
                  </a>
                </div>
                <div class="col-sm-6 mb-4 mb-md-7">
                  
                  <div class="w-100 max-250 mb-4">
                    <h3 class="h5 mb-4"><?php echo $item->content('titulo'); ?></h3>
                    <p class="font-size-1"><?php echo Lazy::get_resumo($item->content('texto'), 80); ?>...</p>
                  </div>

                  <a class="btn btn-sm btn-soft-primary px-4 transition-3d-hover" href="projeto/<?php echo $item->content('slug'); ?>">
                    Acessar <span class="fas fa-angle-right ml-1"></span>
                  </a>

                </div>
              </div>
            </div>
            <?php
          }

        ?>
        
      </div>
    </div>

  </main>