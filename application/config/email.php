<?php if (!defined('BASEPATH'))	exit('No direct script access allowed');

$config['protocol']	 = "mail";
$config['mailpath']  = '/usr/sbin/sendmail';
// $config['smtp_port'] = "587";
$config['charset']	 = "utf-8";
$config['mailtype']	 = "html";
$config['newline']	 = "\r\n";
$config['wordwrap']	 = true;
