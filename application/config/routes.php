<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$parts = explode('/', strtolower($_SERVER['REQUEST_URI']));
$parts = (in_array($_SERVER['HTTP_HOST'], array('localhost'))) ? $parts[2] : $parts[1];

$route['default_controller'] = 'main/index';

switch ($parts) {
	case 'painel':
		$route[$parts] = 'painel/dashboard';
		$route[$parts.'/(:any)'] = 'painel/$1';
		break;

	case 'inicio':
		$route[$parts] = 'main/inicio';
		break;

	case 'quem-somos':
		$route[$parts] = 'main/empresa';
		break;

	case 'servicos':
		$route[$parts] = 'main/servicos';
		$route[$parts.'/(:any)'] = 'main/servicos/$1';
		$route[$parts.'/(:any)/(:any)'] = 'main/servicos/$1/$2';
		break;

	case 'blog':
		$route[$parts] = 'main/blog';		
		$route[$parts.'/(:any)'] = 'main/blog/$1';
		break;

	case 'noticia':	
		$route[$parts.'/(:any)'] = 'main/noticia/$1';
		break;

	case 'buscar':
		$route[$parts] = 'main/buscar';
		$route[$parts.'/(:any)'] = 'main/buscar/$1';
		break;

	case 'portfolio':
		$route[$parts] = 'main/portfolio';
		break;

	case 'projeto':
		$route[$parts.'/(:any)'] = 'main/projeto/$1';
		break;

	case 'propostas':
		$route[$parts.'/(:any)'] = 'main/propostas/$1';
		break;

	case 'contato':
		$route[$parts] = 'main/contato';
		break;

	case 'async':
		$route[$parts] = 'async/'.$parts.'/$1/$2';
		break;

	default:
		$route['(:any)'] = 'main/show404';
		break;
}

$route['404_override'] = 'main/show404';
$route['translate_uri_dashes'] = FALSE;
