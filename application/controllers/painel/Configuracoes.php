<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuracoes extends MY_Painel {

	public function __construct(){
		parent::__construct();
        $this->load->model('configuracao');
        $this->load->helper('date');
		$this->data["nav_configuracoes"] = true;

		$config = new Configuracao();
		$config->get_this();

		$this->data['config'] = $config;
	}

	public function index(){

		$this->data['nav_tab'] = "informacoes";

		$this->data['breadcrumb'] = array('configuracoes/' => "Configurações", 'configuracoes' => "Informações");
		$this->loadPage('painel/pages/configuracoes/informacoes', "header_configuracoes");
	}

	public function salvar_informacoes(){

		$post = $this->input->post('post');

		$configuracoes = new Configuracao();
		$configuracoes->get_this();
		$configuracoes->id        = 1;
		$configuracoes->titulo    = $post["titulo"];
		$configuracoes->descricao = $post["descricao"];
		$configuracoes->logo  = $post["logo"];
		$configuracoes->icone = $post["icone"];
		$configuracoes->img   = $post["img"];
		$configuracoes->tags  = $post["tags"];
		$configuracoes->status  = (isset($post["status"])) ? 1 : 0;
		$configuracoes->save();

		if(count($configuracoes->fails) == 0){
			$response['status'] = 200;
			$response['id']     = $configuracoes->id;
			$response['url']    = 'painel/configuracoes/';
		}
		else{
			$response['status'] = 0;
			$response['fails']  = $configuracoes->fails;
		}

		echo json_encode($response);
	}

	public function navegacao( $menu = NULL ){
        $this->data['script'] = array('js/script-navegacao.js');

        $this->load->model('area');
        $this->load->model('conteudo');
        $this->load->model('categoria');

		$config = new Configuracao();
		$config->get_this();

		$navegacao = ($config->navegacao) ? json_decode($config->navegacao, true) : array();
		$menus     = $navegacao;

		if($menu){
			$navegacao = $navegacao[$menu]['itens'];
		}
		else{
			reset($navegacao);
			$menu = key($navegacao);
			$navegacao = ($menus) ? $navegacao[$menu]['itens'] : array();
		}

		$lista_paginas = new Area();
		$this->data['lista_paginas'] = $lista_paginas->get_lista_paginas();
		
		$lista_categorias = new Categoria();
		$this->data['lista_categorias'] = $lista_categorias->get_where(array('status' => 1));

		$this->data['select_menu'] = $menu;
		$this->data['menus']       = $menus;
		$this->data['navegacao']   = $navegacao;
		$this->data['nav_tab']     = "navegacao";

		$this->data['breadcrumb'] = array('configuracoes/' => "Configurações", 'configuracoes/navegacao' => "Navegação");
		$this->loadPage('painel/pages/configuracoes/navegacao', "header_configuracoes");
	}

	public function salvar_navegacao(){

		$post = $this->input->post();
		$menu  = $post['menu'];
		$itens = (isset($post['itens'])) ? $post['itens'] : array();

		$config = new Configuracao();
		$config->get_this();

		$navegacao = json_decode($config->navegacao, true);
		$navegacao[$menu]['itens'] = $itens;

		$config->navegacao = json_encode($navegacao);
		$config->save();

		if(count($config->fails) == 0){
			$response['status'] = 200;
			$response['id']     = $config->id;
			$response['url']    = true;
		}
		else{
			$response['status'] = 0;
			$response['fails']  = $config->fails;
		}

		echo json_encode($response);		
	}

	public function scripts(){

		$this->data['nav_tab'] = "scripts";

		$this->data['breadcrumb'] = array('configuracoes/' => "Configurações", 'configuracoes/scripts' => "Scripts");
		$this->loadPage('painel/pages/configuracoes/scripts', "header_configuracoes");
	}

	public function social(){
        $this->data['script'] = array('js/script-social.js');

        $config = new Configuracao();
		$config->get_this();

		$this->data['sociais'] = json_decode($config->sociais);
		$this->data['nav_tab'] = "social";

		$this->data['breadcrumb'] = array('configuracoes/' => "Configurações", 'configuracoes/social' => "Social");
		$this->loadPage('painel/pages/configuracoes/social', "header_configuracoes");
	}

	public function salvar_social(){

		$post = $this->input->post('itens');

		$config = new Configuracao();
		$config->get_this();
		$config->sociais = json_encode($post);
		$config->save();
	}

	public function salvar_menu(){

		$post = $this->input->post();

		$config = new Configuracao();
		$config->get_this();

		$navegacao = json_decode($config->navegacao, true);

		$menu = Lazy::getSlug($post['titulo'], true);

		$json = array(
					"titulo" => $post['titulo'],
					"status" => $post['status'],
					"itens" => array()
				);

		if(!isset($navegacao[$menu])){
			$navegacao[$menu] = $json;
		}

		$config->navegacao = json_encode($navegacao);
		$config->save();

		$response['status'] = 200;
		$response['slug']   = $menu;
		$response['url']    = true;

		echo json_encode($response);
	}

	public function salvar_scripts(){

		$post = $this->input->post('post');

		$config = new Configuracao();
		$config->get_this();
		$config->css    = $post['css'];
		$config->script = $post['script'];
		$config->save();

		if(count($config->fails) == 0){
			$response['status'] = 200;
			$response['id']     = $config->id;
		}
		else{
			$response['status'] = 0;
			$response['fails']  = $config->fails;
		}

		echo json_encode($response);
	}
}
