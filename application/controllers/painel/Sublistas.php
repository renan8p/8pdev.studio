<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sublistas extends MY_Painel {	

	public function __construct(){
		parent::__construct();
        $this->load->model('conteudo');
		$this->load->model('sublista');
	}

	public function sublista(){
		$this->load->model('custom');

		$post = $this->input->post('post');

		$area = new Area();
		$area->get_this(array('id' => $post['cod_area']));

		$sublista = new Sublista();
		$sublista->get_this(array('cod_area' => $post['cod_area'], 'tipo' => $post['post_tipo'], 'slug' => $post['tipo']));	

		$conteudo = new Conteudo();
		$conteudo = $conteudo->get_where(array('tipo' => $sublista->slug, 'cod_area' => $post['cod_area'], 'cod_pai' => $post['cod_pai']), array('order' => 'ordem ASC'));

		$custom = new Custom();
		$fields = $custom->get_fields($area, $sublista->slug);

		foreach ($conteudo as $key => $item){

			$item_conteudo = ($item->conteudo) ? json_decode($item->conteudo) : NULL;

			$this->load->view('painel/includes/subitem', array('item' => $item, 'fields' => $fields, 'conteudo' => $item_conteudo));
		}

		return false;
	}

	public function salvar_sublista($id_area, $tipo){
		$this->load->model('sublista');

		$post = $this->input->post('post');

		$sub = new Sublista();

		if(isset($post['id'])) $sub->get_this(array('id' => $post['id']));

		$sub->cod_area  = $id_area;
		$sub->titulo    = $post['titulo'];
		$sub->slug      = (isset($sub->id)) ? $sub->slug : Lazy::getSlug($post['titulo'], true);
		$sub->tipo      = $tipo;

		$sub->save();

		$response['status'] = 200;
		$response['url']    = true;

		echo json_encode($response);
	}

	public function salvar_subitem(){
		$this->load->library('parser');

		$post   = $this->input->post('post');
		$custom = $this->input->post('custom');

		$area = new Area();
		$area->get_this(array('id' => $post['cod_area']));
	
		if(empty($post['post_pai'])){

			$conteudo = new Conteudo();
			$conteudo->cod_area = $post['cod_area'];
			$conteudo->slug = Lazy::getSlug('rascunho', true);
			$conteudo->slug = $conteudo->getUnique("slug");
			$conteudo->titulo = $conteudo->slug;
			$conteudo->status = 3;
			$conteudo->tipo   = $post['post_tipo'];
			$conteudo->controller    = $area->controller;
			$conteudo->modificado_em = date("Y-m-d H:i");
			$conteudo->criado_em     = date("Y-m-d H:i");
			$conteudo->cod_usuario = $this->session->userdata('id');

			$conteudo->save();
		}

		$item = new Conteudo();

		if(isset($post['item_id'])) $item->get_this(array('id' => $post['item_id']));
		$item->cod_pai  = (isset($conteudo->id)) ? $conteudo->id : $post['post_pai'];
		$item->cod_area = $post['cod_area'];
		$item->titulo   = $post['titulo'];
		$item->slug     = Lazy::getSlug($item->titulo, true);
		$item->slug     = $item->getUnique("slug");
		$item->tipo     = $post['item_tipo'];
		$item->modificado_em = date("Y-m-d H:i");
		$item->conteudo      = json_encode($custom);
		$item->status        = 1;

		$item->cod_usuario = $this->session->userdata('id');
		$item->criado_em   = (isset($item->id)) ? $item->criado_em : date("Y-m-d H:i");
		$item->ordem       = (isset($item->id)) ? $item->ordem : -1;		
		$item->controller  = $area->controller;

		if($item->save()){

			// Cadastra o relacionamento quando existe um tipo de campo relacional
			if(isset($post['relacional'])){
				$item->set_relacional($post['relacional'], $custom, $item->tipo);
			}
			// ---

			$response['status'] = 200;
			$response['item']   = $item;
		}
		else{
			$response['status'] = 0;
		}

		echo json_encode( $response );
	}

	public function deleta_subitem($id){

		$item = new Conteudo();
		$item->get_this(array('id' => $id));

		if($item->delete()){
			$response['status'] = 200;
			$response['item']   = $item;
		}
		else{
			$response['status'] = 0;
		}

		echo json_encode($response);
	}

	public function deleta_sublista($item){
		$this->load->model('custom');

		$sublista = new Sublista();
		$sublista->get_this(array('id' => $item));

		$custom = new Custom();
		$custom->delete_all(array('controller' => $sublista->slug, 'cod_area' => $sublista->cod_area));

		$conteudo = new Conteudo();
		$conteudo->delete_all(array('cod_pai <>' => 0, 'tipo' => $sublista->slug, 'cod_area' => $sublista->cod_area));

		$sublista->delete();

		$response['status'] = 200;
		$response['url'] = true;

		echo json_encode( $response );
	}

	public function reordena_subitem(){

		$ordem = $this->input->post('ordem');

		foreach ($ordem as $key => $id) {
			$item = new Conteudo();
			$item->get_this(array('id' => $id));
			$item->ordem = $key;

			$item->save();
		}
	}

	public function reordena_sublista(){

		$ordem = $this->input->post('ordem');

		print_r($ordem);

		foreach ($ordem as $key => $id) {
			$sublista = new Sublista();
			$sublista->get_this(array('id' => $id));
			$sublista->ordem = $key;

			$sublista->save();
		}
	}
}
