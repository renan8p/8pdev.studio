<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sessao extends MY_Painel {

	public function __construct(){
		parent::__construct();
        $this->load->model('sublista');
        $this->load->model('conteudo');
        $this->load->model('categoria');
	}

	public function formulario($tipo, $cod_area, $item = NULL){
		$this->load->model('custom');

		$area = new Area();
		$area->get_this(array('id' => $cod_area));

		$conteudo_post = new Conteudo();

		if($tipo == 'pagina'){
			$conteudo_post->get_this(array('cod_area' => $area->id, 'tipo' => $tipo));
			$controller = base_url($area->slug);
		}
		else{
			$tipo = 'item';
			$conteudo_post->get_this(array('cod_area' => $area->id, 'id' => $item, 'tipo' => $tipo));
			$controller = (isset($conteudo_post->slug)) ? base_url($area->slug.'/'.$conteudo_post->slug) : base_url($area->slug);
		}		

		$conteudo_post->tipo = $tipo;

		$categorias = new Categoria();
		$categorias = $categorias->get_where(array("cod_area" => $area->id, "controller" => $area->controller."_categorias", "lixeira" => 0, "status" => 1));

		if($area->getDetalhes("blocos") && $tipo == 'item'){
        	$this->data['script'] = array('wysiwyg/js/medium-editor.min.js', 'wysiwyg/js/script-medium.js');
			$this->data['editable_blocks'] = (!empty($conteudo_post->postagem_json)) ? $conteudo_post->postagem_json : NULL;
		}

		$fields = new Custom();

		$this->data['label_titulo']   = $fields->get_titulo($area, $tipo);
		$this->data['fields_main']    = $fields->get_fields($area, $tipo, 'main');
		$this->data['fields_sidebar'] = $fields->get_fields($area, $tipo, 'sidebar');

		$sublista = new Sublista();
		$this->data['sublistas'] = $sublista->get_where(array('cod_area' => $area->id, 'tipo' => $tipo), array("order" => 'ordem ASC'));

		$this->data['area'] = $area;
		$this->data['conteudo_post'] = $conteudo_post;
		$this->data['categorias'] = ($tipo == 'item') ? $categorias : false;
		$this->data['values']     = (isset($conteudo_post->id)) ? json_decode($conteudo_post->conteudo) : NULL;
		$this->data['nav_tab']    = $tipo;

		$this->data['controller'] = ($tipo == 'item' && isset($conteudo_post->slug)) ? strtolower($area->getDetalhes('singular')).'/'.$conteudo_post->slug : $controller;
		$this->data['controller'] = ($area->getDetalhes('ativa')) ? $controller : '';

		if($tipo == 'pagina'){
			$this->data['breadcrumb'] = array('sessao/formulario/'.$tipo.'/'.$area->id => $area->titulo);
		}
		else{
			$bread = ($item) ? "Editando " : "Cadastrando ";
			$this->data['atividade'] = $bread;

			$breads = ($area->getDetalhes('conteudo')) ? 'formulario/'.$tipo.'/'.$area->id : 'lista/'.$area->id.'/';

			$this->data['breadcrumb'] = array('sessao/'.$breads => $area->titulo, 'sessao/lista/'.$area->id => "Lista", 'sessao/formulario/item/'.$area->id.'/'.$item => $bread);
		}

		$this->loadPage('painel/pages/sessao/formulario', "header");
	}

	public function salvar_post(){
        $this->load->model('categoria');
        $this->load->model('categoria_rel');

		$post = $this->input->post();
		$custom = isset($post['custom']) ? $post['custom'] : NULL;
		parse_str($post['post'], $post);

		$conteudo = new Conteudo();
		$conteudo->from_array($post, false);
		$conteudo->img  = (isset($post['img'])) ? $post['img'] : NULL;
		$conteudo->tipo = $post['tipo'];
		$conteudo->slug = Lazy::getSlug($conteudo->titulo, true);
		$conteudo->slug = $conteudo->getUnique("slug");
		$conteudo->modificado_em = date("Y-m-d H:i");
		$conteudo->conteudo = json_encode($custom);
		$conteudo->status   = (isset($post['status'])) ? 1 : 2;
		$conteudo->destaque = (isset($post['destaque'])) ? 1 : 0;

		$area = new Area();
		$area->get_this(array('id' => $post['cod_area']));
		$conteudo->controller = $area->controller;

		$conteudo->postagem_json = (isset($post['postagem_json'])) ? $post['postagem_json'] : NULL;			

		if(empty($conteudo->id) || empty($post['criado_em'])){
			$conteudo->cod_usuario = $this->session->userdata('id');
			$conteudo->criado_em   = date("Y-m-d H:i");
		}
		else{
			$conteudo->criado_em = Lazy::dataTimeBd($conteudo->criado_em);
		}

		$conteudo->save();

		if(count($conteudo->fails) == 0){

			// Cadastra o relacionamento quando existe um tipo de campo relacional
			if(isset($post['relacional'])){
				$conteudo->set_relacional($post['relacional'], $custom, "conteudo");
			}
			// ---

			if(isset($post['categorias'])){

				$categoria_rel = new Categoria_rel();
				$categoria_rel->delete_all(array("cod_item" => $conteudo->id, "cod_area" => $conteudo->cod_area, "tipo" => $conteudo->tipo));

				foreach ($post['categorias'] as $key => $cat) {
					$categoria = new Categoria();
					$categoria->get_this(array("id" => $cat));

					$categoria_rel = new Categoria_rel();
					$categoria_rel->cod_item = $conteudo->id;
					$categoria_rel->cod_area = $categoria->cod_area;
					$categoria_rel->tipo     = $conteudo->tipo;
					$categoria_rel->status   = 1;
					$categoria_rel->cod_categoria = $categoria->id;

					$categoria_rel->save();
				}
			}

			$response['status'] = 200;
			$response['id']     = $conteudo->id;
			$response['url']    = 'painel/sessao/formulario/'.$post['tipo'].'/'.$conteudo->cod_area.'/'.$conteudo->id;
		}
		else{
			$response['status'] = 0;
			$response['fails']  = $conteudo->fails;
		}

		echo json_encode($response);
	}

	public function lista($cod_area){

		$area = new Area();
		$area->get_this(array('id' => $cod_area));

		$conteudo = new Conteudo();
		$conteudo = $conteudo->get_where(array('cod_area' => $area->id, 'cod_pai' => 0, 'tipo <>' => 'pagina', 'lixeira' => 0), array("order" => "criado_em DESC"));

		$this->data['controller'] = base_url($area->slug);		
		$this->data['controller'] = ($area->getDetalhes('ativa')) ? $area->controller : '';

		$this->data['conteudo'] = $conteudo;
		$this->data['area']     = $area;
		$this->data['nav_tab']  = 'lista';

		$breads = ($area->getDetalhes('conteudo')) ? 'formulario/pagina/'.$area->id : 'lista/'.$area->id.'/';

		$this->data['breadcrumb'] = array('sessao/'.$breads => $area->titulo, 'sessao/lista/'.$area->id => "Lista");


		$this->loadPage('painel/pages/sessao/lista', "header");
	}

	public function categorias($cod_area){
        $this->load->model('custom');
        $this->load->model('categoria');

		$area = new Area();
		$area->get_this(array('id' => $cod_area));

		$categorias = new Categoria();
		$categorias = $categorias->get_where(array('cod_area' => $area->id, 'lixeira' => 0));

		$this->data['categorias'] = $categorias;
		$this->data['area']       = $area;
		$this->data['nav_tab']    = 'categorias';

		$this->data['breadcrumb'] = array('sessao/formulario/pagina/'.$area->id => $area->titulo, 'sessao/lista/'.$area->id => $area->getDetalhes('plural'), 'sessao/categorias/'.$area->id => 'Categorias');
		$this->loadPage('painel/pages/sessao/categorias', "header");
	}

	public function categoria($cod_area, $item = NULL){
        $this->load->model('custom');
        $this->load->model('categoria');

		$area = new Area();
		$area->get_this(array('id' => $cod_area));

		$categoria = new Categoria();
		$categoria->get_this(array('cod_area' => $area->id, 'id' => $item));

		$categorias_pai = new Categoria();
		$categorias_pai = $categorias_pai->get_where(array("cod_area" => $area->id, "controller" => $area->controller."_categorias", "lixeira" => 0, "status" => 1));

		$fields = new Custom();
		$this->data['fields_main']    = $fields->get_fields($area, 'categorias', 'main');
		$this->data['fields_modal']   = $fields->get_fields($area, 'categorias', 'modal');
		$this->data['fields_sidebar'] = $fields->get_fields($area, 'categorias', 'sidebar');

		$this->data['categoria'] = $categoria;
		$this->data['categorias_pai'] = $categorias_pai;
		$this->data['area']   = $area;
		$this->data['conteudo'] = (isset($categoria->id)) ? json_decode($categoria->conteudo) : NULL;
		$this->data['nav_tab']  = 'categorias';

		$bread = ($item) ? "Editando " : "Cadastrando ";
		$this->data['breadcrumb'] = array('sessao/pagina/'.$area->id => $area->titulo, 'sessao/lista/'.$area->id => $area->getDetalhes('plural'), 'sessao/categorias/'.$area->id => 'Categorias', 'sessao/categoria/'.$item => $bread);
		$this->loadPage('painel/pages/sessao/categoria', "header");
	}

	public function salvar_categoria(){

		$post = $this->input->post('post');
		$custom = $this->input->post('custom');

		$categoria = new Categoria();
		$categoria->from_array($post);
		$categoria->conteudo = json_encode($custom);
		$categoria->slug = Lazy::getSlug($categoria->titulo, true);
		$categoria->slug = $categoria->getUnique("slug");
		$categoria->modificado_em = date("Y-m-d H:i:s");

		if(empty($categoria->id)){
			$categoria->criado_em = date("Y-m-d H:i:s");
		}

		$categoria->save();

		if(count($categoria->fails) == 0){
			$response['status'] = 200;
			$response['id']     = $categoria->id;
			$response['url']     = 'painel/sessao/categorias/'.$categoria->cod_area;
		}
		else{
			$response['status'] = 0;
			$response['fails']  = $categoria->fails;
		}

		echo json_encode($response);
	}

	public function colecao($acao, $tipo = "conteudo"){
        $this->load->model('lixo');

		$colecao = $this->input->post('colecao');

		if($colecao){
			foreach ($colecao as $key => $col){

				$item = new $tipo();
				$item->get_this(array('id' => $col['id'], 'cod_area' => $col['cod_area'], 'controller' => $col['controller']));

				if($acao == "publicar"){    $item->status  = 1; }
				if($acao == "despublicar"){ $item->status  = 2; }
				if($acao == "lixeira"){

					$lixo = new Lixo();
					$lixo->item = $item->id;
					$lixo->desc = $item->titulo;
					$lixo->cod_area    = $item->cod_area;
					$lixo->controller  = $item->controller;
					$lixo->img         = (isset($tipo->img)) ? $tipo->img : 0;
					$lixo->usuario     = $this->session->userdata('id');
					$lixo->origem      = $tipo;
					$lixo->excluido_em = date("Y-m-d H:i:s");
					$lixo->save();

					$item->lixeira = 1;
				}

				$item->save();
			}

			$response['status'] = 200;
			$response['url']    = true;
		}
		else{
			$response['status'] = 0;
			$response['msg']    = 'Selecione pelo menos 1 item!';
		}

		echo json_encode($response);
	}

	public function exportar($cod_area){
		$area = new Area();
		$area->get_this(array('id' => $cod_area));

		$lista = new Conteudo();
		$lista = $lista->get_where(array('cod_area' => $area->id, 'cod_pai' => 0, 'tipo' => 'item', 'lixeira' => 0), array("order" => "titulo ASC"));

		foreach($lista as $key => $item) {

	        $conteudo = json_decode($item->conteudo);

	        if($conteudo){
	            foreach ($conteudo as $key => $value) {
	                $item->{$key} = $value;
	            }
	        }

	        unset($item->id);
	        unset($item->cod_pai);
	        unset($item->cod_usuario);
	        unset($item->slug);
	        unset($item->controller);
	        unset($item->cod_area);
	        unset($item->postagem_json);
	        unset($item->img);
	        unset($item->tipo);
	        unset($item->ordem);
	        unset($item->destaque);
	        unset($item->status);
	        unset($item->lixeira);
			$item->kill_extra();
	    }

	    $lista = json_decode(json_encode($lista), true);


		header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"".$area->titulo."-".date("d-m-Y")."".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");

		$file = fopen('php://output', 'w');
		foreach ($lista as $key => $line){ 
			fputcsv($file, $line); 
		}
		fclose($file); 
		exit; 
	}
}
