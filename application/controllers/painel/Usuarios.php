<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends MY_Painel {

	public function __construct(){
		parent::__construct();
        $this->load->model('usuario');
        $this->load->helper('date');
		$this->data["nav_usuarios"] = true;
		$this->data['nav_tab']  = 'lista';		
	}

	public function lista(){

		if($this->session->userdata('tipo') == "usuario"){
			redirect('painel/', 'location');
		}
		else{
			$usuarios = new Usuario();
			$usuarios = $usuarios->get_where(array('tipo <>' => 'master'));

			$this->data['usuarios']   = $usuarios;

			$this->data['breadcrumb'] = array('usuarios/lista/' => 'Usuários');		
			$this->loadPage('painel/pages/usuarios/lista', "header_usuario");
		}
	}

	public function usuario($user_id = NULL){

		if($this->session->userdata('tipo') == "usuario" && $this->session->userdata('id') != $user_id){
			redirect('painel/', 'location');exit();
		}
		else{
			$usuario = new Usuario();
			$usuario->get_this(array('id' => $user_id));

			$areas = new Area();
			$this->data['areas']   = $areas->get_where(array('status' => 1));
			$this->data['acessos'] = (isset($usuario->acessos) && json_decode($usuario->acessos) != NULL) ? json_decode($usuario->acessos) : array();
			$this->data['usuario'] = $usuario;

			if(isset($usuario->tipo) && $this->session->userdata('tipo') == "admin"){				
				redirect('painel/usuarios/lista', 'location');
			}
			else{
				$bread = ($user_id) ? "Editando " : "Cadastrando ";
				$this->data['breadcrumb'] = array('usuarios/lista/' => 'Usuários', 'usuarios/usuario/'.$user_id => $bread);		
				$this->loadPage('painel/pages/usuarios/usuario', "header_usuario");
			}
		}
	}

	public function salvar_usuario(){

		parse_str($this->input->post('post'), $post);

		if(isset($post['acessos'])){
			$post['acessos'] = json_encode($post['acessos']);
		}

		$usuario = new Usuario();
		$usuario->from_array($post);
		$usuario->modificado_em = date("Y-m-d H:i:s");

		$usuario->status = (isset($post['status'])) ? 1 : 0;

		if(empty($usuario->id)){
			$usuario->criado_em = date("Y-m-d H:i:s");
		}

		$usuario->save();

		if(count($usuario->fails) == 0){

			if($this->session->userdata("login") == $usuario->login){

				$data = array(
					'id'	  => $usuario->id,
					'nome'    => $usuario->nome,
					'login'   => $usuario->login,
					'img'     => $usuario->get_img('sm'),
					'email'   => $usuario->email,
					'acessos' => $usuario->acessos,
					'tipo'    => $usuario->tipo,
					'logado'  => true
				);
				$this->session->set_userdata($data);
			}

			$response['status'] = 200;
			$response['id']     = $usuario->id;
			$response['url']    = 'painel/usuarios/usuario/'.$usuario->id;
		}
		else{
			$response['status'] = 0;
			$response['fails']  = $usuario->fails;
		}

		echo json_encode($response);
	}

	public function reset_senha(){

		$post = $this->input->post('post');

		if( !empty($post['senha']) && $post['senha'] == $post['reset_senha'] ){

			$usuario = new Usuario();

			if($this->session->userdata('tipo') == 'master'){
				$usuario->get_this(array('login' => $post['usuario']));
			}
			else{
				$usuario->get_this(array('id' => $this->session->userdata('id')));
			}
			
			$usuario->modificado_em = date("Y-m-d H:i:s");
			$usuario->senha = md5($post['senha']);

			$usuario->save();

			$response['status'] = 200;
			$response['msg'] = "Senha alterada!";
		}
		else{
			$response['status'] = 0;
			$response['msg'] = ($post['senha'] == $post['reset_senha']) ? "Preencha os campos corretamente." :  "Os campos não correspondem.";
		}

		echo json_encode($response);
	}

	public function colecao($acao){

		$colecao = ($acao != "excluir") ? $this->input->post('colecao') : $colecao = $this->input->post('post');

		if($colecao){			
			foreach ($colecao as $key => $col){

				$usuario = new Usuario();
				$usuario->get_this(array('id' => $col['id']));

				if($acao == "bloquear"){    $usuario->status  = 2; }
				if($acao == "desbloquear"){ $usuario->status  = 1; }

				$usuario->save();

				if($acao == "excluir"){ $usuario->delete(); }
			}

			$response['status'] = 200;
			$response['url']    = true;
		}
		else{
			$response['status'] = 0;
			$response['msg']    = 'Selecione pelo menos 1 item!';
		}

		echo json_encode($response);
	}
}
