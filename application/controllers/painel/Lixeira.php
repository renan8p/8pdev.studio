<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lixeira extends MY_Painel {

	public function __construct(){
		parent::__construct();
		$this->load->database();
        $this->load->model('lixo');
        $this->load->helper('date');
		$this->data["nav_lixeira"] = true;

        $this->load->model('conteudo');
        $this->load->model('categoria');	
        $this->load->model('categoria_rel');        
	}

	public function index(){

		$lixo = new Lixo();
		$this->data['lixos'] = ($this->session->userdata('tipo') != 'usuario') ? $lixo->get_all() : $lixo->get_where(array('usuario' => $this->session->userdata('id')));
		
		$this->data['breadcrumb'] = array('lixeira/' => 'Lixeira');
		$this->loadPage('painel/pages/lixeira');
	}

	public function recuperar(){

		$colecao = $this->input->post('colecao');

		if($colecao){
			foreach ($colecao as $key => $col){

				$item = new $col['origem']();
				$item->get_this(array('id' => $col['id'], 'cod_area' => $col['cod_area'], 'controller' => $col['controller']));
				$item->lixeira = 0;
				$item->save();

				$lixo = new Lixo();
				$lixo->get_this(array('item' => $col['id'], 'cod_area' => $col['cod_area'], 'controller' => $col['controller']));
				$lixo->delete();
			}

			$response['status'] = 200;
			$response['url']    = true;
		}
		else{
			$response['status'] = 0;
			$response['msg']    = 'Selecione pelo menos 1 item!';
		}

		echo json_encode($response);
	}

	public function excluir(){

		$colecao = $this->input->post('post');

		if($colecao){
			foreach ($colecao as $key => $col){
				
				$item = new $col['origem']();
				$item->get_this(array('id' => $col['id'], 'cod_area' => $col['cod_area']));
				$item->delete();				

				$subitem = new Conteudo();
				$subitem->delete_all(array('cod_pai' => $col['id']));

				$rel = new Categoria_rel();
				$rel->delete_all(array('cod_item' => $col['id'], 'cod_area' => $col['cod_area']));

				$lixo = new Lixo();
				$lixo->get_this(array('item' => $col['id'], 'cod_area' => $col['cod_area']));
				$lixo->delete();
			}

			$response['status'] = 200;
			$response['url']    = true;
		}
		else{
			$response['status'] = 0;
			$response['msg']    = 'Selecione pelo menos 1 item!';
		}

		echo json_encode($response);
	}
}


















