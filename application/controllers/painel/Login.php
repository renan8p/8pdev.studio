<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	public function __construct(){
		parent::__construct();
        $this->load->library('session');
		$this->load->model('usuario');
		$this->load->model('configuracao');
		$this->data["login"] = true;
	}

	public function index(){		

		if($this->session->userdata('logado')){
            redirect('painel');
        }
        else{
			$this->loadLogin();
        }		
	}

	public function loadLogin($string = 'login'){
		$this->data['conteudo'] = $this->load->view('painel/pages/login/'.$string, $this->data, true);

		$this->load->view('painel/templates/default', $this->data); 
	}

	public function accesso(){

		if($this->input->post()){

			$post = $this->input->post('post');

			$usuario = new Usuario();
			$usuario = $usuario->login($post);

			if($usuario){
	            $response['status'] = 200;
				$response['msg']    = 'Acesso liberado!';
				$response['url']    = 'painel';			
			}
			else{
				$response['status'] = 0;
				$response['msg']    = 'Algo deu errado...';
				$response['fails']  = array(array('login' => NULL), array('senha' => 'Ops... Seus dados de acesso não batem'));
			}
		}
		else{
			$response['status'] = 0;
			$response['msg']    = 'Algo deu errado...';
			$response['fails']  = array(array('login' => NULL), array('senha' => 'Ops... Seus dados de acesso não batem'));
		}

		echo json_encode($response);
	}

	public function logout(){

		$user_data = $this->session->all_userdata();
		foreach ($user_data as $key => $value) {
			$this->session->unset_userdata($key);
		}
		$this->session->sess_destroy();

		redirect('painel/login');
	}

	public function recuperar(){
		$this->data['pagina'] = 'Recuperar Acesso';

		$this->loadLogin('pages/painel/login/recuperar');
	}

	public function enviar(){

		$this->data['pagina'] = 'Recuperar Acesso';

		if($this->input->post()){
			$post = $this->input->post();

			$this->load->helper('email');

			$usuario = new Usuario();
			$usuario->get_this(array('email' => $post['email']));

			if(isset($usuario->id)){

				$config = new Configuracao();
				$config = $config->get_row();

				$this->load->library('email');
				$this->email->initialize();

				$this->email->from($config->email, $config->titulo);
				$this->email->to($post['email']);

				$this->email->subject('Recuperação de acesso Painel');
				$this->email->message("Recuperação de acesso ".$config->titulo."<br><br>Seu login é: ".$usuario->login."<br><br>Acesse o link para redefinir sua senha: <a target='_blank' href='".base_url("painel/login/redefinir/".$usuario->senha.'_'.$usuario->login)."'>REDEFINIR SENHA</a>");	

				if($this->email->send()){

					$this->data['msg']    = 'Acesse o link enviado no seu endereço de e-mail para redefinir sua senha!';
		        }
		        else{

		            $this->data['error'] = implode($this->email->print_debugger());         
		        }
		    }
		    else{
		    	
		        $this->data['error'] = 'Insira um endereço de e-mail válido e que esteja cadastrado! ';  
		    }	        
		}
		else{
			$this->data['error'] = 'Insira um endereço de e-mail válido e que esteja cadastrado! '; 
		}

		$this->loadLogin('pages/painel/login/recuperar');
	}

	public function redefinir($acesso = NULL){

		$this->data['pagina'] = 'Redefinir senha';

		$acesso = explode('_', $acesso);

		$usuario = new Usuario();

		$usuario->get_this(array('senha' => $acesso[0], 'login' => $acesso[1]));
		$this->data['atual'] = $usuario->senha.'_'.$usuario->login;

		if(isset($usuario->senha)){
			$this->loadLogin('pages/painel/login/redefinir'); 
		}else{
			redirect('painel/'); 
		}
	}

	public function nova(){

		$post  = $this->input->post();
		$this->data['pagina'] = 'Redefinir senha';

		if(isset($post['senha']) && !empty($post['senha'])){
			$senha = $post['senha'];

			$acesso = explode('_', $post['atual']);

			$usuario = new Usuario();
			$usuario->get_this(array('senha' => $acesso[0], 'login' => $acesso[1]));

			$usuario->senha = md5($post['senha']);		

			if($usuario->save()){

				$this->data['msg'] = 'Sua senha foi redefinida! Efetue o login na página de acesso.';
				$this->loadLogin();
	        }
	        else{
	            $this->data['error'] = implode($postagem->error->all);
				$this->data['atual'] = $usuario->senha.'_'.$usuario->login;

	            $this->loadLogin('pages/painel/login/redefinir');    
	        }
		}
		else{

			$this->data['error'] = 'Insira uma senha válida';
            $this->loadLogin('pages/painel/login/redefinir');            
        }
        
	}
}
