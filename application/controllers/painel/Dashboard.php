<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Painel {

        public function __construct(){
                parent::__construct();
                date_default_timezone_set('America/Sao_Paulo');

                $this->data["nav_dashboard"] = true;
        }

        public function index(){
                $this->load->model('mensagem');
                $this->load->model('analytics');

                $post = $this->input->post();

                $analytics = new Analytics();
                $analytics->mes = (isset($post["mes"])) ? $post["mes"] : date('m', time());
                $analytics->ano = (isset($post["ano"])) ? $post["ano"] : date('Y', time());

                $this->data['ano'] = $analytics->ano;
                $this->data['mes'] = $analytics->mes;

                $this->data['grafico']     = $analytics->getChart();
                $this->data['acessos']     = $analytics->getAcessos();
                $this->data['ranking']     = $analytics->getRanking();
                $this->data['rejeicao']    = $analytics->getRejeicao();
                $this->data['permanencia'] = $analytics->getPermanencia();

                $novos = new Mensagem();
                $this->data['novos'] = $novos->count(array('status' => 0));

                $mensagem = new Mensagem();
                $this->data['mensagens'] = $mensagem->get_where(array('status >' => -1, 'data >' => $analytics->ano.'-'.$analytics->mes.'-'.'01 00:00:00',  'data <' => $analytics->ano.'-'.$analytics->mes.'-'.'30 00:00:00'));

                $this->loadPage('painel/pages/dashboard');
        }

        public function mensagem($ref){

                $this->load->model('mensagem');
                $post = $this->input->post('post');

                $mensagem = new Mensagem();
                $mensagem->get_this(array('id' => $ref));
                $mensagem->status = $post['status'];
                $mensagem->save();

                $response['status'] = 200;
                $response['item']   = $ref;
                $response['badge']  = 'badge-soft-'.$mensagem->getBadge();
                $response['texto']  = $mensagem->getStatus();

                echo json_encode($response);
        }
}
