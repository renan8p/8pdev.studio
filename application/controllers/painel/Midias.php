<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Midias extends MY_Painel {

	public function __construct(){
		parent::__construct();
        $this->load->model('midia');

        $this->data['pagina'] = 'Mídias';
        $this->data['singular'] = 'midia';
        $this->data['controller'] = 'midias';
	}

	public function lista(){

		$post = $this->input->post();
        $checks = (!empty($post['checks'])) ? json_decode($post['checks'], true) : array();
        $checks = (!is_array($checks)) ? array($checks) : $checks;

		$midias = new Midia();
		$this->data['midias'] = $midias->get_filter($post);
		$this->data['checks'] = $checks;

        if($this->data['midias']){
            $response['lista']  = $this->load->view('painel/includes/midia', $this->data);
            echo json_encode($response);
        }
        else{
            echo "fail";
        }        
	}

    public function midia_data(){

        $post = $this->input->post();

        $midia = new Midia();
        $midia->get_this(array('id' => $post['id']));
        $midia->url = $midia->get_midia('xs');

        echo json_encode($midia);
    }

    public function midia_data_save(){

        $post = $this->input->post();

        $midia = new Midia();
        $midia->get_this(array('id' => $post['id']));
        $midia->from_array($post, false);
        $midia->save();
        
        if(count($midia->fails) == 0){
            $response['status'] = 200;
            $response['id']     = $midia->id;         
        }
        else{
            $response['status'] = 0;
            $response['fails']  = $midia->fails;
        }

        echo json_encode($response);
    }

	// Faz upload de arquivos
    public function midia_upload(){
        
        if($_FILES){

            $sizes = array('1600', '1200', '550', '360', '250');

            $array_img    = array();
            $diretorio    = "upload/".date("Y");
            $file_element = "arquivo";

            // Organiza diretórios por mes e ano
            if(!is_dir($diretorio)){
                mkdir($diretorio, 0755, true);
            }

            $config['upload_path']   = $diretorio;
            $config['overwrite']     = FALSE;
            $config['allowed_types'] = 'gif|jpg|png|bmp|jpeg|mp3|mp4|pdf|zip|rar|doc|xls|svg';

            //Biblioteca de Upload/Thumb
            $this->load->library('upload');
            $this->load->library('image_lib');

            $files        = $_FILES;
            $erros        = 0;
            $erro         = NULL;
            $return_list  = array();
            $numero_files = count($files);

            // for ($i = 0; $i < $numero_files; $i++) {            

            foreach($_FILES as $key => $file){                

                $config['max_size'] = Lazy::get_maxsize_file($files[$key]['type'][0]);

                $this->upload->initialize($config);

                $_FILES[$key]['name']     = $files[$key]['name'][0];    
                $_FILES[$key]['type']     = strtolower($files[$key]['type'][0]);
                $_FILES[$key]['tmp_name'] = $files[$key]['tmp_name'][0];
                $_FILES[$key]['error']    = $files[$key]['error'][0];
                $_FILES[$key]['size']     = $files[$key]['size'][0];

                $return_name = $files[$key]['name'][0];

                if($this->upload->do_upload($file_element))
                {
                    $dados = $this->upload->data();

                    $midia           = new Midia();
                    $midia->extensao = strtolower($dados['file_ext']);
                    $midia->caminho  = $diretorio . "/" . date("m") . '/';
                    $midia->titulo   = $dados['raw_name'];                
                    $midia->slug     = Lazy::getSlug($dados['raw_name']);
                    $midia->arquivo  = $midia->slug.strtolower($dados['file_ext']);
                    $midia->url      = base_url($midia->caminho . $midia->arquivo);                
                    $midia->tipo     = Lazy::get_format_file($midia->extensao);
                    $midia->data     = date('Y-m-d');
                    $midia->lixeira  = 0;

                    // Organiza diretórios por mes e ano
                    if(!is_dir($midia->caminho)){
                        mkdir($midia->caminho, 0755, true);
                    }

                    // Verifica arquivo existente
                    if(file_exists($midia->caminho . $midia->arquivo)){
                        $rand            = "_" . rand(0,100);
                        $midia->titulo  .= $rand;
                        $midia->slug     = Lazy::getSlug($midia->titulo);
                        $midia->arquivo  = $midia->slug.strtolower($dados['file_ext']);
                        $midia->url      = base_url($midia->caminho . $midia->arquivo);
                    }

                    rename($config["upload_path"] . "/" . $this->upload->file_name, $midia->caminho . $midia->arquivo);

                    if($midia->tipo == "img"){
                        foreach ($sizes as $key => $size) {

                            $config['image_library']  = 'gd2';
                            $config['source_image']   = $midia->caminho . $midia->arquivo;
                            $config['create_thumb']   = TRUE;
                            $config['maintain_ratio'] = TRUE;
                            $config['thumb_marker']   = '-'.$key;
                            $config['width']          = $size;

                            $this->image_lib->clear();
                            $this->image_lib->initialize($config);
                            $this->image_lib->resize();
                        }
                    }

                    // Insere no banco
                    if($midia->save()){
                        $response['status']   = "200";
                        $response['msg']      = "Enviado com sucesso!";
                        $response['arquivo']  = base_url($midia->caminho . $midia->arquivo);
                    }
                    else{
                        $erros++;
                    }

                    if($erros <= 0){
                        array_push($array_img, array('temp' => $midia->id, 'url' => $midia->url) );
                    }

                    array_push($return_list, array("alert" => "success", "file" => $return_name, "feedback" => "Enviado com sucesso!"));
                }
                else{
                    $erros++;
                    array_push($return_list, array("alert" => "danger", "file" => $return_name, "feedback" => $this->upload->display_errors() ));
                }
            }
            
            $response['status'] = "200";
            $response['return_list']  = $return_list;
            $response['midias'] = $array_img;

            if($erros > 0){
                $response['status'] = "0";
                $response['msg']    = $erros . " arquivo(s) não foram salvos! ". $erro;
            }
        }
        else{
            $response['status'] = "0";
            $response['msg'] = "Não foram encontrados arquivos para salvar.";
        }

        echo json_encode($response);
    }

    public function lixeira($id){
        $this->load->model('midia_rel');

        $midia = new Midia();
        $midia->get_this(array('id' => $id));

        unlink($midia->caminho . $midia->arquivo);

        if($midia->tipo == "img"){
            $sizes = array('1600', '1200', '550', '360', '250');
            foreach ($sizes as $key => $size) {
                $unlink = $midia->caminho . $midia->slug . "-" . $key . $midia->extensao;
                unlink($unlink);
                $response['unlink'][$key] = $unlink;
            }
        }

        if(count($midia->fails) == 0){

            $response['status'] = 200;
            $response['id']     = $midia->id; 

            $midia_rel = new Midia_rel();
            $midia_rel->delete_all(array('midia' => $midia->id));

            $midia->delete();          
        }
        else{
            $response['status'] = 0;
            $response['fails']  = $midia->fails;
        }

        echo json_encode($response);
    }
}
