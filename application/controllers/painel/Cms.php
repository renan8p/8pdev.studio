<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cms extends MY_Painel {

	public function __construct(){
		parent::__construct();
		$this->data['nav_tab'] = 'custom';

		if($this->session->userdata('tipo') != "master"){
			redirect('painel');
		}
	}

	public function index(){
		$order = array(
			"order" => 'ordem ASC'
		);

		$area = new Area();
		$area = $area->get_where(array("status"), $order);

		$this->data['areas'] = $area;

		$this->loadPage('painel/pages/cms/areas');
	}

	public function form($id = NULL){

		$area = new Area();
		$area->get_this(array("id" => $id));

		$extra = array(
			"or_where" => array('tipo' => 'lista', 'tipo' => 'produtos', 'tipo' => 'postagens')
		);

		$this->data['area'] = $area;

		$relacionamento = new Area();
		$this->data['relacionamento'] = $relacionamento->get_where(array('status' => 1), $extra);

		$this->loadPage('painel/pages/cms/form');
	}

	public function salvar(){
        $this->load->model('conteudo');

		$post = $this->input->post('post');

		$area = new Area();
		$area->from_array($post);
		$area->slug       = Lazy::getSlug($area->titulo, true);
		$area->slug       = $area->getUnique("slug");
		$area->controller = Lazy::getSlug($area->controller, true);
		$area->controller = $area->getUnique("controller");
		$area->status     = (isset($post['status'])) ? 1 : 0;
		$area->categoria  = (isset($post['categoria'])) ? 1 : 0;

		$detalhes = array(
			'plural'   => (isset($post['plural']))   ? $post['plural']   : "",
			'singular' => (isset($post['singular'])) ? $post['singular'] : "",
			'imagem'   => (isset($post['imagem']))   ? $post['imagem']   : 0,
			'conteudo' => (isset($post['conteudo'])) ? $post['conteudo'] : 0,
			'lista'    => (isset($post['lista']))    ? $post['lista']    : "",
			'blocos'   => (isset($post['blocos']))   ? $post['blocos']   : "",
			'ativa'    => (isset($post['ativa']))    ? $post['ativa']    : ""
		);

		$area->detalhes = json_encode($detalhes);

		$area->save();

		if(count($area->fails) == 0){
			$conteudo = new Conteudo();
			$conteudo->get_this(array('cod_area' => $area->id));

			if(!isset($conteudo->id)){
				$conteudo->cod_area = $area->id;
				$conteudo->titulo   = $area->titulo;
				$conteudo->slug     = $area->slug;
				$conteudo->tipo     = 'pagina';
				$conteudo->criado_em = date('Y-m-d H:i');		
				$conteudo->status    = 1;
				$conteudo->modificado_em = date('Y-m-d H:i');			
				$conteudo->save();
			}

			$response['status'] = 200;
			$response['id']     = $area->id;
			$response['url']    = "painel/cms";
		}
		else{			
			$response['status'] = 0;
			$response['fails']  = $area->fails;
		}

		echo json_encode($response);
	}

	public function custom($cod_area, $tipo = "pagina"){
        $this->load->model('custom');
        $this->load->model('sublista');

		$area = new Area();
		$area->get_this(array('id' => $cod_area));

		$tipos = array('pagina', 'item', 'categorias');

		if(in_array($tipo, $tipos)){
    		$controller = $tipo;
		}

		$extra = array(
			"order" => 'ordem ASC'
		);

		$fields = new Custom();
		$fields = $fields->get_where("name <> 'label_titulo' AND cod_area = ".$area->id." AND controller = '".$controller."' AND (bloco = 'main' OR bloco = 'sidebar')", $extra);

		$relacionado = new Area();
		$this->data['relacionado'] = $relacionado->get_where(array('status' => 1, 'tipo <>' => 'pagina'));

		$list_fields = NULL;

		foreach ($fields as $key => $field) {

			$this->data['tipo']  = $field->get_tipo();
			$this->data['field'] = $field;
			$this->data['bloco'] = true;

			$list_fields .= $this->load->view('painel/includes/custom_fields/'.$field->get_tipo(), $this->data, true);
		}

		// Sublistas
		$sublistas = new Sublista();
		$sublistas = $sublistas->get_where(array('cod_area' => $area->id, 'tipo' => $tipo), array("order" => 'ordem ASC, id DESC'));

		if(isset($sublistas)){

			foreach ($sublistas as $key => $sublista){

				$custom = new Custom();
				$fields = $custom->get_fields($area, $sublista->slug);

				$sublista_fields = NULL;

				foreach ($fields as $key => $field) {

					$this->data['tipo']  = $field->get_tipo();
					$this->data['field'] = $field;
					$this->data['bloco'] = false;

					$sublista_fields .= $this->load->view('painel/includes/custom_fields/'.$field->get_tipo(), $this->data, true);
				}

				$sublista->fields = $sublista_fields;

				//$sublista_blocos .= $this->load->view('painel/includes/custom_fields/sublista', $this->data, true);
			}

			$this->data['sublistas'] = $sublistas;
		}
		// FIM sublistas

		$label_titulo = new Custom();
		$label_titulo = $label_titulo->get_titulo($area, $controller);

		$this->data['area'] = $area;
		$this->data['label_titulo'] = $label_titulo;
		$this->data['fields'] = $list_fields;		
		$this->data['nav_tab'] = $tipo;
		$this->data['controller'] = $controller;

		$this->loadPage('painel/pages/cms/custom', "header");
	}

	public function custom_salvar(){
        $this->load->model('custom');

		$post = $this->input->post();
		$area = $post['area'];
		$label = (isset($post['label'])) ? $post['label'] : NULL;
		$controller = $post['controller'];
		$fields = (isset($post['fields'])) ? $post['fields'] : array();

		$permanecem = array();

		// configura label de titulo
		if($label){
			$titulo = new Custom();
			$titulo->cod_area = $area;
			$titulo->label    = ($label) ? $label : 'Título';
			$titulo->name     = 'label_titulo';
			$titulo->ordem    = 0;
			$titulo->status   = 1;
			$titulo->bloco    = 'main';
			$titulo->tipo     = 'input';
			$titulo->controller = $controller;

			if($titulo->save()) array_push($permanecem, $titulo->id);		
		}
		// --

		foreach ($fields as $key => $field) {

			$custom = new Custom();
			$custom->cod_area = $area;
			$custom->from_array($field);
			$custom->name     = (empty($custom->name)) ? Lazy::getSlug($custom->label, true) : Lazy::getSlug($custom->name, true);
			$custom->name     = $custom->get_unique_custom('name');
			$custom->ordem    = $key;
			$custom->status   = 1;
			$custom->controller = $controller;

			$custom->save();

			array_push($permanecem, $custom->id);
		}

		$extra = (count($permanecem)) ? array('where_not_in' => $permanecem) : NULL;

		$custom = new Custom();
		$custom->delete_all(array("cod_area" => $area, "controller" => $controller), $extra);

		$response['status'] = 200;

		echo json_encode($response);
	}

	public function custom_fields(){
		$post = $this->input->post();

		$this->data['bloco'] = $post['bloco'];
		$this->data['tipo']  = $post['tipo'];
		$this->data['field'] = $post['field'];

		if($post['tipo'] == "relacionado"){
			$relacionado = new Area();
			$this->data['relacionado'] = $relacionado->get_where(array('status' => 1, 'tipo <>' => 'pagina'));
		}

		$this->load->view('painel/includes/custom_fields/'.$post['tipo'], $this->data);
	}

	public function reorder(){

		$post = $this->input->post('ordem');

		foreach ($post as $key => $item) {
			
			$area = new Area();
			$area->get_this(array('id' => $item));	
			$area->ordem = $key;
			$area->save();
		}
	}

	public function excluir_sessao($cod_area){
        $this->load->model(array('custom', 'categoria', 'conteudo'));

        $area  = new Area();
        $area->get_this(array('id' => $cod_area));

        if($area->delete()){

	        $where = array('cod_area' => $cod_area);        

	        $custom = new Custom();
	        $custom->delete_all($where);

	        $categoria = new Categoria();
	        $categoria->delete_all($where);

	        $conteudo = new Conteudo();
	        $conteudo->delete_all($where);

			$response['status'] = 200;
			$response['id']     = $cod_area;
			$response['url']    = "painel/cms";
		}
		else{
			$response['status'] = 0;
			$response['id']     = $cod_area;
		}

		echo json_encode($response);
	}
}
