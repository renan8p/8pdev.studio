<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends MY_Site {

	public function __construct(){
		parent::__construct();
        $this->load->model('conteudo');
		$this->load->library('pagination');

		$configuracao = new Configuracao();
		$this->data['configuracao'] = $configuracao->get_content();

		$contato = new Conteudo();
		$this->data['contato'] = $contato->get_content('contato');
	
		$servicos = new Conteudo();
		$this->data['servicos'] = $servicos->get_list('servicos', array('destaque' => 1));

		$this->seo['titulo'] = $configuracao->titulo;
		$this->seo['description'] = $configuracao->descricao;
		$this->seo['url'] = base_url();
		$this->seo['image'] = $configuracao->get_img('lg');
		$this->seo['keywords'] = $configuracao->tags;
		$this->data['seo'] = $this->seo;

		$this->data['page'] = 'interna';

		$this->data['id_body'] = '';

		if(!$this->session->userdata('logado')){
			if(isset($configuracao->status) && !$configuracao->status){
				redirect( 'http://creditofinnance.com.br/franquia' );
				exit();
			}
		}
	}

	public function index(){
		$this->Inicio();
	}

	public function Inicio(){

		$main = new Conteudo();
		$this->data['inicio'] = $main->get_content('inicio');

		$home = new Conteudo();
		$this->data['home'] = $home->get_list('inicio');

		$servico = new Conteudo();
		$this->data['servico'] = $servico->get_content('servicos');

		$slide = new Conteudo();
		$this->data['slide'] = $slide->get_list('portfolio', array('destaque' => 1), array('limit' => 4));

		$portfolio = new Conteudo();
		$this->data['portfolio'] = $portfolio->get_list('portfolio');

		$depoimento = new Conteudo();
		$this->data['depoimento'] = $depoimento->get_content('depoimentos');

		$depoimentos = new Conteudo();
		$this->data['depoimentos'] = $depoimentos->get_list('depoimentos', array('destaque' => 1));

		$blog = new Conteudo();
		$this->data['blog'] = $blog->get_content('blog');

		$noticias = new Conteudo();
		$this->data['noticias'] = $noticias->get_list('blog', array('destaque' => 1));

		$this->seo['titulo'] .= ' - '.$main->content('titulo');
		$this->seo['url'] = base_url();
		$this->data['seo'] = $this->seo;

		$this->data['page'] = 'home';

		$this->loadPage('pages/inicio');
	}

	public function Empresa(){

		$empresa = new Conteudo();
		$this->data['empresa']  = $empresa->get_content('empresa');		

		$this->seo['titulo'] .= ' - '.$empresa->content('titulo');
		$this->seo['url'] = base_url('empresa');
		$this->data['seo'] = $this->seo;

		$this->data['page'] = 'empresa';

		$this->loadPage('pages/quem-somos');
	}

	public function Portfolio(){

		$portfolio = new Conteudo();
		$this->data['portfolio']  = $portfolio->get_content('portfolio');

		$pecas = new Conteudo();
		$this->data['pecas'] = $pecas->get_list('portfolio');

		$this->seo['titulo'] .= ' - '.$portfolio->content('titulo');
		$this->seo['url'] = base_url('portfolio');
		$this->data['seo'] = $this->seo;

		$this->data['page'] = 'portfolio';

		$this->loadPage('pages/portfolio');
	}

	public function Projeto($slug){

		$slug = Lazy::getSlug($slug);

		$projeto = new Conteudo();
		$this->data['projeto'] = $projeto->get_item('portfolio', $slug);

		$pecas = new Conteudo();
		$this->data['pecas'] = $pecas->get_list('portfolio', array('id <>' => $projeto->id), array('limit' => 2));

		$this->seo['titulo']     .= ' - '.$projeto->content('titulo');
		$this->seo['url']         = base_url('projeto/'.$slug);
		$this->seo['description'] = $projeto->content('texto');
		$this->seo['image']       = $projeto->get_img('lg');
		$this->seo['keywords']    = $projeto->content('tags_seo');
		$this->data['seo']        = $this->seo;

		$this->data['page'] = 'portfolio';

		$this->loadPage('pages/portfolio-single');
	}

	public function Servicos($slug = NULL){

		$slug = Lazy::getSlug($slug);

		$servico = new Conteudo();
		$this->data['servico']  = $servico->get_content('servicos');

		$servicos = new Conteudo();
		$servicos = $servicos->get_list('servicos');

		if($slug){
			$reorder = array();
			$first   = NULL;

			foreach ($servicos as $key => $item) {

				if($item->slug == $slug){
					$first = $item;
					unset($servicos[$key]);
				}
				else{
					array_push($reorder, $item);
				}
			}

			array_push($reorder, $first);
			$reorder = array_reverse($reorder);

			$this->data['servicos'] = array_values(array_filter($reorder));
		}
		else{
			$this->data['servicos'] = $servicos;
		}

		$this->seo['titulo'] .= ' - '.$servico->content('titulo');
		$this->seo['url'] = base_url('servicos');
		$this->data['seo'] = $this->seo;

		$this->data['item'] = $slug;

		$this->data['page'] = 'servicos';

		// $this->data['script'] = Lazy::go_to($slug);

		$this->loadPage('pages/servicos');
	}

	public function Contato(){

		$contato = new Conteudo();
		$this->data['contato']  = $contato->get_content('contato');

		$this->seo['titulo'] .= ' - '.$contato->content('titulo');
		$this->seo['url'] = base_url('contato');
		$this->data['seo'] = $this->seo;

		$this->data['page'] = 'contato';

		$this->loadPage('pages/contato');
	}

	public function Blog($pagina = 1){

		$blog = new Conteudo();
		$this->data['blog']  = $blog->get_content('blog');

		$extra["order"] = "views DESC";
		$extra["limit"] = 4;

		$destaques = new Conteudo();
		$this->data['destaques'] = $destaques->get_list('blog', array('lixeira' => 0), $extra);

		$qtd_por_page = 6;

		$extra = NULL;
		$extra['limit'] = $qtd_por_page;
		$extra['offset'] = ($pagina-1) * $qtd_por_page;

		$m_noticias = new Conteudo();
		$noticias = $m_noticias->get_list('blog', array('lixeira' => 0), $extra);
		$count = count( $m_noticias->get_list('blog', array('lixeira' => 0)) );

		$this->data['noticias'] = $noticias;

		$page_config = Lazy::pagination('blog/', $count, $qtd_por_page);
		$this->pagination->initialize($page_config);

		$this->data['pagination'] = $this->pagination->create_links();

		$this->seo['titulo'] .= ' - '.$blog->content('titulo');
		$this->seo['url'] = base_url('blog');
		$this->data['seo'] = $this->seo;

		$this->data['page'] = 'blog';

		$this->loadPage('pages/blog');
	}

	public function Noticia($slug = NULL){

		$blog = new Conteudo();
		$this->data['blog']  = $blog->get_content('blog');

		$noticia = new Conteudo();
		$noticia = $noticia->get_item('blog', $slug);

		$extra["order"] = "views DESC";
		$extra["limit"] = 4;

		$destaques = new Conteudo();
		$this->data['destaques'] = $destaques->get_list('blog', array('lixeira' => 0), $extra);

		$this->data['noticia'] = $noticia;
		$this->data['texto'] = json_decode($noticia->postagem_json);

		$this->seo['titulo'] .= ' - '.$noticia->content('titulo');
		$this->seo['description'] = $noticia->resumo;
		$this->seo['image'] = $noticia->get_img('lg');
		$this->seo['keywords'] = $noticia->tags;
		$this->seo['url'] = base_url($slug);
		$this->data['seo'] = $this->seo;
		
		$this->data['page'] = 'noticia';

		$noticia_single = new Conteudo();
		$noticia_single->get_this(array('id' => $noticia->id));
		$noticia_single->views = $noticia_single->views + 1;
		$noticia_single->save();

		$this->loadPage('pages/blog-single');
	}	

	public function show404(){
		
		$this->loadPage('pages/show404');
	}

	public function propostas($slug){

		$proposta = new Conteudo();
		$proposta = $proposta->get_item('proposta', $slug);

		$this->data['proposta'] = $proposta;

		$this->loadView('pages/proposta');
	}
}
