<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Async extends MY_Site{

	public function __construct(){
		parent::__construct();
        $this->load->model('conteudo');
        $this->load->library('session');
	}

	public function Analytics(){
        $this->load->library('session');

        if(!$this->session->userdata('logado')){
	        $this->load->model('analytics');

			$post = $this->input->post();

			$visita = new Analytics();
			$visita->setVisita($post);			
			$visita->save();
        }
	}

	public function lead(){
		$this->load->helper('email');
        $this->load->model('mensagem');

		$contato = new Conteudo();
		$contato->get_content('contato');

		$post = $this->input->post();

		if(!empty($post['nome']) && !empty($post['whatsapp'])){

			$msg = NULL;
			foreach ($post as $key => $value) {
				$msg .= ($key != "email_contato") ? $value."<br>" : NULL;
			}	

			$this->load->library('email');
			$this->email->initialize();

			$this->email->from('rafael@8pdev.studio', 'Lead Via site');
			$this->email->to($post['email_contato']);

			$this->email->subject('Lead Via site');
			$this->email->message($msg);

			if($this->email->send()){

				$mensagem = new Mensagem();
				$mensagem->remetente = $post['nome'];
				$mensagem->para      = 'rafael@8pdev.studio';
				$mensagem->assunto   = 'Lead Via site';					
				$mensagem->conteudo  = $msg;
				$mensagem->status    = 0;
				$mensagem->data      = date('Y-m-d');
				$mensagem->save();

				$response['status'] = 200;
			}
			else{
				$response['status'] = 0;
				$response['msg'] = $this->email->print_debugger();
			}
		}
		else{
			$response['status'] = 0;
			$response['msg'] = $this->email->print_debugger();
		}

		echo json_encode($response);
	}

	public function pedido(){
		$this->load->helper('email');
        $this->load->model('mensagem');

		$contato = new Conteudo();
		$contato->get_content('contato');

		$post = $this->input->post();

		if(!empty($post['whats_email_pedido'])){

			$msg = NULL;
			foreach ($post as $key => $value) {
				$msg .= ($key != "email_contato") ? $value."<br>" : NULL;
			}

			$this->load->library('email');
			$this->email->initialize();

			$this->email->from('rafael@8pdev.studio', 'Via site');
			$this->email->to($post['email_contato']);

			$this->email->subject('Pedido Via site');
			$this->email->message($post['whats_email_pedido']);

			if($this->email->send()){

				$mensagem = new Mensagem();
				$mensagem->remetente = $post['nome_pedido'];
				$mensagem->para      = 'rafael@8pdev.studio';
				$mensagem->assunto   = 'Pedido Via site';					
				$mensagem->conteudo  = $msg;
				$mensagem->status    = 0;
				$mensagem->data      = date('Y-m-d');
				$mensagem->save();

				$response['status'] = 200;
			}
			else{
				$response['status'] = 0;
				$response['msg'] = $this->email->print_debugger();
			}
		}
		else{
			$response['status'] = 0;
			$response['msg'] = $this->email->print_debugger();
		}

		echo json_encode($response);
	}

	public function Enviar(){
        $this->load->model('mensagem');
		$this->load->helper('email');

		$post = $this->input->post();
		$post['assunto'] = "Contato via site";

		if(!empty($post['nome']) && !empty($post['email']) && !empty($post['mensagem'])){

			if(valid_email($post['email'])){

				$msg = NULL;
				foreach ($post as $key => $value) {
					$msg .= ($key != "to") ? $value."<br>" : NULL;
				}

				$this->load->library('email');
				$this->email->initialize();

				$this->email->from($post['email'], $post['nome']);
				$this->email->to($post['to']);

				$this->email->subject($post['assunto']);
				$this->email->message($msg);

				if($this->email->send()){

					$mensagem = new Mensagem();
					$mensagem->remetente = $post['email'];
					$mensagem->para      = $post['to'];
					$mensagem->assunto   = $post['assunto'];					
					$mensagem->conteudo  = $msg;
					$mensagem->status    = 0;
					$mensagem->data      = date('Y-m-d');
					$mensagem->save();

					$response['status'] = 200;
					$response['msg']    = 'Enviado com sucesso <i class="fa fa-check"></i>';			
				}
				else
				{
					$response['status'] = 0;
		            $response['msg']    = 'Ops! Algo deu errado ao enviar.';
				}
			}
			else
			{
				$response['status'] = 0;
	            $response['msg']    = 'Tente um e-mail válido';
			}
		}
		else
		{
			$response['status'] = 0;
            $response['msg']    = '<p class="text-danger">Algum campo pode estar vazio.</p>';
		}	

		echo json_encode($response);
	}
}
