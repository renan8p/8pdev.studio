<?php if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class MY_Painel extends MY_Controller{

    public $data = array();

    function __construct()
    {
        parent::__construct();
        $this->load->library('session'); 

        $this->data['style'] = array();
        $this->data['script'] = array();       

        if(!$this->session->userdata('logado')){
            redirect('painel/login');
        }
        else{
            $this->load->model('area');

            $order = array("order" => 'ordem ASC');
            $areas = new Area();

            $this->data['controller'] = base_url();

            if($this->session->userdata('tipo') == "usuario"){
                $acessos = json_decode($this->session->userdata('acessos'));
                $this->data['menu'] = $areas->get_in_array($acessos, $order);
            }
            else{
                $this->data['menu'] = $areas->get_where(array('status' => 1), $order);
            }
        }
    }

    public function loadPage($pagina = NULL, $extra = NULL){

        $this->data['lateral']  = $this->load->view('painel/includes/lateral', $this->data, true);
        $this->data['topo']     = $this->load->view('painel/includes/topo', $this->data, true);
        $this->data['header']   = ($extra) ? $this->load->view('painel/includes/'.$extra, $this->data, true) : $extra;
        $this->data['conteudo'] = $this->load->view($pagina, $this->data, true);
        $this->data['midia']    = $this->load->view('painel/includes/midias', $this->data, true);

        $this->load->view('painel/templates/default', $this->data);
    }

    public function show404()
    {
        $this->loadPage('pages/painel/404');
    }

}
