<?php

//class News_model extends CI_Model {
class MY_Model extends CI_Model{

	var $feedback = array();
	var $fails    = array();

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get_all($limit = NULL, $offset = NULL)
	{
		$this->db->from($this->table);
		$this->db->order_by('id', 'DESC');
		$this->db->limit($limit, $offset);

		$query = $this->db->get();

		return $query->result(get_called_class());
	}

	public function get_row($where = array())
	{
		if($where){
			$this->db->where($where);
		}

		$query = $this->db->get($this->table);
		$query = $query->first_row();

		return $query;
	}

	public function get_where($where = array(), $extra = array())
	{
		if($where){
			$this->db->group_start()->where($where)->group_end();
		}

		$this->db->from($this->table);

		if(isset($extra['where_not_in'])){
			$this->db->where_not_in('id', $extra['where_not_in']);
		}

		if(isset($extra['or_where'])){
			$this->db->group_start()->or_where($extra['or_where'])->group_end();
		}

		if(isset($extra['limit'])){
			$this->db->limit($extra['limit']);
		}

		if(isset($extra['offset'])){
			$this->db->offset($extra['offset']);
		}

		if(isset($extra['order'])){
			$this->db->order_by($extra['order']);
		}
		else{
			$this->db->order_by('id', 'DESC');
		}

		$query = $this->db->get();

		return $query->result(get_called_class());
	}

	public function return_to_array($where = array(), $to = "id", $extra = array())
	{
		if($where){
			$this->db->where($where);
		}

		$this->db->from($this->table);

		if(isset($extra['order'])){
			$this->db->order_by($extra['order']);
		}
		else{
			$this->db->order_by('id', 'DESC');
		}

		if(isset($extra['limit'])){
			$this->db->limit($extra['limit']);
		}

		$query = $this->db->get();

		$result = $query->result_array();

		$retorno = array();
		foreach ($result as $key => $item) {
			array_push($retorno, $item[$to]);
		}

		return $retorno;
	}

	public function get_like($like, $where = array(), $extra = array())
	{
		$this->db->from($this->table);

		if($where){
			$this->db->group_start()->where($where)->group_end();
		}

		if(isset($extra['or_where'])){
			$this->db->group_start()->or_where($extra['or_where'])->group_end();
		}		

		if(isset($extra['limit'])){
			$this->db->limit($extra['limit']);
		}

		if(isset($extra['offset'])){
			$this->db->offset($extra['offset']);
		}

		if(isset($extra['order'])){
			$this->db->order_by($extra['order']);
		}
		else{
			$this->db->order_by('id', 'DESC');
		}

		$this->db->group_start();

		foreach ($like as $key => $item) {
			$this->db->or_like($key, $item);
		}

		$this->db->group_end();

		$query = $this->db->get();

		return $query->result(get_called_class());
	}

	public function get_in_array($array = array(), $extra = array())
	{
		$this->db->from($this->table);

		if(isset($extra['order'])){
			$this->db->order_by($extra['order']);
		}

		if(isset($extra['where'])){
			$this->db->group_start()->where($extra['where'])->group_end();
		}

		$this->db->group_start()->where_in('id', $array)->group_end();

		$query = $this->db->get();

		return $query->result(get_called_class());
	}

	public function get_this($where = array())
	{	
		if($where){
			$this->db->where($where);
		}

		$query = $this->db->get($this->table);
		$array = $query->row_array();

		foreach ($this->db->list_fields($this->table) as $position) {			
			if(is_array($array) && array_key_exists($position, $array)){

				if(isset($array[$position]) && $position != "table"){
					$this->{$position} = trim($array[$position]);
				}
			}
		}
			
		return $this;
	}

	public function count($where = array(), $extra = array()){
        
		if($where){
			$this->db->group_start()->where($where)->group_end();
		}

		if(isset($extra['like'])){
			$this->db->group_start()->or_like($extra['like'])->group_end();
		}

		$query = $this->db->get($this->table);

		return $query->num_rows();
	}

	public function getSlug($slug){

		$slug = Lazy::get_slug_by_string(url_title($slug, 'dash', TRUE));

		$this->db->where(array('slug' => $slug));
		if(isset($this->id)){
			$this->db->where(array('id <>' => $this->id));
		}

		$query = $this->db->get($this->table);
		$retorno = ($query->num_rows() > 0) ? $query->num_rows() + 1 : NULL;

		return $slug.$retorno;
	}

	public function get_img($dim = NULL, $img = "img"){
		$this->load->model('midia');

		if (isset($this->{$img})) {
		
			$midia = new Midia();
			$midia->get_this( array('id' => $this->{$img}) );

			return (isset($midia->id)) ? $midia->get_midia($dim) : NULL;
		}

		return NULL;
	}

	public function get_file($file = "img"){
		$this->load->model('midia');

		$midia = new Midia();
		$midia->get_this( array('id' => $this->{$file}) );

		return (isset($midia->id)) ? $midia->url : NULL;
	}

	public function get_gallery($dim = NULL, $col = "img"){
		$this->load->model('midia');

		$midias = ( is_array($this->{$col}) ) ? $this->{$col} : json_decode($this->{$col}); 

		$galeria = array();

		foreach ($midias as $key => $m) {

			$midia = new Midia();
			$midia->get_this( array('id' => $m) );

			array_push($galeria, $midia->get_midia($dim));
		}

		return $galeria;
	}

	public function get_html_midia($dim = NULL, $img = "img"){
		$this->load->model('midia');

		$midia = new Midia();
		$midia->get_this( array('id' => $this->{$img}) );

		return (isset($midia->id)) ? $midia->get_midia_content($dim) : NULL;
	}

	public function get_img_id($dim = NULL, $img = 0, $html = false){
		$this->load->model('midia');

		if($img){
			$midia = new Midia();
			$midia->get_this( array('id' => $img) );

			return ($html) ? $midia->get_midia_content($dim) : $midia->get_midia($dim);
		}

		return true;
	}

	public function getUnique($coluna){

		$string = Lazy::getSlug($this->{$coluna}, true);

		$this->db->where(array($coluna => $string));

		if(isset($this->id)){
			$this->db->where(array('id <>' => $this->id));			
		}

		$query = $this->db->get($this->table);
		$query = $query->num_rows();

		if($query > 0){
			$this->{$coluna} = $string.$query;
			return $this->getUnique($coluna);
		}else{
			return $string;
		}
	}

	public function set_relacional($relacional, $values, $tipo = "conteudo"){
    	$this->load->model('relacional');

		$where = array(
					"cod_principal"    => $this->id,
					"sessao_principal" => $this->cod_area,
					"tipo_principal"   => $tipo
				);

		$relacionar = new Relacional();
		$relacionar->delete_all($where);		

		foreach ($relacional as $key => $rel) {
			$itens = json_decode($rel);

			if(isset($values[$itens->field])){				

				if(!is_array($values[$itens->field])){
					$values[$itens->field] = array($values[$itens->field]);
				}

				foreach ($values[$itens->field] as $key => $item) {

					$relacionar = new Relacional();
					$relacionar->cod_item             = $item;
					$relacionar->cod_principal        = $this->id;
					$relacionar->sessao_principal     = $this->cod_area;
					$relacionar->controller_principal = $this->get_controller();
					$relacionar->sessao_origem        = $itens->area;
					$relacionar->controller_origem    = $this->get_controller($itens->area);
					$relacionar->tipo_principal       = $tipo;
					$relacionar->save();

					//cod_principal: item principal a ser relacionado - 1:n
					//sessao_principal: sessao do item principal $this->cod_area
					//cod_item: itens a serem relacionados - n:1
					//sessao_origem: sessao dos itens listados em campo $itens->area
				}
			}
		}
	}

	public function get_controller($cod_area = NULL){
        $this->load->model('area');

        $cod_area = ($cod_area) ? $cod_area : $this->cod_area;

        $area = new Area();
        $area->get_this(array("id" => $cod_area));

        return $area->controller;
	}

	/** 
	* Função para gravar em DB objeto setado, verificando
	* regras pré-estabelecidas pela função get_required()
	* @access public
	* @return boolean
	*/
	public function save()
	{
		if($this->get_required()){

			$fields = array();

			foreach ($this->db->list_fields($this->table) as $position) {
				if(array_key_exists($position, $this) && !is_array($this->{$position}) && $position != "table"){
					//echo $fields[$position].' - '.$this->{$position}.' < <br>';
					$fields[$position] = trim($this->{$position});					
				}
			}

			if(isset($this->id) && !empty($this->id)){
				$this->db->where('id', $this->id);
				$this->db->update($this->table, $fields);
				$this->get_this(array('id' => $this->id));
			}
			elseif($this->db->insert($this->table, $fields)){
				$this->id = $this->db->insert_id();
			}
		}

		return $this;
	}


	/** 
	* Função para deletar objeto setado
	* @access public
	* @return boolean
	*/
	public function delete(){

		if($this->db->delete($this->table, array('id' => $this->id))){
			return true;
		}

		return false;
	}

	/** 
	* Função para deletar em lote
	* @access public
	* @return boolean
	*/
	public function delete_all($where, $extra = array())
	{
		if(isset($where)){
			$this->db->where($where);
		}

		if(isset($extra['where_not_in'])){
			$this->db->where_not_in('id', $extra['where_not_in']);
		}

		if(isset($where)){
			if($this->db->delete($this->table)){
				return true;
			}
		}

		return false;
	}

	/** 
	* Função para verificação, relacionamento e comparação entre campos existentes na table (correspondentes a classe)
	* e posições do array (passado por parametro). Sem que colunas ou variaveis a mais ou a menos sejam setadas ou utilizadas.
	* A variavel validate confirma se o objeto deve ser preenchido com os valores ja registrados no banco ou não. Caso seja preciso fazer a
	* validação dos campos é indicado setar como false, assim os valores do objeto não serão populados pelos registros do banco.
	* @access public
	* @param array $array
	* @param validate bool
	* @return object
	*/
	public function from_array($array, $validate = true)
	{	
		foreach ($this->db->list_fields($this->table) as $position){
			if(is_array($array) && array_key_exists($position, $array)){

				if(isset($array[$position]) && $position != "table"){
					$this->{$position} = (isset($array[$position])) ? trim($array[$position]) : NULL;
					//echo $position.' - '.$this->{$position}.'<br>';
				}
			}
		}

		if(isset($this->id) && $validate){

			if($this->id != NULL){

				$this->db->where(array('id' => $this->id));

				$query = $this->db->get($this->table);
				$result = $query->row_array();

				foreach ($this->db->list_fields($this->table) as $position) {

					if(empty($this->{$position}) && $position != "table"){
						$this->{$position} = trim($result[$position]);
						//echo $position.' | '.trim($result[$position]).'<br>';
					}
				}
			}
		}
			
		return $this;
	}

	public function from_obj($array)
	{
		$this->db->where(array('id' => $this->id));

		$query = $this->db->get($this->table);
		$result = $query->row_array();

		foreach ($result as $key => $position) {

			if(isset($array[$key])){

				$this->{$key} = trim($array[$key]);
				
				// if(empty($array[$key]) && !array_search($array[$key], $this->validation)){
					
				// }
				// else{
				// 	$this->{$key} = $position;
				// }

			}
			else{
				$this->{$key} = $position;
			}
		}		

		return $this;
	}

	/** 
	* Função para verificar se os campos setados como obrigatório 
	* na classe filha estão preenchidos ou correspondem os tipo indicado.
	* @access public
	* @return array or boolean
	*/
	public function get_required()
	{
		if(isset($this->validation)){

			foreach($this->validation as $array){

				$mensagem = NULL;
				$input_value = trim($this->{$array["field"]});

				if( in_array("required", $array["rules"]) > 0 && empty($input_value) ){
					$mensagem .= "Este campo é obrigatório.";
				}

				if( in_array("unique", $array["rules"]) > 0 ){

					$where = array( $array["field"] => $input_value );
					
					if(isset($this->id)){
						$where = array( 'id <>' => $this->id, $array["field"] => $input_value );
					}

					if( $this->count( $where ) > 0 ){
						$mensagem .= "Ops! Alguém já está usando.";
					}
				}

				if( in_array("valid_email", $array["rules"]) > 0 ){

					if( !filter_var($input_value, FILTER_VALIDATE_EMAIL) || empty($input_value) ){
						$mensagem .= " Insira um endereço de e-mail válido. ";
					}
				}

				if($mensagem){
					array_push( $this->fails, array($array["field"] => $mensagem) );
				}
			}

			return (count($this->fails) == 0) ? $this : false;
		}
		
		return true;
	}

	public function object_to_array($data)
	{
	    if (is_array($data) || is_object($data))
	    {
	        $result = array();
	        foreach ($data as $key => $value)
	        {
	            $result[$key] = $this->object_to_array($value);
	        }
	        return $result;
	    }
	    return $data;
	}

	public function check_last_query(){
		print_r($this->db->last_query());
	}

	public function kill_extra(){
		unset($this->table);
		unset($this->validation);
		unset($this->feedback);
		unset($this->conteudo);
		unset($this->fails);
	}
}