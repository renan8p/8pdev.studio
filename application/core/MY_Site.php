<?php if (! defined ( 'BASEPATH' )) exit ( 'No direct script access allowed' );

class MY_Site extends MY_Controller{

    public $data = array();
    public $theme = 'site';

    function __construct(){
        parent::__construct();
        $this->load->library('session');

        $this->load->model('objeto');
        $this->load->model('configuracao');

        $this->data['style'] = NULL;
        $this->data['script'] = NULL;

        $configuracao = new Configuracao();
        $configuracao->get_this();

        $this->data['config']           = $configuracao;
        $this->data['meta_titulo']      = $configuracao->titulo;
        $this->data['meta_url']         = base_url();
        $this->data['meta_image']       = $configuracao->get_img('md');
        $this->data['meta_site_name']   = $configuracao->titulo;
        $this->data['meta_description'] = $configuracao->descricao;
        $this->data['meta_keywords']    = $configuracao->tags;

        $this->data['social']    = json_decode($configuracao->sociais);
        $this->data['navegacao'] = json_decode($configuracao->navegacao);

        if($this->session->userdata('usuario')){
            $this->data['logado'] = 'logado';
        }
        else{
            $this->data['logado'] = 'deslogado';
        }
    }

    public function loadPage($pagina = NULL, $extra = NULL){

        $this->data['topo']     = $this->load->view('themes/site/includes/topo', $this->data, true);
        $this->data['conteudo'] = $this->load->view('themes/site/'.$pagina, $this->data, true);
        $this->data['rodape']   = $this->load->view('themes/site/includes/rodape', $this->data, true);

        $this->load->view('themes/site/templates/default', $this->data);
    }

    public function loadView($pagina = NULL){

        $this->data['topo']     = NULL;
        $this->data['conteudo'] = $this->load->view('themes/site/'.$pagina, $this->data, true);
        $this->data['rodape']   = NULL;

        $this->load->view('themes/site/templates/default', $this->data);
    }   

    public function show404(){

        exit('404');
        $this->loadPage('themes/site/pages/404');
    }
}
