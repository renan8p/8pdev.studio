<?php

class Lazy{

	public static function get_data_extensa($data_f){
		if($data_f != ''){
			$data_f = Lazy::dataSite($data_f);
			$data_f = explode('/', $data_f);
			return $data_f[0].' de '.Lazy::getNomeMes($data_f[1]).' de '.$data_f[2];
		}else{
			return false;
		}
	}
	
	public static function dataSite($data){

		return date('d/m/Y', strtotime($data));
	}

	public static function dataBd($data){

		return date('Y-m-d', strtotime($data));
	}

	public static function dataTimeBd($data){
		if($data){
			$data = explode(' ', $data);
			$date = explode("/", $data[0]);
			$date = $date[2]."-".$date[1]."-".$date[0];
			$time = $data[1];
			return $date." ".substr($time, 0,5);
		}else
		return false;
	}

	public static function dataTimeBdHis($data){
		if($data){
			$data = explode(' ', $data);
			$date = explode("/", $data[0]);
			$date = $date[2]."-".$date[1]."-".$date[0];
			$time = $data[1];
			return $date." ".substr($time, 0,5).":00";
		}else
		return false;
	}

	public static function get_text_pure($content){

		return preg_replace('/(<[^>]+) style=".*?"/i', '$1', strip_tags( $content, '<p><b><strong><a>'));
	}

	public static function pagination($slug, $count, $per_page, $uri_segment = 2){

		$config = array(
			'base_url' => base_url($slug),
			'total_rows' => $count,
			'per_page' => $per_page,
			'uri_segment' => $uri_segment,
			'num_links' => 4,
			'reuse_query_string' => TRUE,
			'use_page_numbers' => TRUE,
			'next_link' => '<span class="fa fa-angle-right"></span>',
			'prev_link' => '<span class="fa fa-angle-left"></span>',
			'full_tag_open' => '<ul class="pagination justify-content-center">',
			'full_tag_close' => '</ul>',
			'num_tag_open' => '<li class="page-item">',
			'num_tag_close' => '</li>',
			'cur_tag_open' => '<li class="page-item active"><a class="page-link">',
			'cur_tag_close' => '</a></li>',
			'prev_tag_open' => '<li class="page-item">',
			'prev_tag_close' => '</li>',
			'next_tag_open' => '<li class="page-item">',
			'next_tag_close' => '</li>',
			'first_link' => TRUE,
			'last_link' => TRUE,
			'first_tag_open' => '<li class="page-item">',
			'first_tag_close' => '</li>',
			'last_tag_open' => '<li class="page-item">',
			'last_tag_close' => '</li>'
		);

		return $config;
	}

	public static function get_social_share($url){

		$url = base_url($url);

    	?>
			<ul class="list-inline mb-0">
				<li class="list-inline-item">
					<button type="button" class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent" onclick="window.open('http://www.facebook.com/sharer.php?u=<?php echo $url ?>', 'newwindow', 'width=300, height=250')">
						<span class="fab fa-facebook-f btn-icon__inner"></span>
					</button>
				</li>
				<li class="list-inline-item">
					<button type="button" class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent" onclick="window.open('https://twitter.com/share?url=<?php echo $url ?>', 'newwindow', 'width=300, height=250')">
						<span class="fab fa-twitter btn-icon__inner"></span>
					</button>
				</li>
				<li class="list-inline-item">
					<a class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent" target="_blank" href="https://api.whatsapp.com/send?text=<?php echo $url ?>">
						<span class="fab fa-whatsapp btn-icon__inner"></span>
					</a>
				</li>
			</ul>
    	<?php
    }

	public static function getScript($string){

		$map = array('[removed]' => '');
        $string = strtr(trim($string), $map);

        return '<script>'.$string.'</script>';
	}

	public static function getTimeDateDb($data){
		if($data){
			$data = explode(' ', $data);
			$time = $data[1];
			return substr($time, 0,5);
		}else
		return false;
	}	

	public static function getPrimeiroNome($nome){

		$nome = explode(" ", $nome);

		return $nome[0];
	}

	public static function getDateDb($data){
		if($data){
			$data = explode(' ', $data);
			$data = explode('-', $data[0]);
			return $data[2]."/".$data[1]."/".$data[0];
		}else
		return false;
	}

	public static function getDateTimeDb($data){
		if($data){
			$data = explode(' ', $data);
			$date = explode('-', $data[0]);
			return $date[2]."/".$date[1]."/".$date[0]." ".$data[1];
		}else
		return false;
	}

	public static function convertDateDb($data){
		if($data){
			$data = explode(' ', $data);
			$data = explode('-', $data[0]);
			return $data[0]."-".$data[1]."-".$data[2];
		}else
		return false;
	}

	public static function get_horario($hora, $tipo){

		$hora = explode(':', $hora);

		return ($tipo == "hora") ? $hora[0] : $hora[1];
	}

	public static function hora_site($hora){
		if($hora){
			$hora = explode(':', $hora);
			return $hora[0].":".$hora[1];
		}else
		return false;
	}

	public static function nome_curto($nome){
		$nome = explode(' ', $nome);
		return $nome[0];
	}

	public static function manter_numeros($str){
		$pattern = '/[^0-9]*/';
		return preg_replace ( $pattern, '', $str );
	}
	
	public static function getExt($file){
		$aux = explode(".", $file);
		return $aux[count($aux) - 1];
	}

    public static function strtoupper($string){

        return mb_convert_case($string, MB_CASE_UPPER, 'UTF-8');
    }

    public static function strtolower($string){

        return mb_strtolower($string);
    }
	
	public static function get_slug_by_string($str){
		
		$acentos = array("á", "à", "â", "ã", "ä", "é", "è", "ê", "ë", "í", "ì", "î", "ï", "ó", "ò", "ô", "õ", "ö", "ú", "ù", "û", "ü", "ç", "Á", "À", "Â", "Ã", "Ä", "É", "È", "Ê", "Ë", "Í", "Ì", "Î", "Ï", "Ó", "Ò", "Ô", "Õ", "Ö", "Ú", "Ù", "Û", "Ü", "Ç", 'ñ', 'Ñ', ' ');
		$sem     = array("a", "a", "a", "a", "a", "e", "e", "e", "e", "i", "i", "i", "i", "o", "o", "o", "o", "o", "u", "u", "u", "u", "c", "A", "A", "A", "A", "A", "E", "E", "E", "E", "I", "I", "I", "I", "O", "O", "O", "O", "O", "U", "U", "U", "U", "C", 'n', 'n', '-');
		
		//$resto = '/[^a-z0-9\-]+/i';		
		$str = str_replace($acentos, $sem, $str);
		$str = preg_replace('/[^A-Za-z0-9-]+/', '_', $str);
		$str = strtolower($str);
		
		return $str;
	}

	public static function urlGetArray($url, $decode = false){

		$query = explode("&", $url);

		$parameters = array();
		foreach($query as $parameter) {

			$param = explode("=", $parameter);

			if (!empty($param) && count($param) == 2){
				$parameters[$param[0]] = $decode == true ? urldecode($param[1]) : $param[1];
			}
		}

		return $parameters;
	}

	public static function getSlug($string, $controller = false){

		$string = (string) $string;

		$controller = ($controller) ? '_' : '-';

        $map = array('ä' => 'a', 'ã' => 'a', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ê' => 'e', 'ë' => 'e', 'è' => 'e', 'é' => 'e', 'ï' => 'i', 'ì' => 'i', 'í' => 'i', 'ö' => 'o', 'õ' => 'o', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'ü' => 'u', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'À' => 'A', 'Á' => 'A', 'É' => 'E', 'Í' => 'I', 'Ó' => 'O', 'Ö' => 'O', 'Ú' => 'U', 'Ü' => 'U', 'ñ' => 'n', 'Ñ' => 'N', 'ç' => 'c', 'Ç' => 'C', '-' => '', '(' => '', ')' => '', ',' => '', ';' => '-', ':' => '', '|' => '-', '!' => '', '"' => '', '#' => '', '$' => '', '%' => '', '&' => '', '+' => '', '=' => '', '?' => '', '~' => '', '^' => '', '/' => '', '>' => '', '<' => '', 'ª' => '', 'º' => '', ' ' => $controller, "'" => '', "�" => '');

        $map['-'] = ($controller) ? '_' : '-';

        $string = strtr(trim($string), $map);
        $string = strtolower($string);

        return Lazy::get_slug_by_string($string);
    }

	public static function get_resumo($conteudo = NULL, $tamanho = 100){

        return html_entity_decode(substr($conteudo, 0, $tamanho));
    }

	public static function get_format_file($ext){

		if(in_array($ext, array(".gif", ".jpg", ".png", ".bmp", ".jpeg"))){
			$ext = "img";
		}

		if(in_array($ext, array(".doc", ".xls", ".pdf"))){
			$ext = "documento";
		}

		if($ext == ".mp3"){
			$ext = "audio";
		}

		if($ext == ".mp4"){
			$ext = "video";
		}

		return $ext;
	}

	public static function get_maxsize_file($type){

		$png_mimes  = array('image/x-png');
		$jpeg_mimes = array('image/jpg', 'image/jpe', 'image/jpeg', 'image/pjpeg');

		if (in_array($type, $png_mimes))
		{
			$type = 'image/png';
		}
		elseif (in_array($type, $jpeg_mimes))
		{
			$type = 'image/jpeg';
		}

		$img_mimes = array('image/gif',	'image/jpeg', 'image/png');		

		return (in_array($type, $img_mimes, TRUE)) ? 1024 * 2 : 1024  * 200;
	}

	public static function get_id_youtube($url){
		$itens = parse_url ($url);
		parse_str($itens['query'], $params);

		return $params['v'];
	}

	public static function get_tags($tags = NULL){

		if( $tags ){

			$tag = NULL;
			$tags = explode(',', $tags);

			foreach ($tags as $key => $val) {
				$tag .= '<a class="badge badge-light mr-2 my-1" href="buscar/'.trim($val).'">#'.trim($val).'</a>';
			}

			return $tag;
		}
	}

	public static function go_to($goto, $click = NULL){
		return ($goto) ? '<script type="text/javascript">
							$(document).ready(function(){
								$("html, body").animate({
								    scrollTop: ($("#'.$goto.'").offset().top - 100)
								}, 500);
							});
						</script>' : NULL;
	}

	public static function array_unique($input, $col){	

		$input = (is_array($input)) ? $input : json_decode(json_encode($input), true);

		$return = array();

		foreach ($input as $key => $value) {
			if(isset($value->{$col})) array_push($return, $value->{$col});
		}

		return array_filter(array_unique($return));
	}

	public static function explode($string){

		return array_map('trim', explode(',', $string));
	}

	public static function getNomeMes($mes){
		
		switch ($mes) {
			
			case 1:
				return "janeiro";
				break;
			case 2:
				return "fevereiro";
				break;
			case 3:
				return "março";
				break;
			case 4:
				return "abril";
				break;
			case 5:
				return "maio";
				break;	
			case 6:
				return "junho";
				break;
			case 7:
				return "julho";
				break;
			case 8:
				return "agosto";
				break;
			case 9:
				return "setembro";
				break;
			case 10:
				return "outubro";
				break;
			case 11:
				return "novembro";
				break;
			case 12:
				return "dezembro";
				break;
		}
	}
	
	public static function get_estado($uf){

		switch ($uf) {
			
			case "AC":
				return "Acre";
				break;

			case "AL":
				return "Alagoas";
				break;

			case "AM":
				return "Amazonas";
				break;

			case "AP":
				return "Amapá";
				break;

			case "BA":
				return "Bahia";
				break;

			case "CE":
				return "Ceará";
				break;

			case "DF":
				return "Distrito Federal";
				break;

			case "ES":
				return "Espírito Santo";
				break;

			case "GO":
				return "Goiás";
				break;

			case "MA":
				return "Maranhão";
				break;

			case "MT":
				return "Mato Grosso";
				break;

			case "MS":
				return "Mato Grosso do Sul";
				break;

			case "MG":
				return "Minas Gerais";
				break;

			case "PA":
				return "Pará";
				break;

			case "PB":
				return "Paraíba";
				break;

			case "PR":
				return "Paraná";
				break;

			case "PE":
				return "Pernambuco";
				break;

			case "PI":
				return "Piauí";
				break;

			case "RJ":
				return "Rio de Janeiro";
				break;

			case "RN":
				return "Rio Grande do Norte";
				break;

			case "RS":
				return "Rio Grande do Sul";
				break;

			case "RO":
				return "Rondônia";
				break;

			case "RR":
				return "Roraima";
				break;

			case "SC":
				return "Santa Catarina";
				break;

			case "SP":
				return "São Paulo";
				break;

			case "SE":
				return "Sergipe";
				break;

			case "TO":
				return "Tocantin";
				break;
		}		
	}

	public static function get_home_form($form_principal){

		$campos = $form_principal->get_sublista_item("campos_do_formulario");

		foreach($campos as $key => $campo){

			if($campo->content('destacar_inicio') == 'true'){

				$link_to = ($campo->content('tipo_simulacao') == "email") ? "[type='email']" : '#simula-'.$form_principal->slug.'-'.Lazy::getSlug($campo->content('titulo'), true);

				$encurt = Lazy::getSlug($form_principal->titulo, true).'-'.$campo->content('slug');
                ?>
                    <label for="simula-<?php echo $encurt; ?>">
                    	<?php echo $campo->content('titulo'); ?>
                    </label>
					<input type="<?php echo $campo->content('tipo_simulacao'); ?>" class="form-control simula-<?php echo $encurt; ?>" id="banner-<?php echo $encurt; ?>" data-linked-to="<?php echo $link_to; ?>" name="simula-<?php echo $encurt; ?>" <?php echo $campo->content('obrigatorio_simulacao'); ?> placeholder="<?php echo $campo->content('placeholder_simulacao'); ?>">
					<button type="submit" class="btn btn-primary btn-arrow" data-toggle="modal" data-target="#modal-<?php echo Lazy::getSlug($form_principal->titulo, true); ?>">
						Seguir
					</button>				
				<?php

				return true;
			}
		}
	}

	public static function get_modal_forms($servicos, $simulacoes){

		foreach ($servicos as $key => $item){
			foreach ($simulacoes as $key => $simulacao){

				if($simulacao->tipo_de_servico == $item->id){
					?>
					<div class="modal fade" id="modal-<?php echo $item->content('slug'); ?>" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="static">
					    <div class="modal-dialog modal-dialog-centered" role="document">
					        <div class="modal-content text-center pt-0">
					        	<button class="btn btn-close" data-dismiss="modal"><i class="fal fa-times"></i></button>
					            <div class="modal-body">
									<form class="simulacao" method="POST" name="simulacao">
										<div class="form-content">
											<h2 class="title-theme mb-1"><?php echo $simulacao->content('titulo'); ?></h2>

											<input type="hidden" class="form-control" name="formulario" id="formulario" value="<?php echo $simulacao->id; ?>">
											<input type="hidden" class="input_key" name="input_key" value="<?php echo $item->content('slug'); ?>">

											<?php

												if($simulacao->content('link_externo')){
													?><input type="hidden" class="form-control" name="link_externo" value="<?php echo $simulacao->content('link_externo'); ?>"><?php
												}

											?>
											<input type="hidden" class="form-control" name="simulacao-produto" id="simulacao-<?php echo $item->content('slug'); ?>">
											<div class="form-group mb-4">
												<p class="mb-4"><span><?php echo $simulacao->content('legenda'); ?></span></p>										
											</div>
											<?php

												if($simulacao->content('ativar_slide') == "ativado"){
													?>
													<div class="form-group mb-4">
														<p class="mb-0">
															<strong class="title-theme">
																<small class="fz-1x">R$</small>
																<span class="valor-label"><?php echo $simulacao->content('minimo_slide'); ?></span><small class="fz-2x ml--3">,00</small>
															</strong>
														</p>										
														<input type="range" id="simula-<?php echo $simulacao->content('slug'); ?>" name="simula-<?php echo $simulacao->content('slug'); ?>" class="form-control custom-range" data-output="valor-label" data-linked-to="#banner-<?php echo $simulacao->content('slug'); ?>" min="<?php echo $simulacao->content('minimo_slide'); ?>" max="<?php echo $simulacao->content('maximo_slide'); ?>" step="<?php echo $simulacao->content('minimo_slide'); ?>" value="<?php echo $simulacao->content('minimo_slide'); ?>"/>
													</div>
													<?php
												}
												else{
													?><div class="py-2"></div><?php
												}

												foreach ($simulacao->get_sublista_item("campos_do_formulario") as $key => $subitem) {

													if($subitem->content('tipo_simulacao') == "select"){
							                    		?>
														<div class="form-group">
									                        <label for="simula-<?php echo $item->slug.'-'.$subitem->content('slug'); ?>">
									                        	<?php echo $subitem->content('titulo'); ?>							                        
									                        </label>
									                        <div class="custom-select">
																<select class="form-control" id="simula-<?php echo $item->slug.'-'.$subitem->content('slug'); ?>" name="simula-<?php echo $item->slug.'-'.$subitem->content('slug'); ?>" <?php echo $subitem->content('obrigatorio_simulacao'); ?> data-linked-to=".simula-<?php echo $item->slug.'-'.$subitem->content('slug'); ?>">
																	<option value="0" selected="" disabled="">Escolha seu perfil</option>
																	<?php

																		foreach (Lazy::explode($subitem->content('options')) as $key => $option) {
																			?><option value="<?php echo $option; ?>"><?php echo $option; ?></option><?php
																		}

																	?>
																</select>
									                        </div>
														</div>
							                    		<?php
							                    	}
							                		else{
								                        ?>
														<div class="form-group">
									                        <label for="simula-<?php echo $item->slug.'-'.$subitem->content('slug'); ?>">
									                        	<?php echo $subitem->content('titulo'); ?>
									                        </label>
									                        <?php

									                        	if($subitem->content('tipo_simulacao') != "textarea"){

									                        		$link_to = ($subitem->content('tipo_simulacao') == "email") ? "[type='email']" : '.simula-'.$item->slug.'-'.$subitem->content('slug');
									                        		?>
																	<input type="<?php echo $subitem->content('tipo_simulacao'); ?>" class="form-control" id="simula-<?php echo $item->slug.'-'.$subitem->content('slug'); ?>" name="simula-<?php echo $item->slug.'-'.$subitem->content('slug'); ?>" <?php echo $subitem->content('obrigatorio_simulacao'); ?> placeholder="<?php echo $subitem->content('placeholder_simulacao'); ?>" data-linked-to="<?php echo $link_to; ?>">
																	<?php
																}
																else{
																	?>
																	<textarea class="form-control" id="simula-<?php echo $item->slug.'-'.$subitem->content('slug'); ?>" name="simula-<?php echo $item->slug.'-'.$subitem->content('slug'); ?>" <?php echo $subitem->content('obrigatorio_simulacao'); ?> data-linked-to=".simula-<?php echo $item->slug.'-'.$subitem->content('slug'); ?>"><?php echo $subitem->content('placeholder_simulacao'); ?></textarea>
																	<?php
																}
															?>			
														</div>
														<?php
													}
												}

											?>
											<button type="submit" class="btn btn-primary btn-arrow btn-block m-0">
												<?php echo ($simulacao->content('texto_do_botao')) ? $simulacao->content('texto_do_botao') : "Confirmar"; ?>
											</button>
										</div>
										<div class="form-feedback text-center">
										</div>
									</form>					            	
					            </div>
					        </div>
					    </div>
					</div>
					<?php
				}
			}
		}
	}
}