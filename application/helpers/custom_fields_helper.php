<?php

class Custom_fields{	

	public static function custom_list($fields, $conteudo = NULL, $identify = 0){
		?>
		<div class="row">
			<?php

			foreach ($fields as $key => $field){

				$value = (isset($conteudo->{$field->name})) ? $conteudo->{$field->name} : NULL;

				if(in_array($field->tipo, array('input', 'number', 'tel', 'date', 'money', 'textarea', 'password'))){

					$mascara = ($field->tipo == "money") ? 'placeholder="$" data-mask="#.##0,00" data-mask-reverse="true"' : NULL;
					$mascara = ($field->tipo == "tel") ? 'placeholder="(__)____-___" data-mask="(00) 00000-0000"' : $mascara;
					$mascara = ($field->tipo == "date") ? ' data-toggle="flatpickr"' : $mascara;

					?>
						<div class="col-12 col-md-<?php echo $field->coluna; ?>">
							<div class="form-group">
								<label>
									<?php echo $field->label; ?>
								</label> 
								<?php

									if($field->tipo == "textarea"){
										if(strpos($field->classe, 'no-quill') > -1){
											?>
											<textarea class="custom-field form-control form-control-lg <?php echo $field->classe; ?>" placeholder="<?php echo $field->placeholder; ?>" name="<?php echo $field->name; ?>"><?php echo $value; ?></textarea>
											<?php
										}
										else{
											?>
											<div class="custom-field get-html <?php echo $field->classe; ?>"  data-quill-placeholder="<?php echo $field->placeholder; ?>" name="<?php echo $field->name; ?>" data-toggle="quill"><?php echo $value; ?></div>								
											<?php
										}
									}
									else{
										?>
										<input <?php echo $mascara; ?> type="<?php echo $field->tipo; ?>" class="custom-field form-control form-control-lg <?php echo $field->classe; ?>" name="<?php echo $field->name; ?>" <?php echo ($field->max) ? "maxlength='".$field->max."'" : NULL; ?> value='<?php echo html_escape($value); ?>' placeholder="<?php echo $field->placeholder; ?>">
										<?php
									}
								?>
								<small class="form-text text-muted"><?php echo $field->ajuda; ?></small>
							</div>
						</div>
					<?php
				}

				if($field->tipo == "select"){

					$options = ($field->options) ? explode(",", $field->options) : array();
					?>
					<div class="col-12 col-md-<?php echo $field->coluna; ?>">
						<div class="form-group">
							<label><?php echo $field->label; ?></label>
							<select class="custom-field form-control form-control-lg <?php echo $field->classe; ?>" data-toggle="select" data-minimum-results-for-search="-1" name="<?php echo $field->name; ?>">
								<?php

									foreach ($options as $key => $option) {
										$explode = explode(":", $option);
										$val = (isset($explode[0])) ? $explode[0] : $option;
										$opt = (isset($explode[1])) ? $explode[1] : $option;

										?>
										<option value="<?php echo $val; ?>" <?php echo ($value == $val) ? "selected" : NULL;?>>
											<?php echo $opt; ?>
										</option>
										<?php
									}

								?>
							</select>
							<small class="form-text text-muted"><?php echo $field->ajuda; ?></small>
						</div>
					</div>	
					<?php
				}

				if($field->tipo == "divisao"){
					?>
						<div class="col-12">
							<div class="header">
								<div class="header-body pb-0 pt-5">
									<h3 class="header-title mb-1">
										<?php echo $field->label; ?>
									</h3>
									<p class="text-muted font-14 mb-3">
										<?php

											if($field->placeholder) echo $field->placeholder;

										?>
									</p>
								</div>
							</div>
						</div>
					<?php
				}

				if( in_array( $field->tipo, array('image', 'galeria', 'thumb') ) ){

					if($field->tipo == 'image') {
						?>
						<div class="col-12 col-md-<?php echo $field->coluna; ?>">
							<div class="form-group">
								<label><?php echo $field->label; ?></label>
							    <div id="<?php echo $identify.'-'.$field->name; ?>-load" class="row midia-box single">
							        <?php

							        	$check = json_decode($value);
							        	$value = (is_array($check)) ? count($check) : $value;

							        	if($value){
							        		
							        		?>
							        			<div class="midia-list" data-midia="<?php echo $value; ?>">
							        				<div class="midia-delete" data-target="#<?php echo $identify.'-'.$field->name; ?>">
							            				<i class="fe fe-x"></i>
							            			</div>
							            			<?php echo $field->get_img_id('sm', $value, true); ?>
							            		</div>
							        		<?php
							        	}

							        ?>
								</div>
							    <input type="hidden" class="custom-field" name="<?php echo $field->name; ?>" id="<?php echo $identify.'-'.$field->name; ?>" value="<?php echo (isset($value)) ? $value : NULL; ?>">
								<button type="button" class="btn btn-white btn-block btn-midia" data-target="#<?php echo $identify.'-'.$field->name; ?>">
									<i class="fe fe-image"></i> <?php echo (empty($field->placeholder)) ? 'Selecionar' : $field->placeholder; ?>
								</button>
							</div>
						</div>
			        	<?php
			    	}

			    	if($field->tipo == 'thumb') {
						?>
						<div class="col-12 col-md-<?php echo $field->coluna; ?>">
							<div class="form-group">
								<label><?php echo $field->label; ?></label>
							    <div class="row">
							    	<div class="col-auto pr-0">
							    		<div class="max-100">
										    <div id="<?php echo $identify.'-'.$field->name; ?>-load" class="row midia-box single mr-1">
										        <?php

										        	$check = json_decode($value);
										        	$value = (is_array($check)) ? count($check) : $value;

										        	if($value){
										        		
										        		?>
										        			<div class="midia-list" data-midia="<?php echo $value; ?>">
										        				<div class="midia-delete" data-target="#<?php echo $identify.'-'.$field->name; ?>">
										            				<i class="fe fe-x"></i>
										            			</div>
										            			<?php echo $field->get_img_id('sm', $value, true); ?>
										            		</div>
										        		<?php
										        	}

										        ?>
											</div>
										</div>
									</div>
									<div class="col pl-0">
										<input type="hidden" class="custom-field" name="<?php echo $field->name; ?>" id="<?php echo $identify.'-'.$field->name; ?>" value="<?php echo (isset($value)) ? $value : NULL; ?>">
										<button type="button" class="btn btn-white btn-block btn-midia" data-target="#<?php echo $identify.'-'.$field->name; ?>">
											<i class="fe fe-image"></i> <?php echo (empty($field->placeholder)) ? 'Selecionar' : $field->placeholder; ?>
										</button>
									</div>						    	
							    </div>
							</div>
						</div>
			        	<?php
			    	}

			    	if($field->tipo == 'galeria') {
			    		?>
			    		<div class="col-12 col-md-<?php echo $field->coluna; ?>">
			    			<div class="form-group">
				                <label for="galeria"><?php echo $field->label; ?></label>				                
							    <input type="hidden" class="custom-field" name="<?php echo $field->name; ?>" id="<?php echo $identify.'-'.$field->name; ?>" value="<?php echo (isset($value)) ? $value : NULL; ?>">
								<button type="button" class="btn btn-white btn-block btn-midia" data-multiple="multiple" data-target="#<?php echo $identify.'-'.$field->name; ?>">
									<i class="fe fe-image"></i> <?php echo (empty($field->placeholder)) ? 'Selecionar' : $field->placeholder; ?>
								</button>

								<div id="<?php echo $identify.'-'.$field->name; ?>-load" class="row midia-box multiple">
							        <?php

							        	if($value){
							        		foreach (json_decode($value) as $key => $img) {
							        			?>
								        			<div class="midia-list" data-midia="<?php echo $img; ?>">
								        				<div class="midia-delete" data-target="#<?php echo $identify.'-'.$field->name; ?>">
								            				<i class="fe fe-x"></i>
								            			</div>
								            			<?php echo $field->get_img_id('sm', $img, true); ?>
								            		</div>
								        		<?php
							        		}
							        	}

							        ?>
								</div>
				            </div>
						</div>
			    		<?php
			    	}
				}

				if($field->tipo == "relacionado"){

					?>
					<div class="col-12 col-md-<?php echo $field->coluna; ?>">
						<!--
							Input responsável por guardar regras de relacionamento em tabela
							area: sessao de origem dos itens a serem listados no input
							field: campo do json que guardará os itens a serem relacionados ao item principal
						-->
						<input type="hidden" name="relacional[]" value='{"area": <?php echo $field->relacionado; ?>, "field": "<?php echo $field->name; ?>"}'>
						<div class="form-group">
							<label><?php echo $field->label; ?></label>
							<select class="custom-field form-control form-control-lg <?php echo $field->classe; ?>" data-toggle="select" <?php echo ($field->options == "single") ? "single" : "multiple"; ?> name="<?php echo $field->name; ?>">
								<?php
								
									$select = (!empty($value)) ? $value : array();
									$select = (is_array($select)) ? $select : array($select);
									foreach ($field->get_rel() as $key => $rel) {										
										?>
										<option value="<?php echo $rel->id; ?>" <?php echo (in_array($rel->id, $select)) ? "selected" : NULL;?>>
											<?php echo $rel->titulo; ?>
										</option>
										<?php
									}

								?>
							</select>
							<small class="form-text text-muted"><?php echo $field->ajuda; ?></small>
						</div>
					</div>	
					<?php
				}
			}
			?>
		</div>
		<?php
	}

	public static function custom_sublist($sublista, $fields){
		?>
			<div class="card">
				<div class="card-header">
					<h4 class="card-title m-0">
						<?php echo $sublista->titulo; ?>
					</h4>
					<div class="text-right">
						<button type="button" class="btn btn-small btn-white px-4" data-toggle="modal" data-target="#modal-<?php echo $sublista->slug; ?>">
							Adicionar
						</button>
					</div>
				</div>
				<div class="card-body sublista-<?php echo $sublista->slug; ?> p-0" data-sublista-load="<?php echo $sublista->slug; ?>">
					<div class="text-center py-4">
						<div class="spinner-grow spinner-grow-sm" role="status"></div>
					</div>
				</div>
			</div>

			<div class="modal fade" id="modal-<?php echo $sublista->slug; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">				
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-body pb-3">
							<div class="form-sublista" id="form-<?php echo $sublista->slug; ?>">
								<div class="form-group">
									<label>
										Título
									</label>
									<input type="text" name="titulo_sublista" value="<?php echo (isset($conteudo_post->titulo)) ? $conteudo_post->titulo : NULL; ?>" class="form-post form-control">
								</div>					
								<?php

									Custom_fields::custom_list($fields);

								?>
							</div>
						</div>	
						<div class="modal-footer justify-content-between bd-top py-3 pl-3">
							<button type="button" class="btn btn-link" data-dismiss="modal">Cancelar</button>
							<button type="button" class="btn btn-primary px-4" data-sublista="<?php echo $sublista->slug; ?>" data-dismiss="modal">Inserir</button>
						</div>			
					</div>
				</div>
			</div>
		<?php		
	}

	public static function custom_blog($editable_blocks = NULL){

		?>
			<div class="card">
				<div class="card-body">
					<div class="form-group">
					    <label for="postagem">Conteúdo:</label>
						<textarea name="postagem_json" id="postagem_json" class="form-post editable_blocks"><?php echo (isset($editable_blocks)) ? $editable_blocks : NULL; ?></textarea>
					    <div id="blog-editor">
					    	<?php

					    		$blog = json_decode($editable_blocks);

					    		if($blog && count($blog)){

					        		foreach ($blog as $key => $block) {

										if($block->type == "img"){
											?>
					            				<div class="editor-block img-box">
													<figure><?php echo $block->content; ?></figure>
												</div>
					            			<?php
					            		}
										if($block->type == "text"){
					            			?>
					            				<div class="editor-block text-box">
													<textarea class="textarea editable"><?php echo $block->content; ?></textarea>
												</div>
					            			<?php
					            		}
					            		if($block->type == "youtube"){
					            			?>
					            				<div class="editor-block youtube-box loaded">
					            					<object><?php echo $block->content; ?></object>
												</div>
					            			<?php
					            		}
					        		}
					        	}
					        	else{
					    		?>
									<div class="editor-block text-box">
										<textarea class="textarea editable"></textarea>
									</div>
					    		<?php
					        	}

					    	?>
						</div>
					</div>

					<div class="form-group mb-0">
						<div class="row">
							<div class="col-4">
								<button type="button" class="btn btn-white btn-block btn-text">
									<span class="fe fe-align-left"></span> Texto
								</button>
							</div>
							<div class="col-4">
								<button type="button" class="btn btn-white btn-block btn-midia btn-image" data-filter="img" data-gerencia="url">
									<span class="fe fe-image"></span> Imagem
								</button>
							</div>
							<div class="col-4">
								<button type="button" class="btn btn-white btn-block btn-youtube">
									<span class="fe fe-video"></span> Vídeo
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php
	}
}