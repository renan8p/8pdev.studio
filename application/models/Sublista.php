<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sublista extends MY_Model {

	public $table = 'sublista';
	public $validation = array(
						array(
							'field' => 'titulo',
							'label' => 'Título',
							'rules' => array('required')
						)
					);

	public function __construct()
	{
		parent::__construct();
	}

	public function get_sublista_fields(){
		$this->load->model('custom');
		$this->load->model('area');

		$area = new Area();
		$area->get_this(array('id' => $this->cod_area));

		$fields = new Custom();
		return $fields->get_fields($area, $this->slug);
	}
}