<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Custom extends MY_Model {

	public $table = 'custom';
	public $validation = array(
							array(
								'field' => 'label',
								'label' => 'label',
								'rules' => array('required')
							)
						);

	public function __construct()
	{
		parent::__construct();
	}

	public function get_fields($area, $ambiente, $bloco = "main"){

		$order["order"] = 'ordem ASC';

		return $this->get_where(array('name <>' => 'label_titulo', 'cod_area' => $area->id, "controller" => $ambiente, "bloco" => $bloco), $order);
	}

	public function get_titulo($area, $ambiente){

		$titulo = $this->get_row(array('name' => 'label_titulo', 'cod_area' => $area->id, "controller" => $ambiente));
		return (isset($titulo->label)) ? $titulo->label : "Título";
	}

	public function get_rel(){
        $this->load->model('conteudo');

		$area = new Area();
		$area->get_this(array('id' => $this->relacionado));

		$extra["order"] = 'titulo ASC';

		if($this->options != "categorias"){
			$items = new Conteudo();
			$items = $items->get_where(array('cod_area' => $this->relacionado, 'tipo <>' => 'pagina', 'lixeira' => 0, "status" => 1), $extra);	
		}
		else{
			$items = new Categoria();
			$items = $items->get_where(array('cod_area' => $this->relacionado, 'lixeira' => 0, "status" => 1), $extra);			
		}

		return ($items) ? $items : array();
	}

	public function get_unique_custom($coluna){

		$string = Lazy::getSlug($this->{$coluna}, true);

		$this->db->where(array('cod_area' => $this->cod_area, $coluna => $string, 'id <>' => $this->id));

		$query = $this->db->get($this->table);
		$query = $query->num_rows();

		if($query > 0){
			$this->{$coluna} = $string.$query;
			return $this->getUnique($coluna);
		}else{
			return $string;
		}
	}

	public function get_tipo(){

		$tipo = (in_array($this->tipo, array('image', 'galeria', 'thumb'))) ? 'midia' : NULL;
		$tipo = (in_array($this->tipo, array('select'))) ? 'select' : $tipo;
		$tipo = (in_array($this->tipo, array('divisao'))) ? 'divisao' : $tipo;
		$tipo = (in_array($this->tipo, array('relacionado'))) ? 'relacionado' : $tipo;
		$tipo = (in_array($this->tipo, array('input', 'number', 'phone', 'date', 'money', 'textarea', 'password'))) ? 'text' : $tipo;

		return $tipo;
	}

	public function get_sublistas($sublistas, $tipo){

		$sublistas = json_decode($sublistas);

		foreach ($sublistas as $key => $sublista) {
			print_r($sublista);
		}
	}
}