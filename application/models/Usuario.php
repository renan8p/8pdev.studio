<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends MY_Model {

	public $table = 'usuario';
	public $validation = array(
							array(
								'field' => 'nome',
								'label' => 'Nome',
								'rules' => array('required')
							),
							array(
								'field' => 'email',
								'label' => 'Email',
								'rules' => array('required', 'unique')
							),
							array(
								'field' => 'login',
								'label' => 'Login',
								'rules' => array('required', 'unique')
							)
						);

	public function __construct(){
		parent::__construct();
	}

	public function login($post){

		$acesso = (strstr($post['login'], '@')) ? "email" : "login";
		$usuario = $this->get_this(array($acesso => $post['login'], "senha" => md5($post['senha']), "status" => 1));

		if(isset($usuario->id)){

			$data = array(
				'id'	  => $usuario->id,
				'nome'    => $usuario->nome,
				'login'   => $usuario->login,
				'img'     => $this->get_img_id('sm', $usuario->img),
				'email'   => $usuario->email,
				'acessos' => $usuario->acessos,
				'tipo'    => $usuario->tipo,
				'logado'  => true
			);

			$this->session->set_userdata($data);

			return $usuario;
		}

		return false;
	}
}