<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Area extends MY_Model {

	public $table = 'area';
	public $validation = array(
							array(
								'field' => 'titulo',
								'label' => 'Título',
								'rules' => array('required')
							),
							array(
								'field' => 'controller',
								'label' => 'Controller',
								'rules' => array('required')
							)
						);

	public function __construct()
	{
		parent::__construct();
	}

	public function getDetalhes($string){

		$detalhes = json_decode($this->detalhes);
		$detalhe  = (isset($detalhes->{$string})) ? $detalhes->{$string} : NULL;

		return $detalhe;
	}

	public function get_lista_paginas(){

		$query = $this->db->query('
			SELECT * FROM
				conteudo conteudo,
				area area
			WHERE
				conteudo.cod_area = area.id
				AND conteudo.status = 1
				AND conteudo.tipo = "pagina"
			', FALSE);

        $result = $query->result(get_called_class());

		return $result;
	}
}