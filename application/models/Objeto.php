<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Objeto extends MY_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function get_midia($col = NULL, $dim = NULL){

		$col = $this->{$col};

		return $this->get_img_id($dim, $col);
	}

	public function get_midia_group($col = NULL, $dim = NULL){
		$this->load->model('midia');

		$array = array();
		$col = json_decode($this->{$col});

		if(count($col)){

			foreach ($col as $key => $midias) {

				$midia = new Midia();
				$midia->get_this(array('id' => $midias));
				array_push($array, $midia);
			}
		}

		return $array;
	}
}