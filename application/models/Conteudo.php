<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Conteudo extends MY_Model {

	public $table = 'conteudo';
	public $validation = array(
							array(
								'field' => 'titulo',
								'label' => 'titulo',
								'rules' => array('required')
							),
							array(
								'field' => 'cod_area',
								'label' => 'cod_area',
								'rules' => array('required')
							)
						);

	public function __construct(){
		parent::__construct();
	}

    public function get_content($slug, $tipo = 'pagina'){
        $this->load->model('area');
        $this->load->model('sublista');

        $area = new Area();
        $area->get_this(array('controller' => $slug, 'status' => 1));

        if(isset($area->id)){

        	$this->get_this(array('cod_area' => $area->id, 'controller' => $slug, 'tipo' => $tipo, 'status' => 1));

	        if(isset($this->id)){
		        $conteudo = json_decode($this->conteudo);

		        if($conteudo){
		            foreach ($conteudo as $key => $value) {
		                $this->{$key} = $value;
		            }
		        }

			    $group_sublista = array();

			    $sublistas = new Sublista();
			    $sublistas = $sublistas->get_where(array('cod_area' => $area->id, 'tipo' => $tipo));

				foreach($sublistas as $key => $sublista){

					$conteudos = $this->get_where(array('cod_area' => $sublista->cod_area, 'cod_pai' => $this->id, 'tipo' => $sublista->slug, 'status' => 1), array('order' => 'ordem ASC'));

					$group_conteudo = array();

					foreach($conteudos as $key => $conteudo){
						$json = json_decode($conteudo->conteudo);

				        if($json){
				            foreach ($json as $key => $value) {
				                $conteudo->{$key} = $value;
				            }
				        }

						$conteudo->kill_extra();
						array_push($group_conteudo, $conteudo);
				    }

				    $sublista->items = $group_conteudo;

					$sublista->kill_extra();
					$group_sublista[$sublista->slug] = $sublista;
				}

				$this->kill_extra();
				$this->sublistas = $group_sublista;
		    }
		}

        return $this;
	}

	public function get_item($controller, $slug){
		$this->load->model('area');
		$this->load->model('sublista');

		$area = new Area();
		$area->get_this(array('controller' => $controller, 'status' => 1));

		$this->get_this(array('cod_area' => $area->id, 'slug' => $slug, 'controller' => $controller, 'tipo' => 'item', 'status' => 1));

		if(isset($this->id)){  
	        $conteudo = json_decode($this->conteudo);

	        if($conteudo){
	            foreach ($conteudo as $key => $value) {
	                $this->{$key} = $value;
	            }
	        }

		    $group_sublista = array();

		    $sublistas = new Sublista();
		    $sublistas = $sublistas->get_where(array('cod_area' => $area->id, 'tipo' => 'item'));

			foreach($sublistas as $key => $sublista){

				$conteudos = $this->get_where(array('cod_area' => $sublista->cod_area, 'cod_pai' => $this->id, 'tipo' => $sublista->slug, 'status' => 1), array('order' => 'ordem ASC'));

				$group_conteudo = array();

				foreach($conteudos as $key => $conteudo){
					$json = json_decode($conteudo->conteudo);

			        if($json){
			            foreach ($json as $key => $value) {
			                $conteudo->{$key} = $value;
			            }
			        }

					$conteudo->kill_extra();
					array_push($group_conteudo, $conteudo);
			    }

			    $sublista->items = $group_conteudo;

				$sublista->kill_extra();
				$group_sublista[$sublista->slug] = $sublista;
			}

			$this->kill_extra();
			$this->sublistas = $group_sublista;
	    }

        return $this;
	}

	public function get_list($slug, $where = array(), $extra = array()){
        $this->load->model('area');

		$area = new Area();
		$area->get_this(array('controller' => $slug));

		if(isset($area->id)){

			$where['cod_area'] = $area->id;
			$where['cod_pai']  = 0;
			$where['lixeira']  = 0;
			$where['tipo']     = 'item';
			$where['status']   = 1;

			if(!isset($extra['order'])){
				$extra["order"] = "criado_em DESC";
			}

			$lista = $this->get_where($where, $extra);

			foreach($lista as $key => $item) {

		        $conteudo = json_decode($item->conteudo);

		        if($conteudo){
		            foreach ($conteudo as $key => $value) {
		                $item->{$key} = $value;
		            }
		        }

				$item->kill_extra();
		    }

			return $lista;
		}
		return array();
	}

	public function get_sublista_item($slug, $where = array(), $extra = array()){
        $this->load->model('sublista');

        $sublista = new Sublista();
		$sublista->get_this(array('slug' => $slug, 'tipo' => 'item'));

		if(isset($sublista->id)){

			$where['cod_area'] = $this->cod_area;
			$where['cod_pai']  = $this->id;
			$where['lixeira']  = 0;
			$where['tipo']     = $sublista->slug;
			$where['status']   = 1;

			$this->db->from('conteudo');
			$this->db->where($where);
			$this->db->order_by("ordem", "DESC");

			$lista = $this->db->get();
			$lista = $lista->result(get_called_class());
			
			foreach($lista as $key => $item) {

		        $conteudo = json_decode($item->conteudo);

		        if($conteudo){
		            foreach ($conteudo as $key => $value) {
		                $item->{$key} = $value;
		            }
		        }

				$item->kill_extra();
		    }

			return $lista;
		}

		return array();
	}

	public function get_list_in_array($array, $slug = NULL){
		$this->load->model('area');

		$area = new Area();
		$area->get_this(array('slug' => $slug));

		$extra = array();

		if($slug){
			$where['cod_area'] = $area->id;
			$where['cod_pai']  = 0;
			$where['lixeira']  = 0;
			$where['tipo']     = 'item';
			$where['status']   = 1;

			$extra['where'] = $where;
		}

		$lista = array();

		if($array){

			$lista = $this->get_in_array($array, $extra);

			foreach($lista as $key => $item) {

		        $conteudo = json_decode($item->conteudo);

		        if($conteudo){
		            foreach ($conteudo as $key => $value) {
		                $item->{$key} = $value;
		            }
		        }

				$item->kill_extra();
		    }
		}

		return $lista;
	}

	public function content($col){
		return (isset($this->{$col})) ? $this->{$col} : NULL;
	}

	public function get_categoria_check($id){
        $this->load->model('categoria_rel');
        $this->load->model('area');

        $area = new Area();
        $area->get_this(array("id" => $this->cod_area));        

        $rel = new Categoria_rel();
        $rel = $rel->get_row(array("cod_categoria" => $id, "cod_item" => $this->id, "cod_area" => $area->id, "tipo" => $this->tipo));

		return ($rel) ? true : false;
	}
}