<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lixo extends MY_Model {

	public $table = 'lixo';
	public $validation = array(
						array(
							'field' => 'item',
							'label' => 'item',
							'rules' => array('required')
						)
					);

	public function __construct()
	{
		parent::__construct();
	}

	public function getOrigem(){		
        $this->load->model('area');

        $area = new Area();
        $area->get_this(array('id' => $this->cod_area));

		return $area->titulo;
	}

	public function getUsuario(){		
        $this->load->model('usuario');

        $usuario = new Usuario();
        $usuario->get_this(array('id' => $this->usuario));

		return (isset($usuario->nome)) ? $usuario->nome : NULL;
	}
}