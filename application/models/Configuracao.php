<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuracao extends MY_Model {

	public $table = 'configuracoes';
	public $validation = array(
							array(
								'field' => 'titulo',
								'label' => 'Título',
								'rules' => array('required')
							)
						);

	public function __construct()
	{
		parent::__construct();
	}

	public function get_content(){

		$this->get_this();

		$this->sociais = json_decode($this->sociais);
		$this->navegacao = json_decode($this->navegacao);

		unset($this->table);
		unset($this->validation);
		unset($this->feedback);
		unset($this->fails);

		return $this;
	}

	public function get_social($col, $slug = NULL){

		if (isset( $this->sociais )) {
			foreach ($this->sociais as $key => $value) {

				if($value->plataforma == $col){

					if($slug == 'slug'){

						$link = explode('.com/', $value->link);
						$link = explode('/', $link[count($link) - 1]);
						$user = $link[0];
			
						return $user;
					}
					else{
						return $value->link;
					}					
				}

			}
		}

		return false;
	}
}