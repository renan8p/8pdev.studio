<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria extends MY_Model {

	public $table = 'categorias';
	public $validation = array(
							array(
								'field' => 'titulo',
								'label' => 'titulo',
								'rules' => array('required')
							),
							array(
								'field' => 'cod_area',
								'label' => 'cod_area',
								'rules' => array('required')
							)
						);

	public function __construct()
	{
		parent::__construct();
	}

	public function get_current(){
        $this->load->model('area');

        $area = new Area();
        $area->get_this(array('id' => $this->cod_area));

		return $area->slug;
	}


    public function get_content($where){

        $this->get_this($where);

        if(isset($this->id)){  
	        $conteudo = json_decode($this->conteudo);

	        if($conteudo){
	            foreach ($conteudo as $key => $value) {
	                $this->{$key} = $value;
	            }
	        }

			$this->kill_extra();
	    }

        return $this;
	}

	public function get_group($group = array()){

		$categorias = $this->get_in_array($group);

		foreach ($categorias as $key => $categoria) {

			$conteudo = json_decode($categoria->conteudo);

			if($conteudo){
				foreach ($conteudo as $key => $value) {
					$categoria->{$key} = $value;
				}
			}

			$categoria->kill_extra();
		}

		return $categorias;
	}

	public function get_list($where = array()){

		$categorias = $this->get_where($where);

		foreach ($categorias as $key => $categoria) {

	        $conteudo = json_decode($categoria->conteudo);

	        if($conteudo){
	            foreach ($conteudo as $key => $value) {
	                $categoria->{$key} = $value;
	            }
	        }

			$categoria->kill_extra();
		}

		return $categorias;
	}

	public function content($col){
		return (isset($this->{$col})) ? $this->{$col} : NULL;
	}
}