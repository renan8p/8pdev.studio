<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Analytics extends MY_Model {

	public $table = 'analytics';

	public function __construct()
	{
		parent::__construct();
	}

	public function setVisita($post){

		$this->from_array($post);

		$this->ip = $this->setIp();

		if(!strstr($this->ip, '::')){
			$this->pais   = $this->getGeo('pais');
			$this->estado = $this->getGeo('estado');
			$this->cidade = $this->getGeo('cidade');
		}
	}

	public function getAcessos(){

		$acessos = $this->count(array("mes" => $this->mes, "ano" => $this->ano));

		return $acessos;
	}

	public function getRanking(){

		$this->db->select('*, COUNT(id) as total');
		$this->db->where(array("mes" => $this->mes, "ano" => $this->ano));
		$this->db->group_by('url');
		$this->db->order_by('total', 'desc'); 

		$ranking = $this->db->get($this->table);

		return $ranking->result(get_called_class());
	}

	public function getChart(){

		$dias = cal_days_in_month(CAL_GREGORIAN, $this->mes, $this->ano);

		$labels = array();
		$data   = array();
		$datasets = array();

		for ($i=1; $i <= $dias; $i++) {
			$visitas = $this->count(array("mes" => $this->mes, "dia" => $i));

			array_push($data, $visitas);
			array_push($labels, $i);
		}

		return json_encode(array("labels" => $labels, "datasets" => array(array("label" => "Acesso mensal", "data" => $data))));
	}

	public function getRejeicao(){

		return true;
	}

	public function getPermanencia(){
		
		$this->db->select('*, COUNT(id) as total');
		$this->db->where(array("mes" => $this->mes, "ano" => $this->ano));
		$this->db->group_by('codigo');
		$this->db->order_by('total', 'desc'); 

		$visitas = $this->db->get($this->table);

		return true;
	}

	public function setIp(){

		$ip = NULL;
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) $ip = $_SERVER['HTTP_CLIENT_IP'];
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else $ip = $_SERVER['REMOTE_ADDR'];

		return $ip;
	}

	public function setCidade(){

		$php_file = (unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=".$this->ip."&base_currency=USD")));

		return $ip;
	}

	public function setEstado(){

		$ip = NULL;
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) $ip = $_SERVER['HTTP_CLIENT_IP'];
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else $ip = $_SERVER['REMOTE_ADDR'];

		return $ip;
	}

	public function getGeo($retorno){

		$php_file = (unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=".$this->ip."&base_currency=USD")));

        $this->longitude = $php_file['geoplugin_longitude'];
        $this->latitude  = $php_file['geoplugin_latitude'];

        $json_obj = json_decode(file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyA8KQ1-HVNmynwa77wDVFSexog6kWEBxP8&latlng=".$this->latitude.",".$this->longitude), true);

        switch ($retorno) {
			case 'pais':
				return $json_obj["results"][0]["address_components"][5]["short_name"];
				break;

			case 'cidade':
				return $json_obj["results"][0]["address_components"][3]["long_name"];
				break;

			case 'estado':
				return $json_obj["results"][0]["address_components"][4]["short_name"];
				break;
        }
	}
}