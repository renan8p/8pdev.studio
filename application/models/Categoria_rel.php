<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria_rel extends MY_Model {

	public $table = 'categorias_rel';
	public $validation = array(
							array(
								'field' => 'cod_item',
								'label' => 'cod_item',
								'rules' => array('required')
							),
							array(
								'field' => 'cod_categoria',
								'label' => 'cod_categoria',
								'rules' => array('required')
							),
							array(
								'field' => 'cod_area',
								'label' => 'cod_area',
								'rules' => array('required')
							),
							array(
								'field' => 'tipo',
								'label' => 'tipo',
								'rules' => array('required')
							)
						);

	public function __construct()
	{
		parent::__construct();
        $this->load->model('categoria');
	}

	public function get_rel($categoria){

		$itens = $this->return_to_array(array('cod_categoria' => $categoria), 'cod_item');
		
		return $itens;
	}
}