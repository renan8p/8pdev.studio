<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mensagem extends MY_Model {

	public $table = 'mensagens';
	public $validation = array(
						array(
							'field' => 'remetente',
							'label' => 'Remetente',
							'rules' => array('required')
						)
					);

	public function __construct(){
		parent::__construct();
	}

	public function getStatus(){

		$status = NULL;

		if($this->status == 0) $status = "Novo";
		if($this->status == 1) $status = "Em espera";
		if($this->status == 2) $status = "Pendente";
		if($this->status == 3) $status = "Atrasado";
		if($this->status == 4) $status = "Finalizado";
		if($this->status == -1) $status = "Excluído";

		return $status;
	}

	public function getBadge(){

		$badge = NULL;

		if($this->status == 0) $badge = "success";
		if($this->status == 1) $badge = "secondary";
		if($this->status == 2) $badge = "warning";
		if($this->status == 3) $badge = "danger";
		if($this->status == 4) $badge = "info";
		if($this->status == -1) $badge = "dark";

		return $badge;
	}
}