<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Midia extends MY_Model {

	public $table = 'midia';

	public function __construct()
	{
		parent::__construct();
	}

	public function get_midias($id = 0){

		if(strstr($id, '.')){
			$array = array_filter(explode('.', $id));
			$query = $this->get_in_array($array, array('lixeira' => 0));
		}
		else{
			$query = $this->get_this(array('id' => $id, 'lixeira' => 0));
		}

		return ($id) ? $query : NULL;
	}

	public function get_single_midia($id){

		$query = $this->get_this(array('id' => $id, 'lixeira' => 0));
		return (isset($query->id)) ? $query : NULL;
	}

	public function get_url($id = 0){

		$query = $this->get_this(array('id' => $id, 'lixeira' => 0));

		if(isset($query->id)){
			$arq = $this->caminho.$this->slug.$this->extensao;
			return base_url($arq);
		}
	}

	public function get_filter($post)
	{
		if($post["filtro"] != "todos"){
			$this->db->group_start()->where(array("tipo" => $post["filtro"]))->group_end();
		}

		if($post["buscar"]){
			$this->db->like( array("titulo" => trim($post["buscar"])) );
		}

		$this->db->order_by('id', 'DESC');
		$this->db->where(array("lixeira" => 0));
		$this->db->from($this->table);

		$query = $this->db->get();

		return $query->result(get_called_class());
	}

	public function get_midia($dim = NULL)
	{
		if(isset($this->id)){
			$arq = $this->caminho.$this->slug.$this->extensao;				

			if($this->tipo == "img"){

				if($dim == "all"){

					$arq = array(
						base_url($this->caminho.$this->slug.'-0'.$this->extensao),
						base_url($this->caminho.$this->slug.'-1'.$this->extensao),
						base_url($this->caminho.$this->slug.'-2'.$this->extensao),
						base_url($this->caminho.$this->slug.'-3'.$this->extensao),						
						base_url($this->caminho.$this->slug.$this->extensao)
					);

					return $arq;
				}
				else{

					switch ($dim) {
						case 'lg':
							$dim = '-0';
							break;

						case 'md':
							$dim = '-1';
							break;

						case 'sm':
							$dim = '-2';
							break;

						case 'xs':
							$dim = '-3';
							break;

						default:
							$dim = NULL;
							break;
					}

					$dim = ($this->extensao == ".gif" || $this->extensao == ".svg") ? NULL : $dim;
					$arq = $this->caminho.$this->slug.$dim.$this->extensao;
				}
			}	
			
			return base_url($arq);
		}

		return NULL;
	}

	public function get_midia_content($dim = NULL)
	{
		if(isset($this->id) && $this->id){

			$arq = $this->caminho.$this->slug.$this->extensao;

			if($this->tipo == "img"){
				switch ($dim) {
					case 'lg':
						$dim = '-0';
						break;

					case 'md':
						$dim = '-1';
						break;

					case 'sm':
						$dim = '-2';
						break;

					case 'xs':
						$dim = '-3';
						break;
					
					default:
						$dim = NULL;
						break;
				}
			
				$arq = $this->caminho.$this->slug.$dim.$this->extensao;
				return "<img src='".base_url($arq)."'/>";
			}

			if($this->tipo == "video"){
				return "<video controls><source src='".base_url($arq)."' type='audio/mp4'></video>";
			}

			if($this->tipo == "audio"){
				return "<audio controls><source src='".base_url($arq)."' type='audio/mpeg'></audio>";
			}

			if($this->tipo == "documento"){
				return '<div class="pdf"><div class="pdf-desc"><p>' . $this->titulo . '</p><span class="badge badge-soft-primary p-2">' . $this->tipo . '</span></div></div>';
			}
		}

		return NULL;
	}
}